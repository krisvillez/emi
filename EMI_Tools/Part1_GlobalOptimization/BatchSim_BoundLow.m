function [g,Alpha_L,Beta_L,g1_L,g2_L,Q2_L,Q2_U] = BatchSim_BoundLow(Model,IntX,~,Graph,Theta,XsimL,XsimU)

% -------------------------------------------------------------------------
% EMI Toolbox - BatchSim_BoundLow.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the lower bound.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[g,Alpha_L,Beta_L,g1_L,g2_L,Q2_L,Q2_U] = 
%				BatchSim_BoundLow(Model,IntX,~,Graph,Theta,XsimL,XsimU)
%
% INPUT
%   Model   :   Optimization problem as a structure
%   IntX  	:   Parameter vector set (normalized scale)
%   Graph   :   [optional] Boolean indicating whether graph should be made,
%					default = false
%   Theta   :   [optional] Parameter vector set, default defined by 
%					Model.Intercept and Model.Slope
%	XsimL 	:	[optional] Lower bounds for the concentration (at all 
%					simulation sampling times), default defined by 
%					simulation according to Theta
%	XsimU 	:	[optional] Upper bounds for the concentration (at all 
%					simulation sampling times), default defined by 
%					simulation according to Theta
%
% OUTPUT
%   g       :   Relaxed weighted least-squares objective function value
%   Alpha_L	:	Regression parameter scalar for lower bound
%	Beta_L	:	Regression parameter vector for lower bound
%	g1_L	:	First term in the objective function (substrate)
%	g2_L 	:   Second term in the objective function (OUR) 
%	Q2_L	:	Lower bounds to the relative reaction rate
%	Q2_U	:	Lower bounds to the relative reaction rate
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


global options_fmincon

if nargin<4 || isempty(Graph)
    Graph = false ;
end



% =========================================================================
% 1. Extent Simulation

if nargin<5 || isempty(Theta)
    Theta	=	ones(2,1)*Model.Intercept	+	IntX*diag(Model.Slope)	;
end
   
 if nargin<6 
        
    [   X1_L,   X1_U,  oo,     oo, XsimL,  XsimU	]	=	ExtremalState(Model,Theta,Graph)	;
    
    C1_L    =   1-X1_L      ;
    C1_U    =   1-X1_U      ;
 else
    if isfield(Model,'Y1')
    C1_L	=	1-XsimL(Model.I1)     ;
    C1_U	=	1-XsimU(Model.I1)     ;
    end
 end
 
 if isfield(Model,'shapecon') && Model.shapecon
     
     [CImax] =EvaluateTransitions(Theta) ;
     
 end
        
% =========================================================================
% 2. Initial substrate concentration estimation

if isfield(Model,'Y1')
    
    % 2.1. Interval regression - without shape constraints, this
    % problem is only 1-dimensional
    Xn1_L = C1_L./Model.sigma1;
    Xn1_U = C1_U./Model.sigma1 ;
    Yn1  = Model.Y1n ;
    [g1_L,Alpha_L]	=	ExecuteIntReg(Xn1_L,Xn1_U,Yn1)         ;
    g1_L            =   g1_L/(Model.n1+Model.n2)	;
    
    % 2.3. Display
    if Graph>=3
        figure(137),
            
            if exist('FigPrep'), FigPrep, end
            if exist('AxisPrep'), AxisPrep, end 
            hold on,
            plot(   Model.Tsim, (1-XsimL)*Alpha_L,  'b-'    )
            plot(   Model.Tsim, (1-XsimU)*Alpha_L,  'r-'    )
            plot(   Model.T1,   C1_L*Alpha_L,       'bo'    )
            plot(   Model.T1,   C1_U*Alpha_L,       'ro'    )
            plot(   Model.T1,   Model.Y1,           'k.'    )
    end
    
else
    
    g1_L	=	0	;
    
end


% =========================================================================
% 3. OUR estimation: endogenous respiration rate + apparent yield

if isfield(Model,'Y2')
    
    % 3.1 bounding the relative reaction rate
    C2_L = 1-XsimL(ismember(Model.Tsim,Model.T2));
    C2_U = 1-XsimU(ismember(Model.Tsim,Model.T2));
    [Q2_L,Q2_U]     =   ExtremalRate(Model,Theta,XsimL,XsimU,Graph) ;
    
    % 3.2. Interval regression
    Xn2_L = [ 1./Model.sigma2 Q2_L./Model.sigma2 ];
    Xn2_U = [ 1./Model.sigma2 Q2_U./Model.sigma2 ];
    Yn2  = Model.Y2n;
    [g2_L,Beta_L]	=	ExecuteIntReg(Xn2_L,Xn2_U,Yn2)          ;
    
    % 3.3 Shape constraints
    if (isfield(Model,'shapecon') && Model.shapecon) % && g2_L>0
        
    end
    
    g2_L            =	g2_L/(Model.n1+Model.n2)	;
    
    % 3.4 Display
    if Graph>=3
        figure(173), 
        if exist('FigPrep'), FigPrep, end
        if exist('AxisPrep'), AxisPrep, end
        hold on,
        plot(   Model.T2,	[ ones(Model.n2,1) Q2_L]*Beta_L,    'b-'    )
        plot(   Model.T2,   [ ones(Model.n2,1) Q2_U]*Beta_L,	'r-'    )
        plot(   Model.T2,   [ ones(Model.n2,1) Q2_L]*Beta_L,    'b.'    )
        plot(   Model.T2,   [ ones(Model.n2,1) Q2_U]*Beta_L,    'r.'    )
        plot(   Model.T2,   Model.Y2,       'ko'	)
        
        
%         figure(373), hold on
%         plot(Model.T2,max(Model.Y2-[ ones(Model.n2,1) Q2_L]*Beta_L,0),'r.')
%         plot(Model.T2,min(Model.Y2-[ ones(Model.n2,1) Q2_L]*Beta_L,0),'b.')
    end
    
else
    
    g2_L	=	0	;
    
end

% =========================================================================
% 4. Combine
g      =   g1_L +   g2_L	;

% % =========================================================================
% % 5. Tighten
% 
% global BestObj
% global lowestlevel
% 
% if and(g1<= BestObj,lowestlevel) && (isfield(Model,'shapecon') && Model.shapecon)
% %if g1<= BestObj && (isfield(Model,'shapecon') && Model.shapecon)
%     
%     disp('                  APPLYING SHAPE CONSTRAINTS')    
%     
%     if isfield(Model,'Y1')
%         % ===============
%         %  SETUP PROBLEM
%         A       =   ShapeConstraints1(Model,C1_L,C1_U,CImax)   ;
%         
%         if size(A,1)>=1
%             % ===============
%             % SOLVE PROBLEM
%             Tol     =   1e-4 	;
%             MaxIter =   42    ;
%             
%             An      =	A*diag(Model.sigma1) ;
%             
%             [Alpha_L,g,C1,Converged] = ExecuteCALS(Alpha_L,Yn1,An,Xn1_L,Xn1_U,Tol,MaxIter) ;
%             
%             %             nA      =	size(A,1) ;
%             %             A       =   [   sparse(nA,1)    A	]   ;
%             %             par0    =   [   Alpha_L	;   Model.Y1n/Alpha_L	]   ;
%             %             obj     =   @(z) sum( (Model.Y1n-z(2:end).*z(1)).^2 );
%             %
%             %             [par1,g]    =	fmincon(@(z) obj(z),par0,A,zeros(nA,1),[],[],[0 ; Xn_L],[+inf ; Xn_U],[],options_fmincon);
%             %             Alpha_L	=	par1(1)     ;
%             %             C1      =   par1(2:end) ;
%             if Converged
%                 g1_L = g ;
%             end
%             
%             %             figure, hold on,
%             %             plot(Model.T1,diag(Model.sigma1)*Model.Y1n,'k.')
%             %             plot(Model.T1,diag(Model.sigma1)*C1*Alpha_L,'-')
%             %             plot(Model.T1,diag(Model.sigma1)*Xn_L*Alpha_L,'b-')
%             %             plot(Model.T1,diag(Model.sigma1)*Xn_U*Alpha_L,'r-')
%             %
%             %             figure, hold on,
%             %             plot(A*diag(Model.sigma1)*C1,'-')
%             %             plot(An*C1,'--')
%             
%         end
%         g1_L            =   g1_L/(Model.n1+Model.n2)	;
%     else
%         g1_L=0;
%     end
%     
%     
%     if isfield(Model,'Y2')
%         % ===============
%         %  SETUP PROBLEM
%         A       =   ShapeConstraints2(Model,C2_L,C2_U,CImax,CIrise,CIfall)	;
%         
%         if size(A,1)>=1
%             
%             % ===============
%             % SOLVE PROBLEM
%             Tol     =   1e-4 	;
%             MaxIter =   42    ;
%             
%             An = A*diag(Model.sigma2) ;
%             
%             [Beta_L,g,Q2,Converged] = ExecuteCALS(Beta_L,Yn2,An,Xn2_L,Xn2_U,Tol,MaxIter) ;
%             
%             
%             %             nA      =	size(A,1) ;
%             %             A       =   [   sparse(nA,2)    A	]   ;
%             %             par0    =   [   Beta_L	;   (Model.Y2n-Beta_L(1))/Beta_L(2)	]   ;
%             %             obj     =   @(z) sum( (Model.Y2n-z(1)./Model.sigma2-z(3:end).*z(2)).^2 );
%             %
%             %             [par1,g]    =	fmincon(@(z) obj(z),par0,A,zeros(nA,1),[],[],[zeros(2,1) ; Q2_L./Model.sigma2],[+inf(2,1) ; Q2_U./Model.sigma2],[],options_fmincon);
%             %             Beta_L	=	par1(1:2)     ;
%             %             Q2      =   par1(3:end) ;
%             
%             
%             %             figure, hold on,
%             %             plot(Model.T2,diag(Model.sigma2)*Model.Y2n,'k.')
%             %             plot(Model.T2,diag(Model.sigma2)*(Q2*Beta_L(2)+Xn_L(:,1)*Beta_L(1)),'-')
%             %             plot(Model.T2,diag(Model.sigma2)*Xn_L*Beta_L,'b--')
%             %             plot(Model.T2,diag(Model.sigma2)*Xn_U*Beta_L,'r--')
%             %
%             %             figure, hold on,
%             %             plot(A*diag(Model.sigma2)*Q2,'-')
%             %             plot(An*Q2,'--')
%             if Converged
%                 g2_L = g ;
%             end
%         
%         
%         g2_L            =	g2_L/(Model.n1+Model.n2)	;
%             
%         else
%             g2_L=0;
%         end
%     end
%     
%     % =========================================================================
%     % 6. Combine
%     
%     g      =   g1_L +   g2_L	;
%         
%     
% end
    
    
    
end



