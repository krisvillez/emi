function [Output,RMSR] = SimulateAndFitDirect(select_model,Knots,SFP)

% -------------------------------------------------------------------------
% EMI Toolbox - SimulateAndFitDirect.m
% -------------------------------------------------------------------------
% Description
%
% Execute model fitting on the basis of simulated rate measurements.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Output,RMSR] = SimulateAndFitDirect(select_model,Knots,SFP)
%
% INPUT
%   select_model	:   Cell array with strings indicating the ground truth
%                       growth rate models
%   Knots           :   Cell array with knot location vectors
%   SFP             :   Structure with parameters common to all ground
%                       truth simulations
%
% OUTPUT
%   Output          :   Structured array compiling all results
%   RMSR            :   Matrix with RMSR values
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-05
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Output	=	struct()                        ;
nFun	=	length(select_model)            ;
Library	=	CreateLibrary                   ;
nLib    =   length(Library)                 ;
RMSR	=   zeros(nFun,nLib+length(Knots))	;

for iFun = 1:nFun 
    
    % ====================================================================
    % 0. SIMULATE AND PROCESS DATA
    
    disp(['Case: ' num2str(iFun)])
    disp(['	Simulating rate function: ' SFP.model_names{iFun}])
    
    ratefun                     =	SelectFunction(select_model{iFun})  ;
    
    Output(iFun).true_rate_name	=	SFP.model_names{iFun}               ;
    Output(iFun).true_rate_rate	=	ratefun(SFP.SS,SFP.P)               ;
    Output(iFun).true_rate_SS	=	SFP.SS                              ;

    % ====================================================================
    % I. CLASSIC FUNCTION LIBRARY
    
    disp('	Modeling via classic function library')
    
    for iLib=1:nLib

        Model                               =	Library(iLib)                                                       ;
        
        disp(['             Fitting using the ' Model.name ' model.'])
        true_growth_rate                    =	ratefun(SFP.SS,SFP.P)                                               ;
        OptimParsLib                        =	FitLibrarytoRates_Optim(SFP.SS,true_growth_rate,Model)              ;
        
        disp('         Direct fit to growth rate finished')
        Lib_model_rate                      =	Model.ratefun(SFP.SS,OptimParsLib)                                  ;
        RMSR(iFun,iLib)                     =	sqrt(sum((true_growth_rate-Lib_model_rate).^2)/length(SFP.SS)/2)    ;
        Output(iFun).Lib_models(iLib).rate	=	Lib_model_rate                                                      ;
        Output(iFun).Lib_models(iLib).pars	=	OptimParsLib                                                        ;
        
    end

    % ====================================================================
    % II. SCS FUNCTION
    
    disp('	Modeling via SCS function')
    
    for k = 1:length(Knots)

        knots                           =	Knots{k}                                                                            ;
        
        disp(['             Fitting using ' num2str(length(knots)) ' knots.'])
        modelSCS                        =	FitSCStoRates(SFP.SS,true_growth_rate,'C',knots)                                    ;
        
        disp('         Direct fit to growth rate finished')
        RMSR(iFun,nLib+k)               =	sqrt(sum((true_growth_rate'-EvalDynSCS(modelSCS,SFP.SS,0)).^2)/length(SFP.SS)/2)    ;
        
        Output(iFun).SCS_models(k).rate	=	EvalDynSCS(modelSCS,SFP.SS,0)                                                       ;
        
    end


end

    
end

