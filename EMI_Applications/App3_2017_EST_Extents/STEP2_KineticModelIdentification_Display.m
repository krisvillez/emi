
% -------------------------------------------------------------------------
% EMI Toolbox - STEP2_KineticModelIdentification_Display.m
% -------------------------------------------------------------------------
% Description
%
% Visualize the extent simulations with all considered rate law models and
% associated best-fit parameter values.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script displays the results obtained with \n' ...
            '''STEP2_KineticModelIdentification.m'' \n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Visualization choices

SensorSetup	=	3       ; % Select sensor setup  used for extent computation
Rk          =   2       ;
J           =	5       ; % number of candidate rate laws
GrayScale   =	false	;


% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Visualization parameters

if GrayScale
    Colours = {ones(1,3)*0.73,ones(1,3)*0.55,ones(1,3)*0.37,ones(1,3)*0};
else
    Colours = {	 [    0.4940    0.1840    0.5560 ] ; 
                    [    0.4660    0.6740    0.1880 ] ; 
                    [    0.8500    0.3250    0.0980 ] ; 
                    [	0.9290    0.6940    0.1250 ] ; 
                [	0         0.4470    0.7410 ] ; 
                    [	0.3010    0.7450    0.9330 ] ; 
                    [   0.6350    0.0780    0.1840] };
end

a           =	.02     ;
d           =	.05     ;
FS          =   17      ;
LW          =   1       ;

PanelOffset	=	.11                 ;
PanelSpace	=	(1-PanelOffset)/3	;
PanelHeight	=	PanelSpace*.93      ;

Xlim        =	[   0-d	10+d	]   ;

RateLawNames	=	cell(J,1)       ;
WRMSR           =	nan(J,Rk)       ;
WRMSRlim99      =	nan(J,Rk)       ;

    
for i=1:Rk  % for each extent
    
    disp(' ')
    disp('==================================')
    disp(['   Reaction: ' num2str(i) '  '])
    disp('==================================')
    disp(' ')
    
    for j=1:J
        
        disp(['---  Candidate Rate Law ' num2str(j)])
        
        FILEPATH = [FOLDER 'STEP2_Case' num2str(SensorSetup) '_Reaction' num2str(i) '_RateLaw' num2str(j)] ;
        load(FILEPATH)
        
        
        % -------------------------------------------------------------------------
        % Get results
        index	=	ExtentModel.Measurement.sample_index	;
        tsim	=	ExtentModel.Measurement.tsim            ;
        xsim	=	ExtentModel.simulation.xsim             ;
        
        xtilde	=	ExtentModel.data.xtilde                 ;
        lambda	=	ExtentModel.data.lambda                 ;
        
        % -------------------------------------------------------------------------
        % Some processing
        
        tsim            =	tsim*24                 ;
        ty              =	tsim(index)             ;
        xhat            =	xsim(:,index)           ;
        
        Xsim(j,:)       =   xsim                    ;
        Xhat(j,:)       =	xhat                    ;
        
        ExtentModel.WSSRlim95	=	sqrt(chi2inv(0.95,[ExtentModel.data.H-ExtentModel.Reactions{1}.ntheta])/ExtentModel.data.H)	;
        
        WRMSR(j,i)      =	ExtentModel.WSSR        ;
        WRMSRlim99(j,i) =	ExtentModel.WSSRlim99	;
        WRMSRlim95(j,i) =	ExtentModel.WSSRlim95	;
        
        Name            =   ExtentModel.Reactions{1}.name ;
        Name            =   [	upper(Name(1)) lower(Name(2:end))   ];
        RateLawNames{j} =   Name	;
    end
    
    
    
    disp(['---  Visualization'])
    
    T0  =   [	ty-d	;	ty+d	]	;
    T1	=	[	ty      ;	ty      ]	;
    
    fighan = figure(100+i*1);
    set(gcf,'Position',[100 100 1000 800 ])
    
    
        for panel=1:3
            
            SubPos = [0.12 PanelOffset+(3-panel)*PanelSpace 0.6 PanelHeight ] ;
            
            subplot('Position',SubPos), 
            
                % -------------------------------------------------------------------------
                % Setup panel
            
                grid on
                hold on,
                set(gca,'FontSize',FS,...
                    'Xlim',[0-d 10+d],'Xtick',[0:10])
                
                % -------------------------------------------------------------------------
                % Actual plotting
            
                jj = 2:J ;
                                
                switch panel
                    case 1
                        
                        plot(ty,xtilde,'ko','MarkerSize',5,'MarkerFaceColor','w','Linewidth',LW)
                        for j=1:J
                            plot(tsim,Xsim(j,:),'-','LineWidth',2,'Color',Colours{j})
                        end
                        
                        z1 = xtilde+3*lambda ;
                        z2 = xtilde-3*lambda ;
                        
                        ylabeltext = {['$\hat{x}_{k,' num2str(i) '}(t)$'],['$\tilde{x}_{k,' num2str(i) '}(t_h)$'],'[mol]'} ;
                        
                        Ylim = [0-a 5/2/14+a] ;
                        Yticks = [0:.03:.18] ;
                        
                    case 2
                
                        for j=jj
                            plot(ty,Xhat(j,:)-xtilde,'.','MarkerSize',23+(J-j)*7,'Color',Colours{j})
                        end
                        
                        z1 = +3*lambda ;
                        z2 = -3*lambda ;
                        
                        
                        ylabeltext = {['${\hat{x}_{k,' num2str(i) '}(t_h)-\tilde{x}_{k,' num2str(i) '}(t_h)}$'],...
                            '\qquad \quad ~[mol]'};
                        
                        Ylim =[0-1.5*a 0+1.5*a];
                        Yticks = [-.03:.01:.03] ;
                    case 3
                        
                        for j=jj
                            plot(ty,(Xhat(j,:)-xtilde)./lambda,'.','MarkerSize',23+(J-j)*7,'Color',Colours{j})
                        end
                        
                        z1 = +3+0*lambda ;
                        z2 = -3+0*lambda ;
                        
                        ylabeltext = {['$\underline{\hat{x}_{k,' num2str(i) '}(t_h)-\tilde{x}_{k,' num2str(i) '}(t_h)}$'],...
                            ['$\qquad \quad ~\lambda_{' num2str(i) ',h}$'],...
                            '\qquad \quad ~~[-]'} ;
                        
                        Ylim =[-7 +7];
                        Yticks = [-6:2:6] ;
                end
                
                plot(T0,[ z1 ; z1],'-','Linewidth',LW,'Color','k')
                plot(T0,[ z2 ; z2],'-','Linewidth',LW,'Color','k')
                plot(T1,[ z1 ; z2],'-','Linewidth',LW,'Color','k')
            
                if panel==1
                    plot(ty,xtilde,'ko','MarkerSize',5,'MarkerFaceColor','w','Linewidth',LW)
                    for j=1:J
                        plot(tsim,Xsim(j,:),'-','LineWidth',2,'Color',Colours{j})
                    end
                end
                
                % -------------------------------------------------------------------------
                % Panel adjustment and Text elements
            
                set(gca,'Ylim',Ylim,'Ytick',Yticks)
                
                switch panel
                    case 1
                        set(gca,'Xticklabel',[])
                        leg = legend(['Experimental ' char(10) 'extent'],'Zeroth-order','First-order','Monod','Tessier','Haldane');
                        
                LegPos      =	get(leg,'Position')         ;
                Xlim        =	get(gca,'Xlim')             ;
                LegPos(1)	=	SubPos(1) + SubPos(3)*1.1	;
                LegPos(2)   =   0.73 ;
                
                set(leg,'Position',LegPos)
                    case 2
                        set(gca,'Xticklabel',[])
                           set(gca,'Xticklabel',[])
                        leg = legend('First-order','Monod','Tessier','Haldane');
                        
                LegPos      =	get(leg,'Position')         ;
                Xlim        =	get(gca,'Xlim')             ;
                LegPos(1)	=	SubPos(1) + SubPos(3)*1.1	;
                LegPos(2)   =   0.37 ;
                
                set(leg,'Position',LegPos)
                    case 3
                        xlabel(['Time ($t$, $t_h$) [h]' ],'interpreter','latex')
                end
                
                ylabel(ylabeltext,'interpreter','latex');
                
                textX	=	Xlim(1)+1.03*(Xlim(2)-Xlim(1))	;
                textY	=	mean(Ylim)                  ;
                textT	=	['(' char(96+panel) ')']	;
                text(textX,textY,textT,'fontsize',FS,'HorizontalAlignment','center','VerticalAlignment','middle')
                
                drawnow
                disp([' '])
            
        end
    
    
    SaveMyFigure(fighan,['.\Figures\Results3_ModelExtent' num2str(i)])
    
    
    
end


% =========================================================================
% =========================================================================
% =========================================================================

% WRMSR PLOT

fighan = figure	;

set(gcf,'Position',[100 100 1000 800 ])

% -------------------------------------------------------------------------
% Setup panel
hold on
set(gca,...
    'FontSize',FS,...
    'Xlim',[.5 J+.5],'Xtick',[1:J],'Xticklabel',RateLawNames,...
    'Ylim',[.1 1009],...
    'yscale','log')

% -------------------------------------------------------------------------
% Actual plotting
han                 =	bar(WRMSR,'BaseValue',.1)   ;
han(1).FaceColor	=   ones(1,3)*.73               ;
han(2).FaceColor	=   ones(1,3)*.37               ;

for j=1:J
    for i=1:Rk
        lvl         =	WRMSRlim95(j,i)             ;
        plot([-.05 +.45]-(i-1)*.4+j,[lvl lvl],'k-')
    end
end

% -------------------------------------------------------------------------
% Text elements
xlabel('Kinetic rate law candidate')
ylabel('WRMSR [-]')
legend('Extent 1','Extent 2')

SaveMyFigure(fighan,'.\Figures\Results4_WRMSR')



figure(42), hold on,
    
FILEPATH	=	[	FOLDER	'STEP0_All' ]	;
load(FILEPATH)

ytrue = Model.simulation.ysim ;
ytilde0 = Model.data.ytilde ;
plot(ytilde0')


FILEPATH	=   [	FOLDER 'STEP1_ExtentComputation_Case' num2str(3) ] ; 
load(FILEPATH)

ytilde1 = Model.data.ytilde ;
plot(ytilde1','+')