function xbar_o = ExtentsCompute (static,projection,n_0,V,y) 

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentsCompute.m
% -------------------------------------------------------------------------
% Description
%
%   Computes values for the observable extents and extent directions among
%   the ambiguous extents.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	xbar_o = ExtentsCompute (static,projection,n_0,V,y) 
%
% INPUT
%	static      :   structure describing what is known about the model and
%                   the experiment based on invariants alone 
%	projection	:   projection matrix (projection.Lbar_o) and covariance
%                   matrix (projection.Sigmabar_o)
%   n_0         :   initial conditions
%   V           :   volume
%   y           :   measurements as matrix with columns corresponding to
%                   sampling time
%   
% OUTPUT
%	xbar_o      :   computed values of the observable extents and the
%                   observable directions among the ambiguous extents
%   
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% matrices
M_     =	static.M_           ;
Lbar_o	=	projection.Lbar_o	;

% known noise-free values of the initial measurements
y0      =	M_*n_0/V            ;

% compute extents
xbar_o  =	Lbar_o*(y-y0)       ;
