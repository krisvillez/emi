function [g,XM,Xsim,Alpha_U,Beta_U,g1,g2] = BatchSim_BoundUpp(Model,IntX,~,Graph,Xo,theta,Xsim)

% -------------------------------------------------------------------------
% EMI Toolbox - BatchSim_BoundUpp.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the upper bound.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[g,XM,Xsim,Alpha_U,Beta_U,g1,g2] = 
%					BatchSim_BoundUpp(Model,IntX,[],Graph,Xo,theta,Xsim)
%
% INPUT
%   Model   :   Optimization problem as a structure
%   IntX  	:   Parameter vector set (normalized scale)
%   Graph   :   [optional] Boolean indicating whether graph should be made,
%					default = false
%	Xo 		:	[optional] Proposed solution for the upper bound 
%					(normalized scale), default: mean of IntX
%	theta	:	[optional] Proposed solution for the upper bound (original 
%					scale). When given, this overrides solution defined by 
%					Xo. Default corresponds to Xo.
%	Xsim 	:	[optional] Simulated states corresponding to input 'theta',
%					if given, default defined by simulation according to 
%					theta
%
% OUTPUT
%   g       :   Weighted least-squares objective function value
%   XM 		:	Applied solution for the upper bound
%	Xsim 	: 	Simulated states corresponding to upper bound
%	Alpha_U	:	Regression parameter scalar for upper bound
%	Beta_U	:	Regression parameter vector for upper bound
%	g1		:	First term in the objective function (substrate)
%	g2 		:   Second term in the objective function (OUR) 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Upper Bound

if nargin<4 || isempty(Graph)
    Graph = false ;
end
if nargin<5 || isempty(Xo)
    XM      =	mean(IntX)' ;
else
    XM      =	Xo(:)       ;
end

%nSample = Model.nSample ;

g = 0;

% =========================================================================
% 1. Extent Simulation

if nargin<=5
    % Simulation corresponding to mean parameter vector:
    theta       =	Model.Intercept	+	XM'*diag(Model.Slope)	;
    [~,Xsim]	=   BatchSim(Model.Tsim,Model.fun,theta)     	;
elseif nargin==6
    [~,Xsim]	=   BatchSim(Model.Tsim,Model.fun,theta)     	;
end

% =========================================================================



% =========================================================================
% 2. Initial substrate concentration estimation

if isfield(Model,'Y1')
    
    xx      =   1-Xsim(Model.I1)                       ;   % Basis
    XXn       =   [  xx./Model.sigma1 ] ;
    yyn     =       Model.Y1n ;
    
    [g1,Alpha_U] =	ConstrainedLeastSquares(XXn,yyn);
    
    g1     =   g1/(Model.n1+Model.n2)	;   % MWSR
    
    g      =       g + g1                        ;
    
    if Graph>=3
        
%         figure(42), hold on,
%         plot(Model.T1,yyn.*Model.sigma1,'k.')
%         plot(Model.T1,(XXn*Alpha_U).*Model.sigma1,'--')
%         title('Conc')
        
    end
    
end

% =========================================================================
% 3. OUR estimation: endogenous respiration rate + apparent yield
    
if isfield(Model,'Y2')
    
    xx      =   	Model.fun(1-Xsim(Model.I2),theta)         ;   % Normalized reaction rate
    % WLS: https://en.wikipedia.org/wiki/Least_squares#Weighted_least_squares
    yyn     =       Model.Y2n ;
    XXn     =   [	1./Model.sigma2 xx./Model.sigma2	]       ;   % Basis
    
    [g2,Beta_U] =	ConstrainedLeastSquares(XXn,yyn);
    
    g2     =       g2/(Model.n1+Model.n2)	;   % MWSR
    
    g      =       g + g2                        ;
    
    if Graph>=3
        
        figure(173), hold on
        plot(Model.T2,yyn.*Model.sigma2,'k.')
        plot(Model.T2,(XXn*Beta_U).*Model.sigma2,'--')
        title('UB - OUR')
        
%         figure(373), hold on
%         plot(Model.T2,Y2-XX*Beta_U,'o')
    end
    
end

end
