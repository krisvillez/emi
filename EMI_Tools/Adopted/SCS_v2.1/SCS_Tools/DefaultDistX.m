function DistX = DefaultDistX(Model) 

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - DefaultDistX.m
% -------------------------------------------------------------------------
% Description 
%
% This function computes the default distance constraints on the
% transitions.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	DistX = DefaultDistX(Model) 
%
% INPUT
%   Model:      Structure describing the Shape Constrained Spline function
%
% OUTPUT
%   DistX:      Default distance contraints for transitions
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-17
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

t1      =   Model.tk(1)     ;
t2      =   Model.tk(end)   ;
QR      =   Model.QR        ;
nQR     =   size(QR,1)      ;

DistX(:,2)    =   ( t2-t1 )*ones(nQR-2,1)  ;
DistX(:,1)    =    0    ;
