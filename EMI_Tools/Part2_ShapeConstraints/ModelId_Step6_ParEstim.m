function [par_hat,g_hat,C_hat]	=	ModelId_Step6_ParEstim(par_init,ratefun1,c0,sigmaY,tmeas,Cmeas,Shape) 

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Step6_ParEstim.m
% -------------------------------------------------------------------------
% Description
%
% Numerical optimization of the parameter estimates, starting from initial
% guesses.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[par_hat,g_hat,C_hat] = ...
%	ModelId_Step6_ParEstim(par_init,ratefun1,c0,sigmaY,tmeas,Cmeas,Shape) 
%
% INPUT
%   par_init    :   Initial parameter guesses
%   ratefun1	:   Rate law function handle
%   c0          :   Initial concentrations
%   sigmaY      :   measurement error standard deviations
%   tmeas       :   Sampling times
%   Cmeas       :   Measurement profiles
%   Shape       :   Shape constraint [for SCS type rate laws only]
%
% OUTPUT
%   par_hat     :   best parameter estimates
%   g_hat       :   best objective function value
%   C_hat       :   optimal concentration estimates
%
% -------------------------------------------------------------------------
% Last modification: 2016-10-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<7 || isempty(Shape)
    Shape = [];
end

% =========================================================================
% Setup optimization problem

[R,Chat0,g0]	=   EvaluateODE(par_init,ratefun1,c0,tmeas,Cmeas,sigmaY)        ;

resid           =	@(pars) EvaluateODE(pars,ratefun1,c0,tmeas,Cmeas,sigmaY)	;
npar            =	length(par_init)                                            ;
lb              =	zeros(npar,1)                                               ;
if ~isempty(Shape) && strcmp(Shape,'N')
     lb(end)	=	-inf                                                        ; % remove constraint on last parameter
end

% =========================================================================
% Opimize all parameters simultaneously

[~,~,g0]            =	EvaluateODE(par_init,ratefun1,c0,tmeas,Cmeas,sigmaY)	;
optimizationmethod	=	'lsqnonlin'                                             ;

switch optimizationmethod
    case 'direct'
        
        % 1. Establish bounds for parameters. Row dimension need to match number of
        % parameters!
        bounds              =	[	zeros(npar,1)	...
                                    ones(npar,1)*max(par_init(:))*2	]           ;
        
        % 2. Send options to Direct
        options.testflag    =	0       ;
        options.showits     =	1       ;
        options.tol         =	0.01    ;
        options.maxevals    =	1e4     ;
        options.maxits      =   1e3     ;
        
        % 2a. NEW!
        % Pass Function as part of a Matlab Structure
        Problem.f           =	g       ;
        
        % 3. Call DIRECT
        [fmin,xmin,hist]	=	Direct(Problem,bounds,options)	;
        
    case 'fmincon'
        options         =	optimoptions('fmincon')                             ;
        options.TolX	=	1e-9                                                ;
        options.TolFun	=	1e-9                                                ;
        options.Display	=   'off'                                               ;
        par1            =	fmincon(g,par_init,[],[],[],[],lb,[],[],options)	;
    case 'fminsearch'
        options         =	optimset('fminsearch')                      ;
        options.TolX	=	1e-9                                        ;
        options.TolFun	=	1e-9                                        ;
        options.Display	=   'iter'                                      ;
        par1            =	fminsearch(g,par_init,options)              ;
    case 'lsqnonlin'
        options         =	optimoptions('lsqnonlin')                   ;
        options.TolX	=	1e-6                                        ;
        options.TolFun	=	1e-6                                        ;
        options.Display	=   'off'                                       ;
        par1            =	lsqnonlin(resid,par_init, lb,[],options)	;
end

[~,C_hat,g1]	=	EvaluateODE(par1,ratefun1,c0,tmeas,Cmeas,sigmaY)	;

if g1<g0
    par_hat     =	par1        ;
    g_hat       =	g1          ;
else
    par_hat     =	par_init	;
    g_hat       =	g0          ;
end

% =========================================================================
 
end

