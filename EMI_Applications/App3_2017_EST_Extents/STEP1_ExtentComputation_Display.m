
% -------------------------------------------------------------------------
% EMI Toolbox - STEP1_ExtentComputation_Display.m
% -------------------------------------------------------------------------
% Description
%
% Visualize experimental extents.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script displays the results obtained with \n' ...
            '''STEP1_ExtentComputation.m'' \n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

addpath(genpath('..\..\EMI_Tools'))

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Visualization choices

SensorSetups        = [	3	]	;
OneEllipsoidGraph	=   true	;
GrayScale           =	true	;

ellipoid_index      = [ 1:6:25	] + 3	;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Visualization parameters

a       =	.017	; 
d       =	.05     ;
dy      =	.03     ;

FS      =   17      ;
LW      =   2       ;
LW2     =   2       ;
LW3     =   1.5     ;

nSensorSetup = length(SensorSetups);

Strings	=   {'^','v'}   ;

PanelOffset = .11 ;
PanelSpace = (1-PanelOffset)/nSensorSetup ;
PanelHeight = PanelSpace*.93;

if GrayScale
    Colours     =   {.55*ones(1,3),'k',.37*ones(1,3),.73*ones(1,3)} ; % For paper (gray-scale)
else
    Colours     =   {[0.635  0.078 0.184 ],[0.929 0.694 0.125],[0 0.447 0.741],'k'} ; % For demos
end

colours_time = [	0         0.4470    0.7410
                    1       0       0
                    0.4940    0.1840    0.5560
                    0.9290    0.6940    0.1250
                    0.4660    0.6740    0.1880
                    0.3010    0.7450    0.9330
                    0.6350    0.0780    0.1840];

% -------------------------------------------------------------------------
% Get ground truth data

FILEPATH     =   [	FOLDER 'STEP0_All' ] ;
load(FILEPATH)

xsim = Model.simulation.xsim  ;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Create Figure

fighan = figure;
set(gcf,'Position',[100 100 1000 800 ])

for SensorSetup=SensorSetups
    
    
    % -------------------------------------------------------------------------
    % Get extent results

    FILEPATH     =   [	FOLDER	'STEP1_ExtentComputation_Case' num2str(SensorSetup) ] ;
    load(FILEPATH)
    
    Rk      =   size(Model.Nbar_k,1)                    ;
    tsim	=	Model.Measurement.tsim                  ;
    tsim	=	tsim*24                                 ; 
    tmeas	=	tsim(Model.Measurement.sample_index)	;
    
    % -------------------------------------------------------------------------
    % Left Panel
    
    Xlim	=	[   tmeas(1)	tmeas(end)  ]	;
    Ylim	=	[   0-dy        2.5/14+dy	]   ;
    SubPos	=	[   0.15 ...
                    PanelOffset+(nSensorSetup-find(SensorSetups==SensorSetup))*PanelSpace ...
                    0.50 PanelHeight        ]	;
    
    subplot('Position',SubPos), 
        set(gca,'FontSize',FS,'Xlim',Xlim,'Xtick',[0:1:10],'Ylim',Ylim)
        grid on
        hold on,
        
        for i=1:Rk
            
            z1	=	xsim(i,:)      ;
            z2	=	xtilde(i,:)	;
            
            plot(tsim,z1,'-',...
                'Color',Colours{i},...
                'MarkerSize',3,...
                'Linewidth',LW)
            plot(tmeas,z2,Strings{i},...
                'Color',Colours{i},...
                'MarkerFaceColor',Colours{i},...
                'MarkerSize',5)
            plot(-tsim,z1,'--',...
                'Color',Colours{i},...
                'MarkerSize',3,...
                'Linewidth',LW3)
            
        end
        
        if SensorSetup==1
         
            leg = legend({ ...
                '${x}_{k,1}(t)$',...
                '$\tilde{x}_{k,1}(t_h)$',...
                '$\tilde{x}_{k,1}(t_h)\pm 3 \lambda_{1,h}$'...
                '${x}_{k,2}(t)$',...
                '$\tilde{x}_{k,2}(t_h)$',...
                '$\tilde{x}_{k,2}(t_h)\pm 3 \lambda_{2,h}$'...
                },'Interpreter','Latex','Location','NorthEast') ;
            
            if OneEllipsoidGraph
                
                LegPos      =	get(leg,'Position')         ;
                Xlim        =	get(gca,'Xlim')             ;
                LegPos(1)	=	SubPos(1) + SubPos(3)*1.11	;
                LegPos(2)   =   0.73 ;
                
                set(leg,'Position',LegPos)
                
            end
            
        end
    
        for i=1:Rk
        
            z1	=	xsim(i,:)                   ;
            z2	=	xtilde(i,:)                 ;
            z3	=	xtilde(i,:)+3*lambda(i,:)	;
            z4	=	xtilde(i,:)-3*lambda(i,:)	;
            
            plot(tsim,z1,'-',...
                'MarkerSize',3,'Color',Colours{i},'Linewidth',LW,'Color',Colours{i})
            plot(tmeas,z2,Strings{i},...
                'MarkerSize',3,'Color',Colours{i},'MarkerFaceColor',Colours{i})
            plot([ tmeas-d ; tmeas+d],[ z3 ; z3],'-',...
                'MarkerSize',14,'Linewidth',LW3,'Color',Colours{i})
            plot([ tmeas-d ; tmeas+d],[ z4 ; z4],'-',...
                'MarkerSize',14,'Linewidth',LW3,'Color',Colours{i})
        
            
        end
        
        if SensorSetup>=3
            for j=1:length(ellipoid_index)
                TimeEllips	=	tmeas(ellipoid_index(j))	;
                if j>=3
                    PlotVertical(TimeEllips,'-','Color',colours_time(j,:),'LineWidth',LW2)
                else
                    PlotVertical(TimeEllips,'--','Color',colours_time(j,:),'LineWidth',LW3)
                end
            end
        end
    
        if SensorSetup==max(SensorSetups)
            xlabel({'Time ($t$, $t_h$) [h]'},...
                'Interpreter','latex','HorizontalAlignment','center')
        else 
            set(gca,'Xticklabel',[])
        end
    
        ylabel({'$\tilde{x}_{k,i}(t)$','${x}_{k,i}(t)$','[mol]'},...
                'Interpreter','latex','HorizontalAlignment','center')
    
        if OneEllipsoidGraph
            textX	=       Xlim(1)+1.05*range(Xlim)                 ;
            textY	=       mean(Ylim)                              ;
            textT	=	[   '(' char(96+find(SensorSetup==SensorSetups))        ')'	]	;
        else
            textX	=       Xlim(1)+.95*range(Xlim)                 ;
            textY	=       0 ; %mean(Ylim)                              ;
            textT	=	['(' char(96+1+(find(SensorSetup==SensorSetups)-1)*2)	')' ]	;
        end
        text(textX,textY,textT,'fontsize',FS,...
                'HorizontalAlignment','center','VerticalAlignment','middle')
            
    % ---------------------------------------------------------------------
    % Right Panel
    
    Xlim        =	[   -a	+a	]           ;
    Ylim        =	[   -a	+a	]           ;
    ratio       =	min(1,PanelHeight/.25)	;
    
    if OneEllipsoidGraph
        SubPos	=	[   0.7 ...
                        PanelOffset ...
                        0.20*ratio 0.25*ratio ] ;
    else
        SubPos	=	[   0.7	...
                        PanelOffset+(nSensorSetup-SensorSetup)*PanelSpace ...
                        0.20*ratio 0.25*ratio ] ;
    end
    
    textY       =	Ylim(2)+.05*range(Ylim) ;
    textX       =	mean(Xlim) ; % Xlim(1)-.05*range(Xlim) ;
    
    subplot('Position',SubPos);
        hold on,
        grid on
        set(gca,'FontSize',FS,'Xlim',Xlim,'Ylim',Ylim,'Yaxislocation','right')
    
        SIG = 3*Lambda(:,:,1);
        switch SensorSetup
            case 1
                han             =   error_ellipse(SIG,'style','k--')    ;
                han.LineWidth	=   LW2                                  ;
            case 2
                han             =   error_ellipse(SIG,'style','--')     ;
                han.LineWidth	=   LW2                                  ;
                han.Color       =   ones(1,3)*.55                       ;
            otherwise
                for j=length(ellipoid_index):-1:1
                    SIG         =	3*Lambda(:,:,ellipoid_index(j))     ;
                    if j>=3
                        han     =   error_ellipse(SIG,'style','-')      ;
                    han.LineWidth   =   LW2                             ;
                    else
                        han     =   error_ellipse(SIG,'style','--')      ;
                    han.LineWidth   =   LW3                             ;
                    end
                    han.Color       =   colours_time(j,:)               ;
                end
        end
        
        PlotVertical(0,'k-')
        PlotHorizontal(0,'k-')
        
        if SensorSetup==max(SensorSetups)
            xlabel( ...
                {'$\tilde{x}_{k,1}(t) - {x}_{k,1}(t)$ [mol]'},...
                'Interpreter','latex',...
                'HorizontalAlignment','center'  )
        end
        
        if nSensorSetup>=5
            ylabel(...
                {'$\tilde{x}_{k,2}(t) - {x}_{k,2}(t)$','\quad [mol]'},...
                'Interpreter','latex',...
                'HorizontalAlignment','center'  )
        else
            ylabel({'$\tilde{x}_{k,2}(t)$-${x}_{k,2}(t)$ [mol]'},...
                'Interpreter','latex',...
                'HorizontalAlignment','center'  )
        end
    
        if OneEllipsoidGraph
            textT = ['(' char(96+nSensorSetup+1) ')'] ;
        else
            if SensorSetup~=nSensorSetup
                set(gca,'Xticklabel',[])
            end
            textT = ['(' char(96+SensorSetup*2) ')'] ;
        end
        
        text(textX,textY,textT,'fontsize',FS,'HorizontalAlignment','center','VerticalAlignment','bottom')
        
end

SaveMyFigure(fighan,'.\Figures\Results2_Extents')
