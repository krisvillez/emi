function Trans = VerifySCS(Model,Interval)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - VerifySCS.m
% -------------------------------------------------------------------------
% Description 
%
% VerifySCS is a function which finds the values of transitions to absolute
% precision when feasible.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Trans = VerifySCS(Model,Interval)
%
% INPUT
%	Model:     	Structure describing the spline model.
%               (required) 
%   Interval:   Matrix describing the bounds on the transitions. It must be
%               specified as 2-column matrix with each row corresponding to
%               one transition. The left (right) column specifies the lower
%               (upper) bounds for the transitions.
%               (required)
%
% OUTPUT
%   Trans:      Values for the transitions. Transitions which cannot be
%               solved to absolute precision are given as NaN. 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


QR = Model.QR ;
ncol = size(QR,2) ;
cols = 3:(ncol-2) ;

Tol = 1e-4 ;
m = size(Interval,1) ;

Trans      =	nan(m,1) ; 

te      =	unique(sort([Model.tk(:) ; Interval(:) ])) ;

for o=1:Model.Order
    ysim{o}    =	EvalSCS(Model,te,Model.Order-o) ;
end


for j=1:m
    
    if Model.verifiable(j)
        
        
        anyzero = any(QR(j:j+1,cols)==0,2) ;
        if any(anyzero)
            
            if all(anyzero)
                Trans(j) = nan ; %mean(Interval(j,:)) ;
            else
                Trans(j) = Interval(j,anyzero) ;
            end
            
        else
            IsDisc =	QR(j+1,end)>0	; % check for discontinuity
            if IsDisc
                Trans(j) = nan;
            else
                
                if sum(QR(j,cols).*QR(j+1,cols)==-1)==1
                    col = cols( QR(j,cols).*QR(j+1,cols)==-1 ) ;
                    
                    order = ncol-col ;
                    switch order
                        case 1
                            % Constant
                            Trans(j) 	=	Interval(j+1,1) 						;
                        case 2
                            % Linear case
                            inc     =   ismember(te,Interval(j,:))              ;
                            y_inc   =	ysim{order}(inc)                    ;
                            dy      =   -y_inc(1)/(y_inc(2)-y_inc(1))       ;
                            Trans(j)   =   dy/(Interval(j,2)-Interval(j,1))+Interval(j,1)	;
                            Trans(j)   =   max(Interval(j,1),min(Interval(j,2),Trans(j))) ;
                        case 3
                            % Quadratic case
                            inc     =   ismember(te,Interval(j,1))              ;
                            y1   =	ysim{1}(inc)                    ;
                            y2   =	ysim{2}(inc)                    ;
                            y3   =	ysim{3}(inc)                    ;
                            
                            %                         figure
                            %                         hold on
                            % %                         plot(ysim{1},'k.-')
                            % %                         plot(ysim{2},'b.-')
                            %                         plot(te,ysim{1},'ko-')
                            %                         plot(te,ysim{2},'bo-')
                            %                         plot(te,ysim{3},'ro-')
                            
                            %                         xx = (Interval(j,1):.01:Interval(j,2))-Interval(j,1) ;
                            %                         yy = y1/2*xx.^2 + y2*xx.^1 +y3  ;
                            C   =   [y1/2 y2 y3] ;
                            sol =   roots(C)+Interval(j,1) ;
                            %                             Interval(j,:)
                            %                             sol
                            sol = sol(and(sol>=Interval(j,1)-Tol,sol<=Interval(j,2)+Tol)) ;
                            %                             sol
                            if length(sol)==1
                                Trans(j) = max(min(sol,Interval(j,2)),Interval(j,1)) ;
                                %                             plot([sol sol],[-1 +1],'k-')
                            end
                            %                         plot(xx+Interval(j,1),yy,'r.-')
                            %
                            %                         yy = y1*xx.^1 + y2  ;
                            %                         plot(xx+Interval(j,1),yy,'b.-')
                            %                         yy = y1*xx.^0  ;
                            %                         plot(xx+Interval(j,1),yy,'k.-')
                            %                         pause
                        case 4
                            % Cubic case
                            inc     =   ismember(te,Interval(j,1))              ;
                            y1   =	ysim{1}(inc)                    ;
                            y2   =	ysim{2}(inc)                    ;
                            y3   =	ysim{3}(inc)                    ;
                            y4   =	ysim{4}(inc)                    ;
                            
                            %                         figure
                            %                         hold on
                            % %                         plot(ysim{1},'k.-')
                            % %                         plot(ysim{2},'b.-')
                            %                         plot(te,ysim{1},'ko-')
                            %                         plot(te,ysim{2},'bo-')
                            %                         plot(te,ysim{3},'ro-')
                            
                            %                         xx = (Interval(j,1):.01:Interval(j,2))-Interval(j,1) ;
                            %                         yy = y1/2*xx.^2 + y2*xx.^1 +y3  ;
                            C = [y1/6 y2/2 y3 y4] ;
                            sol = roots(C)+Interval(j,1) ;
                            sol = sol(and(sol>=Interval(j,1)-Tol,sol<=Interval(j,2)+Tol)) ;
                            if length(sol)==1
                                Trans(j) = sol ;
                                plot([sol sol],[-1 +1],'k-')
                            end
                            %                         plot(xx+Interval(j,1),yy,'r.-')
                            %
                            %                         yy = y1*xx.^1 + y2  ;
                            %                         plot(xx+Interval(j,1),yy,'b.-')
                            %                         yy = y1*xx.^0  ;
                            %                         plot(xx+Interval(j,1),yy,'k.-')
                            %                         pause
                            
                        otherwise
                            warning('higher order zero-crossings not implemented yet')
                            pause
                    end
                    
                end
                
            end
        end
    end
end
