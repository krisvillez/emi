
% -------------------------------------------------------------------------
% EMI Toolbox - Figure1.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces Figure 1 in [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% This script plots Figure 1.

disp('===================== FIGURE 1 =====================')

fig1        =	figure(1)   ;

P           =	SFP.P       ;

purple1     =	153/255     ;
purple2     =	255/255     ;
gray        =	200/255     ;
                
% Colours8 = [    purple1     purple1     purple2;
%                 purple1     purple1     purple2;
%                 0           0           0;
%                 0           0           0;
%                 0           0           0;
%                 gray        gray        gray;
%                 gray        gray        gray;
%                 gray        gray        gray;    ];      

Colours8 = [    flipud([0:.2:.8]')*ones(1,3)                ;
                flipud([0:.4:.8]')*ones(1,3)	]           ;                  
                
linestyles  =	{'-','-','-','-','-','--','--','--'}        ;
nFun        =	length(model_names)                         ;

SS      =       0:.01:25                                    ;
S0      =       25                                          ;

for iFun = 1:nFun
    
    disp(['Case: ' num2str(iFun)])
    disp(['	Simulating rate function: ' model_names{iFun}])
    
    model_name  =   model_names{iFun}                       ;
    ratefun     =	SelectFunction(model_name)              ;
    
    figure(1), hold on
        plot(SS,ratefun(SS,P),linestyles{iFun},'LineWidth',2,'Color',Colours8(iFun,:))
    
end

disp('Editing the figure.')

model_names_legend = model_names ;
% [tf,index]=ismember('Peeters & Eilers',model_names_legend) ;
% model_names_legend{index} = ['Peeters' char(10) '& Eilers'] ;
 
figure(1), 
    box on
    set(gca,'FontSize',20)
    xlabel('substrate concentration S [mg N/L]')
    ylabel('growth rate r(S) [1/day]')
    set(gca,'Xlim',[0 25])
    set(gca,'Ylim',[0 P(1)+0.1])
    set(gca,'Xtick',0:5:25)
    set(gca,'Ytick',0:0.4:P(1))
    
    [legend_h,object_h,plot_h,text_strings] = columnlegend(2, model_names_legend(:),'location','south','boxoff') ;
    set(fig1,'name','Figure 1','numbertitle','off') ;
%     grid on

disp('---Finished---')
