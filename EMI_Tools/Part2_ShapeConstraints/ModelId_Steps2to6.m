function [ratefun1,par_hat,g_hat,C_hat,meta]  =   ModelId_Steps2to6(TrendModels,Name,c0,sigmaY,Cmeas,tmeas,StartTimeDecay,Knots,Shape)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Steps2to6.m
% -------------------------------------------------------------------------
% Description
%
% Estimates kinetic parameter values starting from fitted shape constrained
% spline trend models.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[ratefun1,par_hat,g_hat,C_hat,meta]	=	...
%   ModelId_Steps2to6(TrendModels,Name,c0,sigmaY,Cmeas,tmeas,StartTimeDecay,Knots,Shape)
%
% INPUT
%   TrendModels     :   Cell array of fitted shape constrained spline
%                       functions 
%   Name            :   Name of the rate law
%   c0              :   Initial concentrations
%   sigmaY          :   measurement error standard deviations
%   Cmeas           :   Measurement profiles
%   tmeas           :   Sampling times
%   StartTimeDecay	:   Start time of phase without growth
%   Knots           :   Location of spline knots [necessary if Name starts
%                       with 'SCS ' ]
%   Shape           :   Shape constraint [for SCS type rate laws only]
%
% OUTPUT
%   ratefun1        :   Rate law function handle
%   par_hat         :   best parameter estimates
%   g_hat           :   best objective function value
%   C_hat           :   optimal concentration estimates
%   meta            :   shape constrained spline rate law [if applicable]
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-04
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ========================================
% STEP 2: Setup growth rate structure

disp(['         -- Setup'])

if nargin<8 || isempty(Knots)
    Knots               =	[   ]                                                                   ;
end
if nargin<9 || isempty(Shape)
    Shape               =	[   ]                                                                   ;
end
[ratefun1,npar1,meta]	=   ModelId_Step2_SetupGrowth(Name,Knots)                                   ;

% ========================================
% STEP 3-5: Initial guesses
disp(['         -- Initial guesses'])

% STEP 3: Get initial guesses for growth rate law
par1_hat                =	ModelId_Step3_GuessGrowth(TrendModels,ratefun1,npar1,Shape)             ;

% STEP 4: Get initial guesses for decay rate law
par2_hat                =	ModelId_Step4_GuessDecay(TrendModels{2},StartTimeDecay)                 ;

% STEP 5: Get initial guesses for yield coefficient
par3_hat                =	ModelId_Step5_GuessYield(TrendModels,c0,par2_hat)                       ;

% ========================================
% STEP 6: Simultaneous estimation

disp(['         -- Optimization'])
% combine all parameters:
par_init                =	[	par3_hat	;	par2_hat	;	par1_hat	]                       ;

% optimize:
[par_hat,g_hat,C_hat]	=	ModelId_Step6_ParEstim(par_init,ratefun1,c0,sigmaY,tmeas,Cmeas,Shape)	;

    

end

