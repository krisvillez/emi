function Model = QualitySCS(Model)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - QualitySCS.m
% -------------------------------------------------------------------------
% Description
%
% QualitySCS computes the criteria for the spline function (e.g., fit,
% smoothing penalty, ...). 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model = QualitySCS(Model)
%
% INPUT
%	Model:     	Structure describing the spline model.
%               (required)
%
% OUTPUT
%	Model:     	Structure describing the updated spline Model with added
%               fields and values for the criteria. 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ---------------------
% Get necessary parameters:
B   	=   eval_basis(Model.ty,Model.basis,0)	; % obtained required basis for specified coefficients and derivative d
W 		=	Model.W 		;
x       =   Model.x     	;
y		=	Model.y			;

% ---------------------
% Evaluate fit:
yhat    =   B*x         	;
e 		=	yhat(:)-y(:) 	;
Jfit	=	sum(e'*W*e)		;

% ---------------------
% Evaluate penalty:
if isfield(Model,'L')
    L   	= 	Model.L 	;
    Jpen	=	x'*L*x		;
else
    Jpen    =   0           ;
end

% ---------------------
% Sum and save results:
J			=	Jfit + Jpen	;

Model.J 	=	J		;
Model.Jfit 	=	Jfit 	;
Model.Jpen 	=	Jpen 	;