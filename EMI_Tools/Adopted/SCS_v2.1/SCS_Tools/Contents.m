% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - v2.0
% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
% SCS_Tools - m-files (./SCS_Tools/*.m)
% -------------------------------------------------------------------------
%
%   Alphabet   			-	Character definitions for primitives
%	ApplyQR				-	Adds qualitative sequence/representation to existing spline function structures
%	AssessVerifiability	-	Evaluates whether transitions can be solved to absolute precision
%	AutoSCS				-	Short-cut function for shape constrained spline fitting with default settings
% 	BoundSCS			- 	Bounds for Shape Constrained Spline function fit
%   BoundSCSlow         - 	Lower bound for Shape Constrained Spline function fit
%	BoundSCSupp			- 	Upper bound for Shape Constrained Spline function fit
% 	CFD					- 	Visualization aid: Colours For Drawing
%	CheckResolution		-	Checks if transitions are known to knot resolution.
%	CreateSCS			-	Create Shape Constrained Spline structure
% 	DefaultDistX		-	Default distance constraints for transitions
% 	DefaultIntX			-	Default location constraints for transitions
%	EvalSCS				-	Evaluate spline function or one of its derivatives
% 	FitSCS				-	Fit Shape Constrained Spline function
%	GetFeasibleSolution	-	Produce a solution to a quadratic optimization problem with 	linear inequality constraints, if feasible
%	GetMapsX2P			-	Get projection matrices to map spline function coefficients to polynomial coefficients of the function and its derivatives
%	PlotQRT				-	Visualization aid: Plot Qualitative Representation of Trends
%	PlotSCS 			-	Visualization aid: Plot Shape Constrained Spline and its 1st derivative
%	QualitySCS			-	Evaluate objective criteria for the spline function 
% 	SCS2QR 				-	Convert Shape Constrained Spline object into a Qualitative Representation for plotting
%	String2QR			-	Convert a qualitative sequence or representation defined with a string into its matrix form
%   TryMOSEK            -   Script that tries using the Mosek software
%	VerifySCS 			-	Compute the values for transitions to absolute precision when possible
%

%