
% -------------------------------------------------------------------------
% EMI Toolbox - STEP0_DataGeneration_Display.m
% -------------------------------------------------------------------------
% Description
%
% Visualizes the data obtained from the simulated batch experiments.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script displays the results obtained with \n' ...
            '''STEP0_DataGeneration.m'' \n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% -------------------------------------------------------------------------
% Visualization choices
GrayScale   = true ;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Load results and gather data

FILEPATH	=	[	FOLDER	'STEP0_All' ]	;
load(FILEPATH)

tsim	=   Model.Measurement.tsim                  ;
tsim    =	tsim *24                                ;
tmeas	=	tsim(Model.Measurement.sample_index)    ;

csim    =   Model.simulation.csim                   ;
ysim    =   Model.simulation.ysim                   ;
ytilde	=   Model.data.ytilde                       ;


% -------------------------------------------------------------------------
% Visualization parameters

FS      =   17  ;
LW      =   3   ;
LW2     =   2   ;

if GrayScale
    Colours	=   {.73*ones(1,3),.55*ones(1,3),.37*ones(1,3),'k'} ; % For paper (gray-scale)
else
    Colours	=   {[0.635  0.078 0.184 ],[0.929 0.694 0.125],[0 0.447 0.741],'k'} ; % For demos
end

Lines	=	{':',':',':','-'}           ;
Strings	=   {'^','s','v','o'}           ;

gN4mol	=	14                          ;
factor	=	[ gN4mol*ones(1,3) 1/2 ]    ;

Ylim	=	[   -.365	5.865   ]       ;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Create Figure

fighan = figure;
    
set(gcf,'Position',[100 100 1000 800 ])
    set(gca,'FontSize',FS)
    hold on
    
    iy =    1	; 
    plot(tsim,csim(3,:)*gN4mol,'-','Linewidth',LW,'Color',Colours{iy})
    
    for iy=1:4
        plot(tsim,ysim(iy,:)*factor(iy),Lines{iy},'Linewidth',LW,'Color',Colours{iy})
        plot(tmeas,ytilde(iy,:)*factor(iy),Strings{iy},'Color',Colours{iy},'MarkerFaceColor',Colours{iy})
    end
    
    grid on
    
    set(gca,    'Xlim',[0 10],...
                'Xtick',[0:10])
    set(gca,    'Ylim',Ylim,...
                'Ytick',[0:.5:5.5])

    xlabel({'Time [h]'})
    ylabel({'Nitrogen concentration [g N/L]'},...
                'Position',[-1 mean(Ylim)],'VerticalAlignment','middle')
            
    legend({...
        '$c_{NH3}$','$y_{TAN}$','$\tilde{y}_{TAN}$',...
        '$y_{TNO2}$','$\tilde{y}_{TNO2}$',...
        '$y_{TNO3}$','$\tilde{y}_{TNO3}$',...
        '$y_{pH}$','$\tilde{y}_{pH}$',...
        },'fontsize',FS,'Color','w','Interpreter','Latex')  ;
    
    
axpos = get(gca,'Position');
axes('Position',axpos,'Color','none')

Ylim = [-.73 11.73];
set(gca,'Xlim',[0 10],'Xtick',[0:10],'Xticklabel',[ ])
set(gca,'Ylim',Ylim,'Ytick',[0:1:11],'Yaxislocation','right')
ylabel({'pH [-]'},'Position',[10.5 5.5],'VerticalAlignment','middle','HorizontalAlignment','center')


    set(gca,'FontSize',FS)

    SaveMyFigure(fighan,'.\Figures\Results1_Simulation')
    
    
    
% Evaluate objective function
ytilde	=	Model.data.ytilde                                   ;
sigma	=	Model.Measurement.sigma                             ;
WSSR	=	sum(  sum( ( diag(1./sigma)*( ytilde-ysim(:,Model.Measurement.sample_index))).^2 ) )  ;
