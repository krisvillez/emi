

% ------------------------------------------------------------------------
%   Definition of experimental conditions
% ------------------------------------------------------------------------
V       =   1	;
n_0      =   [ .73 .42 0 0 0 0 ]'*V;
t_h   =   (0:5:(10*60))'/60 ; % minutes

% ------------------------------------------------------------------------
%   Definition of dynamics
% ------------------------------------------------------------------------
N_       =   [   -1  -1	+1	0	0	0       ;
                -2	0	0	+1	0	0       ;
                0   +1	-2  +1  0	0       ;
                0	0   0	-1  +1	0       ;
                0	0   0	-2  +1	+1  ]	;
ratecell = { ...
    @(c,theta) theta(1)*(c(1).*c(2)-theta(6)*c(3)) ; ...
    @(c,theta) theta(2)*c(1).^2 ; ...
    @(c,theta) theta(3)*c(3) ; ...
    @(c,theta) theta(4)*c(4) ; ...
    @(c,theta) theta(5)*c(4).^2 };
theta_   =   2*[ 1 .25 .5 .2 1*.8 .7 ]';

% ------------------------------------------------------------------------
%   Meta-information
% ------------------------------------------------------------------------
M       =	length(t_h)     ;
[R,S]   =	size(N_)     ;
T = length(theta_);
name_species = {'A','B','C','D','E','F'};
name_parameters = {'k_1','k_2','k_3','k_4','k_5','K_1'};
unit_concentration = 'mol/L';
unit_extents = 'mol';

% ------------------------------------------------------------------------
%   Definition of measurements, assuming all concentrations are measured
% ------------------------------------------------------------------------
M_master       =   eye(S)      ;
M_eps   =   eye(S)*.01  ;
M_master=sparse(M_master);
M_eps=sparse(M_eps);

% ------------------------------------------------------------------------
%   Simulation info
% ------------------------------------------------------------------------


pick=[ 3 2 1 4:5 ] ;
pick=[ 1:5 ] ;
%pick=[ 4:5 1:3  ] ;
N_ = N_(pick,:) ;
ratecell = ratecell(pick);

name_reactions = cell(R,1);
for r=1:R
    name_reactions{r}     =	[ 'x_' num2str(pick(r)) ]    ;
end
model.static.name_reactions = name_reactions;


ratefun     =	@(c,theta) cellfun(@(exp) exp(c,theta), ratecell)                   ;
t_        =	unique(sort([ ((t_h(1)*60):(ceil(t_h(end))*60))/60   t_h(:)' ]))'   ;
select_h	=	ismember(t_,t_h)                                                ;

