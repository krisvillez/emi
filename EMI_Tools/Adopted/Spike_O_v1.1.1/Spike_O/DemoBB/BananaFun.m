function z = BananaFun(u,model) 

% -------------------------------------------------------------------------
% Spike_O Toolbox - BananaFun.m
% -------------------------------------------------------------------------
% Description 
%
% This function evaluates the Rosenbrock's banana function
% 
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	z   =   BananaFun(u,model) 
%
% INPUT
%   u       :   m x 2 matrix with each column corresponding to one argument
%   model   :   structure with parameter values a, b, c as fields
%
% OUTPUT
%   z       :   banana function value for the provided arguments
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2013-05-31
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


a = model. a;
b = model. b;
c = model. c;

x = u(:,1) ;
y = u(:,2) ;

z = (a-x).^2  +c*(y-(b-x).^2).^2 ;
