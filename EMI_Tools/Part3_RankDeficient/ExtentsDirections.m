function projection	= ExtentsDirections (static,V) 

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentsDirections.m
% -------------------------------------------------------------------------
% Description
%
%   Define the projection and covariance matrices associated with the
%   observable extents and the observable directions among the ambiguous
%   extents.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	projection	= ExtentsDirections (static,V) 
%
% INPUT
%   static      :   structure including all information based on
%                   invariants.
%   V           :   volume of reactor
%
% OUTPUT
%	projection	:   projection matrix (projection.Lbar_o) and covariance
%                   matrix (projection.Sigmabar_o)
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% ------------------------------------------------------------------------
%	Get information from structure
% ------------------------------------------------------------------------

M_         =	static.M_                      ;
M_eps       =	static.M_eps                    ;
G_         =	static.G_                      ;
pivot       =	static.pivot                    ;
row_a       =	static.row_a                    ;
row_o       =	static.row_o                    ;

Gbar_o = static.Gbar_o ;

% ------------------------------------------------------------------------
%	Computation
% ------------------------------------------------------------------------

% Variance-covariance matrix of measurement errors
Sigma_eps	=	M_*(M_eps*M_eps')*M_'         ;


% Compute projection matrix P and expected variance-covariance matrix
if ~isa(Gbar_o,'sym')
    P                       =	V * (Gbar_o'*Sigma_eps^(-1)*Gbar_o)^(-1)*Gbar_o'*Sigma_eps^(-1) ;
    Sigmabar_o              =	V *((Gbar_o'*Sigma_eps^(-1)*Gbar_o)^(-1))'*V                    ;
    projection.Sigmabar_o	=	Sigmabar_o      ;
    projection.Lbar_o       =	P               ;
end
