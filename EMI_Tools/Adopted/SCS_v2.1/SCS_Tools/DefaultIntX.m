function IntX = DefaultIntX(Model) 

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - DefaultDistX.m
% -------------------------------------------------------------------------
% Description 
%
% This function computes the default interval constraints on the
% transitions.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	IntX = DefaultIntX(Model) 
%
% INPUT
%   Model:      Structure describing the Shape Constrained Spline function
%
% OUTPUT
%   IntX:       Default intervals for transitions
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-06-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

t1      =   Model.tk(1)     ;
t2      =   Model.tk(end)   ;
QR      =   Model.QR        ;
nQR     =   size(QR,1)      ;
IntX    =   [ t1 ; t2 ]*ones(1,nQR-1)   ;
    