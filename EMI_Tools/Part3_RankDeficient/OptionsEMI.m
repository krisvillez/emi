function [options_compute,options_graph] = OptionsEMI (displaystyle,unit_concentration,unit_extents)

% -------------------------------------------------------------------------
% EMI Toolbox - OptionsEMI.m
% -------------------------------------------------------------------------
% Description
%
%
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[options_compute,options_graph] = OptionsEMI (displaystyle,unit_concentration,unit_extents) 
%   
% INPUT
%	displaystyle        :   [optional] string defining color scheme. Use
%                           'grey' for print-friendly scheme
%   unit_concentration	:   Measurement unit for the concentration
%                           measurements (for graphics)
%   unit_extents        :   Unit of the extents (for graphics)
%
% OUTPUT
%   options_compute     :   structure defining default fine-tuning options
%                           for all computations
%   options_graph       :   structure defining default fine-tuning options
%                           for all graphs  
%   
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------



% ------------------------------------------------------------------------
% Computational options
% ------------------------------------------------------------------------

optim1                      =	optimset('fmincon')     ;
optim1.MaxFunEvals          =   inf                     ;
optim1.MaxIter              =   inf                     ;
optim1.Algorithm            =	'interior-point'        ;
optim1.Display              =	'off'                   ;
optim1.TolFun               =   1e-7                    ;

optim2                      =	optimset('fmincon')     ;
optim2.MaxFunEvals          =   inf                     ;
optim2.MaxIter              =   inf                     ;
optim2.Algorithm            =	'active-set'            ;
optim2.Display              =	'off'                   ;
optim2.TolFun               =   1e-7                    ;

extent.factorization        =   'rref'                  ;
partition.objective         =   'no_overlap'            ;

verbose                     =   2                       ;

options_compute.optim1      =	optim1                  ;
options_compute.optim2      =	optim2                  ;
options_compute.verbose     =	verbose                 ;
options_compute.extent      =   extent                  ;
options_compute.partition   =   partition               ;
% ------------------------------------------------------------------------
% Graphical options
% ------------------------------------------------------------------------

switch displaystyle
    case 'grey'
        markers     =	{'^','v','o','d','<','>','s','s'}       ;
        linestyles	=	{'--',':','-','--',':','-','--','-'}	;
        colours     =	[ 0  0  0 .42 .42 .42 .73 .73]'*[1 1 1] ;
    otherwise
        markers     =	{'o','+','x','o','+','x'}               ;
        linestyles	=	{'-','--',':','-','-','-'}              ;
        colours     =	distinguishable_colors(max(R,S))*.5     ;
end

graph                               =   false       ;
linewidth                           =	2           ;
FS                                  =	16          ;

options_graph.graph                 =	graph               ;
options_graph.FS                    =	FS                  ;
options_graph.colours               =	colours             ;
options_graph.markers               =	markers             ;
options_graph.linewidth             =	linewidth           ;
options_graph.linestyles            =	linestyles          ;
options_graph.unit_concentration	=	unit_concentration	;
options_graph.unit_extents          =	unit_extents        ;