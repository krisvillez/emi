
% -------------------------------------------------------------------------
% EMI Toolbox - s_TechnicalReport_v3_SimulationStudy.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces all figures related to the simulated example
% appearing in [2]. 
%
% REFERENCES
%   [1] Villez, K, Billeter, J, Bonvin, D (2019). Incremental Parameter
%       Estimation under Rank-Deficient Measurement Conditions. Processes,
%       7(2), 75.
%   [2] Villez, K (2018). Extent-based Model Identification under
%       Incomplete Observability Conditions. (TR-006-03-0). Technical
%       Report, Eawag, D�bendorf, Switzerland.  
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% ------------------------------------------------------------------------- 

clc
clear all
close all

addpath ('.\systemdef\')
addpath ('..\..\EMI_Tools\Part3_RankDeficient\')
rng (42,'twister')

for enumerate = [false true]
    
    % ========================================================================
    %   SYSTEM DEFINITION AND INFORMATION
    % ========================================================================
    
    s_TechnicalReport_SystemDef
    
    % ========================================================================
    %   SIMULATE EXPERIMENT
    % ========================================================================
    [n_t,ctil_t]        =	SimulationExperiment (N_,M_eps,M,S,V,select_h,n_0,ratefun,t_,theta_) ;
    
    groundtruth.theta_	=	theta_	;
    groundtruth.n_t     =	n_t	;
    
    % ========================================================================
    %   TUNING OF PARAMETER ESTIMATION AND GRAPHICAL OUTPUT
    % ========================================================================
    
    model = ModelSetup (V,n_0,M_eps,N_,name_species,t_,select_h,ratecell,T,name_parameters);
    
    displaystyle                    =	'grey'                      ;
    [options_compute,options_graph] =	OptionsEMI (displaystyle,unit_concentration,unit_extents)   ;
    
    options_graph.gridpos_conc      =   [	2.3     .33      ]   ;
    options_graph.gridpos_extent	=   [	0.1     .3      ]   ;
    
    xX      =	[   -.5      -.5       2.5       2.5     .5        ]   ;
    yX      =	[	-.8     1.8     1.8       -.8     1.8          ]   ;
    xT      =	[	-.5       -.5       +2.5     +2.5      1.5  .5  1.5 ]   ;
    yT      =	[	+.2     .8	.8	0.2	1.8    -.8  -.8]   ;
    
    % ========================================================================
    % ========================================================================
    %Setup =1;
    scenarioname = {'A1','A2','A3','A4','A5'};
    
    picks0 = flipud(perms(1:(R-1))) ;
    picks0 = [ picks0 5*ones(size(picks0,1),1) ];
    model00 = model ;
    xX0 = xX;
    yX0 = yX;
    
    if enumerate
        opt_svd     =   0   ;
        opt_small	=   1   ;
    else
        opt_svd     =	0:1	;
        opt_small	=	0:1	;
    end
    
    for do_svd =opt_svd
        for do_small = opt_small
            
            if do_svd==1
                options_compute.extent.factorization = 'svd';
                xCHI    =	[	+.2     +1      1.8	]   ;
                yCHI	=	[	+.4     +.5     +.6	]	;
            else
                xCHI    =	[	-.8     +.4      1	]   ;
                yCHI	=	[	+.2     +.5     +.5	]	;
            end
            
            if do_small==1
                options_compute.partition.objective = 'min_overlap';
            end
            
            if and(do_svd==0,do_small==1)
                if enumerate
                    picks	=	picks0      ;
                else
                    picks	=	picks0([1 2 size(picks0,1) ],:)      ;
                end
                SubSys	=	nan(size(picks,1),model.kinetic.T)	;
                SubPar  =   nan(size(picks,1),3)                ;
            else
                picks	=	picks0(1,:)                         ;
            end
            
            for Setup = 1:size(picks,1)
                
                pick = picks(Setup,:)  ;
                scenarioname = [ '_tr_' options_compute.extent.factorization '_' options_compute.partition.objective '_' num2str(pick(:))' ];
                
                disp('====================================================================')
                disp(['  SETUP: ' upper(options_compute.extent.factorization) ' - ' upper(options_compute.partition.objective)  ' Permutation: ' num2str(pick(:))' ])
                disp('====================================================================')
                
                model = model00 ;
                model.static.name_reactions = model00.static.name_reactions(pick) ;
                model.static.N_ = model00.static.N_(pick,:);
                model.kinetic.ratecell = model00.kinetic.ratecell(pick);
                model.static.name_reactions = model00.static.name_reactions(pick);
                
                ratefun         =	@(c,theta) cellfun(@(exp) exp(c,theta), model.kinetic.ratecell)   ;
                sym_theta       =	sym('sym_theta',[T 1 ])           ;
                sym_conc       =	sym('sym_conc',[S 1 ])           ;
                F               =	zeros(T,R) ;
                for t=1:T
                    ds          =   diff(   ratefun(  sym_conc, sym_theta ), sym_theta(t), 1)    ;
                    F(t,1:R) = (~logical(ds==0)) ;
                end
                
                xX = xX0(pick);
                yX = yX0(pick);
                % ====================================================================
                %   MEASUREMENT GENERATION
                % ====================================================================
                
                M_	=   M_master                   ;
                
                ytil_h                      =	M_*ctil_t       ;
                model.static.M_             =	M_              ;
                
                model.observations.ytil_h	=   ytil_h          ;
                groundtruth.y_              =   M_*n_t/V        ;
                
                % ====================================================================
                %   INITIAL MODEL
                % ====================================================================
                
                model0	=	model	;
                theta_0	=	theta_  ;
                
                if ~enumerate
                    % ====================================================================
                    %   SIMULTANEOUS MODEL IDENTIFICATION
                    % ====================================================================
                    
                    disp(['------------------------------------------------------------' ])
                    disp('	PART I: SIMULTANEOUS MODEL IDENTIFICATION')
                    disp(['------------------------------------------------------------' ])
                    
                    model1	=	SimultaneousEstimation (model0,theta_0,options_compute)	;
                    
                end
                
                % ====================================================================
                %   INCREMENTAL MODEL IDENTIFICATION
                % ====================================================================
                
                disp(['------------------------------------------------------------' ])
                disp('	PART II: INCREMENTAL MODEL IDENTIFICATION')
                disp(['------------------------------------------------------------' ])
                
                % --------------------------------------------------------------------
                %   LABEL EXTENTS AND EXTENT DIRECTIONS + COMPUTE THEIR VALUES
                % --------------------------------------------------------------------
                
                % Analysis
                [xbarhat_o,xbar_o,model]	=   Extents (model,options_compute,groundtruth)	;
                groundtruth.xbar_o          =   xbar_o                                          ;
                
                % --------------------------------------------------------------------
                %   SYSTEM PARTITIONING
                % --------------------------------------------------------------------
                
                % Analysis - variation: sequential
                options_compute_alt	=	options_compute ;
                if strcmp(options_compute.partition.objective,'min_overlap')
                    options_compute_alt.partition.objective = 'onebyone' ;
                end
                model               =	Partitioning (model,options_compute_alt)           ;
                
                if ~enumerate
                    
                    % Produce figure 1
                    Xdata           =	[ xX xCHI(end-model.static.rho_o+1:end)	xT	]   ;
                    Ydata           =	[ yX yCHI(end-model.static.rho_o+1:end)	yT	]   ;
                    options_graph.Xdata                 =   Xdata                       ;
                    options_graph.Ydata                 =   Ydata                       ;
                    options_graph.showmeasurementarcs	=	false                       ;
                    [fighan,figname]    =	PlotGraph (model,options_graph,scenarioname);
                    for f=1:length(fighan)
                        figpath             =	[	'.\figures\'	figname{f}	'_without'	]	;
                        savefig (fighan(f),figpath)    ;
                        saveas (fighan(f),figpath,'epsc')
                    end
                    
                    % Produce figure 2
                    options_graph.showmeasurementarcs	=	true                        ;
                    [fighan,figname]	=	PlotGraph (model,options_graph,scenarioname);
                    figpath             =	[	'.\figures\'	figname{1}	'_with'	]       ;
                    savefig (fighan(1),figpath)                                            ;
                    saveas (fighan,figpath,'epsc')
                end
                
                if strcmp(options_compute_alt.partition.objective,'onebyone')
                    % Analysis - variation: parallel
                    model           =	Partitioning (model,options_compute)           ;
                end
                
                if and(do_svd==0,do_small==1)
                    nSub = size(model.kinetic.partition,2) ;
                    for iSub=1:nSub
                        sub = SubSys(Setup,model.kinetic.graph.vrtx_par{iSub}-7 );
                        sub(~isnan(sub))=4;
                        sub(isnan(sub))=iSub;
                        SubSys(Setup,model.kinetic.graph.vrtx_par{iSub}-7 )=sub;
                        SubPar(Setup,iSub)=sum(sum(F(:,model.static.Vbar_o(:,iSub)~=0)));
                    end
                end
                
                
                
                if ~enumerate
                    
                    
                    % --------------------------------------------------------------------
                    %   PARAMETER ESTIMATION FOR SUBSYSTEMS
                    % --------------------------------------------------------------------
                    model1j	=	IncrementalEstimation (model,options_compute,theta_0)                  ;
                    
                    % --------------------------------------------------------------------
                    %   PARAMETER ESTIMATION FOR WHOLE SYSTEM
                    % --------------------------------------------------------------------
                    theta1	=	model1j.kinetic.theta_hat                                    ;
                    model10 =	SimultaneousEstimation (model1j,theta1,options_compute)  ;
                    
                end
            end
            
            
        end
        
    end
    
    if enumerate
        cols = [ 1 6 2 3 4 7 5];
        parnames = model.kinetic.name_parameters(cols);
        SubSys = SubSys(:,cols);
        
        for r=1:size(SubSys,1)
            loc=find(and(SubSys(r,:)>=2,SubSys(r,:)<=3),1,'first');
            if SubSys(r,loc)==2
            else
                c2 = find(SubSys(r,:)==2) ;
                c3 = find(SubSys(r,:)==3) ;
                SubSys(r,c2)=3;
                SubSys(r,c3)=2;
            end
        end
        
        [~,index]= sort(SubSys(:,4)) ;
        SubSysSort= SubSys(index,:);
        picks = picks(index,:);
        SubPar = SubPar(index,:);
        picknames = num2str(picks) ;
        
        
        fighan=figure; hold on
        set(gcf,'Position',(get(0,'ScreenSize').*[1 1 .3 .9]+[50 50 0 0]))
        if exist('AxisPrep','file'), AxisPrep, end
        for Setup = 1:size(picks,1)
            for t=1:model.kinetic.T
                h = rectangle('Position',[ t-.45 (size(picks,1)-Setup+1)-.45 .9 .9]);
                switch SubSysSort(Setup,t)
                    case 1
                        h.FaceColor = 'k';
                    case 2
                        h.FaceColor = 'b';
                    case 3
                        h.FaceColor = 'w';
                    case 4
                        h.FaceColor = 'c';
                end
            end
        end
        set(gca,'Position',[.42 .1 .55 .85])
        set(gca,'Xtick',[1:model.kinetic.T],'Ytick',[1:size(picks,1)])
        set(gca,'Yticklabel',cellstr(picknames))
        set(gca,'Xticklabel',parnames)
        axis equal
        
        figpath             =	[	'.\figures\partitioningVSpermutation'	]       ;
        savefig (fighan,figpath)                                            ;
        saveas (fighan,figpath,'epsc')
        
    end
    
end

return





