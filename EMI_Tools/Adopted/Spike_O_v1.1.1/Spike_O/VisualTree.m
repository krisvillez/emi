function VisualTree(Tree,Alive,str,index,opt,nopt,graphtype)

% -------------------------------------------------------------------------
% Spike_O Toolbox - VisualTree.m
% -------------------------------------------------------------------------
% Description
%
% This function is an visualization aid for the branch-and-bound algorithm
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	VisualTree(Tree,Alive,str,index,opt,nopt,graphtype)
%
% INPUT
%   Tree    	:	Solution tree
%	Alive		:	Column vector of boolean indicators
%	str			:	String indicating colour for patches
%	index		:	Index indicating first column in Tree representing a bounding value
%	opt			:	Indices of variables which are subject to optimization
%	nopt		:	Number of variables which are subject to optimization
%	graphtype	:	Integer indicating type of graph. Choices are 1 and 2.
%
% OUTPUT
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


Indices
colour      =   true    ;   % use colour (1/true) or not (0/false)
% colour      =   false    ;   % use colour (1/true) or not (0/false)

Alpha = .5 ;
if ~colour
    Alpha = 0;
    str = 'k' ;
end


if nopt==1
    figure(99)
    xx  =   Tree(Alive,index+(opt(1)-1)*2+[0 1 1 0])'   ;
    switch graphtype
        case 1
            yy  =   (ones(length(Alive),1)*[0 0 1 1])'          ;
        case 3
            yy  = [  Tree(Alive,iLB)*ones(1,2)  Tree(Alive,iUB)*ones(1,2)  ]'                  ;
    end
    patch( xx,yy,str,'EdgeColor','k','FaceAlpha',Alpha)
elseif nopt==2
    figure(99)
    xx  =   Tree(Alive,index+(opt(1)-1)*2+[0 1 1 0])' ;
    yy  =   Tree(Alive,index+(opt(2)-1)*2+[0 0 1 1])' ;
    if colour
        patch( xx,yy,str,'EdgeColor','k','FaceAlpha',Alpha)
    else
        patch( xx,yy,str,'EdgeColor','k','FaceAlpha',Alpha)
    end
end
drawnow
