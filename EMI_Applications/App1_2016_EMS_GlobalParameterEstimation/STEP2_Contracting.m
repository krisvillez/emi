
% -------------------------------------------------------------------------
% EMI Toolbox - STEP2_Contracting.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

A=sprintf('This program executes the iterations of the branch-and-bound algorithm \n to a low resolution (1/8). This takes some considerable time but can \n easily be executed on a normal machine. \n');
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

% ============================================================
% SETTINGS FOR OPTIMIZATION

global optionsode
global options_fmincon
global options_lsqlin

Indices

resolpow                =	3                   ;

optionsBB               =	OptimsetBB          ;
optionsBB.boundfun      =	@BatchSim_Bound     ;
optionsBB.boundupp      =	@BatchSim_BoundUpp	;
optionsBB.boundlow      =	@BatchSim_BoundLow	;
optionsBB.resolution	=	(1/2)^resolpow      ;
optionsBB.node          =   1                   ;
optionsBB.graphprogress	=   true                ;
optionsBB.graphinterval =	100                 ;
optionsBB.verbose       =   1                   ;

    
% =========================================================================
% 0. LOAD PROBLEM SETUP

pulse   =   4   ;
load(['Results\SETUP_PULSE' num2str(pulse)])

% =========================================================================
% 1. INITIALIZE
Iter        =   0       ;
Converged	=	false	;

ListParamBounds{Iter+1} =	IntX*diag(Model.Slope)+ones(2,1)*Model.Intercept  ;
ListProgress{Iter+1}    =	[]  ;
ListParamBest{Iter+1}   =   []	;

% =========================================================================
% 2. REPEATED BRANCH-AND-BOUND OPTIMIZATION

while ~Converged
    
    Iter    =   Iter+1  ;
    
    % ---------------------------------------------------------------------
    % Execute B&B and track CPU time
    T0                      =	cputime                         ;
    [Tree,Model,Progress]   =	BB(Model,optionsBB,IntX,[])     ;
    T1                      =	cputime                         ;
    deltaT(Iter+1)          =	T1-T0                           ;
    
    % ---------------------------------------------------------------------
    % Bounding box of retained sets
    BBox        =   [   min(Tree(:,index:2:end))        ;   ...
                        max(Tree(:,index+1:2:end))  ]   ; 
    
    % ---------------------------------------------------------------------
    % Evaluate termination
    Converged	=       all(all(BBox==IntX)) ;
    
    % ---------------------------------------------------------------------
    % Update problem setup for next iteration
    Params          =       BBox*diag(Model.Slope)+ones(2,1)*Model.Intercept ;
    theta           =       Model.SolX(:)'*diag(Model.Slope)+Model.Intercept ;
    
    Model.Slope     =       Params(2,:)-Params(1,:)     ;
    Model.Intercept	=       Params(1,:)                 ;
    IntX            =   [   zeros(1,3)  ; ones(1,3) ]   ;
    
    % ---------------------------------------------------------------------
    % Store results at current iteration
    ListParamBounds{Iter+1}	=	Params      ;
    ListProgress{Iter+1}	=	Progress	;
    ListParamBest{Iter+1}	=	theta       ;
    
    % ---------------------------------------------------------------------
    % Move B&B progress report files to dedicated folder
    Folder = ['Results\PULSE' num2str(pulse) '_CONTRACT_ITER_' num2str(Iter)  ];
    mkdir(Folder)
    FileList = dir('Progress*.csv') ;
    nFile = length(FileList);
    for iFile=1:nFile
        movefile(FileList(iFile).name,[Folder '\' FileList(iFile).name])
    end
    
end

% =========================================================================
% 3. STORE RESULTS
Folder = ['Results/PULSE' num2str(pulse) '_CONTRACT_FINAL'];
mkdir(Folder)
save([Folder '/CONTRACT_PULSE' num2str(pulse)],'Params','ListParamBest','ListParamBounds','ListProgress','Model','optionsBB','options_fmincon','options_lsqlin','optionsode')

disp(sprintf(['Optimization terminated. \n']))

return

