function model = Partitioning (model,options) 

% -------------------------------------------------------------------------
% EMI Toolbox - Partitioning.m
% -------------------------------------------------------------------------
% Description
%
%   This function executes the partitioning step of the extent-based method
%   for incremental parameter estimation
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	model = Partitioning (model,options) 
%
% INPUT
%   model	:   model as structure
%   options	:	partitioning method options, including the fields:
%       options.objective : specifies the partitioning method as a string,
%                           which should be one of:
%                           - 'no_overlap'	(as in [1])
%                           - 'min_overlap' (as discussed in [2])
%                           - 'sequential'	(as discussed in [2])
%                           - 'onebyone'	(as discussed in [2])
%
% OUTPUT
%	model	:   model as structure with partitioning result added
%
% REFERENCES
%   [1] Villez, K, Billeter, J, Bonvin, D (2019). Incremental Parameter
%       Estimation under Rank-Deficient Measurement Conditions. Processes,
%       7(2), 75.
%   [2] Villez, K (2018). Extent-based Model Identification under
%       Incomplete Observability Conditions. (TR-006-03-0). Technical
%       Report, Eawag, D�bendorf, Switzerland.  
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% ------------------------------------------------------------------------
%	Get information from structure
% ------------------------------------------------------------------------

T               =	model.kinetic.T                 ;
ratecell        =	model.kinetic.ratecell          ;
name_parameters	=	model.kinetic.name_parameters	;

index_bar_o     =	model.static.index_bar_o        ;
NNbar_o         =	model.static.NNbar_o            ;
NNbar_u         =	model.static.NNbar_u            ;
R               =	model.static.R                  ;
R_o             =	model.static.R_o                ;
rho_o           =	model.static.rho_o              ;
vertexnames     =	model.static.varnames           ;
Vo              =	model.static.V_o                ;

objective       =	options.partition.objective     ;
verbose         =	options.verbose                 ;

% --------------------------------------------------------------------
%   Symbolic analysis to generate graph
% --------------------------------------------------------------------

if options.verbose, disp(['PARTITIONING - GRAPH - ' upper(options.partition.objective)]), end

ratefun         =	@(c,theta) cellfun(@(exp) exp(c,theta), ratecell)   ;

% symbolic variables
%sym_c           =	sym('sym_c',[S 1 ])                 ;
sym_x           =	sym('sym_x',[R 1 ])                 ;
sym_chi_bar_o	=	sym('sym_chi_bar_o',[R_o+rho_o 1 ]) ;
sym_theta       =	sym('sym_theta',[T 1 ])           ;

% graph in matrix form
%   1. initialize
F               =	zeros(R+rho_o+T,R+rho_o+T) ;
%   2. links from extents to extents
for r=1:R
    ds          =   diff(	ratefun(...
                                NNbar_u'*sym_x+NNbar_o'*sym_chi_bar_o,...
                                sym_theta ),...
                            sym_x(r), 1)    ;
    F(r,1:R)	=	(~logical(ds==0)) ;
end
%   3. links from extents to extent directions
F(1:R,R+(1:rho_o)) = (Vo~=0) ;
%   4. links from extent directions to extents
for r=1:(R_o+rho_o)
    ds          =	diff(   ratefun(...
                                NNbar_u'*sym_x+NNbar_o'*sym_chi_bar_o,...
                                sym_theta ),...
                            sym_chi_bar_o(r), 1)    ;
    F(index_bar_o(r),1:R)	=	or(F(index_bar_o(r),1:R),(~logical(ds==0))') ;
end
%   5. links from parameters to extents
for t=1:T
    ds          =   diff(   ratefun(...
                                NNbar_u'*sym_x+NNbar_o'*sym_chi_bar_o,...
                                sym_theta ),...
                            sym_theta(t), 1)    ;
    F(R+rho_o+t,1:R) = (~logical(ds==0)) ;
end

% create directed graph
G       =   digraph(F-diag(diag(F)))      ;

% --------------------------------------------------------------------
%   Graph clustering
% --------------------------------------------------------------------

if options.verbose, disp('PARTITIONING - CLUSTERING'), end

% remove edges from extent directions
F_                  =   F           ;
F_(index_bar_o,:)	=   0           ;

% create directed graph
G_                  =   digraph(F_) ;

% partitioning
switch objective
    case 'no_overlap'
        partition =	nan(R+rho_o+T,1)	;
        part                =   0               ;
    otherwise
        partition = false(R+rho_o+T, length(index_bar_o)) ;
end
for i=1:length(index_bar_o)
    r = index_bar_o(i) ;
    tree            =	shortestpathtree(G_,1:(R+rho_o+T), r)     ;
    vertex          =	unique([ r ; tree.Edges.EndNodes(:,1)]) ;
    switch objective
        case 'no_overlap'
            labeled         =	~isnan(partition(vertex))               ;
            if any(labeled)
                partition(vertex)	=	min(partition(vertex(labeled)))	;
            else
                part                =   part+1  ;
                partition(vertex)	=   part	;                
            end
        otherwise
            partition(vertex,i) = true ;
    end
end

switch objective
    case 'no_overlap'
    case 'min_overlap'
        J=size(partition,2);
        pars = R+rho_o+(1:T);
        for j=1:J
            parinc = find(partition(pars,j));
            col = setdiff(1:J,j) ;
            if any(all(partition(pars(parinc),col),1))
                jn = find(all(partition(pars(parinc),col),1),1,'first') ;
                partition(:,col(jn)) = or(partition(:,col(jn)) ,partition(:,j)) ;
                partition(:,j) = false(R+rho_o+T,1);
            end
            
        end
        partition = partition(:,any(partition,1) );
        
        [~,~,ic] = unique(partition(R+rho_o+(1:T),:)','rows');
        uic = unique(ic);
        nic = length(uic);
        partition2 = zeros(R+rho_o+T,nic) ;
        for i = 1:nic
            partition2(any(partition(:,ic==uic(i)),2),uic(i)) = uic(i) ;
        end
        partition = partition2 ;
        
    case 'sequential'
        pars = R+rho_o+(1:T);
        [~,uic,gic] = unique(partition(pars,:)','rows') ;
        nic = length(uic);
        partition2 = zeros(R+rho_o+T,nic) ;
        for i=1:nic
            col = find(gic==i);
            partition2(any(partition(:,col),2),i) = i ;
        end
        partition = partition2 ;
        
        % do some sorting so that sequential solving means going
        % from lower index graph to higher index graph
        [~,index] =sortrows((partition==0)','descend');
        partition = partition(:,index);
        nic = size(partition,2);
        % Renumbering
        for i=1:nic
            partition(partition(:,i)~=0,i) = i ;
        end
        
    case 'onebyone'
        
        % do some sorting so that sequential solving means going
        % from lower index graph to higher index graph
        [~,index] =sortrows((partition==0)','descend');
        partition = partition(:,index);
        nic = size(partition,2);
        % Renumbering
        for i=1:nic
            partition(partition(:,i)~=0,i) = i ;
        end
        
    otherwise
        error('unknown partitioning objective')
end

% --------------------------------------------------------------------
%   Post-processing
% --------------------------------------------------------------------

switch objective
    case 'no_overlap'
        j_               =	unique(partition )  ;
        j_               =	j_(~isnan(j_))      ;
        J               =	length(j_)          ;
    otherwise
        J               =	size(partition,2)	;
        j_              =   (1:J)'              ;
end

% cell array of vertices, each for one type of vertex:
vrtx_interp         =	cell(J,1)       ;   % interpolated extent vertices
vrtx_sim_ext        =	cell(J,1)       ;   % simulated extent vertices
vrtx_sim_dir        =	cell(J,1)       ;   % simulated extent direction vertices
vrtx_par            =	cell(J,1)       ;   % kinetic parameter vertices

% add vertices for every subsystem:
for j=1:J

    switch objective
        case 'no_overlap'
            vrtx_target     =	find(partition==j) ;
        otherwise
            vrtx_target     =	find(partition(:,j))	;
    end

    Fp              =	F(:,vrtx_target)                        ;
    vrtx_source     =	find(any(Fp,2))                         ;
    
    vrtx_interp{j}	=	setdiff(vrtx_source,vrtx_target)        ;
    vrtx_par{j}     =	intersect(vrtx_target,R+rho_o+(1:T))    ;
    vrtx_sim_ext{j}	=	intersect(vrtx_target,1:R)              ;
    vrtx_sim_dir{j}	=	intersect(vrtx_target,R+(1:rho_o))      ;
    
end

% select names for the vertices:
for t=1:T
    vertexnames{R+rho_o+t} =  name_parameters{t};
end

% ------------------------------------------------------------------------
%	Output
% ------------------------------------------------------------------------

% Save results:
model.kinetic.graph.G               =	G               ;
model.kinetic.graph.vertexnames     =	vertexnames     ;
model.kinetic.graph.vrtx_interp     =	vrtx_interp     ;
model.kinetic.graph.vrtx_sim_ext	=	vrtx_sim_ext	;
model.kinetic.graph.vrtx_sim_dir	=	vrtx_sim_dir	;
model.kinetic.graph.vrtx_par        =	vrtx_par        ;
model.kinetic.partition             =	partition       ;
model.kinetic.graph.objective       =   objective       ;

% Command line output:
if verbose>=2
    substr = {'\','{','}'};
    disp(['	Number of subsystems: ' num2str(J)])
    
    for j=j_(:)'
        disp(['	Subsystem: ' num2str(j)])
        disp(['     Simulated extents:     ' StripString(sprintf('%s ',vertexnames{vrtx_sim_ext{j}}),substr)	]   )
        disp(['     Simulated directions:  ' StripString(sprintf('%s ',vertexnames{vrtx_sim_dir{j}}),substr)	]   )
        disp(['     Interpolation:         ' StripString(sprintf('%s ',vertexnames{vrtx_interp{j}}),substr)	]   )
        disp(['     Parameters:            ' StripString(sprintf('%s ',vertexnames{vrtx_par{j}}),substr)      ]   )
    end
end

