
% -------------------------------------------------------------------------
% EMI Toolbox - STEP2_Contracting_Plot.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-06-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

A=sprintf('This program generates the figure used in the manuscript to vizualize \n the results of the initial branch-and-bound algorithm iterations. \n This is executed fast.\n');
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

% =========================================================================
% LOAD RESULTS

pulse   =       4   ;
Folder  =	[   'Results\PULSE' num2str(pulse) '_CONTRACT_FINAL'	];
load([Folder '\CONTRACT_PULSE' num2str(pulse) ])

% =========================================================================
% PROCESSING

nSet        =	length(ListParamBounds) ;
ParamBounds =	nan(nSet,3,2)           ;
Params      =	nan(nSet,3)             ;
Iterations	=	nan(nSet,1)             ;

for Iter=1:nSet
    ParamBounds(Iter,:,:)	=	ListParamBounds{Iter}'                          ;
    if Iter>=2
        Params(Iter,:)      =	ListParamBest{Iter}'                            ;
        Iterations(Iter)	=	find(~isnan(ListProgress{Iter}(:,1)),1,'last')	;
    end
end
Repetitions	=	(1:nSet)-1              ;

% =========================================================================
% VIZUALIZATION

fighan = figure; FigPrep, AxisPrep, hold on
plot(Repetitions,squeeze(ParamBounds(:,3,2)),'k^-','MarkerSize',7)
plot(Repetitions,squeeze(ParamBounds(:,2,2)),'ko-','MarkerSize',11)
plot(Repetitions,squeeze(ParamBounds(:,1,2)),'ks-','MarkerSize',15)
%         plot(Repetitions,Params(:,3),'k^-','MarkerSize',7,'MarkerFaceColor','k')
%         plot(Repetitions,Params(:,2),'ko-','MarkerSize',11,'MarkerFaceColor','k')
%          plot(Repetitions,Params(:,1),'ks-','MarkerSize',15,'MarkerFaceColor','k')
plot(Repetitions,squeeze(ParamBounds(:,1,1)),'kv-','MarkerSize',15)
plot(Repetitions,squeeze(ParamBounds(:,2,1)),'kv-','MarkerSize',11)
plot(Repetitions,squeeze(ParamBounds(:,3,1)),'kv-','MarkerSize',7)
set(gca,'Xlim',[-.25 5.25],'Ylim',[-.5 10.5])
legend({'$\overline{\theta_3}$',...
    '$\overline{\theta_2}$',...
    '$\overline{\theta_1}$',...
    '$\underline{\theta_1}$',...
    '$\underline{\theta_2}$',...
    '$\underline{\theta_3}$',...
    },'Interpreter','latex')

drawnow
disp(sprintf(['All figures generated. \n']))

SaveMyFigure(fighan,'Figures\Fig7_Contract')

drawnow
disp(sprintf(['All figures saved. \n']))

return