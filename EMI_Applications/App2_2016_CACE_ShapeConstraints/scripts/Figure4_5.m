
% -------------------------------------------------------------------------
% EMI Toolbox - Figure4_5.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces Figures 4-5 in [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-09-04
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% This script plots Figure 4 and 5.

% ------------- FIGURE 4 ----------------

disp('===================== FIGURE 4 =====================')

fig4 = figure(4);

switch mode
    case 'fast'
        
        load('MAT_files\Output4.mat') 
        
    case 'simulateall'
        
        select_model4{1} = 'Monod';
        Knots4 = {0:26/(2^2):26, 0:26/(2^4):26};
        Output4 = SimulateAndFitDirect(select_model4,Knots4,SFP);
        
        save Output4 Output4 Knots4 select_model4
end


figure(4); hold on; box on
plot(Output4.true_rate_SS,Output4.true_rate_rate,'LineWidth',7,'Color',1/255*[215 187 192])
plot(Output4.true_rate_SS,Output4.Lib_models(1).rate,'k-','LineWidth',2)
plot(Output4.true_rate_SS,Output4.Lib_models(2).rate,'k:','LineWidth',2)
plot(Output4.true_rate_SS,Output4.SCS_models(1).rate,'--','LineWidth',2,'Color',1/255*[80 80 80])
plot(Output4.true_rate_SS,Output4.SCS_models(2).rate,'--','LineWidth',2,'Color',1/255*[255 102 0])

disp('Editing the figure.')

axis([0, 25, 0, 1.5])
legend({'true rate','Monod','Tessier','SCS1','SCS2'},'Location','SouthEast')
set(gca,'FontSize',20,'XTick',0:5:25,'YTick',0:.25:1.5)
xlabel('substrate concentration S [mg N/L]')
ylabel('growth rate r_g(S) [1/day]')
set(fig4,'name','Figure 4','numbertitle','off')

disp('---Finished---')


% ------------- FIGURE 5 ----------------

disp('===================== FIGURE 5 =====================')

fig5 = figure(5);
figure(5), hold on, box on

select_model5 = SFP.model_names(1:5) ; %{'Monod','Tessier','Tanh','M+T','Root'};

switch mode
    case 'fast'
        
        load('MAT_files\Output5.mat') 
        
    case 'simulateall'

    Knots5 = {0:26/(2^2):26, 0:26/(2^4):26};
    [Output5,RMSR] = SimulateAndFitDirect(select_model5,Knots5,SFP);
    
        save Output5 Output5 Knots5 RMSR
end

PlotRMSR(RMSR,select_model5,fig5)

set(fig5,'name','Figure 5','numbertitle','off')

disp('---Finished---')
