% -------------------------------------------------------------------------
% EMI Toolbox - s_Processes2019_alphaPinene.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces all figures related to the alpha-pinene example
% appearing in [1]. 
%
% REFERENCES
%   [1] Villez, K, Billeter, J, Bonvin, D (2019). Incremental Parameter
%       Estimation under Rank-Deficient Measurement Conditions. Processes,
%       7(2), 75.
%   [2] Villez, K (2018). Extent-based Model Identification under
%       Incomplete Observability Conditions. (TR-006-03-0). Technical
%       Report, Eawag, D�bendorf, Switzerland.  
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% ------------------------------------------------------------------------- 

clc
clear all
close all

addpath ('.\systemdef\')
addpath ('..\..\EMI_Tools\Part3_RankDeficient\')

% ========================================================================
%   SYSTEM DEFINITION AND INFORMATION
% ========================================================================

scenarioname	=	'alphaPinene'	;
s_alphaPinene_SystemDef

Y_(:,1) = [	100	88.35	76.4	65.1	50.4	37.5	25.9	14.0	4.5     ]   ;
Y_(:,2) = [ 0	7.3     15.6	23.1	32.9	42.7	49.1	57.4	63.1	]   ;
Y_(:,3) = [ 0	2.3     4.5     5.3     6.0     6.0     5.9     5.1     3.8     ]   ;
Y_(:,4) = [ 0	0.4     0.7     1.1     1.5     1.9     2.2     2.6     2.9     ]   ;
Y_(:,5) = [ 0	1.75	2.8     5.8     9.3     12.0	17.0	21.0	25.7	]   ;
Y_      =   Y_'	;

% ========================================================================
%   SIMULATE EXPERIMENT
% ========================================================================
[n_t,ctil_t]	=	SimulationExperiment (N_,M_eps,M,S,V,select_h,n_0,ratefun,t_,theta_) ;

% ========================================================================
%   TUNING OF PARAMETER ESTIMATION AND GRAPHICAL OUTPUT
% ========================================================================

model           =	ModelSetup (V,n_0,M_eps,N_,name_species,t_,select_h,ratecell,T,name_parameters);

model.process	=	'alphapinene'	;

% ========================================================================
% ========================================================================

displaystyle                    =	'grey'                      ;
[options_compute,options_graph] =	OptionsEMI (displaystyle,unit_concentration,unit_extents)   ;

% Hard-coded options for graph vizualisation:
options_graph.extentscale       =   [	80	80	4	80	]   ;
options_graph.markersize        =       11                  ;
options_graph.gridpos_conc      =   [	3.7     73      ]   ;
options_graph.gridpos_extent	=   [	0.1     51      ]   ;
xX      =	[   0       0       1       2       2       ]   ;
yX      =	[	1       0       0       0       1       ]   ;
xT      =	[	-.2     -.2     +1       +2.2   +2.2	]   ;
yT      =	[	+1.8	-.8     -.8     -.8     +1.8	]   ;
xCHI    =	[	-.6     +1      3	]   ;
yCHI	=	[	+.5     +.8     1	]	;

% ====================================================================
%   MEASUREMENT GENERATION
% ====================================================================
M_                          =   M_init      ;
model.static.M_             =	M_          ;
model.observations.ytil_h	= 	M_*Y_       ;

[fighan,figname] = PlotMeasurements (model,options_graph,scenarioname,[]);

% ====================================================================
%   INITIAL MODEL
% ====================================================================

model0	=	model	;
theta_0	=	theta_	;

% ====================================================================
%   SIMULTANEOUS MODEL IDENTIFICATION
% ====================================================================

model1	=	SimultaneousEstimation (model0,theta_0,options_compute)	;

% ====================================================================
%   INCREMENTAL MODEL IDENTIFICATION
% ====================================================================

disp(['------------------------------------------------------------' ])
disp('	PART II: INCREMENTAL MODEL IDENTIFICATION')
disp(['------------------------------------------------------------' ])

% --------------------------------------------------------------------
%   LABEL EXTENTS AND EXTENT DIRECTIONS + COMPUTE THEIR VALUES
% --------------------------------------------------------------------

% --- Analysis
[xbarhat_o,~,model]	=   Extents (model,options_compute,[])	;

% --- Produce figure
[fighan,figname]	=       PlotComputedExtents (model,options_graph,scenarioname,[]);
figpath             =	[	'.\figures\'	figname	]   ;
savefig (fighan,figpath);
saveas (fighan,figpath,'epsc')

% --------------------------------------------------------------------
%   SYSTEM PARTITIONING
% --------------------------------------------------------------------

% --- Analysis
model               =	Partitioning (model,options_compute)            ;

% --- Produce figure 1
Xdata               =	[ xX xCHI(end-model.static.rho_o+1:end)	xT	]   ;
Ydata               =	[ yX yCHI(end-model.static.rho_o+1:end)	yT	]   ;
options_graph.Xdata =   Xdata                                           ;
options_graph.Ydata =   Ydata                                           ;

options_graph.showmeasurementarcs	=	false                           ;
[fighan,figname]    =	PlotGraph (model,options_graph,scenarioname)    ;
figpath             =	[	'.\figures\'	figname{1}	'_without'	]       ;
savefig (fighan,figpath)    ;
saveas (fighan,figpath,'epsc')

% --- Produce figure 2
options_graph.showmeasurementarcs	=	true                            ;
options_graph.inset                 = [ .3	1.1	1.4	1.4*0.75	]       ;
[fighan,figname]	=	PlotGraph (model,options_graph,scenarioname)    ;
figpath             =	[	'.\figures\'	figname{1}	'_with'	]           ;
savefig (fighan,figpath)                                                ;
saveas (fighan,figpath,'epsc')

% --------------------------------------------------------------------
%   PARAMETER ESTIMATION FOR SUBSYSTEMS
% --------------------------------------------------------------------

% --- Estimation
model1j     =	IncrementalEstimation (model,options_compute,theta_0)	;

% --- Compute for figure
t_          =   model.experiment.t_                 ;
select_h	=   model.experiment.select_h           ;
t_h         =   t_(select_h)                        ;
tsim_opt    =   t_(and(t_<=max(t_h),t_>=min(t_h)))  ;

xbar_o                  =	[   ]                                       ;
for j=1:length(model1j.part)
    xbar_o              =	[	xbar_o ; model1j.part{j}.xhat_fin	]   ;
end
simulationresult.xbar_o	=	xbar_o                                      ;

% --- Produce figure
[fighan,figname]	=       PlotComputedExtents (model1j,options_graph,scenarioname,simulationresult);
figpath             =	[	'.\figures\'	figname	]   ;

% --------------------------------------------------------------------
%   PARAMETER ESTIMATION FOR WHOLE SYSTEM
% --------------------------------------------------------------------

% --- Estimation
theta1_hat                  =	model1j.kinetic.theta_hat                                   ;
model10                     =	SimultaneousEstimation (model1j,theta1_hat,options_compute)	;

simulationresult.y_         =   M_*model10.SIM.nhat_fin/V                                   ;

[xbarhat_o,xbar_o,model10]	=   Extents (model10,options_compute,simulationresult)          ;
simulationresult.xbar_o     =   xbar_o                                                      ;

% --- Produce figure 1
[fighan,figname]	=	PlotModelFit ([],model10,options_graph,scenarioname,[]) ;
figpath             =	[	'.\figures\'	figname	]                           ;
savefig (fighan,figpath)                                                        ;
saveas (fighan,figpath,'epsc')

% --- Produce figure 2
[fighan,figname]	=       PlotComputedExtents (model10,options_graph,scenarioname,simulationresult)   ;
figpath             =	[	'.\figures\'	figname	]                           ;
savefig (fighan,figpath)                                                        ;
saveas (fighan,figpath,'epsc')





