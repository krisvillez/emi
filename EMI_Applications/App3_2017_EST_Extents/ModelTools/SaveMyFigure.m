function SaveMyFigure(handle,FileName)

% -------------------------------------------------------------------------
% EMI toolbox - SaveMyFigure.m
%
% This function saves a figure in multiple format at once.
%
% Syntax: 	SaveMyFigure(handle,FileName)
%
%	Inputs: 	
%       handle      Figure handle
%       FileName    Filename for the figure without extension
%
%	Outputs: 	
%               N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

pos = get(handle,'Position');
figuresize( pos(3) , pos(4)  )
set(gcf,'renderer','opengl')
saveas(handle,[FileName '.pdf'])
saveas(handle,[FileName '.fig'])
saveas(handle,[FileName '.tiff'])