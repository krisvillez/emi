function TrendModels	=   ModelId_Step1_SCS(Cmeas,t)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Step1_SCS.m
% -------------------------------------------------------------------------
% Description
%
% Fits shape constrained spline functions to the concentration measurement
% profiles.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	TrendModels	=   ModelId_Step1_SCS(Cmeas,t)
%
% INPUT
%   Cmeas       :   Measurement profiles
%   t           :   Sampling times
%
% OUTPUT
%   TrendModels	:   Cell array of fitted shape constrained spline functions
%
% -------------------------------------------------------------------------
% Last modification: 2016-12-21
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

for j=1:2
    
    switch j
        case 1
            y           =	(Cmeas(1,:)+(25-Cmeas(2,:)))/2          ;
            CaseTitle	=   'Substrate profile'                     ;
            String      =	'DA'                                    ;
        case 2
            y           =	Cmeas(3,:)                              ;
            CaseTitle	=   'Biomass profile'                       ;
            String      =	'BNA'                                   ;
    end

    Model               =	CreateSCS(y(:),t(:),t(1:1:end),3,[])	;
    Model               =   ApplyQR(Model,String,[],[])             ;
    Model.tool          =	'matlab'                                ;
    
    Model.positive      =   true ;

    nk                  =   length(Model.tk)                        ;
    nt                  =   size(Model.QR,1)-1                      ;
    IntX                =   Model.tk([1 nk]'*ones(1,nt))            ;
    DistX               =   []                                      ;
    
    options             =   OptimsetBB(Model)                       ;
    options.node        =   4                                       ;
    options.graph       =   false                                   ;
    options.graphprogress	=	false ;
    options.graphinterval	=	false ;
    options.verbose     =   0                                       ;
    options.resolution  =   Model.tk(2)-Model.tk(1)                 ;
    options.verify      =   false                                   ;
    
    [oo,Model]          =   BB(Model,options,IntX,DistX)            ;
    
    TrendModels{j}      =	Model                                   ;
    
    Inflections         =	Model.SolX                              ;
    Extrema             =	[   ]                                   ;
    
    if j==2
        tsim            =	Model.SolX(1):.01:Model.SolX(2)         ;
        dy1             =	EvalSCS(Model,tsim,1)                   ;
        index           =	find(dy1<0,1,'first')                   ;
        Extrema         =	tsim(index)                             ;
    end
        
%     figure(100), 
%         subplot(2,1,j), hold on
%             PlotSCS(Model) ;
%             title([CaseTitle ' - Shape: ' String])
%             PlotVertical(Inflections,'k--')
%             PlotVertical(Extrema,'k-')
%             xlabel(['Time [h]'])
%             ylabel(['Concentration [mg/L]'])
% drawnow

end


end

