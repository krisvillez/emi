function  [static,projection] = ExtentsDefinition (static,V,options) 

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentsDefinition.m
% -------------------------------------------------------------------------
% Description
%
%   This function executes the labeling step of the extent-based method for
%   incremental parameter estimation and computes the projection and
%   covariance matrices associated with the observable extents and the
%   observable directions among the ambiguous extents.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[static,projection] = ExtentsDefinition (static,V,options) 
%
% INPUT
%   static      :	structure describing the invariant relationships in the
%                   model, namely the stoichiometric matrix (static.N_) and
%                   the measurement gain matrix (static.M_).
%   V           :   volume of reactor
%   options     :   tuning options
%	 options.extent.factorization :	factorization method ('rref' or 'svd')
%	 options.verbose : control command line output level (integer)
%   
% OUTPUT
%	static      :   structure describing what is known about the model and
%                   the experiment based on invariants alone (i.e.,
%                   stoichiometric balances) now including all results from
%                   the labelling procedure. This includes:
%                   - static.label_obs :    boolean vector indicating the
%                                           observable extents
%                   - static.label_sensed : boolean vector indicating the
%                                           sensed extents 
%	projection	:   projection matrix (projection.Lbar_o) and covariance
%                   matrix (projection.Sigmabar_o)
%   
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% ------------------------------------------------------------------------
% Labeling
% ------------------------------------------------------------------------

if options.verbose, disp('EXTENTS - LABELING '), end
static          =	ExtentsLabeling (static,options)	;


% ------------------------------------------------------------------------
% Compute matrices necessary to compute extents and extent directions and
% concentration reconstruction from extents and extent directions:
% ------------------------------------------------------------------------

if ~( isa(static.M_,'sym') || isa(static.G_,'sym') )
    if options.verbose, disp('EXTENTS - PROJECTION MATRICES'), end
    projection	=	ExtentsDirections (static,V)        ;
else
    projection	=	[ ]                                 ;
end
