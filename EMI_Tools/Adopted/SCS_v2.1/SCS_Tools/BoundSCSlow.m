function LB = BoundSCSlow(Model,Interval,DistBnd,Graph,Verify) 

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - BoundSCSlow.m
% -------------------------------------------------------------------------
% Description 
%
% BoundSCSlow is a function which evaluates lower bounds for the
% objective function for shape constrained spline fitting. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	LB = BoundSCSlow(Model,Interval,DistBnd,Graph,Verify)
%
% INPUT
%	Model:     	Structure describing the spline model. 
%               (required) 
%   Interval:   Matrix describing the bounds on the transitions. It must be
%               specified as 2-column matrix with each row corresponding to
%               one transition. The left (right) column specifies the lower
%               (upper) bounds for the transitions.
%               (required) 
%   DistBnd:    Matrix describing the bounds on the distances between
%               transitions. It must be specified as 2-column matrix with
%               each row corresponding to one transition. The left (right)
%               column specifies the lower (upper) bounds for the distances
%               between subsequent transitions. An empty entry is
%               interpreted as a monotonicity constraint (transitions are
%               ordered along the data series).
%               (optional, default left bound = 0, default right bound =
%               +inf)   
%   Graph       Boolean indicating whether graphs should be produced to
%               vizualize progress of the optimization algorithm and the
%               obtained fit.
%               (optional, default=false)
%   Verify      Boolean indicating whether the capacity to compute the
%               transitions to absolute precision should be exploited when
%               available.
%               (optional, default=false)
%   
% OUTPUT
%   LB:         Lower bound for the objective function. If the problem is
%               infeasible, its value is +inf.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

[LB] = BoundSCS(Model,Interval,DistBnd,Graph,Verify) ;