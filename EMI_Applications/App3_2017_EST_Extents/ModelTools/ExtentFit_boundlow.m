function [QL,Eplus,Emin] = ExtentFit_boundlow(Model,IntX,~,Graph,Omega,xsimL,xsimU)

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentFit_boundlow.m
% -------------------------------------------------------------------------
% Description
%
% Compute lower bound for the lack-of-fit objective function formulated in
% extents. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[QL,Eplus,Emin] = ExtentFit_boundlow(Model,IntX,~,Graph,Omega,xsimL,xsimU)
%
% INPUT
%   Model	:   Model description given as a structure (see ExtentDAE.m
%               for required structure)
%   IntX    :   Considered set relative to root set
%   []      :   Placeholder entry
%   Graph   :   Boolean controlling graphical output
%   Omega   :   Parameter vector set, original scale
%   xsimL   :   Extent lower bound
%   xsimU   :   Extent upper bound
%
% OUTPUT
%   QL  	:   Lower bound
%   Eplus   :   Upper side bounded residuals
%   Emin    :   lower side bounded residuals
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<4 || isempty(Graph)
    Graph = false ;
end



% =========================================================================
% 1. Extent Simulation

if nargin<5 || isempty(Omega)
    Omega	=	ones(2,1)*Model.Intercept	+	IntX*diag(Model.Slope)	;
end
% ntheta = size(Omega,2);

   
 if nargin<6 
    [   xL,   xU 	]	=	SimExtent_bound(Model,Omega)	;
 else
    if isfield(Model,'data')
        index = Model.Measurement.sample_index ;
        xL	=	xsimL(:,index)     ;
        xU	=	xsimU(:,index)     ;
    end
 end
 
% =========================================================================
% 2. Lower bound computation
 
if isfield(Model,'data')
    
    % 2.1. Interval regression - without shape constraints, this
    % problem is only 1-dimensional
    xLn = xL./Model.data.lambda;
    xUn = xU./Model.data.lambda ;
    xhatn  = Model.data.xtilde./Model.data.lambda ;
    
    Eplus	=	max(xhatn-xUn,0)    ;
    Emin	=	min(xhatn-xLn,0)    ;
    
    QL	=   sum(Eplus.^2+Emin.^2 );
    QL            =   QL/Model.data.H	;
    
%     % 2.3. Display
%     if Graph>=3
%         figure(137),
%             
%             if exist('FigPrep'), FigPrep, end
%             if exist('AxisPrep'), AxisPrep, end 
%             hold on,
%             plot(   Model.Tsim, XsimL,  'b-'    )
%             plot(   Model.Tsim, XsimU,  'r-'    )
%             plot(   Model.Tsim(Model.data.index),   X1_L,       'bo'    )
%             plot(   Model.Tsim(Model.data.index),   X1_U,       'ro'    )
%             plot(   Model.Tsim(Model.data.index),   Model.data.mu,           'k.'    )
%             
%         figure(173), hold on,
%             plot(   Model.Tsim(Model.data.index),   Emin,           'b.'    )
%             plot(   Model.Tsim(Model.data.index),   Eplus,           'r.'    )
%             
%     end
    
else
    
    QL	=	0	;
    
end


end