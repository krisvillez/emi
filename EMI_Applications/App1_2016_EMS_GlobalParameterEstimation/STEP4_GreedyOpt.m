
% -------------------------------------------------------------------------
% EMI Toolbox - STEP4_GreedyOpt.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-12-21
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

% A=sprintf('This program generates the figures used in the manuscript to vizualize \n the results of the final branch-and-bound algorithm execution. \n Especially the last figure takes considerable time. \n');
% disp(A)
% disp(sprintf('Press any key to continue\n'))
%pause

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

Indices
    
% =========================================================================
% 0. LOAD PROBLEM SETUP

pulse   =   4   ;
load(['Results\SETUP_PULSE' num2str(pulse)])

    
% =========================================================================
% 1. PREPARE GREEDY SEARCH

ParamBounds	=	IntX*diag(Model.Slope)+ones(2,1)*Model.Intercept	;

RangeX      =	IntX(2,:) - IntX(1,:)   ;
nDim        =   size(IntX,2)            ;
nGrid       =	30                      ;
nPoint      = 	nGrid.^nDim             ;

g1          =	nan(nPoint,1)           ;
g2          =	nan(nPoint,1)           ;
ExitFlag	=	nan(nPoint,1)           ;
Count       =	nan(nPoint,1)           ;

Options             =	optimoptions(@fminunc,...
                            'Display','off',...
                            'Algorithm','quasi-newton'  )   ;
Options.MaxFunEvals =	inf             ;
Options.MaxIter     =	inf             ;

ExecuteOptimization	=	true            ;

warning('off','MATLAB:singularMatrix')

for iPoint=1:nPoint
    
    clc
    disp([iPoint nPoint])
    % -------------------------------------------------------------------------
    % get parameter values
    
    GridPos     =	dec2base(iPoint-1,nGrid,nDim)                           ;
    GridPos     =	base2dec(GridPos(:),nGrid)'+1                           ;
    SolX        =   IntX(1,:)-1/(nGrid*2)*RangeX + GridPos/(nGrid).*RangeX	;
    Param       =	SolX.*Model.Slope+Model.Intercept                       ;
    
    % -------------------------------------------------------------------------
    % Evaluate objective function for initial parameter vector
    g1(iPoint)	=	BatchSim_BoundUpp(Model,[],[],false,[],Param)           ;
    
    % -------------------------------------------------------------------------
    % Optimize with Quasi-Newton
    if ExecuteOptimization
        
        [Param,g,exitflag,output]	=   ...
            fminunc(    @(p) BatchSim_BoundUpp(Model,[],[],false,[],p),...
                        Param,...
                        Options) ;
                    
        g2(iPoint)          =	g                   ;
        ExitFlag(iPoint)    =	exitflag            ;
        Count(iPoint)       =	output.funcCount	;
        
    else
        g2(iPoint)          =	g1(iPoint)          ;
    end
    
    
end

save('.\Results\LOCAL')

return