function [g_L,g_U,BestX,Model] =  BatchSim_Bound(Model,IntX,~,Graph,Xo)

% -------------------------------------------------------------------------
% EMI Toolbox - BatchSim_Bound.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates bounds to the objective function.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[g_L,g_U,BestX,Model] =  BatchSim_Bound(Model,IntX,~,Graph,Xo)
%
% INPUT
%   Model   :   Optimization problem as a structure
%   IntX  	:   Parameter vector set (normalized scale)
%   Graph   :   [optional] Boolean indicating whether graph should be made,
%					default = false
%	Xo 		:	[optional] Proposed solution for the upper bound 
%					(normalized scale), default: mean of IntX
%
% OUTPUT
%   g_L     :   Lower bound to the objective function value
%   g_U     :   Upper bound to the objective function value
%   BestX	:	Parameter vector for upper bound (normalized scale)
%	Model	:	Updated model structure, corresponding to upper bound
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

oo =false;

if nargin<4 || isempty(Graph)
    Graph = false ;
end
if nargin<5 || or(isempty(Xo),length(Xo)~=size(IntX,2))
    Xo = mean(IntX)'; 
end

if Graph
    figure(23)
    close 23
    figure(42)
    close 42
end

Params      =	ones(2,1)*Model.Intercept	+	IntX*diag(Model.Slope)	;
% Params


% =======================================
% 1. Extremal simulations
[oo,oo,oo,oo,XsimL,XsimU]  =   ExtremalState(Model,Params,Graph) ;

% =======================================
% 2. Lower bound

%disp('Low')
g_L     =	BatchSim_BoundLow(Model,IntX,[],Graph,Params,XsimL,XsimU)   ;
%disp('Low -done')
%[g_L,Alpha_L,Beta_L,g1_L,g2_L]              =	BatchSim_BoundLow(Model,IntX,[],Graph,Params,XsimL,XsimU);

% =======================================
% 3. Upper bound
g_U1	=   BatchSim_BoundUpp(Model,IntX,[],Graph,[],Params(1,:),XsimL)	;
g_U2	=   BatchSim_BoundUpp(Model,IntX,[],Graph,[],Params(2,:),XsimU)	;
%[g_U1,XM,Xsim,Alpha_U,Beta_U,g1_U1,g2_U1]	=   BatchSim_BoundUpp(Model,IntX,[],Graph,[],Params(1,:),XsimL)     ;
%[g_U2,XM,Xsim,Alpha_U,Beta_U,g1_U2,g2_U2]   =   BatchSim_BoundUpp(Model,IntX,[],Graph,[],Params(2,:),XsimU)     ;

[g_U,loc]	=   min([g_U1 g_U2])    ;
BestX       =	IntX(loc,:)         ;
Model.SolX  =   BestX               ;


if g_L>g_U
    [g_L g_U] 
    IntX
    [g_L,Alpha_L,Beta_L,g1_L,g2_L]        =   BatchSim_BoundLow(Model,IntX,[],true,Params,XsimL,XsimU);
    [g_U1]      =   BatchSim_BoundUpp(Model,IntX,[],true,[],Params(1,:),XsimL)     ;
    [g_U2]      =   BatchSim_BoundUpp(Model,IntX,[],true,[],Params(2,:),XsimU)     ;
    g_L
    g_U1
    g_U2
    Alpha_L,Beta_L,g1_L,g2_L
    error('lala')
end
