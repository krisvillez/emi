
% -------------------------------------------------------------------------
% EMI Toolbox - MainScript.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces all figures that appear in [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-12-21
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% ------------------------------------------------------------------------- 

clear all %#ok<CLSCR>


% Choose whether to use previously prepared MAT files ('fast') or to
% simulate everything new ('simulateall')
mode = 'fast';
%mode = 'simulateall' ;

addpath('.\scripts\')
addpath(genpath('..\..\EMI_Tools'))

clc
close all
drawnow

InitializeParameters	;


% =========== FIGURE 1 ================
% Figure: 1.
% All rates
% -------------------------------------
Figure1
drawnow
% =====================================


% ========== FIGURE 2-3 ===============
% Figures: 2, 3a, 3b.
% Basis functions, different SCS models.
% -------------------------------------
Figure2_3
drawnow
% =====================================


% ========== FIGURE 4-5 ===============
% Figures: 4, 5.
% Direct fit Monod rate law, summary RMSR.
% -------------------------------------
Figure4_5
drawnow
% =====================================


% ========== FIGURE 6-8 ===============
% Figures: 6a, 6b, 6c, 6d, 7, 8a, 8b.
% Indirect fit M+T, Steele, summary WRMSR, Steele with SCS3.
% -------------------------------------
Figure6_8
drawnow
% =====================================


% ========== FIGURE 9 ===============
% Figure: 9.
% Validation test with Monod and SCS2.
% -------------------------------------
Figure9_Extrapolation
drawnow
% =====================================


% =========== GRAPHICAL ===============
% Figures: graphical abstract.
% -------------------------------------
FigureGraphical
drawnow
% =====================================

return