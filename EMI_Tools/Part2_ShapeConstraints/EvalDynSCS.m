function ysim = EvalDynSCS(Model,ss,drv)

% -------------------------------------------------------------------------
% EMI Toolbox - EvalDynSCS.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the value or derivative of a shape constrained function
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	ysim = EvalDynSCS(Model,ss,drv)
%
% INPUT
%   Model   :   Shape constrained spline function
%   ss      :   Substrate concentrations
%   drv     :   Derivative degree (0 for function value)
%
% OUTPUT
%   ysim	:   Shape constrained spline function value or derivative
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-05
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

B       =   eval_basis(ss,Model.basis,drv)  ;
ysim	=	B*Model.x                       ;