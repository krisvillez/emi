function [Q2_L,Q2_U] = ExtremalRate(Model,Theta,XsimL,XsimU,Graph)

% -------------------------------------------------------------------------
% EMI Toolbox - ExtremalRate.m
% -------------------------------------------------------------------------
% Description
%
% Produces bounds to the relative reaction rate.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Q2_L,Q2_U] = ExtremalRate(Model,Theta,XsimL,XsimU,Graph)
%
% INPUT
%   Model   :   Optimization problem as a structure
%   Theta   :   Parameter vector set
%   XsimL   :   Lower bounds to process state
%   XsimU   :   Upper bounds to process state
%   Graph   :   Boolean indicating whether graph should be made
%
% OUTPUT
%   Q2_L    :   Lower bounds
%   Q2_U    :   Upper bounds
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

    
    Cpeak = sqrt(Theta(:,1)./(Theta(:,3)));
    
    fun=Model.fun;
    
    S_L = 1-XsimL ;
    S_U = 1-XsimU ;
    S2_L= S_L(Model.I2);
    S2_U= S_U(Model.I2);
    
    HI = 1 ;
    LO = 2 ;
    
    Tsim = Model.Tsim;
    
    if Graph>=2
        
        TextY = 0.4 ;
        TextProp = {'FontSize',14,'VerticalAlignment','bottom','HorizontalAlignment','left'} ;
        Grey = 0.73*ones(1,3); %0*[0 .6 .3];
        Col = 0*ones(1,3); %0*[0 .6 .3];
        
        Times   =   [ 2+37/60 1+3/4 1];
        
        figure(42), 
        
    if exist('FigPrep'), FigPrep, end
    if exist('AxisPrep'), AxisPrep, end
    
    set(gca,'FontSize',23)
            hold on
            cc  =   0:.01:max([S_L(:); S_U(:) ]);
            plot(cc,fun(cc,Theta(HI,:)),'r-','LineWidth',4)
            plot(cc,fun(cc,Theta(LO,:)),'b-','LineWidth',2)
            
%             loc = find(Tsim>=Times(1),1,'first');
%             Int1 = [S_L(loc) S_U(loc)];
%             PlotVertical(Int1(1),'b--')
%             PlotVertical(Int1(2),'r--')
%             F1=fun(Int1(1),Theta(LO,:));
%             F2 =fun(Int1(2),Theta(HI,:));
%             plot(Int1(1),F1,'mx','LineWidth',2,'Color',Col)
%             plot(Int1(2),F2,'m+','LineWidth',2,'Color',Col)
%             rectangle('Position',[Int1(1) F1 Int1(2)-Int1(1) F2-F1 ],'EdgeColor',Col)
%             than=text(Int1(1),F2,' 2h30'' ',TextProp{:});
            
            loc = find(Tsim>=Times(1),1,'first');
            Int1 = [S_L(loc) S_U(loc)];
            if exist('PlotVertical'), PlotVertical(Int1(2),'r--','LineWidth',2), end
            if exist('PlotVertical'), PlotVertical(Int1(1),'b--','LineWidth',1), end
            
            F1=fun(Cpeak(HI),Theta(HI,:));
            F2 =fun(Int1(1),Theta(LO,:));
            
            plot(Cpeak(HI),F1,'o','MarkerSize',11,'LineWidth',3,'Color',Col)
            plot(-Int1(1),F2,'m+','MarkerSize',11,'LineWidth',3,'Color',Col)
            plot(Int1(1),F2,'mx','MarkerSize',11,'LineWidth',3,'Color',Col)
            
            rectangle('Position',[Int1(1) F2 Int1(2)-Int1(1) F1-F2 ],'EdgeColor',Grey,'LineWidth',2,'LineStyle','-')
            
            if exist('PlotVertical'), PlotVertical(Int1(2),'r--','LineWidth',2), end
            if exist('PlotVertical'), PlotVertical(Int1(1),'b--','LineWidth',1), end
            plot(Cpeak(HI),F1,'o','MarkerSize',11,'LineWidth',3,'Color',Col)
            plot(-Int1(1),F2,'m+','MarkerSize',11,'LineWidth',3,'Color',Col)
            plot(Int1(1),F2,'mx','MarkerSize',11,'LineWidth',3,'Color',Col)
            
            than=text(Int1(1),F1,' 2h37'' ',TextProp{:});
            
            loc = find(Tsim>=Times(2),1,'first');
            Int1 = [S_L(loc) S_U(loc)];
            F1=fun(Int1(1),Theta(HI,:));
            F2 =fun(Int1(1),Theta(LO,:));
            rectangle('Position',[Int1(1) F2 Int1(2)-Int1(1) F1-F2 ],'EdgeColor',Grey,'LineWidth',2,'LineStyle','-')
            if exist('PlotVertical'), PlotVertical(Int1(2),'r--','LineWidth',2), end
            if exist('PlotVertical'), PlotVertical(Int1(1),'b--','LineWidth',1), end
            plot(Int1(1),F2,'mx','MarkerSize',11,'LineWidth',3,'Color',Col)
            plot(Int1(1),F1,'m+','MarkerSize',11,'LineWidth',3,'Color',Col)
            than=text(Int1(1),F1,' 1h45'' ',TextProp{:});
            
            
            
            loc = find(Tsim==Times(3));
            Int1 = [S_L(loc) S_U(loc)];
            F1=fun(Int1(1),Theta(HI,:));
            F2 =fun(Int1(2),Theta(LO,:));
            rectangle('Position',[Int1(1) F2 Int1(2)-Int1(1) F1-F2 ],'EdgeColor',Grey,'LineWidth',2,'LineStyle','-')
            if exist('PlotVertical'), PlotVertical(Int1(2),'r--','LineWidth',2), end
            if exist('PlotVertical'), PlotVertical(Int1(1),'b--','LineWidth',1), end
            plot(Int1(2),F2,'x','MarkerSize',11,'LineWidth',3,'Color',Col)
            plot(Int1(1),F1,'+','MarkerSize',11,'LineWidth',3,'Color',Col)
            than=text(mean(Int1),F1,' 1h00'' ',TextProp{1:4},'HorizontalAlignment','center');
            
            set(gca,'Xlim',[min(cc) max(cc)])
            
            legend({'$\overline{q}(s,\mathbf{\theta})$',...
                    '$\underline{q}(s,\mathbf{\theta})$',...
                    '$\overline{s}(t)$',...
                    '$\underline{s}(t)$',...
                    '$\overline{q}(t)$',...
                    '$\overline{q}(t)$',...
                    '$\underline{q}(t)$'},...
                    'Interpreter','latex','Location','SouthEast')
            xlabel('Relative concentration ($s$)','Interpreter','latex')
            ylabel('Relative reaction rate ($q$) [1/h]','Interpreter','latex')
            
    end
    
    
    if Graph
        Col = [0 .6 .3];
        grey = .42*ones(1,3);
        
        figure(73), 
            
            if exist('FigPrep'), FigPrep, end
        subplot(3,1,1), 
        if exist('AxisPrep'), AxisPrep, end 
        
    set(gca,'FontSize',23)
        hold on,
         
        [oo,choice] 	=	min([	fun(S_L,Theta(LO,:)') ...
                                fun(S_U,Theta(LO,:)')],[],2)	;
    
                            
        CsimS = (choice==1).*S_L + (choice==2).*S_U  ;
        plot(Model.Tsim,CsimS,'k-','LineWidth',3,'Color','k')
        if exist('PlotHorizontal'), PlotHorizontal(Cpeak(LO),'k-'), end
        plot(Model.Tsim,CsimS,'k-','LineWidth',3,'Color','k')
        plot(Model.Tsim,S_L,'b--','LineWidth',3)
        plot(Model.Tsim,S_U,'r:','LineWidth',3)
        
        
        subplot(3,1,3), 
        if exist('AxisPrep'), AxisPrep, end 
    set(gca,'FontSize',23)
        hold on,
            plot(Model.Tsim,fun(CsimS,Theta(LO,:)),'k-','LineWidth',3,'Color','k')
        
        subplot(3,1,2), 
        if exist('AxisPrep'), AxisPrep, end 
    set(gca,'FontSize',23)
        
        hold on,
        CsimS =Cpeak(HI) + (S_L>=Cpeak(HI)).*(S_L-Cpeak(HI)) + (S_U<=Cpeak(HI)).*(S_U-Cpeak(HI))  ;
        
        plot(Model.Tsim,CsimS,'k-','LineWidth',3,'Color','k')
        if exist('PlotHorizontal'), PlotHorizontal(Cpeak(HI),'k-'), end
        plot(Model.Tsim,CsimS,'k-','LineWidth',3,'Color','k')
        plot(Model.Tsim,S_L,'b--','LineWidth',3)
        plot(Model.Tsim,S_U,'r:','LineWidth',3)
        
        subplot(3,1,3), hold on,
    set(gca,'FontSize',23)
            plot(Model.Tsim,fun(CsimS,Theta(HI,:)),'k-','LineWidth',3,'Color','k')
            set(gca,'Ytick',[0:.1:.5])
    end
    
    
    Q2_L            =   nan(size(S2_L))     ;
    Q2_U            =   nan(size(S2_U))     ;
    
    
    % 3.1. Lower bound to the rate function
    loc 			=	find(S2_L>=Cpeak(LO)) 					;
    Q2_L(loc)       =	fun(S2_U(loc),Theta(LO,:)') ;
%     if Graph,	
%         subplot(3,1,1), hold on,
%             plot(Model.T2(loc),S2_U(loc),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%         subplot(3,1,3), hold on,
%             plot(Model.T2(loc),Q2_L(loc),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%     end
    
    loc 			=	find(S2_U<=Cpeak(LO)) 					;
    Q2_L(loc) 	=	fun(S2_L(loc),Theta(LO,:)') ;
%     if Graph,	
%         subplot(3,1,1), hold on,
%             plot(Model.T2(loc),S2_L(loc),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%             %plot(Model.T2(loc),S2_U(loc),'bx'), 
%         subplot(3,1,3), hold on,
%             plot(Model.T2(loc),Q2_L(loc),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%     end
    
    loc 			=	find(and(S2_L<Cpeak(LO),S2_U>Cpeak(LO)) ) ;
    
    [rate,choice] 	=	min([	fun(S2_L(loc),Theta(LO,:)') ...
                                fun(S2_U(loc),Theta(LO,:)')],[],2)	;
                            
    Q2_L(loc) 	=	rate ; 
    
%     if Graph,	
%         subplot(3,1,1), hold on,
%             plot(Model.T2(loc(choice==1)),S2_L(loc(choice==1)),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%             plot(Model.T2(loc(choice==2)),S2_U(loc(choice==2)),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%         subplot(3,1,3), hold on,
%             plot(Model.T2(loc),Q2_L(loc),'kx','MarkerSize',11,'LineWidth',2,'Color','k'), 
%     end
    
    % 3.2. Upper bound to the rate function
    
    loc 			=	find(S2_L>=Cpeak(HI));
    Q2_U(loc) 	=	fun(S2_L(loc),Theta(HI,:)') ;
%     if Graph,	
%         subplot(3,1,2), hold on,
%             plot(Model.T2(loc),S2_L(loc),'k+','MarkerSize',11,'LineWidth',2,'Color','k'), 
%         subplot(3,1,3), hold on,
%             plot(Model.T2(loc),Q2_U(loc),'k+','MarkerSize',11,'LineWidth',2,'Color','k'), 
%     end
    
    loc				=	find(S2_U<=Cpeak(HI));
    Q2_U(loc) 	=	fun(S2_U(loc),Theta(HI,:)') ;
%     if Graph,	
%         subplot(3,1,2), hold on,
%             plot(Model.T2(loc),S2_U(loc),'k+','MarkerSize',11,'LineWidth',2,'Color','k'), 
%         subplot(3,1,3), hold on,
%             plot(Model.T2(loc),Q2_U(loc),'k+','MarkerSize',11,'LineWidth',2,'Color','k'), 
%     end
    
    loc				=	find(and(S2_L<=Cpeak(HI),S2_U>=Cpeak(HI)) );
    Q2_U(loc) 	=	fun(Cpeak(HI),Theta(HI,:)') ;
%     if Graph & ~isempty(loc),	
%         subplot(3,1,2), hold on,
%             plot(Model.T2(loc),Cpeak(HI),'ko','MarkerSize',11,'LineWidth',2,'Color','k'), 
%         subplot(3,1,3), hold on,
%             plot(Model.T2(loc),Q2_U(loc),'ko','MarkerSize',11,'LineWidth',2,'Color','k'), 
%     end
    
    
    % Posterior figure processing
    if Graph
        figure(73)
        subplot(3,1,1),
        set(gca,'Ylim',[0 1],'Ytick',[0:.2:1])
        if Graph>=2,
            if exist('PlotVertical'),	PlotVertical(Times,'k--'),  end
        end
        ylabel('s')
        
        subplot(3,1,2),
        set(gca,'Ylim',[0 1],'Ytick',[0:.2:1])
        if Graph>=2,
            if exist('PlotVertical'),	PlotVertical(Times,'k--'),  end
        end
        ylabel('s')
        
        subplot(3,1,3),
        xlabel('Time [h]')
        ylabel('q')
        if Graph>=2,
            if exist('PlotVertical'),	PlotVertical(Times,'k--'),  end
        end
        
    end
    
return
