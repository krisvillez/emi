function string = StripString (string,substr)

% -------------------------------------------------------------------------
% EMI Toolbox - StripString.m
% -------------------------------------------------------------------------
% Description
%
%   This function removes all appearances of the strings in 'substr' from
%   'string'.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	string = StripString (string,substr)
%
% INPUT
%   string	:   string which is processed
%   substr  :   cell array of strings to be removed from the processed
%               string
%
% OUTPUT
%	string	:   string after processing
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Remove all strings in substr from string:
for s=1:length(substr)
    string	=	regexprep(string,substr{s},'');
end
        
end

