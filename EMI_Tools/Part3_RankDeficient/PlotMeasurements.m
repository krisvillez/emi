function [fighan,figname]  = PlotMeasurements (model,options,scenario_name,groundtruth)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotMeasurements.m
% -------------------------------------------------------------------------
% Description
%
%   Customized plotting function to visualize the measurement time series.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[fighan,figname]  = PlotMeasurements (model,options,scenario_name,groundtruth)
%
% INPUT
%   model           :   model of the experiment
%   options         :   plot fine-tuning options
%   scenario_name	:   string to use in title
%   groundtruth     :   [optional] reference data
%   
% OUTPUT
%	fighan          :   figure handle
%   figname         :   figure title
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


colours     =	options.colours                 ;
linewidth	=	options.linewidth               ;
linestyles	=	options.linestyles              ;
markers     =	options.markers                 ;

t_        =   model.experiment.t_           ;
select_h	=   model.experiment.select_h       ;
t_h       =   t_(select_h)                  ;

M_         =	model.static.M_                ;
N_           =   model.static.N_                 ;
V           =	model.experiment.V              ;
ytil_h        =	model.observations.ytil_h         ;

M           =	size(M_,1)                     ;
[R,S]       =	size(N_)                         ;

if ~isempty(groundtruth)
    n_t       =   groundtruth.n_t               ;
    c_t        =	M_*n_t/V                     ;
end

concnames	=	cell(M,1)                       ;
for m=1:M
    g_      =	M_(m,:)                        ;
    varname =	[ '' ]                          ;
    for s=1:S
        if g_(s)==1
            varname	= [ varname '+'	'{c}_{' model.static.name_species{s} '}' ];
        elseif g_(s)
            varname	= [ varname '+' num2str(g_(s)) '{c}_{' model.static.name_species{s} '}' ];
        end
    end
    concnames{m}	=	varname(2:end)	;
end

measnames	=	cellstr ([ repmat('\tilde{y}_',[M 1]) num2str((1:M)') repmat('=',[M 1]) char(concnames) repmat('+\epsilon_',[M 1]) num2str((1:M)')  ]);
if ~isempty(groundtruth)
    str         =   cellstr([repmat('$',[2*M 1]) char([concnames(:);  measnames(:)])  repmat('$',[2*M 1])]) ;
else
    str         =   cellstr([repmat('$',[M 1]) char([ measnames(:)])  repmat('$',[M 1])]) ;
end
nCol        =   2       ;

figname     = [ 'Scenario' scenario_name '_measurements'	]	;
fighan      =	figure ('name',figname)                         ;
if exist ('FigPrep','file'),	FigPrep,	end
if exist ('AxisPrep','file'),	AxisPrep,	end
hold on
A = 0 ;
if ~isempty(groundtruth)
    for m=1:M
        A=A+1;
        ph(A)	=   plot (t_,c_t(m,:),linestyles{m},'Color',colours(m,:),'linewidth',linewidth)  ;
    end
end
for m=1:M
    A=A+1;
    col     =	(2+colours(m,:))/3 ;
    ph(A)	=   plot (t_h,ytil_h(m,:),markers{m},'Color',colours(m,:),'Markerfacecolor',col,'linewidth',linewidth*1) ;
    if isfield(options,'markersize')
        set(ph(A),'MarkerSize',options.markersize);
    end
end
if ~isempty(groundtruth)
    for m=1:M
        plot (t_,c_t(m,:),linestyles{m},'Color',colours(m,:),'linewidth',linewidth)
    end
end
set (gca,'Ylim',[min(ytil_h(:))-0.02 max(ytil_h(:))+.02])
set (gca,'Xtick',t_(1):t_(end))
set (gca,'Xlim',[t_(1) t_(end)])
%set (gca,'Ytick',[0:.1:.7],'Ylim',[min(ytil(:)) max(ytil(:))]+[-1 +1]*.01)
xlabel (['Time [h]'])
ylabel (['Concentration [' options.unit_concentration ']'])

if isfield(options,'gridpos_conc')
    initX       =   options.gridpos_conc(1)	;
    initY       =   options.gridpos_conc(2)	;
else
    initX       =   get(gca,'Xlim') ;
    initX       =   initX(1)        ;
    initY       =   get(gca,'Ylim') ;
    initY       =   initY(1)        ;
end

if exist ('GridLegend','file'),  GridLegend (ph,str,nCol,initX,initY);
else, legend(ph,str(:)','Interpreter','latex')
end

drawnow()

end

