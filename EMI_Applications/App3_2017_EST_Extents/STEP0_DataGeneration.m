

% -------------------------------------------------------------------------
% EMI Toolbox - STEP0_DataGeneration.m
% -------------------------------------------------------------------------
% Description
%
% Simulates data-generating batch experiments.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script simulates noisy measurement by means of \n'...
            'a simplified urine nitrification process \n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% -------------------------------------------------------------------------
% Simulation setup

SetupSimulation % This script defines the complete data generating model

% -------------------------------------------------------------------------
% Setup algebraic equation solver - used for solving equilibrium 
global optfzero
optfzero            =	optimset('fzero')	;
optfzero.Display	=   'off'               ;
optfzero.TolX       =   1e-6                ;
optfzero.TolFun     =   1e-6                ;

AbsTol			=	1e-6	;
RelTol			= 	1e-6	;

% -------------------------------------------------------------------------
% Setup ODE solver - used for dynamics of component and kinetic species
% concentrations
global optionsode
optionsode = odeset('RelTol',RelTol,'AbsTol',AbsTol); 	

FILEPATH	=	[	FOLDER	'NumericSetup'  ]	;
save(FILEPATH,'optionsode','optfzero');

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Simulate extent dynamics
[ xsim , cbarsim , csim , pHsim ] = SimExtent( Model, []) ;

Model.simulation.xsim = xsim ;
Model.simulation.cbarsim = cbarsim ;
Model.simulation.csim  = csim ;
Model.simulation.pHsim  = pHsim ;

% -------------------------------------------------------------------------
% Simulate measurement noise
rng(73,'twister')  ; 
epsilon     =   diag(sigma)*randn(M,H) ;

% -------------------------------------------------------------------------
% Combine to generate measurements
ysim        =	Equation_Measurement(Model,csim)        ;
y           =	ysim(:,Model.Measurement.sample_index)  ;
ytilde      =   y+epsilon                               ;

Model.simulation.y      =	y       ;
Model.simulation.ysim	=	ysim    ;
Model.data.ytilde       =	ytilde	;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Save All Results
FILEPATH	=	[	FOLDER	'STEP0_All' ]	;
save(FILEPATH,'Model')

% -------------------------------------------------------------------------
% Remove Information not Available to Modeller:
Model = rmfield(Model,'simulation') ;
% Model = rmfield(Model,'Reactions') ;

% -------------------------------------------------------------------------
% Save Results Available to Modeller
FILEPATH	=	[	FOLDER	'STEP0_ExperimentalData'    ]	;
save(FILEPATH,'Model')

% =========================================================================
% =========================================================================
% =========================================================================

STEP0_DataGeneration_Display

% =========================================================================
% =========================================================================
% =========================================================================
