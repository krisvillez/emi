function [fighan,figname] = PlotComputedExtents (model,options,scenario_name,simulationresult)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotComputedExtents.m
% -------------------------------------------------------------------------
% Description
%
%   Customized plotting function to visualize the computed extents.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[fighan,figname] = PlotComputedExtents (model,options,scenario_name,simulationresult)
%
% INPUT
%   model               :   model as structure
%   options             :   plotting fine-tuning options
%   scenario_name       :   name for title of figure
%   simulationresult	:   [optional] reference trajectory
%   
% OUTPUT
%	fighan              :   figure handle
%   figname             :   figure title
%
% -------------------------------------------------------------------------
% Last modification: 2018-12-12
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


colours     =	options.colours                 ;
linewidth	=	options.linewidth               ;
linestyles	=	options.linestyles              ;
markers     =	options.markers                 ;

t_        =   model.experiment.t_           ;
select_h	=   model.experiment.select_h       ;
t_h       =   t_(select_h)                  ;

A           =	model.static.A                  ;
xbarhat_o	=   model.observations.xbarhat_o	;
if ~isempty(simulationresult)
    xbar_o      =	simulationresult.xbar_o              ;
end

varnames	=   model.static.varnames(model.static.index_bar_o)         ;
measnames	=	cellstr([ repmat('\tilde',[A 1]) char(varnames)  ])     ;
if ~isempty(simulationresult)
    str         =   cellstr([repmat('$',[2*A 1]) char([varnames(:); measnames(:)])  repmat('$',[2*A 1])]) ;
else
    str         =   cellstr([repmat('$',[A 1]) char([  measnames(:)])  repmat('$',[A 1])]) ;
end

if isfield(options,'gridpos_extent')
    initX       =   options.gridpos_extent(1)	;
    initY       =   options.gridpos_extent(2)	;
else
    initX       =   get(gca,'Xlim') ;
    initX       =   initX(1)        ;
    initY       =   get(gca,'Ylim') ;
    initY       =   initY(1)        ;
end

figname     = [ 'Scenario' scenario_name '_computedextent' ]    ;
fighan      =	figure('name',figname)                          ;
if exist ('FigPrep','file'),	FigPrep,	end
if exist ('AxisPrep','file'),	AxisPrep,	end
hold on

if isfield(options,'extentscale')
    extentscale = options.extentscale ;
else
    extentscale = ones(A,1);
end

L = 0 ;
if ~isempty(simulationresult)
    for a=1:A
        L=L+1;
        t1 = t_(1:length(xbar_o(a,:)));
        y1 = xbar_o(a,:)*extentscale(1)/extentscale(a) ;
        ph(L)	=   plot(t1,y1,linestyles{a},'Color',colours(a,:),'linewidth',linewidth) ;
    end
end
for a=1:A
    L       =   L+1                 ;
    col     =	(2+colours(a,:))/3                                                              ;
    ph(L)	=   plot(t_h,xbarhat_o(a,:)*extentscale(1)/extentscale(a),markers{a},'Color',colours(a,:),'Markerfacecolor',col,'linewidth',linewidth*1) ;
    if isfield(options,'markersize')
        set(ph(L),'MarkerSize',options.markersize);
    end
end


nRow        =   A	;
nCol        =   ceil(length(str)/A) ;
if exist ('GridLegend','file'),  GridLegend (ph,str,nCol,initX,initY);
else, legend (ph,str,'Interpreter','latex','Location','NorthWest')
end

set (gca,'Xlim',[floor(t_(1)) ceil(t_(end))])
xlabel (['Time [h]'])
if isfield(options,'extentscale')
    v =varnames(extentscale==extentscale(1))';
    v =char(v);
    v = [ v [ repmat(',',[ size(v,1)-1 1 ]); ' ' ] ];
    v =v' ;
    v =v(:)';
    v = strrep(v,' ','');
    v = strrep(v,',',', ');
    ylabel (['Extents: ' v   ' [' options.unit_extents  ']' ])
    
    other = find( extentscale~=extentscale(1));
    Xlim = get(gca,'Xlim');
    Ylim = get(gca,'Ylim');
    Xtick = get(gca,'Xtick');
    Ytick = get(gca,'Ytick');
    Posi = get(gca,'Position') ;
    Posi(3) = 1-2*Posi(1) ;
    set(gca,'Position',Posi)
    axes('Position',Posi,'Color','none','Yaxislocation','right')
    if exist ('AxisPrep','file'),	AxisPrep,	end
    set(gca,'Xlim',Xlim,'Xtick',Xtick,'Ylim',Ylim*extentscale(other(1))/extentscale(1),'Ytick',Ytick*extentscale(other(1))/extentscale(1))
    
    v =varnames(other)';
    v =char(v);
    v = [ v [ repmat(',',[ size(v,1)-1 1 ]); ' ' ] ];
    v =v' ;
    v =v(:)';
    v = strrep(v,' ','');
    v = strrep(v,',',', ');
    ylabel (['Extents: ' v   ' [' options.unit_extents  ']' ])
    
else
    ylabel (['Extents [' options.unit_extents ']'])
end
