
% -------------------------------------------------------------------------
% EMI Toolbox - Figure2_3.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces Figures 2-3 in [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-09-04
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% This script plots Figures 2, 3a, 3b.

SS                  =       SFP.SS                              ;
ratefunMonod        =       @(S,pars) pars(1).*S./(S+pars(2))	;
true_growth_rate	=       ratefunMonod(SS,SFP.P)              ;


% ------------- FIGURE 2 ----------------

disp('===================== FIGURE 2 =====================')

fig2	=	figure(2);

% knots9	=	[0:25/(2^3):25]';
knots9	=	[0:25/(2^4):25]';
Shape   =   'C' ;

% =========================================================================
% Setup
[ratefun,npar,SCSmodel]	=	ModelId_Step2_SetupGrowth('SCS',knots9)	;

% =========================================================================
% Estimation
resid       =	@(pars) (ratefun(SS(:),pars)-true_growth_rate(:))	;
lb          =   1e-3*ones(npar,1)                                   ;
if ~isempty(Shape) && strcmp(Shape,'N')
    lb(end) =	-inf                                                ;
end
ub          =	inf(npar,1)                                         ;
par_init    =   lb                                                  ;

par1_hat    =   CLS(resid,par_init,lb,ub)                           ;

% =========================================================================
% Figures

basis       =   SCSmodel.basis                          ;
basis0      =   SCSmodel.basis0                         ;
nbasisx     =	getnbasis(basis)                        ;
nx          =	max([10*nbasisx+1, 501])                ;
rangex      =	getbasisrange(basis)                    ;

x           =	linspace( rangex(1) , rangex(2) , nx )'	;
x6          =	linspace( rangex(1) , rangex(2) , nx )'	;

xbasismat0   =	full( eval_basis( x	, basis0     ) )     ;
x6basismat0  =	full( eval_basis( x6, basis0(7)  ) )     ;

xbasismat   =	full( eval_basis( x	, basis     ) )     ;
x6basismat  =	full( eval_basis( x6, basis(7)  ) )     ;

subplot(2,1,1)
    plot (x, xbasismat0, '-','Color',1/255*[165 153 153],'LineWidth',1.5); hold on
    axis ([rangex(1),rangex(2), 0, 1])
    plot (x6, x6basismat0, 'k-','LineWidth',2); 
 
disp('Editing the figure.')

    set(gca,'Xlim',[0 25])
    set(gca,'XTick',0:5:25,'YTick',0:0.25:1)
    set(gca,'FontSize',20)
    ylabel(['basis ' char(10) 'b_0(S)' char(10) char(10)])
    grid on
    
    
subplot(2,1,2)

    B	=	xbasismat*diag(SCSmodel.P*par1_hat)	;

    plot (x, B, '-','Color',1/255*[165 153 153],'LineWidth',1.5); hold on
    plot (x, ratefun(x(:),par1_hat), '--','Color','k','LineWidth',1.5); hold on
    plot (x, B(:,6), 'k-','LineWidth',2); 
    
    set(gca,'Xlim',[0 25])
    set(gca,'Ylim',[0 P(1)+0.1])
    set(gca,'XTick',0:5:25,'YTick',0:0.5:2)
    set(gca,'FontSize',20)
    xlabel('substrate concentration S [mg N/L]')
    ylabel([' rate function ' char(10) ' \theta \times b_0(S)' char(10) ' summed functions ' char(10) ' diag(\theta) \times b_0(S)'])
    grid on

set(fig2,'name','Figure 2','numbertitle','off')
 

disp('---Finished---')


% ------------- FIGURE 3a ----------------

disp('===================== FIGURE 3a =====================')

fig3a = figure(31);

SFP.S0 ;
Knots = {   (0:1/2:1)*S0,   ...
            (0:1/(2^2):1)*S0, ...
            (0:1/(2^3):1)*S0, ...
            (0:1/(2^4):1)*S0, ...
            (0:1/(2^5):1)*S0, ...
            (0:1/(2^6):1)*S0, ...
            (0:1/(2^7):1)*S0};
NKnots = length(Knots);

Color_nKnots = 1/255*[  0       0       0;
                        0       0       0;
                        0       0       0;
                        128     130     105;
                        0       0       0;
                        218     179     255;
                        78      101     148     ];   
linestyles_nKnots = {'-',':','--','--','-.','--','--'};
                    

Y = zeros(1,NKnots);

figure(31), hold on, box on
plot(SS,true_growth_rate,'LineWidth',6,'Color',1/255*[255 153 200]);

for i3 = 1:NKnots
    
    knots           =	Knots{i3}                                       ;
    
    disp(['	Simulating SCS with ' num2str(length(knots)) ' knots.'])
        
    [model_nKnots]	=	FitSCStoRates(SS,true_growth_rate,'C',knots)    ;
    Y(i3)           =	sqrt(sum((true_growth_rate'-EvalDynSCS(model_nKnots,SS,0)).^2)/length(SS)/2)    ;

    plot(SS,EvalDynSCS(model_nKnots,SS,0),'LineStyle',linestyles_nKnots{i3},'LineWidth',2,'Color',Color_nKnots(i3,:))
    
end

disp('Editing the figure.')

xlabel('substrate concentration S [mg N/L]')
ylabel('growth rate r(S) [1/day]')
set(gca,'Xlim',[0 25])
set(gca,'Ylim',[0 SFP.P(1)+0.1])
set(gca,'XTick',0:5:25,'Ytick',0:0.5:SFP.P(1))
set(gca,'FontSize',20)
legend('true rate','3 knots','5 knots','9 knots','17 knots','33 knots','65 knots','129 knots')
set(fig3a,'name','Figure 3a','numbertitle','off')

disp('---Finished---')


% ------------- FIGURE 3b ----------------

disp('===================== FIGURE 3b =====================')

fig3b = figure(32);
figure(32), hold on, box on
plot(1:NKnots,Y,'ko','MarkerSize',15,'MarkerFaceColor',1/255*[80 80 80],'LineWidth',0.5,'MarkerEdgeColor','k')

disp('Editing the figure.')

xlabel('number of knots')
ylabel('RMSR [-]')
axis([1,NKnots,0,0.04])
set(gca,'XTick',1:NKnots,'XTicklabel',cellfun(@length,Knots),'Ytick',[0:.01:0.04])

set(gca,'FontSize',20)
set(fig3b,'name','Figure 3b','numbertitle','off')

disp('---Finished---')
