
% -------------------------------------------------------------------------
% EMI Toolbox - STEP1_SetupOptimization.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-06-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

A=sprintf('This program gathers the experimental data and generates a structure \n containing all required information regarding the optimization problem.\n It also provides a simulation of the model with an initial guess for \n the parameters. This is executed fast.\n');
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

% ============================================================
% SETTINGS FOR OPTIMIZATION

global optionsode
optionsode                  =	odeset('AbsTol',1e-7,'RelTol',1e-4) ;

global options_fmincon
options_fmincon             =	optimset('fmincon')     ;
options_fmincon.Display     =   'off'                   ;
options_fmincon.MaxFunEvals	=	+inf                    ;
options_fmincon.Algorithm	=	'interior-point'        ;

% ============================================================
% DATA
pulse      	=   4                   ; 
FileIn      =   [   'DATA_PULSE' num2str(pulse) ]   ;
load(FileIn)

% ============================================================
% SETUP OPTIMIZATION PROBLEM

% Rate function
fun             =	@(c,theta)  max( ... 
                        0 , ...
                        c./( theta(1) + c*theta(2) + theta(3)*(c).^2 ) ...
                        )  ;

% Time grid used for simulation
Tsim            =	0:.1:max(max(data.S.t),max(data.OUR.t))         ;
Tsim            =	sort(unique([Tsim(:); data.S.t ; data.OUR.t ]))	;

% Measurement error standard deviations
sigma1          =	2/100*data.S.y+0                ;
sigma2          =	0*data.OUR.y+2                  ;

% Normalized data
Y1n             =	data.S.y./sigma1                ;
Y2n             =	data.OUR.y./sigma2              ;

% Matlab structure describing complete optimization problem:
% - rate function and time grid
Model.fun       =   fun                             ;
Model.Tsim      =   Tsim                            ;

% - Substrate concentration data
Model.n1        =   length(data.S.t)                ;
Model.I1        =   ismember(Model.Tsim,data.S.t)	;
Model.T1        =   data.S.t                        ;
Model.Y1        =	data.S.y                        ;
Model.Y1n       =   Y1n                             ; 
Model.sigma1	=   sigma1                          ;

% - OUR data
Model.n2        =   length(data.OUR.t)              ;
Model.I2        =   ismember(Model.Tsim,data.OUR.t) ;
Model.T2        =   data.OUR.t                      ;
Model.Y2        =   data.OUR.y                      ;
Model.Y2n       =   Y2n                             ;
Model.sigma2	=   sigma2                          ;

% - Shape constraints
Model.shapecon	=	false	;
if Model.shapecon
    Model.A1	=	full(spdiags([  -ones(Model.n1-1,1) ...
                                    ones(Model.n1-1,1)  ...
                                    ],[0 1],Model.n1-1,Model.n1))	;
    Model.A2	=	full(spdiags([  -ones(Model.n1-2,1) ...
                                    2*ones(Model.n1-2,1) ...
                                    -ones(Model.n1-2,1) ...
                                    ],[0 1 2],Model.n1-2,Model.n1)) ;
    
    Model.A1Q	=	full(spdiags([	-ones(Model.n2-1,1) ...
                                    ones(Model.n2-1,1)  ...
                                    ],[0 1],Model.n2-1,Model.n2))	;
    Model.A2Q	=	full(spdiags([  -ones(Model.n2-2,1) ...
                                    2*ones(Model.n2-2,1) ...
                                    -ones(Model.n2-2,1) ...
                                    ],[0 1 2],Model.n2-2,Model.n2)) ;
end

% ============================================================
% SHOW SIMULATION FOR INITIAL GUESS

% Some guesses for the parameters:
alpha	=       data.S.y(1)   	;
beta	=	[	55	400     ]	;
theta	=	[	.2 .9  3.5	]   ;
gamma	=	[   theta	alpha	beta	]	;

% Simulation
[Tsim,Xsim,Csim,Rsim,OURsim] = BatchSim(Model.Tsim,fun,gamma) ;
    
% Graph of dimensionless rate function:
ss      =	[   .01:.01:1   ]   ;
figure, hold on
    plot(ss,fun(ss,theta))
    
% Graph of data and model simulation:
figure,
han11 = subplot(2,1,1) ; 
    hold on
    plot(Model.T1,Model.Y1,'bo')
    plot(Model.T1,Model.Y1-3*Model.sigma1,'b.')
    plot(Model.T1,Model.Y1+3*Model.sigma1,'b.')
    plot(Tsim,(1-Xsim)*alpha,'b-')
    set(gca,'Xlim',[Tsim(1) Tsim(end)])
    set(han11,'Ylim',[0 22])
    grid on
    ylabel('$S~\left[mg~N/L\right]$','Interpreter','latex')

han12 = subplot(2,1,2,'Position',get(han11,'Position'));
    hold on
    plot(Model.T2,Model.Y2,'ro')
    plot(Model.T2,Model.Y2-3*Model.sigma2,'r.')
    plot(Model.T2,Model.Y2+3*Model.sigma2,'r.')
    plot(Tsim,OURsim,'r-')
    set(gca, 'Color', 'none','YAxisLocation','right')
    set(gca,'Xlim',[Tsim(1) Tsim(end)])
    set(gca,'Ylim',[0 220])
    ylabel('$OUR \left[mg~O_2/L\right]$','Interpreter','latex')

subplot(2,1,2), hold on
    plot(Model.T1,(Model.Y1-(1-Xsim(Model.I1))*alpha)./Model.sigma1,'b.')
    plot(Model.T2,(Model.Y2-OURsim(Model.I2))./Model.sigma2,'r.')
        
% ============================================================
% INITIALIZATION

% Define initial feasible set of parameter vectors:
IntX            =	[   zeros(1,3)                      ;
                        ones(1,3)               ]       ;
Theta           =       1/100000+([0 10]'*ones(1,3))	;

Model.Intercept	=       Theta(1,:)                      ;
Model.Slope     =       Theta(2,:)-Theta(1,:)           ;
    
%Model.Type ='Fun10';
    
% ============================================================
% STORE
FileName = ['SETUP_PULSE' num2str(pulse)] ;
save(['.\Results\' FileName],'IntX','Model','options_fmincon','optionsode')
    
disp(sprintf(['Optimization problem defined and saved as ' sprintf(FileName) '.mat .\n']))