function Model  =   ApplyQR(Model,QR,Trans,disc)	

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - ApplyQR.m
% -------------------------------------------------------------------------
% Description 
%
% This function adds a QS or QR (qualitative sequence or qualitative
% representation) to an existing spline model. To do so, implicit presence
% and absence of constraints is automatically rendered explicit (e.g.,
% implied discontinuities, implied zero-valued constraints in higher
% derivatives) and it is checked which transitions can be optimized to
% absolute precision.     
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model  =   ApplyQR(Model,QR,Trans,disc)	
%
% INPUT
%	Model:		Structure describing an existing spline model (see
%               CreateSCS.m)
%               (required)
%	QR:			Qualitative sequence or qualitative representation. This
%               can be given as a matrix (see DemoTitanium.m) or as a
%               string (see DemoRefinery.m). In the matrix form, nan
%               entries can be used when the transitions are not known.
%               (required)
%	Trans:      Vector of values for the episode transitions. This input is
%               ignored if the matrix form for QR is used. Otherwise,
%               absence or empty values imply that the transitions are not
%               known.
%               (optional, default=[])
%	Disc:		Vector of integers indicating the number of discontinuous
%               derivatives (knot multiplicity) at each transition in the
%               qualitative sequence/representation. This allows to add
%               discontinuities when they are not implied by the
%               qualitative sequence.
%               (optional, default=[])
%
% OUTPUT
%	Model:     	Structure describing the updated spline model (see
%               CreateSCS.m) 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<4 || isempty(disc)
    disc    =   [];
end
% 1. QR setup

if ischar(QR)
    % 1.1. QR is given as string
    if nargin<3 || isempty(Trans)
        Trans    =   []                  ;
    else
        Trans    =   [   Model.tk(1) ; Trans(:) ; Model.tk(end) ];
    end
	QR 			=	String2QR(QR,Model.Order,Trans,disc)	; 
else
    % 1.2. QR is given as numeric matrix
	QR(:,1:2)   =   (QR(:,1:2)-Model.t_ref )/Model.t_scale  ;	
end

% 2. Modify QR to ensure zero-valued derivatives are also explicitly
% enforced at all higher derivatives, except last one
m   = size(QR,1) ;
refcol = 2; 
for j=1:m
    drv     =   find(QR(j,(refcol+1):end-2)==0,1,'first')    ;
    if ~isempty(drv)
        QR(j,refcol+drv:end-2) = 0;
    end
end

% 3. Add QR to Model
Model.QR    =   QR                                      ;

% 4. Evaluate which transitions can be solved to absolute precision
Verifiable          =	AssessVerifiability(Model)          ;
Model.verifiable    =	Verifiable                          ;