function [g,beta] = ConstrainedLeastSquares(X,y)
    
% -------------------------------------------------------------------------
% EMI Toolbox - ConstrainedLeastSquares.m
% -------------------------------------------------------------------------
% Description
%
% Executes constrained least-squares fitting
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[g,beta] = ConstrainedLeastSquares(X,y)
%
% INPUT
%   X       :   Input matrix
%   y       :   Outputs
%
% OUTPUT
%   g       :   Weighted least-squares objective function value
%   beta    :   Regression vector estimate
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

beta	=   (X'*X)^(-1)* X'*y           ;   % LS-estimation
if any(beta<0)
    objfun = @(beta1) sum((y - X*beta1).^2);
    A= -eye(length(beta));
    B= zeros(length(beta),1);
    
    global options_fmincon
    [beta,g]=fmincon(@(a) objfun(a),max(beta,0),A,B,[],[],[],[],[],options_fmincon);
else
    E1      =	y - X*beta                   ;   % Prediction error
    g     =   sum(E1.^2)                      ;   % LS objective function
end

end