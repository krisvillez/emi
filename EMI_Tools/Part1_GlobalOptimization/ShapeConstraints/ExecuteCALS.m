function [beta,g,x,Converged] = ExecuteCALS(beta,y,A,xL,xU,Tol,MaxIter)

% -------------------------------------------------------------------------
% EMI Toolbox - ExecuteCALS.m
% -------------------------------------------------------------------------
% Description
%
% Executes constrained alternating least squares.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[beta,g,x,Converged] = ExecuteCALS(beta,y,A,xL,xU,Tol,MaxIter)
%
% INPUT
%   beta        :   Initial guess for regression vector
%   y           :   Outputs
%   A           :   Constraint matrix for inputs
%   xL          :   Lower bounds for the inputs
%   xU          :   Upper bounds for the inputs
%   Tol         :   Tolerance
%   MaxIter     :   Maximum number of iterations
%
% OUTPUT
%   beta        :   Regression vector estimate
%   g           :   Weighted least-squares objective function value
%   x           :   Input estimate
%   Converged	:   Boolean indicating convergence
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

global options_lsqlin
nA = size(A,1);
g=inf ;

NoConvergence = true ;
Iter =0;

y=y(:);
ny =length(y);

switch length(beta)
    case 1 % regression line through zero
        ALS1	=	@(beta,xx)	lsqlin( beta*eye(ny),...
                                        y,A,zeros(nA,1),...
                                        [],[],xL,xU,...
                                        y/beta)                             ;
        ALS2	=	@(beta,xx)	lsqlin( xx(:),y,[],[],[],[],0,+inf,beta)    ;
        x       =	y/beta                                                  ;
    case 2 % regression line with non-zero intercept
        ALS1    =	@(beta,xx)	lsqlin( beta(2)*eye(ny),...
                                        y-beta(1)*xL(:,1),A,zeros(nA,1),...
                                        [],[],xL(:,2),xU(:,2),...
                                        (y-beta(1))/beta(2))                ;
        ALS2    =	@(beta,xx)  lsqlin( [ xL(:,1) xx(:)],...
                                        y,[],[],[],[],zeros(2,1),+inf(2,1),...
                                        beta)                               ;
        x       =	(y-beta(1))/beta(2)                                     ;
        
    otherwise
        error('Unsupported dimension')
end

while NoConvergence && Iter<MaxIter
    
    Iter            =	Iter+1          ;
    
    % ALS convergence reference
    g_old           =   g               ;
    
    % ALS iteration
    [x]             =	ALS1(beta,x)    ;
    [beta,g,res]	=	ALS2(beta,x)    ;
    
    % ALS convergence check
    AbsImp          =   g_old-g                             ;
    NoConvergence	=	and(g~=0,or(AbsImp>Tol,AbsImp<0))   ;
    
end

Converged           =	~NoConvergence	;