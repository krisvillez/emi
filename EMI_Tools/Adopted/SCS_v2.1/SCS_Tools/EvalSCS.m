function Ysim = EvalSCS(Model,Argument,Derivative)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - EvalSCS.m
% -------------------------------------------------------------------------
% Description 
%
% EvalSCS is a function which evaluates the spline functions or one
% of its derivatives. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Ysim = EvalSCS(Model,Argument,Derivative)
%
% INPUT
%	Model:     	Structure describing the fitted spline model. 
%               (required)
%   Argument:   Column vector of argument values at which the polynomial
%               basis should be evaluated.
%               (required)
%   Derivative: The derivative for which the basis should be evaluated. An
%               empty or zero-valued input results in the computation of
%               the basis for the function itself (0th derivative).
%               (optional, default=0)
%
% OUTPUT
%	Ysim:       Column vector matching dimensions of 'Argument' and
%               containing simulated data 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<3 || isempty(Derivative)
    Derivative  =   0   ;
end
if nargin<2 || isempty(Argument)
    Argument	=	unique(sort([Model.ty]))    ;
end       

B   	=   eval_basis(Argument,Model.basis,Derivative)	; % obtained required basis for specified coefficients and derivative d
x       =   Model.x     ;
Ysim    =   B*x         ;