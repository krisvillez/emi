function PlotRMSR(SSR,select_model,fig)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotRMSR.m
% -------------------------------------------------------------------------
% Description
%
% Plot RMSR values.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	PlotRMSR(SSR,select_model,fig)
%
% INPUT
%   SSR             :   Matrix of SSR values   
%   select_model	:   Names associated with rows of SSR
%   fig             :   figure handle
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

fig                                             ;

nFun	=	size(SSR,1)                         ;

if nFun == 1
    if isrow(SSR)
      SSR	=	vertcat(SSR,nan(size(SSR)))     ;
    end
    b2      =	bar(SSR,'grouped')              ;
else
    b2      =	bar(1:nFun,SSR)                 ;
end

set(gca,...
    'Xtick',1:nFun,...
    'Xticklabel',select_model,...
    'FontSize',18,...
    'Xlim',[0.5 (nFun+0.5)],...
    'Ylim',[-0.005 0.06],...
    'Ytick',0:0.01:0.06)

legend({'Monod','Tessier','SCS1','SCS2'},'Location','NorthWest')
xlabel('Simulated rate law')
ylabel('RMSR [-]')

b2(1).FaceColor	=	'k'                         ;
b2(2).FaceColor =	1/255 * [   128 128 128 ]   ;
b2(3).FaceColor =	1/255 * [   255 255 0   ]   ;
b2(4).FaceColor =	1/255 * [   77	190	238 ]   ;

b2(1).BaseValue =	-0.005                      ;

plot(0.5:(nFun+0.5),zeros(1,length(0.5:(nFun+0.5))),'--k','LineWidth',2)