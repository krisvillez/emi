function library = CreateLibrary()

% -------------------------------------------------------------------------
% EMI Toolbox - CreateLibrary.m
% -------------------------------------------------------------------------
% Description
%
% Produces a structured array of rate functions.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	library = CreateLibrary()
%
% OUTPUT
%   library	:   Structured array of rate laws
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-04
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% this creates the model library

% Monod
library(1).ratefun	=	@(S,pars) S./(pars(2)+S*pars(1))                        ;
library(1).pars     =	nan(1,2)                                                ;
library(1).name     =	'Monod'                                                 ;

% Tessier
library(2).ratefun	=	@(S,pars) 1/(pars(1)).*(1-exp(-S./(pars(2)/pars(1))))   ;
library(2).pars     =	nan(1,2)                                                ;
library(2).name     =	'Tessier'                                               ;

end