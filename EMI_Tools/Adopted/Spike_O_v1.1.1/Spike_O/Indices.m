
% -------------------------------------------------------------------------
% Spike_O Toolbox - Indices.m
% -------------------------------------------------------------------------
% Description
%
% This script provides common index variables
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2013-05-14
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% index variables indicate columns for a given variables
iParent     =   1       ;
iLevel      =   2       ;
iLive       =   3       ;
iLB         =   4       ;
iUB         =   5       ;
iVol        =   6       ;
iCity       =   7       ;
index       =   8       ;