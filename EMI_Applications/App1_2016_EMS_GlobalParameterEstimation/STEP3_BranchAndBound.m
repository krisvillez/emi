
% -------------------------------------------------------------------------
% EMI Toolbox - STEP3_BranchAndBound.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

A=sprintf('This program executes the final run of the branch-and-bound \n algorithm as executed for the manuscript. Computation is \n rather lengthy and should be executed on a dedicated machine \n or server. For a fast execution, set the value of ''resolpow''  \n in the script to a lower integer value. \n');
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% ============================================================
% BASIC SETTINGS

rescale     =   true	;
resolpow	=   11      ; % for results as in article, use: 11;

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

% ============================================================
% LOAD AND PROCESS DATA

pulse       =   4       ;
load(['Results\PULSE' num2str(pulse) '_CONTRACT_final\CONTRACT_PULSE' num2str(pulse)])

if rescale
    Model.Slope     =	Params(2,:)-Params(1,:)                 ;
    Model.Intercept =	Params(1,:)                             ;
    IntX            =   [   zeros(1,3)  ;   ones(1,3)   ]       ;
    
else
    Model.Slope     =	max(Params(2,:)-Params(1,:))*ones(1,3)              ;
    Model.Intercept =   min(Params(1,:))*ones(1,3)                          ;
    IntX            =	[	(Params(1,:)-Model.Intercept)./Model.Slope	;	...
                            (Params(2,:)-Model.Intercept)./Model.Slope  ]   ;
end

% ============================================================
% SETTINGS FOR OPTIMIZATION

global optionsode
global options_fmincon
global options_lsqlin

optionsBB               =	OptimsetBB          ;
optionsBB.boundfun      =	@BatchSim_Bound     ;
optionsBB.boundupp      =	@BatchSim_BoundUpp	;
optionsBB.boundlow      =	@BatchSim_BoundLow	;
optionsBB.resolution	=	(1/2)^resolpow      ;
optionsBB.node          =   1                  ;
optionsBB.graphprogress	=   true                ;
optionsBB.graphinterval	=	100                 ;
optionsBB.verbose       =   1                   ;


% ============================================================
% EXECUTE OPTIMIZATION

T0                      =   cputime                         ;
[Tree,Model,Progress]	=	BB(Model,optionsBB,IntX,[])     ;
T1                      =   cputime                         ;
deltaT                  =	T1 - T0                         ;

% ============================================================
% PROCESS RESULTS

Indices
BBox                    =	[   min(Tree(:,index:2:end))        ;   ...
                                max(Tree(:,index+1:2:end))	]   ;
Params                  =       BBox*diag(Model.Slope)+ones(2,1)*Model.Intercept    ;
Vol                     =       prod(Params(2,:)-Params(1,:))                       ;

figure(37), close 37
figure(73), close 73
figure(137), close 137
[LB,UB,X,Model]         =       BatchSim_Bound(Model,Model.IntX,[],true)            ;


Folder	=	['.\Results\PULSE' num2str(pulse) '_RESCALE' num2str(rescale*1) '_RESOL' num2str(resolpow)  ];
mkdir(Folder)

FileList	=	dir('Progress*.csv')	;
nFile       =	length(FileList)        ;
for iFile=1:nFile
    movefile(FileList(iFile).name,[Folder '\' FileList(iFile).name])
end

save([Folder '\OPTIM_PULSE' num2str(pulse) ])

return

