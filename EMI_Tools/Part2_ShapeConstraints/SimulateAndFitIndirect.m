function [Output,WRMSR] = SimulateAndFitIndirect(select_model,Knots,Shapes,SFP)

% -------------------------------------------------------------------------
% EMI Toolbox - SimulateAndFitIndirect.m
% -------------------------------------------------------------------------
% Description
%
% Execute model fitting on the basis of simulated concentration
% measurements.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Output,WRMSR] = SimulateAndFitIndirect(select_model,Knots,Shapes,SFP)
%
% INPUT
%   select_model	:   Cell array with strings indicating the ground truth
%                       growth rate models
%   Knots           :   Cell array with knot location vectors
%   Shapes          :   Primitives indicating the applied shape constraints
%                       for the SCS models
%   SFP             :   Structure with parameters common to all ground
%                       truth simulations
%
% OUTPUT
%   Output          :   Structured array compiling all results
%   RMSR            :   Matrix with RMSR values
%
% -------------------------------------------------------------------------
% Last modification: 2016-10-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% SFP is a structure containing all relevant parameters for this function

Output          =	struct()                                        ;

nFun            =   length(select_model)                            ;

Library         =	CreateLibrary                                   ;
nLib            =   length(Library)                                 ;
Names           =   {Library.name}                                  ;

WRMSR           =	zeros(nFun,nLib+length(Knots))                  ;

tspan           =	0:(1/60):SFP.Tend                               ;
tmeas           =   0:SFP.ts:SFP.Tend                               ;
tmeas           =	tmeas(:)                                        ; 
tmeas_index     =   1:(SFP.ts/(1/60)):length(tspan)                 ;

for iFun = 1:nFun
    
    
    % ====================================================================
    % i. SETUP THE CURRENT CASE
    % ====================================================================
    
%     close all
%     figure(73)
    
    disp(['Case: ' num2str(iFun)])
    disp(['	Simulating rate function: ' select_model{iFun}])
    
 	c0          =	[	SFP.S0	;	SFP.X0	]                       ;
    ratefun     =       SelectFunction(select_model{iFun})          ;
    pars        =       SFP.pars                                    ;

    Output(iFun).true_rate_name =	SFP.model_names{iFun}           ;
    Output(iFun).true_rate_rate =	ratefun(SFP.SS,SFP.P)           ;
    Output(iFun).true_rate_SS	=	SFP.SS                          ;
    
    
    % ====================================================================
    % ii. SIMULATE DATA
    % ====================================================================
    
    disp('      Simulation started')
    
    % Noise-free measurements
    [~,Csim]    =	EvaluateODE(pars,ratefun,c0,tspan)      ;   % noise-free concentrations
    
    Cmeas       =	Csim(:,tmeas_index)                     ;   % select for sampling times
    
    noisefile   =	'noise.mat'                             ;
    if ~exist(noisefile)
        noise   =   randn(size(Cmeas))                      ;
        save(noisefile,'noise')                             ;
    else
        load(noisefile)
    end
    
    Cmeas       =       Cmeas + diag(SFP.sigmaY)*noise      ;
    
    Output(iFun).true_rate_ode          =	Cmeas           ;
    Output(iFun).true_rate_ode_tmeas	=	tmeas           ;

    disp('      Simulation finished')
    
    % ====================================================================
    % iii. QUALITATIVE TREND ANALYSIS
    % ====================================================================

    disp('      Estimation of reconciled concentration profiles as SCS functions')
    
    % STEP 1: Fit SCS models to the trends of (substrate-product)/2 [DA trend] and of the
    % biomass concentration [BNA trend].
    TrendModels     =	ModelId_Step1_SCS(Cmeas,tmeas)  ;
    
    disp('      -- finished')
    
    % ====================================================================
    % iv. PARAMETER ESTIMATION
    % ====================================================================
    
    StartTimeDecay	=	5           ;
    sigmaY          =	SFP.sigmaY	;
    
    for type=1:2
        
        switch type
            case 1
                
                % =========================================================
                % I. CLASSIC FUNCTION LIBRARY - PARAMETER ESTIMATION
                % =========================================================
                
                for iLib=1:nLib
                    
                    Name                                =   Names{iLib}	;
                    
                    disp(['     Evaluating model: ' Name])
                    
                    [ratefun1,par_hat,g_hat,C_hat]      =   ModelId_Steps2to6(TrendModels,Name,c0,sigmaY,Cmeas,tmeas,StartTimeDecay) ;
                    
                    WRMSR(iFun,iLib)                    =	g_hat       ;
                    
                    Output(iFun).Lib_models(iLib).name	=	Name        ;
                    Output(iFun).Lib_models(iLib).rate	=	ratefun1	;
                    Output(iFun).Lib_models(iLib).pars	=	par_hat     ;
                    Output(iFun).Lib_models(iLib).ode	=	C_hat       ;
                    Output(iFun).Lib_models(iLib).tmeas	=	tmeas       ;
                    
%                     close 73
%                     figure(73), hold on 
%                         bar(WRMSR)
%                         drawnow
                    
                    disp(['         -- Finished'])
                        
                end
                
                % =========================================================
                % I. CLASSIC FUNCTION LIBRARY - MODEL SELECTION
                % =========================================================
                
                
                [~,IndexSelect]                         =   min(WRMSR(iFun,1:nLib))                     ;
                
                Output(iFun).Lib_models_selected.name   =	Output(iFun).Lib_models(IndexSelect).name	;
                Output(iFun).Lib_models_selected.rate   =	Output(iFun).Lib_models(IndexSelect).rate	;
                Output(iFun).Lib_models_selected.pars   =	Output(iFun).Lib_models(IndexSelect).pars   ;
                Output(iFun).Lib_models_selected.ode    =	Output(iFun).Lib_models(IndexSelect).ode    ;
                Output(iFun).Lib_models_selected.tmeas	=	Output(iFun).Lib_models(IndexSelect).tmeas  ;
                
            case 2
                    
                % =========================================================
                % II. SCS FUNCTION - PARAMETER ESTIMATION
                % =========================================================
                
                Name	=   'SCS'           ;
                knots	=	Knots{1}        ;
                
                nShape  =   length(Shapes)	;
                
                for iShape=1:nShape
                    
                    
                    Name1                                   =   [   Name	num2str(iShape+1)	]	;
                    
                    disp(['     Evaluating model: ' Name1])
                    
                    disp(['         Parameter estimation'])
                    Shape                                   =	Shapes{iShape}                                                                          ;
                    [ratefun1,par_hat,g_hat,C_hat]          =   ModelId_Steps2to6(TrendModels,Name,c0,sigmaY,Cmeas,tmeas,StartTimeDecay,knots,Shape)    ;
                   

                    WRMSR(iFun,nLib+iShape)                 =	g_hat       ;
                    
                    Output(iFun).SCS_models(iShape).name	=	Name1       ;
                    Output(iFun).SCS_models(iShape).rate    =	ratefun1	;
                    Output(iFun).SCS_models(iShape).pars    =	par_hat     ;
                    Output(iFun).SCS_models(iShape).ode     =	C_hat       ;
                    Output(iFun).SCS_models(iShape).tmeas	=	tmeas       ;
                    
%                     close 73
%                     figure(73), hold on  
%                         bar(WRMSR)
%                         drawnow
                    
                    disp(['         -- Finished'])
                end
                
                if WRMSR(iFun,nLib+1)<WRMSR(iFun,nLib+2) % C-model fits better -> try to start N-model with these parameters
                    
                    iShape                                      =       2                               ;
                    Name1                                       =   [   Name	num2str(iShape+1)     ]	;
                    
                    disp(['     Evaluating model: ' Name1 ' (reprise)'])
                    
                    disp(['         -- Optimization'])
                    
                    par_init                                    =   Output(iFun).SCS_models(iShape-1).pars                                  ;
                    [par_hat,g_hat,C_hat]                       =	ModelId_Step6_ParEstim(par_init,ratefun1,c0,sigmaY,tmeas,Cmeas,Shape)	;
                        
                    
                    if g_hat < WRMSR(iFun,nLib+iShape)
                        
                        Output(iFun).SCS_models(iShape).name    =	Name1       ;
                        Output(iFun).SCS_models(iShape).rate    =	ratefun1	;
                        Output(iFun).SCS_models(iShape).pars    =	par_hat     ;
                        Output(iFun).SCS_models(iShape).ode     =	C_hat       ;
                        Output(iFun).SCS_models(iShape).tmeas	=	tmeas       ;
                        
                        WRMSR(iFun,nLib+iShape)                 =	g_hat       ;
                    end
                    
%                     close 73
%                     figure(73), hold on  
%                         bar(WRMSR)
%                         drawnow
                        
                    disp(['         -- Finished'])
                end
                    
                
        end
        
    end
    
end


