function dxdt = FittoRates_odefun(~,x,model,pars)

% -------------------------------------------------------------------------
% EMI Toolbox - FittoRates_odefun.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates ordinary differential equations (ODE) for growth and decay
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	dxdt = FittoRates_odefun(~,x,model,pars)
%
% INPUT
%   dxdt	:   rates of changes
%
% OUTPUT
%   x       :	state vector
%   model	:   growth rate model
%   pars	:   kinetic parameters
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-03
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% get states:
S       =	x(1)                        ;	%	substrate 
X       =	x(2)                        ;	%	biomass conc 
Y       =	pars(1)                     ;	%	yield
b       =	pars(2)                     ;	%	decay rate

mu      =	model(S,pars(3:end))        ;
        
dSdt 	=	-mu*X                       ;	%	substate uptake rate
dXdt 	=	+mu*X*Y - b*X               ;	%	net biomass growth rate

dxdt	=	[	dSdt	;	dXdt	]	;