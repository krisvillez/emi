function [QU,QL,Reduction,Omega_reduced,Omega_best,Iterations] = ExecuteBB(Model,Omega,optionsBB)

% -------------------------------------------------------------------------
% EMI Toolbox - ExecuteBB.m
% -------------------------------------------------------------------------
% Description
%
% Execute one run of the Branch-and-Bound algorithm for extent model
% parameter estimation.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[QU,QL,Reduction,Omega_reduced,Omega_best,Iterations]
%                                       = ExecuteBB(Model,Omega,optionsBB)
%
% INPUT
%   Model       :   Model description given as a structure (see ExtentDAE.m
%                   for required structure)
%   Omega       :   Bounding box for considered parameter vectors
%   optionsBB	:	Options structure for B&B algorithm
%
% OUTPUT
%   QU              :   Upper bound
%   QL              :   Lower bound
%   Reduction       :   Relative dimensions of bounding box containing
%                       retained parameter vector sets 
%   Omega_reduced	:   Bounding box containing retained parameter vector
%                       sets 
%   Omega_best      :   Live set containing best solution found
%   Iterations      :   Number of iterations required to terminate
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Normalization of parameter bounding box
ntheta                  =       size(Omega,2)                           ;
Model.Intercept         =       Omega(1,:)                              ;
Model.Slope             =       Omega(2,:)-Omega(1,:)                   ;
IntX                    =       [0 1]'*ones(1,ntheta)                   ;

% Execute BB optimization
[Tree,Model,~,Record]	=       BB(Model,optionsBB,IntX,[])             ;


Iterations              =       Record(end,1)                           ;

Indices
BBox                    =   [	min(Tree(:,index:2:end),[],1)	;   ...
                                max(Tree(:,(index+1):2:end),[],1)	]   ;   % Bounding box - normalized scale

Reduction               =       BBox(2,:)-BBox(1,:)                     ;   % Relative size of bounding box
Omega_reduced           =       ones(2,1)*Model.Intercept ...
                                    + BBox*diag(Model.Slope)        	;   % Bounding box - original scale

QL                      =       min(Tree(:,iLB))                        ;   % Overall lower bound
[QU,row_opt]            =       min(Tree(:,iUB))                        ;   % Overall upper bound
IntX                    =       [   Tree(row_opt,index:2:end)	;   ...
                                    Tree(row_opt,(index+1):2:end)   ]	;   % Best box - normalized scale
Omega_best              =       ones(2,1)*Model.Intercept ...
                                    + IntX*diag(Model.Slope)        	;   % Best box - original scale

% clean-up:
delete('Progress*.csv');



end

