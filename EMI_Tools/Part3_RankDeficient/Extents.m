function [xbarhat_o,xbar_o,model] = Extents (model,options,simulationresult) 

% -------------------------------------------------------------------------
% EMI Toolbox - Extents.m
% -------------------------------------------------------------------------
% Description
%
%
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[xbarhat_o,xbar_o,model] = Extents (model,options,simulationresult) 
%
% INPUT
%   model               :	model as structure
%   options             :   options for extent definition and computation
%   simulationresult	:	[optional] reference data
%   
% OUTPUT
%	xbarhat_o           :	computed extents and extent directions
%   xbar_o              :   computed extents and extent directions with
%                           reference data (empty when simulationresult is
%                           empty)
%   model               :	updated model
%   
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% ------------------------------------------------------------------------
%   Structural analysis
% ------------------------------------------------------------------------

% Reactor volume
V                   =	model.experiment.V                          ;

% Structural analysis: Label the extents and identify the corresponding matrices
[static,projection]	=	ExtentsDefinition (model.static,V,options)	;
model.static        =	static                                      ;
model.projection	=	projection                                  ;

% ------------------------------------------------------------------------
%   Computation
% ------------------------------------------------------------------------

if options.verbose, disp('EXTENTS - COMPUTATION'), end

% Apply available data:
n_0                 =   model.experiment.n_0        ;   % initial conditions
ytil_h              =	model.observations.ytil_h   ;   % measurements

% Actual projection:
xbarhat_o           =	ExtentsCompute (static,projection,n_0,V,ytil_h)  ; % computed extents

% ------------------------------------------------------------------------
%   Output
% ------------------------------------------------------------------------

model.observations.xbarhat_o	=	xbarhat_o               ;

if nargin>=3 && isfield(simulationresult,'y_')
    y_      =	simulationresult.y_                         ;   % noise-free measurements
    xbar_o	=	ExtentsCompute (static,projection,n_0,V,y_)	;   % noise-free extents and extent directions
else
    xbar_o	=   [	]                                       ;
end

