function g = RegBound( Beta,y,xL,xU )

% -------------------------------------------------------------------------
% EMI Toolbox - RegBound.m
% -------------------------------------------------------------------------
% Description
%
% Executes regression with bounded inputs.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	g = RegBound( Beta,y,xL,xU )
%
% INPUT
%   Beta    :   Regression vector
%   y       :   Outputs
%   xL      :   Lower bounds for the inputs
%   xU      :   Upper bounds for the inputs
%
% OUTPUT
%   g       :   Relaxed weighted least-squares objective function value
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

E_UB=y-xL*Beta;
E_LB=y-xU*Beta;

EL = min(abs(E_UB),abs(E_LB)) ;
EL(and(E_LB<0,E_UB>0)) = 0 ;

g = sum((EL).^2) ;

end

