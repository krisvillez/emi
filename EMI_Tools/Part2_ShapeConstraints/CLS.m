function par1_hat    =   CLS(resid,par_init,lb,ub)  

% -------------------------------------------------------------------------
% EMI Toolbox - CLS.m
% -------------------------------------------------------------------------
% Description
%
% Executes constrained least squares (CLS)
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	par1_hat    =   CLS(resid,par_init,lb,ub)
%
% INPUT
%   resid       :   Residual function handle   
%   par_init	:   Initial parameter guesses
%   lb          :   Lower parameter bounds
%   ub          :   Upper parameter bounds
%
% OUTPUT
%   par1_hat	:   Parameter estimates
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

options         =   optimset('lsqnonlin')                           ;
options.Display	=   'off'                                           ;
par1_hat        =	lsqnonlin(@(p) resid(p),par_init,lb,ub,options) ; 

end

