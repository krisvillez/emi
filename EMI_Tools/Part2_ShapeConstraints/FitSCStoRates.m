function modelSCS = FitSCStoRates(x,truemodel,string,knots)

% -------------------------------------------------------------------------
% EMI Toolbox - FitSCStoRates.m
% -------------------------------------------------------------------------
% Description
%
% Fits a shape constrained spline function model to direct rate
% measurements.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	modelSCS = FitSCStoRates(x,truemodel,string,knots)
%
% INPUT
%   x           :	Substrate concentrations
%   truemodel   :   Rate measurements
%   string      :   Imposed shape as a primitive
%   knots       :	Spline knots
%
% OUTPUT
%   modelSCS	:   Shape constrained spline function model
%
% -------------------------------------------------------------------------
% Last modification: 2016-05-31
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

ss              =	x(:)                                        ;
yy              =	truemodel(:)                                ;

order           =	4                                           ;
GoThroughOrigin =	true                                        ;
modelSCS        =	SetupDynSCS( knots,order,GoThroughOrigin )	;

Data.ss         =	ss                                          ;
Data.yy         =	yy                                          ;

Shape           =	string                                      ; % Supported: single-letter descriptions A/B/C/D/P/N/Q
modelSCS        =	FitDynSCS(modelSCS,Data,Shape)              ;


