function par3_hat	=	ModelId_Step5_GuessYield(TrendModels,c0,par2_hat)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Step5_GuessYield.m
% -------------------------------------------------------------------------
% Description
%
% Provides and initial guess for the yield coefficient parameter.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	par3_hat	=	ModelId_Step5_GuessYield(TrendModels,c0,par2_hat)
%
% INPUT
%   TrendModels	:   Shape constrained spline models
%   c0          :   Initial concentrations
%   par2_hat    :   Initial guess for the decay rate parameter
%
% OUTPUT
%   par3_hat	:   Initial guess for the yielf coefficient
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =========================================================================
% Process available information:

tsim            =	TrendModels{1}.tk(1):1/60:TrendModels{1}.tk(end)            ;               
x               =	c0(1)-EvalSCS(TrendModels{1},tsim)                          ;   % cumulative substrate conversion at each time sample
[~,ExtentDecay]	=	ode15s(@(t,x) par2_hat*EvalSCS(TrendModels{2},t) ,tsim,0)	;   % cumulative amount of biomass that decayed
y               =	EvalSCS(TrendModels{2},tsim)+ExtentDecay-c0(2)              ;   % cumulative amount of biomass that was produced through growth
    
% =========================================================================
% Estimation

resid           =	@(pars) (x*pars-y)          ;
lb              =   0                           ;
ub              =	+inf                        ;
par_init        =   .01*ones(1,1)               ;
par3_hat        =   CLS(resid,par_init,lb,ub)   ;

end

