function Model = FitDynSCS(Model,Data,Shape)

% -------------------------------------------------------------------------
% EMI Toolbox - FitDynSCS.m
% -------------------------------------------------------------------------
% Description
%
% Fit a shape constrained spline model to a set of measurements.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model = FitDynSCS(Model,Data,Shape)
%
% INPUT
%   Model   :   Shape constrained spline model
%   Data    :   Structure with fields 'ss' and 'yy' (independent and
%               dependent data)
%   Shape   :   Primitive defining the shape
%
% OUTPUT
%   Model   :   Shape constrained spline model with updated parameters
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-05
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Fit Dynamic SCS model

ss              =   Data.ss                         ;
yy              =   Data.yy                         ;

B0ss            =	eval_basis(ss,Model.basis,0)	;
[ny,nx]         =	size(B0ss)                      ;


% ========================
% Problem definition

% objective function
objfun          =	@(x) sum((yy-B0ss*x).^2)        ;

% constraint specifications
B1k             =   Model.B1k                       ;
B2k             =   Model.B2k                       ;
switch Shape
    case 'A'
        A       =   [   +B1k(end,:) ;	-B2k    ]   ;
    case 'B'
        A       =   [   -B1k(1,:)   ;	-B2k    ]   ;
    case 'C'
        A       =   [   -B1k(end,:)	;   +B2k    ]   ;
    case 'D'
        A       =   [   +B1k(1,:)   ;	+B2k    ]   ;
    case 'N'
        A       =   [   +B2k                    ]   ;
    case 'P'
        A       =   [   -B2k                    ]   ;
    case 'Q'
    otherwise
        error('Unsupported shape constraint')
end

nc              =   size(A,1)                       ;
b               =	zeros(nc,1)                     ;

% ========================
% Initialization
x0              =	zeros(nx,1)                                     ;
opt             =	optimoptions('fmincon','Display','off')         ;
opt.MaxFunEvals =	inf                                             ;

% ========================
% Optimization
switch Shape
    case 'Q'
        x1  =   (B0ss'*B0ss)^(-1)*B0ss'*yy                          ;
    otherwise
        x1	=	fmincon(@(x) objfun(x),x0,A,b,[],[],[],[],[],opt)   ;
end

% ========================
% Reporting/store result
Model.B0ss	=	B0ss                                                ;
Model.x     =	x1                                                  ;

end