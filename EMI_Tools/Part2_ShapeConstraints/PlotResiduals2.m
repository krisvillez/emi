function PlotResiduals2(tmeas,C,Clib,Cscs1,Cscs2,sigmaY)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotResiduals2.m
% -------------------------------------------------------------------------
% Description
%
% Plot residual profiles.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	PlotResiduals2(tmeas,C,Clib,Cscs1,Cscs2,sigmaY)
%
% INPUT
%   tmeas       :   Sampling times
%   C           :   Measurement profiles
%   Clib        :   Concentrations with library model   
%   Cscs        :   Concentrations with shape constrained spline model   
%   sigmaY      :   measurement error standard deviations
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

subplot(3,1,1); 
    hold on, box on
    plot(tmeas,(C(1,:)-Clib(1,:))/sigmaY(1),'k.','MarkerSize',12)
    plot(tmeas,(C(1,:)-Cscs1(1,:))/sigmaY(1),'+','MarkerSize',6,'LineWidth',1,'Color',1/255*[0 114 189])
    plot(tmeas,(C(1,:)-Cscs2(1,:))/sigmaY(1),'o','MarkerSize',6,'LineWidth',1,'Color',1/255*[0 114 189])
    plot(tmeas,+ones(size(tmeas)),'--','LineWidth',1.5,'Color',1/255*[128 128 128])
    plot(tmeas,-ones(size(tmeas)),'--','LineWidth',1.5,'Color',1/255*[128 128 128])
    ylabel({'S residuals','(normalized)'})
    set(gca,'FontSize',18,'Xtick',tmeas(1):2:tmeas(end),'Ylim',[-4.1 +4.1],'Ytick',-4:2:4)
    set(gca,'xticklabel',[])
    gcapos = get(gca,'Position') ;
    gcapos(1) = .17 ;
    gcapos(4) = gcapos(4)*1.2 ;
    set(gca,'Position',gcapos)

subplot(3,1,2); 
    hold on, box on
    plot(tmeas,(C(2,:)-Clib(2,:))/sigmaY(2),'k.','MarkerSize',12)
    plot(tmeas,(C(2,:)-Cscs1(2,:))/sigmaY(2),'+','MarkerSize',6,'LineWidth',1,'Color',1/255*[0 114 189])
    plot(tmeas,(C(2,:)-Cscs2(2,:))/sigmaY(2),'o','MarkerSize',6,'LineWidth',1,'Color',1/255*[0 114 189])
    plot(tmeas,+ones(size(tmeas)),'--','LineWidth',1.5,'Color',1/255*[128 128 128])
    plot(tmeas,-ones(size(tmeas)),'--','LineWidth',1.5,'Color',1/255*[128 128 128])
    ylabel({'P residuals','(normalized)'})
    set(gca,'FontSize',18,'Xtick',tmeas(1):2:tmeas(end),'Ylim',[-4.1 +4.1],'Ytick',-4:2:4)
    set(gca,'xticklabel',[])
    gcapos = get(gca,'Position') ;
    gcapos(1) = .17 ;
    gcapos(4) = gcapos(4)*1.2 ;
    set(gca,'Position',gcapos)

subplot(3,1,3); 
    hold on, box on
    plot(tmeas,(C(3,:)-Clib(3,:))/sigmaY(3),'k.','MarkerSize',12)
    plot(tmeas,(C(3,:)-Cscs1(3,:))/sigmaY(3),'+','MarkerSize',6,'LineWidth',1,'Color',1/255*[0 114 189])
    plot(tmeas,(C(3,:)-Cscs2(3,:))/sigmaY(3),'o','MarkerSize',6,'LineWidth',1,'Color',1/255*[0 114 189])
    plot(tmeas,+ones(size(tmeas)),'--','LineWidth',1.5,'Color',1/255*[128 128 128])
    plot(tmeas,-ones(size(tmeas)),'--','LineWidth',1.5,'Color',1/255*[128 128 128])
    xlabel('time [h]')
    ylabel({'X residuals','(normalized)'})
    set(gca,'FontSize',18,'Xtick',tmeas(1):2:tmeas(end),'Ylim',[-4.1 +4.1],'Ytick',-4:2:4)
    gcapos = get(gca,'Position') ;
    gcapos(1) = .17 ;
    gcapos(4) = gcapos(4)*1.2 ;
    set(gca,'Position',gcapos)
