function [model,SelectedNode] =   SelectFromTree(Tree,model,DistX,options)

% -------------------------------------------------------------------------
% Spike_O Toolbox - SelectFromTree.m
% -------------------------------------------------------------------------
% Description
%
% This function selects the best solution from the solution tree and
% provides updated model.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[model,SelectedNode] =   SelectFromTree(Tree,model,DistX,verbose)
%
% INPUT
%   Tree    :   Solution tree
%   model   :   mathematical problem
%   Verbose :   verbose output or not
%
% OUTPUT
%   options :   structure with all default options
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


%disp('Start checking')
Indices % get column indices for variables in Tree

% -------------------------------------------------------------------------
% 1. solution selection
% Alive       =   Tree(:,iLive)               ;   % live nodes
% LiveTree    =   Tree(Alive,:)               ;   % reduce tree to live nodes
Alive       =   find(Tree(:,iLive))         ;   % live nodes
LiveTree    =   Tree(Alive,:)               ;   % reduce tree to live nodes
[oo,select] =   min(LiveTree(:,iLB))        ;   % find node
SelectedNode   =   Alive(select)               ;

% -------------------------------------------------------------------------
% 2. solution evaluation

% interval for interior bounds
IntX        =   LiveTree(select,index:end)  ;   % as row vector
nib2        =   size(IntX,2)             ;   % number of interior bounds X2 
nib         =   nib2/2                      ;   % number of interior bounds
IntX        =   reshape(IntX,[2 nib])    ;   % as 2-row matrix
% -------------------------------------------------------------------------
% 3. info from model

if isfield(model,'type') && strcmp(model.type,'SCS')
    
    DefDistX = DefaultDistX(model)  ;
    
    if nargin<3 || isempty(DistX)
        DistX   =  DefDistX ;
    end
    % any discontinuities?
    anydisc = ~AnyDisc(model)     ;
    % any zero-valued primitives?
    % anyzero     =   any(any(model.QR(3:5,:)==0))  ;

    % report solution subset as time interval
    
    TimeInt     =   IntX     ;   % convert intervals to time-referenced intervals
    
    %KnotInt     =   Times2Knots(TimeInt,tk)    ;
    
    % TimeInt
    model.TimeInt =   TimeInt ;
    %model.KnotInt =   KnotInt ;

    if anydisc
        % with discontinuities: use upper bound
        Knots               =   SCSfeasible(IntX,DistX,model) ;   % get a feasible solution
         [oo,model]            =   options.boundupp(model,IntX,Knots)      ;   % evaluate solution
%         [oo,trans]          =   VerifyShape(model)            ;   % verify shape
        SolX   =   nan(1,nib)  ;
    else
        % without discontinuities: evaluate lower bound
        [oo,model]            =   options.boundlow(model,IntX)       ;   % evaluate solution
        [verified,SolX]  =   SCSverify_Case2(model)        ;   % verify shape
        if ~verified    % if shape cannot be verified, use upper bound solution instead
            Knots           =   SCSfeasible(IntX,DistX,model) ;   % get a feasible solution
            [oo,model]        =   options.boundupp(model,IntX,Knots) ;   % evaluate solution
            [oo,SolX]    =   SCSverify_Case2(model)        ;   % verify shape
        end
    end
    % report solution subset as time interval
    model.IntX    =   TimeInt     ;
    model.SolX    =   SolX     ;
elseif isfield(model,'type') && strcmp(model.type,'SCSD')
    graph = false ;
    [LB,UB,SolX,model]  =   options.boundfun(model,IntX,DistX,graph,options.verify) ;
    model.IntX          =   IntX	;
    model.SolX          =   SolX    ;      
else
    [J,SolX]        =   options.boundupp(model,IntX) ;
    model.IntX      =   IntX        ;
    model.SolX      =   SolX        ;
end


% -------------------------------------------------------------------------
% 3. diagnostic output
if options.verbose
    approx      =   find(isnan(SolX))  ;
    exact       =   find(~isnan(SolX)) ;
    disp('Problem is feasible')
    disp(['     Exact variable indices: '   num2str(exact(:)')  ])
    disp(['     Bounded variable indices: ' num2str(approx(:)') ])
end
