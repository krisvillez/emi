function Model = SetupDynSCS( sk,order,GoThroughOrigin )

% -------------------------------------------------------------------------
% EMI Toolbox - SetupDynSCS.m
% -------------------------------------------------------------------------
% Description
%
% Setup a shape constrained spline function for dynamic simulation.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model = SetupDynSCS( sk,order,GoThroughOrigin )
%
% INPUT
%   sk                  :   Knots
%   order               :   Order of spline function
%   GoThroughOrigin     :   Boolean indicating whether function goes
%                           through zero
%
% OUTPUT
%   Model               :   Shape constrained spline model structure
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Setup Dynamic SCS model
if GoThroughOrigin
    assert(order==4,'Function going through origin only supported for cubic spline functions (order=4).')
end

nk                      =   length(sk)                                  ;
rng                     =	[   min(sk)	max(sk)	]                       ;
nx                      =   nk + order - 2                              ;   % number of basis functions
basis                   =   create_bspline_basis(rng, nx, order, sk(:))	;  % evaluate basis

Model.GoThroughOrigin	=	GoThroughOrigin                             ;
Model.basis0            =	basis                                       ;
if Model.GoThroughOrigin
    basis               =	putdropind(basis, 1)                        ;
end
Model.basis             =	basis                                       ;

B0                      =   eval_basis(sk,basis,0)                      ;
B1                      =   eval_basis(sk,basis,1)                      ;
B2                      =	eval_basis(sk,basis,2)                      ;

Model.B0k               =   B0                                          ;
Model.B1k               =   B1                                          ;
Model.B2k               =   B2                                          ;

end

