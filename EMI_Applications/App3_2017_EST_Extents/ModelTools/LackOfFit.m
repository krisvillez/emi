function [WSSR,Model,chat,yhat] = LackOfFit(Model,Theta)

% -------------------------------------------------------------------------
% EMI Toolbox - LackOfFit.m
% -------------------------------------------------------------------------
% Description
%
% Compute lack-of-fit objective function formulated in original data.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[WSSR,Model,chat,yhat] = ExtentFit(Model,Theta)
%
% INPUT
%   Model	:   Model description given as a structure (see ExtentDAE.m
%               for required structure)
%	Theta	:	Parameter vector
%
% OUTPUT
%   WSSR    :   weighted sum of squared residuals
%   Model   :   Model description with updated parameters, if applicable,
%               and the field 'simulation' including the simulation results
%   chat    :   Species, at measurement sampling intervals
%   yhat    :   Expected measurements
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Process simulation
[ xsim, cbarsim, csim, pHsim, Model ]	=	SimExtent( Model, Theta)	;

% Expected measurements
chat	=	csim(:,Model.Measurement.sample_index)              ;
yhat	=	Equation_Measurement(Model,chat)                    ;

% Evaluate objective function
ytilde	=	Model.data.ytilde                                   ;
sigma	=	Model.Measurement.sigma                             ;
WSSR	=	sum(  sum( ( diag(1./sigma)*( ytilde-yhat)).^2 ) )  ;

if nargout>=2
    Model.simulation.xsim       =	xsim        ;
    Model.simulation.cbarsim	=	cbarsim     ;
    Model.simulation.csim       =	csim        ;
    Model.simulation.pHsim      =	pHsim       ;
end