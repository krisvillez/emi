function A       =   ShapeConstraints2(Model,C2_L,C2_U,CImax,CIrise,CIfall)

% -------------------------------------------------------------------------
% EMI Toolbox - ShapeConstraints2.m
% -------------------------------------------------------------------------
% Description
%
% Generates constraint matrix for OUR
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	A	=	ShapeConstraints2(Model,C1_L,C1_U,CImax)
%
% INPUT
%   Model   :   Model as a structure 
%   C2_L    :   Lower bounds for the concentration
%   C2_U    :   Upper bounds for the concentration
%   CI_max  :   Concentration interval within which maximum rate is
%               obtained 
%   CIrise  :   Concentration interval within which rising inflection point
%               of the reaction rate is obtained 
%   CIfall  :   Concentration interval within which falling inflection
%               point of the reaction rate is obtained 
%
% OUTPUT
%   A       :   Constraint matrix
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

%   1. Evaluate monotonicity constraints (hard coded for Haldane kinetics):
%       Curvature of state -> Monotonicity of rate
Concave	=	C2_L>=CImax(2)                              ;
Concave =	all([ Concave(1:end-1) Concave(2:end)  ],2) ;
Convex	=	C2_U<=CImax(1)                              ;
Convex	=	all([ Convex(1:end-1) Convex(2:end)  ],2)   ;

A1      =	Model.A1Q(:,:)  ;

if or(any(Convex),any(Concave))
    A1	=	-diag(Concave)*A1+diag(Convex)*A1	;
    A1	=   A1(or(Convex,Concave),:)            ;
else
    A1	=	sparse(0,Model.n2)                  ;
end

%   2. Evaluate curvature constraints (hard coded for Haldane kinetics):
Convex2     =	or(C2_L>=CIrise(2),C2_U<=CIfall(1))                             ;
Convex2     =	all([ Convex2(1:end-2) Convex2(2:end-1) Convex2(3:end) ],2)     ;
Concave2	=	and(C2_L>=CIfall(2),C2_U<=CIrise(1))                            ;
Concave2	=	all([ Concave2(1:end-2) Concave2(2:end-1) Concave2(3:end) ],2)  ;

%         figure, hold on
%             stem(Model.T2(2:end-1),Convex2)
%             stem(Model.T2(2:end-1),Concave2)

if or(any(Convex2),any(Concave2))
    A2	=	Model.A2Q(:,:)                      ;
    A2	=	-diag(Concave2)*A2+diag(Convex2)*A2	;
    A2	=   A2(or(Convex2,Concave2),:)          ;
else
    A2	=	sparse(0,Model.n2)                  ;
end

%   3. Gather shape constraints:
A       =	[	A1	;	A2	]                   ;
