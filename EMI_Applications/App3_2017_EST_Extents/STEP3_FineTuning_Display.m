
% -------------------------------------------------------------------------
% EMI Toolbox - STEP3_FineTuning_Display.m
% -------------------------------------------------------------------------
% Description
%
% Visualize the final model simulations.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script displays the results obtained with \n' ...
            '''STEP3_FineTuning.m'' \n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Visualization choices

SensorSetup	=	3       ; % Select sensor setup  used for extent computation
% Rk          =   2       ;
% J           =	5       ; % number of candidate rate laws
GrayScale   =	true	;


% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Visualization parameters


Ylim	=	[-.365 5.865]               ;
FS      =   17  ;
LW      =   3	;
LW2     =   2	;
LW = [ 1 1 1 ];
MS = [ 11 7 7];
gN4mol	=	14	;
factor	=	[ gN4mol*ones(1,3) 1/2 ]    ;


if GrayScale
    Colours	=   {.73*ones(1,3),.55*ones(1,3),.37*ones(1,3),'k'} ; % For paper (gray-scale)
else
    Colours	=   {[0.635  0.078 0.184 ],[0.929 0.694 0.125],[0 0.447 0.741],'k'} ; % For demos
end

Strings =   {'^','s','v','o'}   ;
Lines	=	{'-','--',':','.'}   ;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Get results


FILEPATH	=	[	FOLDER	'STEP0_All' ]	;
load(FILEPATH)

ytrue = Model.simulation.ysim ;
ytilde0 = Model.data.ytilde ;

FILEPATH	=	[	FOLDER	'STEP3_FineTuning_Case' num2str(SensorSetup) '_BEFORE' ] ;
%FILEPATH	=	[	FOLDER	'STEP3_FineTuning'] ;
load(FILEPATH)
ytilde1 = [ Model.simulation.cbarsim(2:4,:) ; Model.simulation.pHsim] ;

FILEPATH	=	[	FOLDER	'STEP3_FineTuning_Case' num2str(SensorSetup) '_AFTER' ] ;
%FILEPATH	=	[	FOLDER	'STEP3_FineTuning'] ;
load(FILEPATH)

% -------------------------------------------------------------------------
% Some processing

tsim        =   Model.Measurement.tsim                  ;
csim        =   Model.simulation.csim                   ;
ytilde      =	Model.data.ytilde                       ;

tsim        =	tsim *24                                ;
tmeas       =	tsim(Model.Measurement.sample_index)    ;
ysim         =	Equation_Measurement(Model,csim)        ;

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Actual plotting

index = Model.Measurement.sample_index ;
tmeas = tsim(index);

fighan = figure;
set(gcf,'Position',[100 100 1000 800 ])
    
    grid on
    hold on
    set(gca,'FontSize',FS,'Xlim',[0 10],'Xtick',[0:10],'Ylim',Ylim,'Ytick',[0:.5:5.5])

    iy=1 ;
    %plot(tsim,csim(3,:)*gN4mol,'-','Linewidth',LW,'Color',Colours{iy})
    for iy=1:4
        plot(-1-tmeas,ytrue(iy,index)*factor(iy),'s-','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(1),'MarkerFaceColor','w')
        plot(-1-tmeas,ytilde1(iy,index)*factor(iy),'o-','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(2),'MarkerFaceColor','w')
        plot(-1-tmeas,ysim(iy,index)*factor(iy),'+-','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(3))
    end
    
    for iy=1:4
        plot(tsim,ytrue(iy,:)*factor(iy),'-','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(1))
        plot(tsim,ytilde1(iy,:)*factor(iy),'-','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(2),'MarkerFaceColor','w')
        plot(tsim,ysim(iy,:)*factor(iy),'-','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(3))
    end
    for iy=1:4
        plot(tmeas,ytilde1(iy,index)*factor(iy),'o','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(2),'MarkerFaceColor','w')
        plot(tmeas,ytrue(iy,index)*factor(iy),'s','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(1))
        plot(tmeas,ysim(iy,index)*factor(iy),'+','Color',Colours{iy},'LineWidth',LW(1),'MarkerSize',MS(3))
    end
    
    %plot(tmeas,Model.data.ytilde(1:3,:)*factor(1),'r+')
    %plot(tmeas,Model.data.ytilde(4,:)*factor(4),'ro')
    %plot(tmeas,ytilde0(1:3,:)*factor(1),'c+')
    %plot(tmeas,ytilde0(4,:)*factor(4),'co')
    
    xlabel({'Time [h]'})
    ylabel({'Nitrogen concentration [g N/L]'},'Position',[-1 mean(Ylim)])
    legend({...
        '$TAN$ - true', '$TAN$ - before', '$TAN$ - after',...
        '$TNO2$ - true', '$TNO2$ - before', '$TNO2$ - after',...
        '$TNO3$ - true', '$TNO3$ - before', '$TNO3$ - after',...
        '$pH$ - true', '$pH$ - before', '$pH$ - after',...
        },'fontsize',FS,'Color','w','Interpreter','Latex')

    
axpos = get(gca,'Position');
axes('Position',axpos,'Color','none')

Ylim = [-.73 11.73];
set(gca,'Xlim',[0 10],'Xtick',[0:10],'Xticklabel',[ ])
set(gca,'Ylim',Ylim,'Ytick',[0:1:11],'Yaxislocation','right')
ylabel({'pH [-]'},'Position',[10.5 5.5],'VerticalAlignment','middle','HorizontalAlignment','center')


    set(gca,'FontSize',FS)

    SaveMyFigure(fighan,'.\Figures\Results5_FineTuning')
    
    
    
% Evaluate objective function
ytilde	=	Model.data.ytilde                                   ;
sigma	=	Model.Measurement.sigma                             ;
WSSR	=	sum(  sum( ( diag(1./sigma)*( ytilde-ysim(:,Model.Measurement.sample_index))).^2 ) )  ;

    return
    