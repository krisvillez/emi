function [XsimL1,XsimU1,XsimL2,XsimU2,XsimL,XsimU]  =   ExtremalState(Model,Theta,Graph) 

% -------------------------------------------------------------------------
% EMI Toolbox - ExtremalState.m
% -------------------------------------------------------------------------
% Description
%
% Produces bounds to the relative substrate concentration.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[XsimL1,XsimU1,XsimL2,XsimU2,XsimL,XsimU]  =   
%										ExtremalState(Model,Theta,Graph) 
%
% INPUT
%   Model   :   Optimization problem as a structure
%   Theta  	:   Parameter vector set
%   Graph   :   Boolean indicating whether graph should be made
%
% OUTPUT
%   XsimL1  :   Lower bounds for the concentration (at concentration 
%					measurement sampling times)
%   XsimU1  :   Upper bounds for the concentration (at concentration
%					measurement sampling times)
%   XsimL2  :   Lower bounds for the concentration (at OUR measurement
%					sampling times)
%   XsimU2  :   Upper bounds for the concentration (at OUR measurement
%					sampling times)
%   XsimL   :   Lower bounds for the concentration (at all simulation
%					sampling times)
%   XsimU   :   Upper bounds for the concentration (at all simulation
%					sampling times)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------
[~,XsimL]   =   BatchSim(Model.Tsim,Model.fun,Theta(1,:))              ;
[~,XsimU]   =   BatchSim(Model.Tsim,Model.fun,Theta(2,:))              ;

XsimL       =   XsimL(:)            ;
XsimU       =   XsimU(:)            ;

XsimL       =   max(XsimL,XsimU)    ;
XsimU       =   min(XsimL,XsimU)    ;

if isfield(Model,'I1')
XsimL1      =   XsimL(Model.I1)     ;
XsimU1      =   XsimU(Model.I1)     ;

else
    XsimL1 = [];
    XsimU1 = [];
end


if isfield(Model,'I2')
    XsimL2      =   XsimL(Model.I2)     ;
    XsimU2      =   XsimU(Model.I2)     ;
else
    XsimL2 = [];
    XsimU2 = [];
end

if Graph
    figure(37), 
    hold on,
    set(gca,'FontSize',23)
    plot(Model.Tsim,1-XsimU,'r-','LineWidth',2)
    if isfield(Model,'I1')
        plot(Model.T1,1-XsimU1,'ro','MarkerSize',7,'MarkerFaceColor','r')
    end
    if isfield(Model,'I2')
        plot(Model.T2,1-XsimU2,'r+','MarkerSize',11)
    end
    Xlim = get(gca,'Xlim');

    grey = ones(1,3)*.5 ;
    plot(-Model.Tsim-10,1-XsimL,'-','Color',grey)
    
    plot(Model.Tsim,1-XsimL,'b:','LineWidth',2)
    if isfield(Model,'I1')
        plot(Model.T1,1-XsimL1,'bs','MarkerSize',7,'MarkerFaceColor','b')
    end
    if isfield(Model,'I2')
        plot(Model.T2,1-XsimL2,'bx','MarkerSize',11)
    end

    xlabel('Time [h]')
    ylabel('Relative concentration [-]')
    if exist('FigPrep'), FigPrep, end
    if exist('AxisPrep'), AxisPrep, end
    
    set(gca,'FontSize',23)
    legend({    
        '$\overline{s}(t)$',...
        '$\overline{\mathbf{s_{k_1}}}$=$\overline{\mathbf{s_{k_{TNN}}}}$',...
        '$\overline{\mathbf{s_{k_2}}}$=$\overline{\mathbf{s_{k_{OUR}}}}$',...
        '${s}(t)$',...
        '$\underline{s}(t)$',...
        '$\underline{\mathbf{s_{k_1}}}$=$\underline{\mathbf{s_{k_{TNN}}}}$',...
        '$\underline{\mathbf{s_{k_2}}}$=$\underline{\mathbf{s_{k_{OUR}}}}$'},...
        'Interpreter','latex')
    set(gca,'Xlim',Xlim)
    return
end