function [n_t,ctil_h] = SimulationExperiment (N_,M_eps,M,S,V,select_h,n_0,ratefun,t_,theta_) 

% -------------------------------------------------------------------------
% EMI Toolbox - SimulationExperiment.m
% -------------------------------------------------------------------------
% Description
%
%   This function executes the simulation of a data-generating experiment
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[n_t,ctil_h] = SimulationExperiment (N_,M_eps,M,S,V,select_h,n_0,ratefun,t_,theta_) 
%
% INPUT
%   N_          :   stoichiometric matrix
%   M_eps       :   output noise gain matrix
%   M           :   measurement gain matrix
%   S           :   number of species
%   V           :   volume of reactor
%   select_h	:   index of simulated time stamps with a measurement sample
%   n_0         :   initial state vector (molar masses)
%   ratefun     :   expression for the vector-valued rate function
%   t_          :   simulation time stamps
%   theta_      :   kinetic parameter vector
%
% OUTPUT
%	n_t         :   simulated molar masses at the simulation time stamps
%	ctil_h      :   simulated noisy measurements
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% Solve ODE:
[~,n_t]     =	ode15s(@(t,n_) N_'*ratefun(n_/V,theta_),t_,n_0 )    ;

% Post-processing
n_t         =	n_t'                ;   % transpose
c_h         =   n_t(:,select_h)/V	;   % select results at measurement sampling times
eps         =	M_eps*randn(S,M)	;   % simulate noise
ctil_h      =	c_h+eps             ;   % add noise

end

