function PlotWRMSR_validation(WRMSR,select_model,fig)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotWRMSR_validation.m
% -------------------------------------------------------------------------
% Description
%
% Plot WRMSR values.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	PlotWRMSR_validation(WRMSR,select_model,fig)
%
% INPUT
%   WRMSR           :   Matrix of WRMSR values   
%   select_model	:   Names associated with rows of WRMSR
%   fig             :   figure handle
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

fig                                                 ; 

nFun            =	size(WRMSR,1)                   ;
select_model{8}	=	'Peeters\newline& Eilers'       ;

if nFun == 1
    if isrow(WRMSR);
      WRMSR     =	vertcat(WRMSR,nan(size(WRMSR))) ;
    end
    b1          =	bar(WRMSR,'grouped')            ;
else
    b1          =	bar(1:nFun,WRMSR)               ;
end

set(gca,...
    'Xtick',1:nFun,...
    'Xticklabel',select_model,...
    'FontSize',18,...
    'Xlim',[0.5 (nFun+0.5)],...
    'Ylim',[0 .5],...
    'Ytick',0:0.1:2)

legend({'Best-fit conventional model','SCS2'},'Location','NorthWest')
xlabel('Simulated rate law')
ylabel('WRMSR [-]')

b1(1).FaceColor	=	'k'                             ; 
b1(2).FaceColor =	1/255 * [	128	128	128 ]       ; 

end

