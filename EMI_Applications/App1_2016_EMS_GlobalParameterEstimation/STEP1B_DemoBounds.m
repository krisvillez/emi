
% -------------------------------------------------------------------------
% EMI Toolbox - STEP1B_DemoBounds.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

A=sprintf('This program generates the figures used in the manuscript to demonstrate \n the bounding procedures. This is executed fast.\n');
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

% ============================================================
% SETTINGS

global optionsode
global options_fmincon
global options_lsqlin

% =========================================================================
% 0. Setup

% ---------------------------------------------------------------------
% Load problem setup

pulse = 4 ;
load(['Results\SETUP_PULSE' num2str(pulse)])

Graph       =   2        ;

% ---------------------------------------------------------------------
% Modify the feasible set for demonstration purposes:

IntX            =	[   zeros(1,3)                  ;
                        ones(1,3)               ]	;

Theta      =  [     0.05    1.6    1.6;
                    0.45    2.0    2.0];

Model.Intercept	=       Theta(1,:)                 ;
Model.Slope     =       Theta(2,:)-Theta(1,:)   ;

[CImax,CIrise,CIfall] =EvaluateTransitions(Theta) ;

% =========================================================================
% 1. Extremal simulations

if Graph>=2
    nFac = 3 ;
    nRep = nFac^3;
    iter=0;
    R = nan(nRep,3);
    for i1=1:nFac
        for i2=1:nFac
            for i3=1:nFac
                iter=iter+1;
                R(iter,:) = ([ i1 i2 i3]-1)/(nFac-1);
            end
        end
    end
    ThetaR = R*diag(Model.Slope)+ones(nRep,1)*Model.Intercept ;
    XX = nan(nRep,length(Model.Tsim)) ;
    DXX = nan(nRep,length(Model.Tsim)) ;
    for iRep=1:nRep
        [~,X]   =   BatchSim(Model.Tsim,Model.fun,ThetaR(iRep,:))              ;
        XX(iRep,:) = X;
        DXX(iRep,:) = Model.fun(1-X,ThetaR(iRep,:));
    end
    
end


% =========================================================================
% 2. Lower bound
[h_L,Alpha_L,Beta_L,g1_L,g2_L,Q2_L,Q2_U] = BatchSim_BoundLow(Model,IntX,[],Graph,Theta) ;

% =========================================================================
% 3. Upper bound
[h_U1,XM,XsimL,Alpha_U1,Beta_U1,g1_U1,g2_U1] = BatchSim_BoundUpp(Model,IntX,[],Graph,IntX(1,:))     ;
[h_U2,XM,XsimU,Alpha_U2,Beta_U2,g1_U2,g2_U2] = BatchSim_BoundUpp(Model,IntX,[],Graph,IntX(2,:))     ;


output = 1;
switch output
    case 1
        Alpha_U     =   [   Alpha_U1	Alpha_U2	]   ;
        [g1_U,loc]  =   min([g1_U1 g1_U2])              ;
        Alpha_U     =	Alpha_U(loc)                    ;
end

% =========================================================================
% 4. Evaluate objective function values for different alpha/beta values to
% demonstrate relaxed regression
if Graph>=2
    iter    =   0               ;
    switch output
        case 1
            Alphas  =   [0:.01:42]       ;
            
            nCoeff	=	length(Alphas)  ;
            
            C1_L    =   1-XsimL(Model.I1)   ;
            C1_U    =   1-XsimU(Model.I1)   ;
            CC1     =   1-XX(:,Model.I1)    ;
        case 2
            Betas   =   [0:1:700]              ;
            Betas   =   [ Betas 300:1:400 ]   ;
            Betas   =   sort(unique(Betas))     ;
            Betas   =   [   Beta_L(1)*ones(1,length(Betas)) ; Betas ];
            nCoeff	=	size(Betas,2)  ;
            
            C2_L    =   1-XsimL(Model.I2)   ;
            C2_U    =   1-XsimU(Model.I2)   ;
    end
    %gh_L_alt    =   nan(nCoeff,1)   ;
    gh_U1       =	nan(nCoeff,1)   ;
    gh_U2       =	nan(nCoeff,1)   ;
    
    
%     disp(['     Evaluating objective functions for ' num2str(nCoeff) ' values for the regression parameters'] )
    for iCoeff = 1:nCoeff
        switch output
            case 1
                alpha           =   Alphas(iCoeff)          ;
                EL              =	Model.Y1 - C1_L*alpha	;   % Prediction error
                EU              =	Model.Y1 - C1_U*alpha	;   % Prediction error
                gh_L(iCoeff)	=   RegBound(   alpha, ...
                    Model.Y1n, ...
                    C1_L./Model.sigma1, ...
                    C1_U./Model.sigma1 )./(Model.n1+Model.n2);
                for iRep=1:nRep
                    E	=	Model.Y1 - CC1(iRep,:)'*alpha                   ;   % Prediction error
                    gg(iCoeff,iRep) = sum((E./Model.sigma1).^2)./(Model.n1+Model.n2)                      ;   % LS objective function
                end
                
                gh_U1(iCoeff)	=   sum((EL./Model.sigma1).^2)./(Model.n1+Model.n2)                      ;   % LS objective function
                gh_U2(iCoeff)	=   sum((EU./Model.sigma1).^2)./(Model.n1+Model.n2)                      ;   % LS objective function
                
            case 2
                Xn_L = [ 1./Model.sigma2 Q2_L./Model.sigma2 ];
                Xn_U = [ 1./Model.sigma2 Q2_U./Model.sigma2 ];
                X_L = [ ones(length(Model.sigma2),1) DXX(1,Model.I2)' ];
                X_U = [ ones(length(Model.sigma2),1) DXX(end,Model.I2)' ];
                beta           =   Betas(:,iCoeff)	;
                EL              =	Model.Y2 - X_L*beta                   ;   % Prediction error
                EU              =	Model.Y2 - X_U*beta                   ;   % Prediction error
                gh_L(iCoeff)	=   RegBound(   beta, ...
                    Model.Y2n, ...
                    Xn_L, ...
                    Xn_U)./(Model.n1+Model.n2);
                for iRep=1:nRep
                    E	=	Model.Y2 - [ ones(length(Model.sigma2),1) DXX(iRep,Model.I2)'] *beta                   ;   % Prediction error
                    gg(iCoeff,iRep) = sum((E./Model.sigma2).^2)./(Model.n1+Model.n2)                      ;   % LS objective function
                end
                
                gh_U1(iCoeff)	=   sum((EL./Model.sigma2).^2)./(Model.n1+Model.n2)                      ;   % LS objective function
                gh_U2(iCoeff)	=   sum((EU./Model.sigma2).^2)./(Model.n1+Model.n2)                      ;   % LS objective function
                
                Alphas = Betas(2,:);
        end
        
        if output==2
            [h_U1,loc1] = min(gh_U1);
            [h_U2,loc2] = min(gh_U2);
            loc = [loc1 loc2];
            [g2_U,sel] = min([h_U1 h_U2]);
            loc =loc(sel);
            Beta_U= [ Beta_L(1); Alphas(loc) ];
        end
        
    end
%     disp(['     Evaluating objective functions finalized'] )
    
end


% =========================================================================
% 5. Vizualization

grey = ones(1,3)*.5 ;

if Graph>=2
    figure(37)
    set(gca,'FontSize',23)
    plot(Model.Tsim,1-XX,'-','Color',grey)
    figure(42)
    plot((1-XX)',DXX','-','Color',grey)
    %set(gca,'Xlim',[0 0.53])
    figure(73),
    subplot(3,1,1), set(gca,'Xticklabel',[])
    set(gca,'FontSize',23)
    plot(Model.Tsim,1-XX,'-','Color',grey)
    subplot(3,1,2), set(gca,'Xticklabel',[])
    set(gca,'FontSize',23)
    plot(Model.Tsim,1-XX,'-','Color',grey)
    subplot(3,1,3)
    set(gca,'FontSize',23)
    plot(Model.Tsim,DXX,'-','Color',grey)
end

% Repeat lower bound computation simply for a better appeal of the figures:
[h_L,Alpha_L,Beta_L,g1_L,g2_L,Q2_L,Q2_U] = BatchSim_BoundLow(Model,IntX,[],Graph,Theta) ;

% Relaxed regression
if Graph>=2
    figure(373), 
    if exist('FigPrep'), FigPrep, end
    if exist('AxisPrep'), AxisPrep, end
    set(gca,'FontSize',23)
    hold on,
    Col = [0 .6 .3];
    plot(Alphas,gg(:,1).^(1/2),'-','Color',grey)
    plot(Alphas,gh_U1.^(1/2),'b--','Linewidth',3)
    plot(Alphas,gh_U2.^(1/2),'r:','Linewidth',3)
    plot(Alphas,gh_L.^(1/2),'k-','Linewidth',3)
    
    switch output
        case 1
            plot(Alpha_L,g1_L.^(1/2),'mx','linewidth',3,'Color',Col,'MarkerSize',11)
            plot(Alpha_U,g1_U.^(1/2),'m+','linewidth',3,'Color',Col,'MarkerSize',11)
            set(gca,'Xlim',[0 42])
            set(gca,'Ylim',[-0.3 18.3])
        case 2
            plot(Beta_L(2),g2_L.^(1/2),'mx','linewidth',3,'Color',Col,'MarkerSize',11)
            plot(Beta_U(2),g2_U.^(1/2),'m+','linewidth',3,'Color',Col,'MarkerSize',11)
            set(gca,'Ylim',[-0.1 52.1])
    end
    
    ggmin_L = min(gh_L);
    insens = find(gh_L<=ggmin_L+1e-9);
    
    if exist('PlotVertical'),	    PlotVertical(Alphas([min(insens) max(insens)]),'-','LineWidth',2,'Color',grey), end
    
    plot(Alphas,gg.^(1/2),'-','Color',grey)
    if exist('PlotHorizontal'),	PlotHorizontal(ggmin_L.^(1/2),'-','Color',grey), end
    
    plot(Alphas,gh_L.^(1/2),'k-','Linewidth',3)
    plot(Alphas,gh_U1.^(1/2),'b--','Linewidth',3)
    plot(Alphas,gh_U2.^(1/2),'r:','Linewidth',3)
    switch output
        case 1
            plot(Alpha_L,g1_L.^(1/2),'mx','linewidth',3,'Color',Col,'MarkerSize',11)
            plot(Alpha_U,g1_U.^(1/2),'m+','linewidth',3,'Color',Col,'MarkerSize',11)
        case 2
            plot(Beta_L(2),g2_L.^(1/2),'mx','linewidth',3,'Color',Col,'MarkerSize',11)
            plot(Beta_U(2),g2_U.^(1/2),'m+','linewidth',3,'Color',Col,'MarkerSize',11)
    end
    
    switch output
        case 1
            xlabel('${\beta}_{1,1}$','Interpreter','latex')
            ylabel('$\sqrt{\frac{h_1}{K_1+K_2}}$','Interpreter','latex')
            
            legend({'$h_{1}(\mathbf{\theta},\beta_{1,1})$',...
                '$h_{1}(\overline{\mathbf{\theta}},\beta_{1,1})$',...
                '$h_{1}(\underline{\mathbf{\theta}},\beta_{1,1})$',...
                '$\underline{h_{1}}(\mathcal{T},\beta_{1,1})$',...
                '$h_{1}(\mathcal{T},\breve{\beta}_{1,1}(\mathcal{T}))$',...
                '$h_{1}(\underline{\mathbf{\theta}},\hat{\beta}_{1,1}(\underline{\mathbf{\theta}}))$',...
                'insensitive region',...
                },'Interpreter','latex','Location','SouthEast')
            Xlim = get(gca,'Xlim');
            set(gca,'Xtick',Xlim(1):2:Xlim(2))
        case 2
            xlabel('${\beta}_2$','Interpreter','latex')
            ylabel('$\sqrt{h_2}$','Interpreter','latex')
            
            legend({'$h_1(\mathbf{\theta},\mathbf{\beta})$',...
                '$h_1(\underline{\mathbf{\theta}},\mathbf{\beta})$',...
                '$h_1(\overline{\mathbf{\theta}},\mathbf{\beta})$',...
                '$\underline{h}_1(\mathbf{\Theta},\mathbf{\beta})$',...
                '$h_1(\mathbf{\Theta},\hat{\mathbf{\beta}}(\mathbf{\Theta}))$',...
                '$h_1(\underline{\mathbf{\theta}},\hat{\mathbf{\beta}}(\underline{\mathbf{\theta}}))$',...
                'insensitive region',...
                },'Interpreter','latex','Location','SouthWest')
            Xlim = get(gca,'Xlim');
            set(gca,'Xtick',Xlim(1):50:Xlim(2))
    end
end

drawnow
disp(sprintf(['All figures generated. \n']))

fighan = figure(37) ;
set(gca,'Ytick',0:.1:1)
SaveMyFigure(fighan,'.\Figures\Fig3_Bound_s')
fighan = figure(42) ;
SaveMyFigure(fighan,'.\Figures\Fig4_Bound_q')
fighan = figure(73) ;
SaveMyFigure(fighan,'.\Figures\Fig5_Bound_q2')
fighan = figure(373) ;
SaveMyFigure(fighan,'.\Figures\Fig6_Relax')

disp(sprintf(['All figures saved. \n']))

return