function [Trans,Feasible] = GetFeasibleSolution(Interval,DistBnd,Domain)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - GetFeasibleSolution.m
% -------------------------------------------------------------------------
% Description
%
% GetFeasibleSolution computes a solution by means of quadratic programming
% subject to linear inequality constraints.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Trans,Feasible] = GetFeasibleSolution(Interval,DistBnd,Domain)
%
% INPUT
%   Interval:   Matrix describing the bounds on the transitions. It must be
%               specified as 2-column matrix with each row corresponding to
%               one transition. The left (right) column specifies the lower
%               (upper) bounds for the transitions.
%               (required) 
%   DistBnd:    Matrix describing the bounds on the distances between
%               transitions. It must be specified as 2-column matrix with
%               each row corresponding to one transition. The left (right)
%               column specifies the lower (upper) bounds for the distances
%               between subsequent transitions. An empty entry is
%               interpreted as the absence of constraints.
%               (optional, default left bound = -inf, default right bound =
%               +inf) 
%   Domain:     Column vector describing the search space (domain of the
%               fitted spline function). 
%               (required)
%
% OUTPUT
%   Trans:      The proposed solution as a column vector (not necessarily
%               feasible). 
%   Feasible:   Boolean scalar indicating feasibility of the reported
%               solution.
%   
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

xL  =	Interval(:,1)   ;
xU  =   Interval(:,2)   ;

m   =   length(xL)  ;

if nargin<2 || isempty(DistBnd)
    DistBnd = [ -inf*ones(m-1,1) +inf*ones(m-1,1) ];
end

% 1. first attempt - middle of the intervals
xM		=	mean([xL xU],2) ;

% 2. second attempt - linearly constrained least squares
if any(diff(xM)<=0)
    
    e =     ones(m-1,1)	;
    A = [   -spdiags([  -e	+e	],	0:1, m-1, m)      ; ...
            +spdiags([  -e	+e	],	0:1, m-1, m)  ]   ;
    b = [   -DistBnd(:,1)     ;	...
            +DistBnd(:,2)	]   ;
    
%     Domain(1) =Interval(1,1) ;
%     Domain(end) =Interval(end,2) ;
    D   =   [  1 zeros(1,m-1); zeros(1,m-1) 1 ];
    
    H   =   (A'*A)+D'*D          ;
    f   =   -(Domain'*D)'            ;
    
    % adjust edges
    xQP     =   quadprog(H,f,A,b,[],[],xL,xU) ;
    xQP     =   xQP(:)   ;
    
    Feasible	=	~isempty(xQP)	;
    
    move        =   abs(xQP-xL)<1e-4    ;
    xQP(move)   =   xL(move)            ;
    
    move        =   abs(xQP-xU)<1e-4    ;
    xQP(move)   =   xU(move)            ;
    
    if Feasible,    Trans      =	xQP(:)          ;
    else            Trans      =	nan(m,1)        ;
    end
    
else
    Trans          =   xM      ;
    Feasible    =	true    ;
end
