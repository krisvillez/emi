function Model = FitSCS(Model)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - FitSCS.m
% -------------------------------------------------------------------------
% Description
%
% FitSCS is a function which optimizes the spline coefficients of the
% spline function, subject to specified shape constraints.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model = FitSCS(Model)
%
% INPUT
%	Model:     	Structure describing the fitted spline model. 
%               (required)
%
% OUTPUT
%	Model:     	Structure describing the fitted spline model with optimized
%               spline coefficients.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-06-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

verbose = false ;


y 		= 	Model.y				;
B       =   sparse(Model.B)     ;
W 		=	Model.W 			;
[ny,nx] =   size(B)     		;

inc     =   any(W,2)        ;
y_inc   =   y(inc)          ;
B_inc   =   B(inc,:)        ;
ny_inc  =   length(y_inc)   ;

if ~isfield(Model,'QR')

	% CLASSIC SMOOTHING SPLINE FUNCTION FITTING
    
    EstimMethod	=	1	;
    %   1:	Using analytical solution
    %   2:  Using Mosek. Use Mosek to verify whether Mosek operates
    %           correctly or when certain weights are zero.
    
    if ny_inc~=ny
        EstimMethod     =   2	;
    end
    
    % Estimation:
	switch EstimMethod
		case 1
		
			if isfield(Model,'L')
				L	=	sparse(Model.L)         ;
				x	=	(B_inc'*B_inc+L)^(-1)*B_inc'*y_inc	;
			else
				% Default: no penalty
				x	=	(B_inc'*B_inc)^(-1)*B_inc'*y_inc	;
			end
			
		case 2
			
			% TO DO: WEIGHT INCLUSION NOT VALIDATED YET

			if isfield(Model,'L')
	
				L 		= 	sparse(Model.L)				;
				[F,ns] 	=	ComputeF(L)					;
				
			else
				% Default: no penalty
				ns 		= 	0 							;
				F		= 	sparse(ns,nx) 				;
				
            end
            
			y_star	=	[	y_inc	;	sparse(ns,1)	]	;
			B_star 	= 	[	B_inc   ; 	F 				]	;
    
			% numbers
			nt 		=	1           ;	% number of variables in objective function
			ne 		=	ny_inc+ns 	;	% number of variables needed for conic formulation of unconstrained quadratic problem. This is equal to the number of measurements (ny) plus the number of non-zero singular values (ns) of the penalty matrix (L).
    
			% Transform QP into conic program			
			[aa,cc,blc,buc,cones] 	=	QPasConic(B_star,y_star,nx,ne,nt) 	;
			
			% Solve conic program
			xet 					=	SolveMosek(aa,cc,blc,buc,cones) 	;
			x 						= 	xet(1:nx)                      		;
	
    end
	
	% Save result:	
	Model.x 	=	x 					;
	Model.W 	=	W 					;
	
else
	
	% TO DO: WEIGHTS NOT INCLUDED YET
	
    if verbose, disp('check 1'), end
    
	% GET REQUIRED ELEMENTS FROM Model STRUCTURE
	tk          =   Model.tk    ;
    
	ty          =   Model.ty    ;
	order       =   Model.Order	;

    QR          =	Model.QR    ;
    
    % CHECK INPUTS
    qr_order    =   size(QR,2)-3	;
    assert(qr_order==order,'QR dimensions do not match spline function order')
    
    qr_time     =	QR(:,1:2)'      ;
    qr_time     =	qr_time(:)      ;
    
    assert(all(diff(qr_time)>=0),'Chronological order invalid in QR field')
    
    % HANDLE DISCONTINUITIES
%     QR
    if ~(isfield(Model,'lowbound') && Model.lowbound)
        disc_deg	=	QR(2:end,end)           ;
%         disc_deg
        
        if any(disc_deg>0)
            
            if isfield(Model,'ArgInc')
                ArgInc      =   Model.ArgInc* Model.t_scale+Model.t_ref  ;
            else
                ArgInc =    [];
            end
            
            
            QR          =   Model.QR * Model.t_scale+Model.t_ref ;
            disc_loc	=	QR(2:end,1)             ;
            disc_loc	=	disc_loc(disc_deg>0)	;
            
            y           =   Model.y         ;
            tk          =   Model.tk * Model.t_scale+Model.t_ref ;
            
            ty          =   Model.ty * Model.t_scale+Model.t_ref ;
            order       =   Model.Order     ;
            lambda      =   Model.Lambda	;
            
            excl        =   ismember(tk,disc_loc)   ;
            tk          =   tk(~excl)               ;
            
            for idisc = 1:length(disc_loc)
                tk  =   [	tk	;   repmat(disc_loc(idisc),[ disc_deg(idisc) 1])	];
            end
            tk      =   sort(tk) ;
            
            Model   =	CreateSCS(y,ty,tk,order,lambda,ArgInc)	;
            
            y 		= 	Model.y				;
            B       =   sparse(Model.B)     ;
            W 		=	Model.W 			;
            [ny,nx] =   size(B)     		;
            
            inc     =   any(W,2)        ;
            y_inc   =   y(inc)          ;
            B_inc   =   B(inc,:)        ;
            ny_inc  =   length(y_inc)   ;
            
            
            Model  =   ApplyQR(Model,QR)	;
            
        end
    end
    
    if verbose, disp('check 2'), end
        
    % --------------
	% HANDLE ZERO VALUED DERIVATIVE
    % Modify QR so that zero derivatives are also enforced at higher
    % derivatives
    for PolyOrder   =  1:order           ;
        ZeroValued = and(QR(:,end-PolyOrder)==0,QR(:,2)~=QR(:,1)) ; 
		% Condition1: Zero-valued derivative
		% Condition2: Zero-valued derivative in a single point is kept as is
        if any(ZeroValued)
            QR(ZeroValued,end-PolyOrder:end-1) = 0 ;
        end
    end
    
    if verbose, disp('check 3'), end
    % --------------
    % SETUP UNCONSTRAINED PROBLEM
    
    if isfield(Model,'L')
        
        L 		= 	sparse(Model.L)				;
        [F,ns] 	=	ComputeF(L)					;
        
    else
        % Default: no penalty
        ns 		= 	0 							;
        F		= 	sparse(ns,nx) 				;
        
    end
    
    y_star	=	[	y_inc	;	sparse(ns,1)	]	;
    B_star 	= 	[	B_inc	; 	F 				]	;
    
    % numbers
    nt 		=	1           ;	% number of variables in objective function
    ne 		=	ny_inc+ns 	;	% number of variables needed for conic formulation of unconstrained quadratic problem. This is equal to the number of measurements (ny) plus the number of non-zero singular values (ns) of the penalty matrix (L).
    nxet 	=	nx+ne+nt    ; % total number of variables in program (so far)
    
    % Transform QP into conic program
    [aa,cc,blc,buc,cones] 	=	QPasConic(B_star,y_star,nx,ne,nt) 	;
    %size(aa)
    
    if verbose, disp('check 4'), end
    % --------------
	% Shape constraints
	
    nq              =   size(QR,1)      ;

    B_shape         =   sparse(0,nx)    ;
    blc_shape       =   sparse(0,1)     ;
    buc_shape       =   sparse(0,1)     ;
    
	nz_tot			=	0				; % nz_tot keeps track of how many additional variables have been added
    
    % Initialize:
    nc3     =   0                   ;
    np3     =   0               ;   % number of polynomial coefficients in constrained polynomials of order 3
    nz3     =   0               ;   % number of addittional variables for order-3 SOC constraints
    B3      =   sparse(np3,nx)  ;
    C3      =   sparse(np3,nz3) ;
    C3ineq  =   sparse(nc3,nz3) ;
    cones3  =   cell(0)         ;
    
    nc4     =   0                   ;
    np4     =   0                   ;   % number of polynomial coefficients in constrained polynomials of order 4
    nz4     =   0                   ;   % number of addittional variables for order-4 SOC constraints
    B4      =   sparse(np4,nx)  ;
    C4      =   sparse(np4,nz4) ;
    cones4  =   cell(0)         ;
            
    if verbose, disp('check 5'), end
    
%     max(B_shape(:))
        
    for PolyOrder   =  1:order           ;
        if verbose, disp(['     PolyOrder: ' num2str(PolyOrder)]), end
        
        if PolyOrder>=5
            QRone = QR(:,[1 2 end-PolyOrder]) ;
            if any(~isnan(QRone(:,3)))
                warning('FitSCS:HighOrder', 'Because SDP constraints are not implemented, shape constraints for derivative functions which are of order 5 or higher are ignored')
            end
        elseif and(order>=PolyOrder,PolyOrder<=inf)
            
            % 1. List episodes for the derivative of corresponding order
            QRone = QR(:,[1 2 end-PolyOrder]) ;
			
            % 2. Merge episodes where possible, this avoids redundant
            % constraints
            for iq=nq:-1:2
                condition(1)  =   QRone(iq,1)==QRone(iq-1,2) ; % times match
                condition(2)  =   QRone(iq,3)==QRone(iq-1,3) ; % shapes match
                if all(condition) % merge the shapes
                    newendtime = QRone(iq,2) ;
                    QRone = QRone([1:iq-1 (iq+1):end],:) ;
                    QRone(iq-1,2) = newendtime ;
                end
            end
			
            % 3. Convert list of episodes into list of linear constraints
            nq      =   size(QRone,1)                                           ;
            tcon    =   unique(sort([ Model.tk(:) ; QRone(:,1); QRone(:,2) ]))  ;
            ncon    =   length(tcon)                                            ;
            scon    =   nan(ncon,1)                                             ;
            for iq=1:nq
                if ~isnan(QRone(iq,3)) % there is a constraint
                    if or(and(PolyOrder==1,QRone(iq,1)~=QRone(iq,2)),QRone(iq,3)==0)
                        % piece-wise constant function: this avoids extending the
                        % constraint to next interval
                        scon(and(tcon>=QRone(iq,1),tcon<QRone(iq,2))) = QRone(iq,3) ; % assign sign of constraint
                    else
                        scon(and(tcon>=QRone(iq,1),tcon<=QRone(iq,2))) = QRone(iq,3) ; % assign sign of constraint
                    end
                end
            end
%             [ tcon scon ]
            
            % 4. Add (linear) equality constraints
            inc0 = scon==0                      ;
            if any(inc0)
                con         =   [tcon(inc0) scon(inc0) ]        ;
                con         =   unique(con,'rows')              ;
                ncon        =   size(con,1)                     ;
                Bcon0       =   eval_basis(con(:,1),Model.basis,order-PolyOrder)	; % obtained required basis for specified coefficients and derivative d
                B_shape     = [ B_shape ; Bcon0]        ;
                blc_shape   = [ blc_shape ; zeros(ncon,1) ] ;
                buc_shape   = [ buc_shape ; zeros(ncon,1) ] ;
                
%                 con(:,1)
%                 max(Bcon0(:))
%                 figure, plot(Bcon0,'.-')
			else
				ncon = 0;
            end
            
            % 5. Add inequality constraints
            
                 
            %   5.1. Add linear inequality constraints 
            %       1.  THESE CONSTRAINTS ARE SUFFICIENT FOR ORDER 1 AND 2. 
            %       2   FOR ORDERS 3 AND 4 THE CONIC CONSTRAINTS BELOW ARE
            %       SUFFICIENT AND NECESSARY. ADD THESE LINEAR CONSTRAINTS
            %       ANYWAY. THIS LEADS TO A MORE ACCURATE NUMERICAL
            %       SOLUTION.
            inc1    =   and(~isnan(scon),~inc0) ;  % DIFFERENT FROM ABOVE
            if any(inc1)
                con = [tcon(inc1) scon(inc1) ]      ;
                ncon = size(con,1) ;
                Bcon1  	=   eval_basis(con(:,1),Model.basis,order-PolyOrder)	; % obtained required basis for specified coefficients and derivative d
                Bcon1   =   diag(con(:,2))*Bcon1 ;  % DIFFERENT FROM ABOVE
                B_shape     = [ B_shape ; Bcon1] ;
                blc_shape   = [ blc_shape ; zeros(ncon,1) ];
                buc_shape   = [ buc_shape ; inf(ncon,1) ] ; % DIFFERENT FROM ABOVE
            else
                ncon = 0;
            end
            
            switch PolyOrder
                case {1,2}
                    
                case {3,4}
                    % 5.2. Add second order cone constraints: 
                    %           polynomials of order 3 (quadratic) or order 4 (cubic)
                    
                    if any(abs(QRone(:,3))==1)
                        
                        % A. List individual polynomial sections which are constrained
                        tc      =   unique(sort([   QRone(1:end,1) ;...
                                                    QRone(1:end,2) ; tk(:) ])) ; % time locations for constraint intervals
                        nc 		=   length(tc)-1						;
                        index_k =   nan(nc,1) ;
                        index_q =   nan(nc,1) ;
                        inc     =   false(nc,1) ;
                        for ic=1:nc
                            index_k(ic)     =   find(tk<=tc(ic),1,'last')       ;
                            index_q(ic)    	=   find(QRone(:,1)<=tc(ic),1,'last')  ;
                            inc(ic)         =   tc(ic)<QRone(index_q(ic),2) ;
                        end
                        a   =   (tc(1:end-1)-tk(index_k))	;	%./(tk(index_k+1)-tk(index_k)) ;
                        b   =   (tc(2:end)-tk(index_k))		;	%./(tk(index_k+1)-tk(index_k)) ;
                        s 	=	QRone(index_q,3)            ;
                        SC 	=   [	index_k	a	b	s	]   ;
                        SC 	= 	SC(inc,:)                   ;
                        SC 	= 	SC(or(SC(:,4)==+1,SC(:,4)==-1),:) ;
                        nSC =	size(SC,1)                  ;
                    
                        % B. List conic constraints
                        n_cone	=	min(2,(PolyOrder-2)) 	;
                        cones 	=	cell( n_cone*nSC ,1)	;
                        for i_cone=1:n_cone
                            for iSC=1:nSC
                                row     =	iSC+(i_cone-1)*nSC      ;
                                col     =   iSC+(i_cone-1)*nSC*3    ;
                                cones{row}.type   =   'MSK_CT_QUAD'	;
                                switch PolyOrder
                                    case 3
                                        sub = 	[ nxet+nz_tot+col-1+(1:nSC:nSC*3)  ]   ;
                                    case 4
                                        sub = 	[ nxet+nz_tot+col-1+(1:nSC:nSC*3)  ]   ;
                                    otherwise
                                        error('FitSCS:Logic','Logical fault in section 6.3')
                                end
                                cones{row}.sub    = sub ;
                                % Select x_00, x_01, and x_11 appearing in each equality constraint above
                            end
                            
                        end
                        
                        mX	= 	floor((PolyOrder-1)/2)+1 	; % mX = dimension of X matrix minus 1
                        mY 	=	floor((PolyOrder-2)/2)+1 	; % mY = dimension of Y matrix minus 1
                        nX 	=	(mX) + (mX-1)*mX/2  	;
                        nY	=	(mY) + (mY-1)*mY/2  	;
                        nc  =   nSC*n_cone              ;
                        np  =   nSC*PolyOrder           ;
                        nz	=   (nX+nY)*nSC             ;
                        nz_tot	=   nz_tot + nz ;
                        
                        % C. List equality constraints for added variables
                        
                        I   =   speye(nSC)      ;   %   Identity matrix
                        O   =   sparse(nSC,nSC) ;   %   Zero matrix
                        
                        
                        
                        % C.0 Extract parameters for each SOC
                        index_k =	SC(:,1) ;
                        a       =	SC(:,2) ;
                        b       =	SC(:,3) ;
                        s       =	SC(:,4)	;
                        
                        switch PolyOrder
                            case 3
                                
                                % C.3.1. Rename variable for future use
                                cones3  =   cones	;
                                
                                nc3 =   nc     ;
                                np3 =   np   ;
                                nz3	=   nz	;
                                
                                % C.3.2. Left hand side of equality
                                % constraints: coefficients for polynomial
                                % coefficients (multiplied by -1)
                                B3 	=	[ 	-diag(s)*Model.X2P_3{3}(index_k,:)	;	...		% p0
                                            -diag(s)*Model.X2P_3{2}(index_k,:)	;	... 	% p1
                                            -diag(s)*Model.X2P_3{1}(index_k,:)	];			% p2
                                
                                % C.3.3. Right hand side of equality
                                % constraints: coefficients for added
                                % variables
                                %			x_00	x_01	x_11 	y_00
                                C3 	=	[	I		O       O       -diag(a.*b)	;	...	% p0
                                            O		2*I     O       +diag(a+b)	;	...	% p1
                                            O		O		I       -I           ]	;	% p2
                                
                                % Equations:
                                % p0 = [ 1 	0	0 	-a*b ] * [	x_00	x_01 	x_11	y_00 ]' ; % p0
                                % p1 = [ 0	2 	0 	a+b	 ] * [	x_00 	x_01 	x_11	y_00 ]' ; % p1
                                % p2 = [ 0	0 	1	-1 	 ] * [	x_00 	x_01	x_11	y_00 ]' ; % p2
                                
                                % C.3.4. Add linear inequalities for y_00 variables:
                                C3ineq  =   [ sparse(nSC,nSC*3)     speye(nSC) ] ;
                                
                            case 4
                                
                                % C.4.1. Rename variable for future use
                                cones4  =   cones	;
                                nc4 =   nc     ;
                                np4 =   np   ;
                                nz4	=   nz	;
                                
                                % C.4.2. Left hand side of equality
                                % constraints: coefficients for polynomial
                                % coefficients (multiplied by -1)
                                B4 	=	[ 	-diag(s)*Model.X2P_4{4}(index_k,:)	;	...		% p0
                                            -diag(s)*Model.X2P_4{3}(index_k,:)	;	... 	% p1
                                            -diag(s)*Model.X2P_4{2}(index_k,:)	;	... 	% p2
                                            -diag(s)*Model.X2P_4{1}(index_k,:)	];			% p3
                                
                                %                                 % C.3. Right hand side of equality
                                %                                 % constraints: coefficients for added
                                %                                 % variables
                                %
                                aaa = spdiags(a,0,nSC,nSC);
                                bbb = spdiags(b,0,nSC,nSC);
                                
                                % C.4.3. Right hand side of equality
                                % constraints: coefficients for added
                                % variables
                                %			x_00 	x_01	x_11 	y_00	y_01    y_11
                                C4 	=	[	-aaa      O   	O       +bbb   	O       O   	;	...	% p0
                                            +I    	-aaa*2  	O       -I   	+bbb*2    O       ;	...	% p1
                                            O		+I  	-aaa      O    	-I    +bbb      ;	...	% p1
                                            O		O     	+I      O    	O       -I      ]	;	% p2
%                                 
                                % Equations:
                                %
                                % p0	= [ -a      0   	0       +b      0       0   ] * z' ;    % p0
                                % p1    = [ +1      -2.a 	0       -1      +2.b	0	] * z' ;	% p1
                                % p2    = [ 0       +1      -a      0       -1      +b	] * z' ;	% p2 
                                % p3    = [	0       0       +1      0       0       -1  ] * z' ;	% p3
                                %
                                % with z:
                                % z     = [	x_00    x_01    x_11	y_00    y_01    y_11 ] ; 
                                
                            otherwise
                                % Statements above prevent this case to be valid but just in case:
                                error('FitSCS:Logic','Logical fault in section 6.2')
                        end
                        
                    end
                otherwise
                    error('FitSCS:NoSDP','SDP constraints for shape constraints on polynomials of order 5 or higher are not implemented')

            end

        end
%         PolyOrder
%         max(B_shape(:))
    end

    n_shape     =   size(B_shape,1) ;
    
    %     nx		%	number of variables of interest
    %     ny		%	number of measurements
    %     nt		% 	number of variables in linear sum objective function
    %     n_shape	%	number of constraints defining sign of 1st and 2nd order polynomials
    %     nc3		%	number of conic constraints for 3rd order polynomials
    %     nc4		%	number of conic constraints for 4th order polynomials
    %     np3		%	number of shape constrained polynomial coefficients for 3rd order polynomials
    %     np4		%	number of shape constrained polynomial coefficients for 4th order polynomials
    %     nz3		%	number of additional variables for conic constraints for 3rd order polynomials 
    %     nz4		%	number of additional variables for conic constraints for 4th order polynomials
    
    % --------------------------------
    
    % Aggregate cone constraints:
    cones           =   cell(1+nc3+nc4,1)                   ;
    cones{1}.type   =   'MSK_CT_QUAD'               ;
    cones{1}.sub    = [ nx+ne+nt    nx+(1:ne)  ]    ;   % making use of transformed t variables, as conic program
	
    cones(2:nc3+1)          =   cones3(:)   ;
    cones((2:nc4+1)+nc3)    =	cones4(:)   ;
    for ic=1:length(cones)
        vec = cones{ic} ;
        VEC(ic,vec.sub) = true ;
    end
    
%     figure
%     imagesc(VEC);
%     colormap(gray);
%     grid on;
    
	
	% ----------------------
	% MATRIX/VECTOR ORGANIZED BY VARIABLES:
	%			x			y , e 				t 					z3					z4
	
    % Aggregate objective function:
    
    % size(cc)
    cc  =    [  cc(:)'                         						sparse(1,nz3)       sparse(1,nz4)    	]' 	;
    
    % Aggregate linear constraint (left hand side):
    
    % size(aa)
    aa  =   [   aa        											sparse(ne,nz3)      sparse(ne,nz4)          ; ...
                B_shape  	sparse(n_shape,ne)	sparse(n_shape,nt)  sparse(n_shape,nz3) sparse(n_shape,nz4)     ; ...
                sparse(nc3,nx+ne) 				sparse(nc3,nt)      C3ineq              sparse(nc3,nz4)         ; ...
                B3        	sparse(np3,ne)      sparse(np3,nt)      C3                  sparse(np3,nz4)         ; ...
                B4        	sparse(np4,ne)      sparse(np4,nt)     	sparse(np4,nz3)     C4                  ] 	;
    
    
	% ----------------------
	% VECTOR ORGANIZED BY EQUATIONS:
	%			QP		1st/2nd order 	3rd order ineq.		3rd/4th order eq.
	
	% Aggregate linear constraint (right hand side):
    blc	=   [	blc ; 	blc_shape	;	sparse(nc3,1)	;	sparse(np3+np4,1)	]	;
    buc =   [	buc	;	buc_shape 	;	inf(nc3,1) 		;	sparse(np3+np4,1)   ]  	;
    
	
	% ----------------------
	% SOLVE:
	
	% Solve conic program
	xet 		=	SolveMosek(aa,cc,blc,buc,cones) 	;
	
	% Save result
	x 			= 	xet(1:nx)                           ;
	Model.x     =	x 					;
	
end


% Model evaluation:
Model 		= 	QualitySCS(Model)	;
    
    

% ---------------------------------------------------------
function [F,ns] 	=	ComputeF(L)		;
				
				[U,S,oo]   =	svd(full(L),0)  ; % complete output necessary for support in Octave. Do not remove the 'oo' output variable
				inc=diag(S)>0;
				U=U(:,inc) ;
    S=S(inc,inc) ;
    ns =sum(inc) ;
    F       =	(U*diag(diag(S).^(1/2)))'  ;
	
	
	
function [aa,cc,blc,buc,cones] 	=	QPasConic(B_star,y_star,nx,ne,nt) 	;
			
			cones           =   cell(1,1)                   	;
			cones{1}.type   =   'MSK_CT_QUAD'               	;
			cones{1}.sub    = [ nx+ne+nt    nx+(1:ne)  ]   		;
			cc 				= [ zeros(1,nx+ne) 1]' 				;
			aa  			= [	B_star -eye(ne)  zeros(ne,1) ] 	;
			blc				=	y_star							;
			buc				=	y_star							;	
			
function	xet 	=	SolveMosek(aa,cc,blc,buc,cones) ;

	clear prob
	
    prob.a      =   aa    ;
    prob.blc    =   blc   ;
    prob.buc    =   buc   ;
    prob.c      =   cc      ;
    prob.cones	=   cones   ;
    try
        [r,res]     =   mosekopt('minimize echo(0)',prob)   ;
    xet         =   res.sol.itr.xx                      ;
    catch
        [r,res]     =   mosekopt('minimize echo(3)',prob)   ;
    xet         =   res.sol.itr.xx                      ;
    end