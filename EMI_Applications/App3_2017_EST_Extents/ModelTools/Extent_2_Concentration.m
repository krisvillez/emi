function [c,cbar,pH] = Extent_2_Concentration(Model,x,pHguess)

% -------------------------------------------------------------------------
% EMI Toolbox - Extent_2_Concentration.m
% -------------------------------------------------------------------------
% Description
%
% Computes species concentrations, kinetic species and equilibrium
% component concentrations, and the pH from the extents of reactions.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[c,cbar,pH] = Extent_2_Concentration(Model,x,pHguess)
%
% INPUT
%   Model       :   Model description given as a structure (see ExtentDAE.m
%                   for required structure)
%   x           :	Extents of reactions
%   pH guess	:	Initial guess for equilibrium pH
%
% OUTPUT
%	c       :	Species concentrations
%	cbar    :	Kinetic species and equilibrium component concentrations
%	pH      :   pH, defined as -log([H+])
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

global optfzero

[Rk,H]=size(x) ;
if length(pHguess)==1 && H~=1
    pHguess     =   repmat(pHguess,[1 H])   ;
end

nbar	=	Model.Nbar_k'*x + repmat(Model.nbar0,[1 H]) ;
cbar    =   nbar/Model.Volume                ;


pH      =   nan(1,H)    ;
for h=1:H
    
    balance =	@(pH) Equation_Constraint( Model , cbar(:,h) , pH )       ;
    pH(h)	=	fzero(@(a) balance(a),pHguess(h),optfzero)  ;
    
end

c = Extent_pH_2_Concentration(Model,pH,cbar) ;

end

