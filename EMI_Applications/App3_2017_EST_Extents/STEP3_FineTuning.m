
% -------------------------------------------------------------------------
% EMI Toolbox - STEP3_FineTuning.m
% -------------------------------------------------------------------------
% Description
%
% Optimize the parameters of the selected rate laws simultaneously.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script executes model fine-tuning with the selected \n' ...
            'rate laws for each reaction \n' ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% -------------------------------------------------------------------------
% Choices

SelectedRateLaws = [ 5 3];
SensorSetup     = 3 ; % Select sensor setup  used for extent computation

FILEPATH     =   [	FOLDER 'NumericSetup' ] ;
load(FILEPATH);

% =========================================================================
% =========================================================================
% =========================================================================

disp(' ')
disp('==================================')
disp(['   Compiling Model'])
disp('==================================')
disp(' ')

FILEPATH      =   [	FOLDER 'STEP1_ExtentComputation_Case' num2str(SensorSetup) ] ;  
load(FILEPATH)

Rk              =	size(Model.Nbar_k,1)    ;
nTheta          =   0                       ;
Theta0          =   nan(73,1)               ;

for i=1:Rk  % for each extent
    
    j           =	SelectedRateLaws(i)	;
    
    % -------------------------------------------------------------------------
    % Load extent modelling result
    
    FILEPATH = [ FOLDER 'STEP2_Case' num2str(SensorSetup) '_Reaction' num2str(i) '_RateLaw' num2str(j) ]	;
    load(FILEPATH)

    % -------------------------------------------------------------------------
    % Some processing
    
    Reactions{i}            =	ExtentModel.Reactions{1}	;
    ntheta                  =	Reactions{i}.ntheta         ;
    Theta2r                 =	nTheta+(1:ntheta)           ;
    Reactions{i}.Theta2r	=	Theta2r                     ;
    nTheta                  =	nTheta+ntheta               ;
    Theta0(Theta2r,1)       =	Reactions{i}.theta          ;
    
end

Theta0          =	Theta0(1:nTheta)	;
Model.Reactions	=	Reactions           ;

[WSSR,Model,chat,yhat]          =	LackOfFit(Model,Theta0)	;


disp(' ')
disp('==================================')
disp(['   Saving Results'])
disp('==================================')
disp(' ')

FILEPATH = [ FOLDER 'STEP3_FineTuning_Case' num2str(SensorSetup) '_BEFORE' ];
save(FILEPATH,'Model')

% =========================================================================
% =========================================================================
% =========================================================================

disp(' ')
disp('==================================')
disp(['   Local Optimization'])
disp('==================================')
disp(' ')

tsim            =	Model.Measurement.tsim                  ;
ty              =   tsim(Model.Measurement.sample_index)	;

WRMSR           =	@(t) LackOfFit(Model,t)                 ;

optionsfminsearch               =	optimset('fminsearch')	;
optionsfminsearch.Display       =	'iter'                  ;
optionsfminsearch.MaxFunEvals	=	inf                     ;
optionsfminsearch.MaxIter       =	inf                     ;

[Theta1,fval,exitflag,output]	=	fminsearch(@(t) WRMSR(t),Theta0,optionsfminsearch)	;

% =========================================================================
% =========================================================================
% =========================================================================

disp(' ')
disp('==================================')
disp(['   Evaluation'])
disp('==================================')
disp(' ')

[WSSR,Model,chat,yhat]          =	LackOfFit(Model,Theta1)	;

% =========================================================================
% =========================================================================
% =========================================================================

disp(' ')
disp('==================================')
disp(['   Saving Results'])
disp('==================================')
disp(' ')

FILEPATH = [ FOLDER 'STEP3_FineTuning_Case' num2str(SensorSetup) '_AFTER' ];
save(FILEPATH,'Model')


% =========================================================================
% =========================================================================
% =========================================================================

STEP3_FineTuning_Display

% =========================================================================
% =========================================================================
% =========================================================================

