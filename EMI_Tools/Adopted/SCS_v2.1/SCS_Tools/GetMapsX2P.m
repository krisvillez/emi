function MapX2P = GetMapsX2P(Bases,PolyOrder)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - GetMapsX2P.m
% -------------------------------------------------------------------------
% Description
%
% This function evaluates the matrices which project the function
% coefficients to polynomial coefficients.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	MapX2P = GetMapsX2P(Bases,PolyOrder)
%
% INPUT
%   Bases:      Array of matrices describing the functional basis. Each
%               element in the array corresponds to a single function or
%               function derivative.
%               (required)
%   PolyOrder:  Maximal polynomial order for which the polynomial
%               coefficient basis needs to be computed
%               (required)
%
% OUTPUT
%   MapX2P:     Array of matrices mapping the function parameters to the
%               polynomial coefficients.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

[order]=length(Bases) ;

MapX2P  =   cell(PolyOrder,1)         ;

for term = 1:PolyOrder
	MapX2P{term}           =   1./factorial(PolyOrder-term) *Bases{end-(term-1)}(1:end-1,:) ; % n-c: 4 .. 1
end



