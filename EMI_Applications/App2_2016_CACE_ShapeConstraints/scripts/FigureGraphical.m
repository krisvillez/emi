
% -------------------------------------------------------------------------
% EMI Toolbox - FigureGraphical.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces the graphical abstract of [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-10-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

disp(['===================== GRAPHICAL ABSTRACT ====================='])

SS      =       0:.01:25         ;
ratefunMonod = @(S,pars) pars(1).*S./(S+pars(2))         ;
true_growth_rate = ratefunMonod(SS,pars);

% ------------- FIGURE GA ----------------

figGA = figure(100);
figure(100), hold on, box on

plot(SS,true_growth_rate,'k','LineWidth',2)


knots9 = [0:25/(2^3):25]';
[model_9knots] = FitSCStoRates(SS,true_growth_rate,'C',knots9);
theta = model_9knots.x;

for i = 1:length(theta)
    plot(SS,theta(i)*model_9knots.B0ss(:,i),'-','Color',1/255*[165 153 153],'LineWidth',1.5), hold on
end
    

axis ([0,25, 0, .12])
% plot (x6, x6basismat, 'k-','LineWidth',2); 

disp('Editing the figure.')

set(gca,'XTick',0:5:25,'YTick',0:.02:.12)
set(gca,'FontSize',20)
xlabel('substrate concentration S [mg N/L]')
ylabel({'growth rate r(S) [1/day]', 'basis functions b_0(S)'})
set(figGA,'name','Figure Graphical Abstract','numbertitle','off')

disp('---Finished---')