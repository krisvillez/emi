function fathom      =   Fathoming(Tree,Alive)  

% -------------------------------------------------------------------------
% Spike_O Toolbox - Fathoming.m
% -------------------------------------------------------------------------
% Description
%
% This function evaluates which nodes are to be fathomed as part of the
% branch-and-bound algorithm
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	fathom  =   Fathoming(Tree,Alive)
%
% INPUT
%   Tree    :   Solution tree
%   Alive   :   Boolean vector indicating whether nodes are alive or not
%
% OUTPUT
%   fathom  :   List of nodes to be fathomed
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2013-05-14
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% column indices for lower bound and upper bound:
iLB =   4   ;
iUB =   5   ;

nAlive          =   length(Alive)       ;   % number of live nodes
fathom          =   false(nAlive,1)     ;   % set fathom to 0/false (unmarked)
[minUB,index]	=	min(Tree(Alive,iUB)) ;

rule1 = Tree(Alive,iLB)<=Tree(Alive,iUB) ;
rule2 = Tree(Alive,iLB)>minUB+1e-9 ;
fathom(and(rule1,rule2)) = true;
if all(fathom)
    Tree(Alive,iLB)
    Tree(Alive,iUB)
end
fathom          =   find(fathom)        ;   % list nodes to be fathomed
fathom          =   unique(fathom)      ;   % list as unique nodes

% nAlive          =   length(Alive)       ;   % number of live nodes
