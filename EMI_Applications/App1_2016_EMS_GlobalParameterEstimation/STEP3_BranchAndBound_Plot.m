
% -------------------------------------------------------------------------
% EMI Toolbox - STEP3_BranchAndBound_Plot.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

A=sprintf('This program generates the figures used in the manuscript to vizualize \n the results of the final branch-and-bound algorithm execution. \n Especially the last figure takes considerable time. \n');
disp(A)
disp(sprintf('Press any key to continue\n'))

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

% =========================================================================
% 0. LOAD RESULTS

pulse       =	4       ;
resolpow	=   11       ; % for results as in article, use: 11;
rescale     =	true	;
Folder = ['.\Results\PULSE' num2str(pulse) '_RESCALE' num2str(rescale*1) '_RESOL' num2str(resolpow)  ];

load([Folder '\OPTIM_PULSE' num2str(pulse) ])
Folder = ['Results\PULSE' num2str(pulse) '_RESCALE' num2str(rescale*1) '_RESOL' num2str(resolpow)  ];


% =========================================================================
% 1. PROCESS AND VIZUALIZE RESULTS

% -------------------------------------------------------------------------
% Algorithm progress

Indices

FileList        =	dir([Folder '\Progress*.csv'])              ;
nFile           =	length(FileList)                            ;
time            =	[FileList.datenum]                          ;
[time,order]    =   sort(time)                                  ;
FileList        =   FileList(order)                             ;

Progress        =	nan(0,5)                                    ;
for iFile = 1:nFile
    dat         =	dlmread([Folder '\' FileList(iFile).name])	;
    Progress	=	[	Progress	;	dat	]                   ;
end
Progress        =	Progress(~isnan(Progress(:,1)),:)           ;

% -------------------------------------------------------------------------
% Algorithm progress - plot

P               =       Progress(:,3)/1000                      ;
XLim            =	[   0	600000          ]                   ;
Ypos            =       XLim(1)-1/10*diff(XLim)                 ;

fighan = figure;
FigPrep
subplot(3,1,1); AxisPrep, grid on, hold on
    stairs(P,'k.-')
    set(gca,'Xticklabel',[])
    set(gca,'Xlim',XLim)
    set(gca,'Ylim',[0 400])
    set(gca,'Ytick',[0:100:400])
    yhan                =	ylabel('Live sets [\times10^3]')    ;
    yhan.Position(1)	=   Ypos                      ;

subplot(3,1,2), AxisPrep, grid on, hold on
    stairs(log10(Progress(:,2)),'k.-')
    axis tight
    set(gca,'Xticklabel',[])
    set(gca,'Xlim',XLim)
    set(gca,'Ylim',[-5 0])
    set(gca,'Ytick',[-5:1:0])
    set(gca,'Yticklabel',{'10^{-5}','10^{-4}','10^{-3}','10^{-2}','10^{-1}','10^{0 }'})
    yhan                =	ylabel('Relative volume [-]')	;
    yhan.Position(1)	=   Ypos                           ;

subplot(3,1,3), AxisPrep, grid on, hold on
    stairs((Progress(:,5).^(1/2)),'r-','Linewidth',2)
    stairs((Progress(:,4).^(1/2)),'b-','Linewidth',1)
    set(gca,'Xlim',XLim)
    set(gca,'Ylim',[0  4.5])
    set(gca,'Ytick',[0:1:5])
    yhan                =	ylabel('WRMSR [-]')          ;
    yhan.Position(1)	=   Ypos                       ;
    xhan                =	xlabel('Iterations [-]')          ;
    PlotVertical(find(Progress(:,5)==min(Progress(:,5)),1,'first'),'r--','Linewidth',2)
    PlotVertical(find(Progress(:,4)==max(Progress(:,4)),1,'first'),'b--','Linewidth',1)
    drawnow
    
    XTL =   cellstr(num2str(get(gca,'Xtick')'));
    XTL	=   strrep(XTL,' ','');
    set(gca,'Xticklabel',XTL)
    legend({'$\overline{h}$','$\underline{h}$'},'Interpreter','latex','Location','SouthEast')
    drawnow
    
SaveMyFigure(fighan,'.\Figures\Fig8_Algo')

% -------------------------------------------------------------------------
% Best solution

[oo,loc]                    =	min(Tree(:,iUB))                        ;
IntX                        =   [	Tree(loc,index:2:end)	;   ...
                                    Tree(loc,index+1:2:end)	]           ;
[g_L,g_U,BestX,Model]       =	BatchSim_Bound(Model,IntX)              ;
BestPar                     =	BestX*diag(Model.Slope)+Model.Intercept	;

[g,XM,Xsim,Alpha_U,Beta_U]	=	BatchSim_BoundUpp(Model,[],[],false,[],BestPar) ;
OURsim                      =	[	ones(length(Xsim),1)	...
                                    Model.fun((1-Xsim),BestPar) ]*Beta_U	;

% -------------------------------------------------------------------------
% Best solution - Display

beta11 = Alpha_U ;
beta20 = Beta_U(1) ;
beta22 = Beta_U(2) ;
theta = BestPar;

disp(' ')
disp('================================')
disp(' Non-dimensionalized parameters')
disp('================================')
disp([' theta_1  ' num2str(theta(1),3)    ] )
disp([' theta_2  ' num2str(theta(2),3)    ] )
disp([' theta_3  ' num2str(theta(3),3)    ] )
disp([' beta_1,1 ' num2str(beta11,3)      ])
disp([' beta_2,0 ' num2str(beta20,3)     ])
disp([' beta_2,2 ' num2str(beta22,3)     ] )
disp('================================')

disp(' ')
disp('================================')
disp(' Conventional parameters')
disp('================================')
disp([' a_max  ' num2str(beta11/theta(2),3)    ] )
disp([' K_S    ' num2str(theta(1)*beta11/theta(2),3)    ] )
disp([' K_I    ' num2str(theta(2)*beta11/theta(3),3)    ] )
disp([' S_N,0  ' num2str(beta11,3)      ])
disp([' b      ' num2str(beta20,3)     ] )
disp([' i_g    ' num2str(beta22/beta11,3)     ])
disp('================================')
disp(' ')

% -------------------------------------------------------------------------
% Best solution - Plot
XLim    =	[	0	6	]   ;
dT      =       2.5/60      ; % 2.5 min.
fighan = figure;
    FigPrep, AxisPrep, hold on
    plot(Model.T1,Model.Y1,'ko','MarkerFaceColor','k')
    plot([ Model.T1(1)  Model.T1(1) ]',[ Model.Y1(1)-3*Model.sigma1(1) Model.Y1(1)+3*Model.sigma1(1)]','k+')
    plot(Model.Tsim,(1-Xsim)*Alpha_U,'k-','LineWidth',2)
    plot([ Model.T1  Model.T1 ]',[ Model.Y1-3*Model.sigma1 Model.Y1+3*Model.sigma1]','k+')
    plot([ Model.T1  Model.T1 ]',[ Model.Y1-3*Model.sigma1 Model.Y1+3*Model.sigma1]','k-')
    set(gca,'Xlim',XLim)
    set(gca,'Ylim',[0 22],'Ytick',[0:2:22])
    grid on
    ylabel('$y_{1}=S_{\mathrm{N}}~\left[mg~N~L^{-1}\right]$','Interpreter','latex')
    xlabel('Time [h]')
    Pos = get(gca,'Position');
    Pos(3) = 0.73 ;
    set(gca,'Position',Pos);
    Pos = get(gca,'Position');
    legend({'$\tilde{y}_{1,k_1}$','$\tilde{y}_{1,k_1} \pm 3~\sigma_{1,k_1}$','$\hat{y}_{1,k_1}$'},'Interpreter','latex','Location','SouthWest')
    set(gca,'YColor','k')
    
    
    axes('Position',Pos),	AxisPrep, hold on
    plot(Model.T2,Model.Y2,'bo')
    plot([ Model.T2(1)  Model.T2(1) ]',[ Model.Y2(1)-3*Model.sigma2(1) Model.Y2(1)+3*Model.sigma2(1)]','bx')
    plot(Model.Tsim,OURsim,'b--','LineWidth',2)
    plot([ Model.T2  Model.T2 ]',[ Model.Y2-3*Model.sigma2 Model.Y2+3*Model.sigma2]','bx')
    plot([ Model.T2  Model.T2 ]',[ Model.Y2-3*Model.sigma2 Model.Y2+3*Model.sigma2]','b-')
    set(gca, 'Color', 'none','YAxisLocation','right')
    set(gca,'Xlim',XLim)
    set(gca,'Ylim',[0 220],'Ytick',[0:20:220])
    set(gca,'Xticklabel',[ ])
    ylabel('$y_{2} = r_{\mathrm{OUR}} \left[mg~O_2~L^{-1}~h^{-1}\right]$','Interpreter','latex')
    legend({'$\tilde{y}_{2,k_2}$','$\tilde{y}_{2,k_2} \pm 3~\sigma_{2,k_2}$','$\hat{y}_{2,k_2}$'},'Interpreter','latex','Color','w')
    set(gca,'YColor','b')

    SaveMyFigure(fighan,'.\Figures\Fig2_Fit')

% -------------------------------------------------------------------------
% Parameter space - Bounding box
BBox        =	[   min(Tree(:,index:2:end))	;	...
                    max(Tree(:,index+1:2:end))	]   ;

ParBox      =   BBox*diag(Model.Slope)+ones(2,1)*Model.Intercept    ;
order       =	(dec2bin((1:2^3)-1)-48)+1                           ;
Corners     =	nan(size(order,1),3)                                ;
for j=1:size(Corners,1)
    Corners(j,:) = ParBox(sub2ind(size(ParBox), order(j,:), [1:3])) ;
end

Corners     =	sortrows(Corners)                                   ;

[rotmat,cornerpoints]	=	...
    minboundbox(Corners(:,1),Corners(:,2),Corners(:,3))         ;

% -------------------------------------------------------------------------
% Parameter space - Convex hull

Centers     =   (Tree(:,index+1:2:end)+Tree(:,index:2:end))/2       ;
Range       =   (Tree(:,index+1:2:end)-Tree(:,index:2:end))/2       ;
rows        =	(dec2bin((1:2^3)-1)-48)*2-1                         ;
Mat         =	nan(0,3)                                            ;
for iRow=1:size(rows,1)
    Mat     =   [	Mat	;	Centers+Range*diag(rows(iRow,:))	]   ;
end

Mat         =	Mat*diag(Model.Slope) ...
                    +	ones(size(Mat,1),1)*Model.Intercept         ;
[K,v]       =	(convhulln(Mat))                                    ;
Mat         =	Mat(K,:)                                            ;
K           =	unique(K(:))                                        ;

% -------------------------------------------------------------------------
% Parameter space - Convex hull - Plot

figure(42), FigPrep
hold on,
plot3(Mat(:,1),Mat(:,2),Mat(:,3),'k.')

% -------------------------------------------------------------------------
% Parameter space - Minimum volume ellipsoid

Tol     = 1e-2 ;
disp('      Computing MVE - this takes some time')
[A , c] = MinVolEllipse(Mat',Tol) ;
disp('      Computing MVE done')

% Dimensions / Volume
[U D V] =	svd(A)          ;
a1      =	1/sqrt(D(1,1))  ;
a2      =	1/sqrt(D(2,2))  ;
a3      =	1/sqrt(D(3,3))  ;
vMVE    =   4/3*pi*a1*a2*a3	;

Line	=	[	V(:,2)*a2*3+c	-V(:,2)*a2*3+c	]   ;

% Plot ellipsoid embedded in 2D plane of the figure:
N           =       20                  ;
theta       =	[   0:1/N:2*pi+1/N	]	;

state(1,:)	=       a1*cos(theta)       ;
state(2,:)	=       a3*sin(theta)       ;

X           =	V(:,[1 3]) * state      ;
X(1,:)      =	X(1,:) + c(1)           ;
X(2,:)      =	X(2,:) + c(2)           ;
X(3,:)      =	X(3,:) + c(3)           ;

% -------------------------------------------------------------------------
% Parameter space - Minimum volume ellipsoid - Plot
plot3(X(1,:),X(2,:),X(3,:),'k-')

view(-120,17)
xlabel('\theta_1')
ylabel('\theta_2')
zlabel('\theta_3')
grid on

% -------------------------------------------------------------------------
% Parameter space - Bounding box - Plot projections
minX    =   min(cornerpoints(:,1))  ;
maxX  	=   max(cornerpoints(:,1))  ;
minY    =   min(cornerpoints(:,2))  ;
maxY    =   max(cornerpoints(:,2))  ;
minZ    =   min(cornerpoints(:,3))  ;
maxZ    =   max(cornerpoints(:,3))  ;
X0      =   get(gca,'Xlim')         ;
Y0      =   get(gca,'Ylim')         ;
Z0      =   get(gca,'Zlim')         ;
plot3( [minX minX maxX maxX minX]', [minY maxY maxY minY minY]',Z0(1)*ones(1,5)','k--')
plot3( [minX minX maxX maxX minX]',Y0(1)*ones(1,5)', [minZ maxZ maxZ minZ minZ]','k--')
plot3( (X0(2)-eps)*ones(1,5)',[minY minY maxY maxY minY]', [minZ maxZ maxZ minZ minZ]','k--')


% -------------------------------------------------------------------------
% Parameter space - Bounding ellipsoid - Plot projections

% Z-projection
K3          =	unique(convhulln(Mat(:,[1 2])))     ;
[A3 , c3]	=	MinVolEllipse(Mat(K3,[1 2])',Tol)	;
figure(13), hold on
    X       =	Ellipse_plot(A3, c3)                ;
    plot(Mat(K3,1)', Mat(K3,2)','k.')
    close 13

figure(42),
    plot3( X(1,:), X(2,:),Z0(1)*ones(1,size(X,2)),'k-')
    plot3( c3(1),c3(2),Z0(1),'kx')
    plot3( BestPar(:,1),BestPar(:,2),Z0(1),'ko')
    drawnow

% Y-projection
K2          =	unique(convhulln(Mat(:,[1 3])))     ;
[A2 , c2]   =	MinVolEllipse(Mat(K2,[1 3])',Tol)	;
figure(12), hold on
    X       =   Ellipse_plot(A2, c2)                ;
    plot(Mat(K2,1)', Mat(K2,3)','k.')
    close 12

figure(42),
    plot3( X(1,:),Y0(1)*ones(1,size(X,2)), X(2,:),'k-')
    plot3( c2(1),Y0(1),c2(2),'kx')
    plot3( BestPar(:,1),Y0(1),BestPar(:,3),'ko')
    drawnow

% X-projection
K1          =	unique(convhulln(Mat(:,[2 3])))     ;
[A1 , c1]	=	MinVolEllipse(Mat(K1,[2 3])',Tol)	;
figure(11); hold on
    X       =   Ellipse_plot(A1, c1)                ;
    plot(Mat(K1,2)', Mat(K1,3)','k.')
    close 11

fighan = figure(42);
    plot3( X0(2)*ones(1,size(X,2)),X(1,:), X(2,:),'k-')
    plot3( X0(2),c1(1),c1(2),'kx')
    plot3( X0(2),BestPar(:,2),BestPar(:,3),'ko')
    drawnow

    plot3(BestPar(:,1),BestPar(:,2),BestPar(:,3),'ko','MarkerFaceColor','r')
    plotminbox(cornerpoints,'k--')
    view(-120,17)
    drawnow
    
SaveMyFigure(fighan,'.\Figures\Fig9_Parameters')

% -------------------------------------------------------------------------
% Extremal simulations - Convex hull
disp(' ')
A=sprintf('The simulations to generate the last figure are coming up. These take considerable time. I suggest to get some coffee. \n');
disp(A)
disp(sprintf('Program will continue automatically in 11 seconds. \n Press Ctrl+C to cancel progress. Press any key to continue. \n'))
pause(11)

nMat        =	size(Mat,1)             ;
XXsim       =	nan(nMat,length(Xsim))	;

disp('      Bounding simulations')
[g,XM,XsimL,Alpha_U,Beta_U]     =	BatchSim_BoundUpp(Model,[],[],false,[],min(Mat)) ;
[g,XM,XsimU,Alpha_U,Beta_U] 	=	BatchSim_BoundUpp(Model,[],[],false,[],max(Mat)) ;
disp('      Bounding simulations done')

disp(['      Extremal simulations - Executing simulation for every point defining the convex hull (' num2str(nMat) ' points)'])
for iMat=1:1:nMat
    if mod(iMat,100)==0
        disp([ iMat nMat])
    end
    [g,XM,Xsim,Alpha_U,Beta_U]	=	BatchSim_BoundUpp(Model,[],[],false,[],Mat(iMat,:))     ;
    XXsim(iMat,:)               =	Xsim                                                    ;
end
disp('      Extremal simulations - Simulations done')

XsimL_emp	=	max(XXsim)  ;
XsimU_emp	=	min(XXsim)  ;

fighan = figure; 
FigPrep, AxisPrep, hold on, grid on

plot(Model.Tsim,1-XsimU,'--','Color','k','LineWidth',1.11)
plot(Model.Tsim,1-XsimU_emp,'-','Color','k','LineWidth',1.11)
plot(Model.Tsim,1-XXsim(1,:),'-','Color',[1 1 1]*.73)

plot(Model.Tsim,1-XsimU,'--','Color','r','LineWidth',1.11)
plot(Model.Tsim,1-XsimU_emp,'-','Color','r','LineWidth',1.11)
plot(Model.Tsim,1-XsimL_emp,'-','Color','b','LineWidth',1.11)
plot(Model.Tsim,1-XsimL,'--','Color','b','LineWidth',1.11)

PlotSim     =   false   ;
if PlotSim % plotting actual profiles -> requires large memory
    disp('      Displaying simulations - this takes some time')
    plot(Model.Tsim,(1-XXsim),'-','Color',[1 1 1]*.73)
    drawnow
    disp('      Displaying done')
else % efficient alternative, sufficient if no zoom is applied:
    patch([Model.Tsim(:) ; flipud(Model.Tsim(:))],[(1-XsimU_emp(:)) ; (1-flipud(XsimL_emp(:)))],[1 1 1]*.73)
end

plot(Model.Tsim,1-XsimU,'--','Color','r','LineWidth',1.37)
plot(Model.Tsim,1-XsimU_emp,'-','Color','r','LineWidth',1.37)
plot(Model.Tsim,1-XsimL_emp,'-','Color','b','LineWidth',1.37)
plot(Model.Tsim,1-XsimL,'--','Color','b','LineWidth',1.37)
legend({'Bounds - bounding box','Bounds - convex hull','Points on the convex hull'},'Interpreter','latex')

ylabel({'Relative concentration','s(t) [-]'})
xlabel('Time [h]')
drawnow

disp(sprintf(['All figures generated. \n']))

SaveMyFigure(fighan,'.\Figures\Fig10_Simulations')

disp(sprintf(['All figures saved. \n']))
return