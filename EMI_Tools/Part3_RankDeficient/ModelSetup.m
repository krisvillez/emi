function model	=	ModelSetup (V,n_0,M_eps,N_,name_species,t_,select_h,ratecell,T,name_parameters)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelSetup.m
% -------------------------------------------------------------------------
% Description
%
%   This function defines a complete model as a structure.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	model	=	ModelSetup (V,n_0,M_eps,N_,name_species,t_,select_h,ratecell,T,name_parameters)
%
% INPUT
%   V               :	Reactor volume
%   n_0             :   Initial state vector (molar masses)
%   M_eps           :   Output noise gain matrix
%   N_              :   Stoichiometric matrix
%   name_species	:   Names of the states (species)
%   t_              :   Simulation timestamps
%   select_h        :   Index of simulation timestamps that are measurement
%                       sampling times
%   ratecell        :   cell array of rate expressions
%   T               :   Number of kinetic parameters
%   name_parameters	:   Names of the parameters
%   
% OUTPUT
%	model           :   model as structure
%   
% -------------------------------------------------------------------------
% Last modification: 2018-12-12
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Set up model structure with static, kinetic, and experimental information

% ------------------------------------------------------------------------
%	Experimental design
% ------------------------------------------------------------------------
model.experiment.V              =	V               ;
model.experiment.n_0            =	n_0             ;
model.experiment.t_             =	t_              ;
model.experiment.select_h       =	select_h        ;

% ------------------------------------------------------------------------
%	Kinetic model elements (e.g., rate laws)
% ------------------------------------------------------------------------
model.kinetic.ratecell          =	ratecell        ;
model.kinetic.name_parameters	=	name_parameters	;
model.kinetic.T                 =	T               ;

% ------------------------------------------------------------------------
%	Static model elements (e.g., stoichiometry)
% ------------------------------------------------------------------------
model.static.M_eps              =	M_eps           ;
model.static.N_                 =	sparse(N_)      ;
model.static.name_species       =	name_species	;

R = length(ratecell);
name_reactions = cell(R,1);
for r=1:R
    name_reactions{r}     =	[ '{x_' num2str(r) '}']    ;
end
model.static.name_reactions = name_reactions;


end

