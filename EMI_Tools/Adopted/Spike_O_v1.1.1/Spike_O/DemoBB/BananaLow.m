function [LB] = BananaLow(model,IntX)

% -------------------------------------------------------------------------
% Spike_O Toolbox - BananaLow.m
% -------------------------------------------------------------------------
% Description 
%
% This function evaluates a lower bound to Rosenbrock's banana function
%
% ------------------------------------------------------------------------- 
% Syntax
%	
% I/O: LB = BananaLow(model,IntX) 
%
% INPUT
%   model   :   Structure describing the parameters of the banana function
%   IntX    :   Polyhedral set of feasible parameters
%
% OUTPUT
%   LB      :   lower bound banana function value
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2013-05-31
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


a =  model.a; 
b =  model.b; 
c = model.c;

IntX1 = [ max(a-IntX(:,1)) ; min(a-IntX(:,1)) ] ;
if all(IntX1>=0) || all(IntX1<=0) 
    IntX1 = [ max(IntX1.^2) ; min(IntX1.^2) ] ;
else
    IntX1 = [ max(IntX1.^2) ; 0 ] ;
end

IntX2 = [ max(b-IntX(:,1)) ; min(b-IntX(:,1)) ] ;
if all(IntX2>=0) || all(IntX2<=0) 
    IntX2 = [ max(IntX2.^2) ; min(IntX2.^2) ] ;
else
    IntX2 = [ max(IntX2.^2) ; 0 ] ;
end

IntXY = [   max([IntX(1,2)-IntX2(:,1) ; IntX(2,2)-IntX2(:,1)]) ;...
            min([IntX(1,2)-IntX2(:,1) ; IntX(2,2)-IntX2(:,1)]) ] ;
if all(IntXY>=0) || all(IntXY<=0) 
    IntXY = [ max(IntXY.^2) ; min(IntXY.^2) ] ;
else
    IntXY = [ max(IntXY.^2) ; 0 ] ;
end

IntZ = [   max([IntX1(1,1)+c*IntXY(:,1) ; IntX1(2,1)+c*IntXY(:,1)]) ;...
            min([IntX1(1,1)+c*IntXY(:,1) ; IntX1(2,1)+c*IntXY(:,1)]) ] ;
LB = IntZ(2) ;