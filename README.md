EMI toolbox v4.1 - EMI: Efficient Model Identification
by Alma Masic, Kris Villez

Website:	http://tinyurl.com/eawag-spike
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave functions and scripts written to evaluate tools for efficient model selection and parameter identification. This particular version of the toolbox provides the necessary code to reproduce the results reported in [1-2].

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2014b) on a Windows system. Tests in Octave are incomplete at the time of writing. To install the Matlab functions in a Windows system, using Matlab or Octave, follow the instructions below.

---------------------------------------------------
 I. Installing the toolbox and other preliminaries
---------------------------------------------------

1. Create a directory to contain all required toolboxes.  For example, in my case all toolbox directories are in:

		C:\Tools\

2. Move the unzipped EMI folder (in which this README.txt file is located) to the following location:
	
		C:\Tools\EMI\

3. The EMI toolbox includes all other code necessary except for the MOSEK optimization software. This software is necessary to reproduce the results in [2]. The MOSEK optimization software can be downloaded from https://www.mosek.com/ and should be installed according to the provided instructions. To produce the results in [2], MOSEK Version 7.1.0.27 was used.
	
------------------------------------------------------
 II(A). Using the toolbox to reproduce results in [1]
------------------------------------------------------

1. Start Matlab

2. Go the following folder:

		EMI\EMI_Applications\App1_2016_EMS_GlobalParameterEstimation

	and execute the available scripts in the following order:
	
		STEP1B_DemoBounds
		STEP2_Contracting_Plot
		STEP3_BranchAndBound_Plot
		STEP4_GreedyOpt_Plot
	
3. To recompute all results and produce the figures, go the folder:

		EMI\EMI_Applications\App1_2016_EMS_GlobalParameterEstimation

	and execute the available scripts in the following order: 

		STEP1_SetupOptimization
		STEP1B_DemoBounds
		STEP2_Contracting
		STEP2_Contracting_Plot
		STEP3_BranchAndBound
		STEP3_BranchAndBound_Plot
		STEP4_GreedyOpt
		STEP4_GreedyOpt_Plot

------------------------------------------------------
 II(B). Using the toolbox to reproduce results in [2]
------------------------------------------------------
		
1. Start Matlab

2. Ensure the MOSEK optimization software is installed correctly. Test this by executing the following in the command line

		mosekopt 
		
3. To reproduce the figures, go the following folder:

		EMI\EMI_Applications\App2_2016_CACE_ShapeConstraints

	and execute the script 'MainScript.m' .	
		
3. To recompute all results and produce the figures, then:

	3.1. Go the folder:	EMI\EMI_Applications\App2_2016_CACE_ShapeConstraints
	3.2. Modify 'MainScript.m' by uncommenting line 42
	3.3. Execute the script 'MainScript.m' .
		
------------------------------------------------------
 II(C). Using the toolbox to reproduce results in [3]
------------------------------------------------------

1. 	Start Matlab

2. Go the following folder:

		EMI\EMI_Applications\App3_2017_EST_Extents
		
3.	Execute the following scripts in this order to reproduce the figures:

	STEP0_DataGeneration_Display
	STEP1_ExtentComputation_Display
	STEP2_KineticModelIdentification_Display
	STEP3_FineTuning_Display
	
4.	Execute the following scripts in this order to recompute all results on your machine:

	STEP0_DataGeneration
	STEP1_ExtentComputation
	STEP2_KineticModelIdentification
	STEP3_FineTuning
	
------------------------------------------------------
 II(D). Using the toolbox to reproduce results in [4]
------------------------------------------------------

1. 	Start Matlab

2. 	Go the following folder:

		C:\G\code\emi\EMI_Applications\App4_2019_Processes_RankDeficient

2.	Execute the following script to reproduce all results and figures concerning the simulated cases:

	s_Processes2019_SimulationStudy.m
	
3.	Execute the following script to reproduce all results and figures concerning the alpha-pinene case:

	s_Processes2019_alphaPinene.m
	
------------------------------------------------------
 II(E). Using the toolbox to reproduce results in [5]
------------------------------------------------------

1. 	Start Matlab

2. 	Go the following folder:

		C:\G\code\emi\EMI_Applications\App5_2019_TechnicalReport_TR006030

2.	Execute the following script to reproduce all results and figures:

	s_TechnicalReport_v3_SimulationStudy.m
	
	
----------------------------
 III. Relevant publications
----------------------------

Please refer to this toolbox by citing following publication:
[1] Masic, A, Udert, K, Villez, K (2016). Global parameter optimization for biokinetic modeling of simple batch experiments. Environ Modell and Softw, 85, 356-373.
[2] Masic, A, Srinivasan, S, Billeter, J, Bonvin, D, Villez, K (2017). Shape constrained splines as transparent black-box models for bioprocess modeling. Comp Chem Eng, 99, 96-105. 
[3] Masic, A, Srinivasan, S, Billeter, J, Bonvin, D, Villez, K (2017). Identification of biokinetic models using the concept of extents. Environ Sci Technol, 51(13), 7520-7513. 
[4] Villez, K, Billeter, J, Bonvin, D (2019). Incremental Parameter Estimation under Rank-Deficient Measurement Conditions. Processes, 7(2), 75.
[5] Villez, K (2019). Extent-based Model Identification under Incomplete Observability Conditions. (TR-006-03-0). Technical Report, Eawag, Dübendorf, Switzerland.

-------------
 IV. Changes
-------------

14-10-2016: Integrated the code for dynamic modelling with shape constrained spline functions into the toolbox.
21-12-2016: Removed redundant instances of the Spike_B and Spike_O toolboxes. Modified use of BB.m to match the selected version of the Spike_O toolbox (v1.1). Modified scripts so that installation folder for Mosek is not assumed known. Corrected a path error in STEP4_GreedyOpt.m. Modified this README file to be clearer and complete.
20.02.2019: Integrated all code for publications [4] and [5].

------------------
 Acknowledgements
------------------

We want to thank our partners at EPFL for their assistance in the EMISSUN project, within which this toolbox was developed:
- Dominique Bonvin
- Julien Billeter
- Sriniketh Srinivasan

We want to thank Gabriël Käempf for the experimental work that made the work in [1] possible.

----------------------------------------------------------
Last modified on 20 February 2019