function par2_hat	=	ModelId_Step4_GuessDecay(TrendModel,StartTimeDecay)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Step4_GuessDecay.m
% -------------------------------------------------------------------------
% Description
%
% Provides and initial guess for the decay rate parameter.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	par2_hat	=	ModelId_Step4_GuessDecay(TrendModel,StartTimeDecay)
%
% INPUT
%   TrendModel      :   Shape constrained spline model
%   StartTimeDecay	:   Start time of phase without growth
%
% OUTPUT
%   par2_hat        :   Parameter guess
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% =========================================================================
% Process available information:

tsim        =	TrendModel.tk(1):1/60:TrendModel.tk(end)	;
Xsim        =   EvalSCS(TrendModel,tsim)                    ;
dXsim       =   EvalSCS(TrendModel,tsim,1)                  ;
Xsim        =   Xsim(tsim>=StartTimeDecay)                  ;
dXsim       =   dXsim(tsim>=StartTimeDecay)                 ;
    
% =========================================================================
% Estimation

resid       =	@(pars) (-Xsim*pars-dXsim)                  ;
lb          =   0                                           ;
ub          =	+inf                                        ;
par_init    =   .01*ones(1,1)                               ;
par2_hat    =   CLS(resid,par_init,lb,ub)                   ;
 
% figure, 
% hold on
%     plot(Xsim,-dXsim,'.')
%     plot(Xsim,Xsim*par2_hat,'-')
%     xlabel('Biomass concentration [mg/L]')
%     ylabel('Biomass concentration decay rate [mg/L.h]')
%     pause



end

