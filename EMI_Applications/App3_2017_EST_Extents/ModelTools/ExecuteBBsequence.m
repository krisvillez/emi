function [theta,QL,QU,Omega] = ExecuteBBsequence(Model,Omega,ResolTarget)

% -------------------------------------------------------------------------
% EMI Toolbox - ExecuteBBsequence.m
% -------------------------------------------------------------------------
% Description
%
% Execute repeated runs of the Branch-and-Bound algorithm for extent model
% parameter estimation until desired granularity is met.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[theta,QL,QU,Omega] = ExecuteBBsequence(Model,Omega,ResolTarget)
%
% INPUT
%   Model       :   Model description given as a structure (see ExtentDAE.m
%                   for required structure)
%   Omega       :   Bounding box for considered parameter vectors
%   ResolTarget	:	Negative Base-2 logarithm of desired relative size of
%                   the smallest live sets relative to the bounding box
%                   containing the live sets.
%
% OUTPUT
%   theta       :   Best-known parameter vector
%   QU          :   Upper bound
%   QL          :   Lower bound
%   Omega       :   Bounding box for retained parameter vectors
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

ntheta          =	size(Omega,2)       ;

% =========================================================================
% setup to keep computationally intensive simulations in memory
global nArchive
global Archive_Xsim Archive_Time Archive_theta
             
                
% =========================================================================
% Part I. B&B
optionsBB               =	OptimsetBB              ;
optionsBB.boundfun      =	@ExtentFit_bound        ;
optionsBB.boundupp      =	@ExtentFit_boundlow     ;
optionsBB.boundlow      =	@ExtentFit_boundupp     ;
optionsBB.node          =   1                       ;
optionsBB.graphprogress	=   true                    ;
optionsBB.graphinterval	=	20                      ;
optionsBB.verbose       =   false                   ;
optionsBB.resolution	=	(1/2)^(ResolTarget) 	;

% Initialization
Terminate               =	false                   ;
iteration               =	0                       ;
Resolution              =   1                       ;
                
while ~Terminate
        
    iteration               =   iteration +1            ;
    optionsBB.resolution	=	(1/2)^(Resolution)      ;

    disp(['-------------- ' num2str(iteration) ' ' num2str(Resolution) ])
    
    % Run BB algorithm
    [QU,QL,Reduction,Omega_reduced,Omega_best,Iterations] = ExecuteBB(Model,Omega,optionsBB) ;
    Omega                   =	Omega_reduced           ;   % New bounding box
        
    if Resolution==ResolTarget
        if prod(Reduction)>= 1
            Terminate       =	true                    ;
        end
    else
        if prod(Reduction)>.75
            Resolution      =	Resolution + 1          ;
        end
    end
    
    % Kick out simulation that are definitely not need anymore
    Kick	=   or(	Archive_theta>repmat(Omega(2,:),...
                    [nArchive 1]),...
                    Archive_theta<repmat(Omega(1,:),[nArchive 1]))  ;
    Kick	=	find(any(Kick,2))                                   ;
    if ~isempty(Kick)
        Archive_Time(Kick,:)	=   -inf	;
        Archive_theta(Kick,:)	=	nan     ;
        Archive_Xsim(:,:,Kick)	=	nan     ;
    end
    
end

Model.Intercept             =	Omega_best(1,:)                         ;
Model.Slope                 =	Omega_best(2,:)-Omega_best(1,:)         ;
IntX                        =	[0 1]'*ones(1,ntheta)                   ;
[QL,QU,BestX,Model]         =	ExtentFit_bound(Model,IntX,[],false)    ;
theta                       =	Model.Intercept+BestX*diag(Model.Slope) ;

    
    
end

