function [GCDvalue] = GCDlist(Integers) 

% -------------------------------------------------------------------------
% Spike_O Toolbox - GCDlist.m
% -------------------------------------------------------------------------
% Description 
%
% GCDlist is a function which computes the greatest common divisor among a
% list of integers.  
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	GCDvalue = GCDlist(integers) 
%
% INPUT
%	Integers:   Column vector of integers. 
%               (required) 
%   
% OUTPUT
%   GCDvalue:   Greatest common divisor
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Integers    =   sort(Integers)  ;
GCDvalue	=   Integers(1)     ;
for k=2:length(Integers)
    GCDvalue = gcd(GCDvalue,Integers(k))   ;
end