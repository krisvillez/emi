function static	=	ExtentsLabeling (static,options)

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentsLabeling.m
% -------------------------------------------------------------------------
% Description
%
%   This function executes the labeling step of the extent-based method for
%   incremental parameter estimation
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	static	=	ExtentsLabeling (static,options)
%
% INPUT
%   static	:	structure describing the invariant relationships in the
%               model, namely the stoichiometric matrix (static.N_) and the
%               measurement gain matrix (static.M_).
%   options	:   tuning options
%	 options.extent.factorization :	factorization method ('rref' or 'svd')
%	 options.verbose : control command line output level (integer)
%
% OUTPUT
%	static	:   structure describing what is known about the model and the
%               experiment based on invariants alone (i.e., stoichiometric
%               balances) now including all results from the labelling
%               procedure. This includes:
%               - static.label_obs :    boolean vector indicating the
%                                       observable extents
%               - static.label_sensed : boolean vector indicating the
%                                       sensed extents
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% ------------------------------------------------------------------------
%	Get information from structure
% ------------------------------------------------------------------------

M_     =	static.M_	;   % Concentration measurement gain matrix
N_      =	static.N_   ;   % Stoichiometric matrix

% --------------------------------------------------------------------
%   Preprocessing
% --------------------------------------------------------------------

% Extent-based measurement gain matrix
G_     =	M_*N_'      ;
[R,S]   =   size(N_)	;

% Compute reduced-row echelon form
if isa(G_,'sym')
    B_           =	rref(G_)                            ;	%   actual rref
    pivot       =	nan(size(B_,1),1)                   ;
    for i=1:size(B,1)
        col     =	find(~logical(B_(i,:)==0),1,'first');
        if ~isempty(col)
            pivot(i)	=   col                         ;
        end
    end
    pivot       =	pivot(~isnan(pivot))                ;
    row         =	find(~all(logical(B_==0),2))        ;   %   find rows with all zeros
else
    [B_,pivot]	=	rref(G_)                            ;   %   actual rref
    B_          =   sparse(B_)                          ;   %   make sparse
    row         =	find(~all(B_==0,2))                 ;   %   find rows with all zeros
end
B_redux              =	B_(row,:)                       ;   %   remove zero rows

%   rank of G_
A               =	length(row)                         ;

% --------------------------------------------------------------------
%   Extent labelling
% --------------------------------------------------------------------

%   1. label extents as sensed / nonsensed
if isa(G_,'sym')
    label_sensed	=	any(~logical(G_==0),1)         ;
else
    label_sensed    =	any(G_,1)                      ;
end


switch options.extent.factorization
    case 'rref'
        
        %   2. label extents as observable / ambiguous
        % rows in Br corresponding to observable extents
        if isa(B_redux,'sym')
            row_o           =   find(sum(~logical(B_redux(:,label_sensed)==0),2)==1) ;
        else
            row_o           =   find(sum(B_redux(:,label_sensed)~=0,2)==1)           ;
        end
        label_obs               =	false(1,R)                          ;

        % rows in Br corresponding to observable extent directions
        row_a                   =	setdiff((1:A)',row_o)               ;


        % --------------------------------------------------------------------
        %   Matrix computation
        % --------------------------------------------------------------------

        V_o         =       B_(row_a,:)'                        ;
        Vbar_o      =	[	B_(row_o,:)'	V_o ]               ;
        
        Gbar_o      =	G_(:,pivot([row_o ; row_a]))   ;
        
    case 'svd'
        
        %   2. label extents as observable / ambiguous
        % rows in Vbar_o corresponding to observable extents
        row_o = [];
        label_obs               =	false(1,R)                          ;

        % rows in Br corresponding to observable extent directions
        row_a                   =	setdiff((1:A)',row_o)               ;

        
        % --------------------------------------------------------------------
        %   Matrix computation
        % --------------------------------------------------------------------
        [Us,Ss,Vs]	=	svd(full(G_(:,label_sensed)),'econ')    ;
        
        V_o         =	sparse(R,A)              ;
        V_o(label_sensed,:) = Vs(:,1:A) ;
        
        V_o(abs(V_o)<1e-9) = 0;
        Vbar_o      =	V_o                                     ;
        
        Gbar_o = G_*pinv(full(Vbar_o))';
   
end

label_obs(label_sensed) =	any(B_redux(row_o,label_sensed),1)       ;

% counting:
R_o                     =	length(row_o)                       ;
rho_o                   =	A-R_o                               ;

index_bar_o             =	[	find(label_obs)	R+(1:rho_o) ]'	;

% generate labels:
varnames                =   cell(R+rho_o,1)             ;
if isfield(static,'name_reactions')
    for r=1:R
        varnames{r}     =	static.name_reactions{r}    ;
    end
else
    for r=1:R
        varnames{r}     =	[	'{x}_' num2str(r)	]   ;
    end
end
if rho_o==1
    varnames{R+rho_o}	=	[	'{\chi}_{{o}}'      ]   ;
else
    for r=1:rho_o
        varnames{R+r}	=	[	'{\chi}_{{o},' num2str(r) '}'	]   ;
    end
end

NNbar_o     =       (N_'*Vbar_o*(Vbar_o'*Vbar_o)^(-1))' ;
NNrec_      =       Vbar_o*NNbar_o                      ;
NNbar_u     =       N_-NNrec_                           ;

% Account for numerical imprecision:
%log(abs(full(NNbar_u)))
r0          =       abs(NNbar_u)<10^-9                  ;
NNbar_u(r0)	=       0                                   ;

% --------------------------------------------------------------------
%   Output
% --------------------------------------------------------------------

static.B_           =   B_              ;
static.G_           =   G_              ;
static.Gbar_o           =   Gbar_o              ;
static.index_bar_o	=	index_bar_o     ;
static.label_obs	=	label_obs       ;
static.label_sensed	=	label_sensed	;
static.pivot        =   pivot           ;
static.row_a        =   row_a           ;
static.row_o        =   row_o           ;
static.varnames     =	varnames        ;
static.A            =   A               ;
static.R            =   R               ;
static.R_o          =   R_o             ;
static.rho_o        =   rho_o           ;
static.S            =   S               ;

static.V_o          =	V_o             ;
static.Vbar_o       =	Vbar_o          ;
static.NNbar_o      =	NNbar_o         ;
static.NNbar_u      =	NNbar_u         ;

% --------------------------------------------------------------------
%   Extent labelling - display
% --------------------------------------------------------------------

if options.verbose>=2
    disp(['  Number of extents:                         ' num2str(R)])
    disp(['  Rank of M:                                 ' num2str(A)])
    disp(['  Number of observable extents:              ' num2str(R_o)])
    disp(['  Number of observable extent directions:    ' num2str(rho_o)])
    
    if options.verbose>=3
        for r=1:(R+rho_o)
            substr      =	{'\','{','}'}'                      ;
            name        =	stripstring(varnames{r},substr)     ;
            if r<=R
                str     =	[	'       '	name	' : '	]   ;
                if label_obs(r)
                    str	=	[	str	' observable '  ]   ;
                elseif label_sensed(r)
                    str =	[	str	' ambiguous '   ]   ;
                else
                    str =	[	str	' non-sensed '  ]   ;
                end
            else
                str     =	[ '       ' name ' : observable direction '];
            end
            disp(str)
        end
    end
end

