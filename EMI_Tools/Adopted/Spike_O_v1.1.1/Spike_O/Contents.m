% -------------------------------------------------------------------------
% Spike_O Toolbox v1.1 
% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
% Root folder - m-files (./*.m)
% -------------------------------------------------------------------------
%
%   BB          	- Branch-and-Bound (B&B) algorithm
%   ContractBound   - Bound contraction algorithm
%   DynProg        	- Dynamic Programming (DP) algorithm
%   Fathoming   	- Fathoming step for B&B
%	GCDlist			- Compute greatest common divisor from a list of integers
%   GenerateLeafs	- Branching step for B&B
%   Indices         - Generated index variables for B&B
%   OptimsetBB      - Default options for B&B algorithm
%	Ranger 			- Evaluates range of matrix columns
%   SelectFromTree  - Returns best solution from the solution tree
%   VisualLeaf      - Vizualization aid - plot leaf in the solution tree
%   VisualTree     	- Vizualization aid - plot branches/leaves in the solution tree
%
% -------------------------------------------------------------------------
% DemoBB folder - m-files (./DemoBB/*.m)
% -------------------------------------------------------------------------
%	BananaBounds    - Bounds to the banana function
%	BananaFun       - Rosenbrock's banana function
%	BananaLow       - Lower bound to the banana function
%	BananaUpp       - Uppe bound to the banana function
%	DemoBB          - Demo based on Rosenbrock's banana function
%	

%
