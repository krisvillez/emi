function [inc,loc] = ismember_spec(v1,v2)

if isempty(v1)
    inc = v1 ;
    if nargout>=2
        loc = inc;
    end
else
    maxLabel    =   max([v1 ; v2]);
    tf          =   sparse(maxLabel,2);
    tf(v1,1)          =	true	;
    tf(v2,2) =	true	;
    tf = and(tf(:,1),tf(:,2)) ;
    inc = tf(v1);
    if nargout>=2
        loc = find(inc);
    end
end

