function  dxdt = ExtentDAE( t,x,Model,verbose)

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentDAE.m
% -------------------------------------------------------------------------
% Description
%
% Computes ordinary differential equations
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:  dxdt = ExtentDAE( t,x,Model,verbose)
%
% INPUT
%   t       :   Time
%   x       :   Extents of reactions
%   Model	:   Model description given as a structure, including the
%                   fields:
%                       .map_c2r    - mapping species to rate law inputs
%                       .x2c        - mapping extents to species conc.
%                       .Reactions	- reactions as cell structure
%                   See SetupSimulation.m for exemplary use.
%   verbose :   Boolean controlling output regarding simulation time    
%
% OUTPUT
%   dxdt 	:	Rates of change
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<4 && isempty(verbose)
    verbose = false ;
end

% Input dimensions
Reactions = Model.Reactions         ;
Rk = length(Model.Reactions) ;

% Pre-allocation
dxdt = nan(Rk,length(t));

% Evaluate species concentrations
c       =   Model.x2c(t,x,Model)	;
    
% Evaluate rates of change for the extents
for i=1:Rk
    
    Reaction    =   Reactions{i};
    
	% Hard-coded fail-safe, assuming non-negative parameters
	%pars 				=	abs(pars)						;
	
	% Rate of change
	ratefun 			=	Reaction.ratefun                ;
	theta               =	Reaction.theta                  ;
	rateinput 			=	c(Model.map_c2r(i,:),:)     	;
    
	dxdt(i,:)           =	ratefun(rateinput,theta) 		;
	
end
    
	
% % Hard-coded fail-safe, assuming irreversible reactions
% dxdt	=	max(0,dxdt)	;
    
global SimTime
if ceil(toc/5)>ceil(SimTime/5)
    SimTime = toc ;
    if verbose
        disp(['------- Simulation time: ' num2str(SimTime/60) ' minutes'])
    end
end

end