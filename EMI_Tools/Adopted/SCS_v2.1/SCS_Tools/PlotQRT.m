function Handle = PlotQRT(QR,Type,Scale)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - PlotQRT.m
% -------------------------------------------------------------------------
% Description
%
% This function vizualizes a qualitative representation (QR) by means of
% coloured patches.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Handle = PlotQRT(QR,Type,Scale)
%
% INPUT
%   QR:         Cell array of qualitative representations. Each cell has a
%               qualitative representation as a matrix with each row
%               representing an episode. Columns 1 to 4 contain the start
%               time, the end time, the sign of the first derivative and
%               the sign of the second derivative for each episode.
%               (required)
%   Type:       Define which primitives to print as characters. 
%                   0: none
%                   1: monotonic
%                   2: triangular
%               (optional, default = 0)
%   Scale:      Scalar to multiply argument values with.
%               (optional, default = 1)
%
% OUTPUT
%   Handle:     Handle of the produced plot axis.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<2
    Type = 0 ;
end
if nargin<=2 || isempty(Scale)
    Scale = 1; 
end

% assert(any(Type==3),'Unknown Type of vizualization')

hold on
if ~isa(QR,'cell')
    QR    =   { QR }    ;
end
n   =   length(QR)    ;

XXX =   []  ;
YYY =   []  ;
CC  =   []  ;
for i=1:n
    Epimat          =   QR{i}                     ;
    if ~isempty(Epimat)

        
        nEpimat         =   size(Epimat,1)                  ;
        Epimat(:,5)     =   4+2*Epimat(:,3)+Epimat(:,4)       ;
        Epimat(isnan(Epimat(:,4)),5) = Epimat(isnan(Epimat(:,4)),3)+9;
        Epimat(isnan(Epimat(:,3)),5) = Epimat(isnan(Epimat(:,3)),4)+12;
        Epimat(and(isnan(Epimat(:,3)),isnan(Epimat(:,4))),5) = 14;
        
        X1      =       Epimat(:,1)             ;   %   left X coordinates
        X2      =       Epimat(:,2)             ;   %   right X coordinates
        Y1      =       ones(nEpimat,1)*(i-1)+.5;   %   bottom Y coordinates
        Y2      =       Y1+1                    ;   %   top Y coordinates

        XX      =   [   X1  X1  X2  X2  ]'      ;   %   X coordinates
        YY      =   [   Y1  Y2  Y2  Y1  ]'      ;   %   Y coordinates
        C       =       Epimat(:,5)             ;   %   qualitative marker (-3 to +3)

        XXX     =   [   XXX  XX ]   ;
        YYY     =   [   YYY  YY ]   ;
        CC      =   [   CC; C ]     ;
       
    end
end

map = 1;

N       =   size(XXX,2)                 ;
CCC     =   reshape(CFD(CC,map),[1 N 3 ])   ;  % Actual Colors

XXX     =   (XXX)*Scale          	;
%     XXX=XXX-1;
    
XMIN    =   min(XXX(:))         ;
XMAX    =   max(XXX(:))         ;
YMIN    =   min(YYY(:))         ;
YMAX    =   max(YYY(:))         ;
axis([XMIN XMAX  YMIN YMAX])    ;
% drawnow

patch(XXX,YYY,CCC,'LineStyle','none')
drawnow

LineWidth = 1.2 ;
%PlotHorizontal((YMIN+1:1:YMAX-1),'w-','LineWidth',LineWidth)
set(gca,'Ydir','reverse');
if Type % add characters
    select  =   find(XXX(3,:)-XXX(1,:)>=1)  ;
    CC      =   CC(select)                  ;
    XXX     =   XXX(:,select)               ;
    YYY     =   YYY(:,select)               ;
    
    QRstring    =   cellstr(Alphabet(CC,Type)')  ;
    TextX       =   mean(XXX)               ;
    TextY       =   mean(YYY)               ;
%     nChar       =   length(QRstring)        ;
    text(TextX,TextY,QRstring,'Color','w','VerticalAlignment','middle','HorizontalAlignment','center','fontsize',10,'fontname','Helvetica-Narrow','fontweight','normal')
end

set(gca,'Ytick',1:n)
drawnow
Handle = gca ;
