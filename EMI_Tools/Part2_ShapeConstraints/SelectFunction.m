function ratefun = SelectFunction(model_name)

% -------------------------------------------------------------------------
% EMI Toolbox - SelectFunction.m
% -------------------------------------------------------------------------
% Description
%
% Create function handle for selected rate law.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	ratefun = SelectFunction(model_name)
%
% INPUT
%   model_name	:	Growth model name
%
% OUTPUT
%   ratefun     :   Rate law
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

switch model_name
        case {'Monod'}
            % monod
            ratefun     =	@(S,pars)	pars(1).*S./(S+pars(2))         ;
        case {'Tessier'}
            % tessier
            ratefun     =	@(S,pars)	pars(1).*(1-exp(-S./pars(2)))   ;
        case {'Tanh'}               
            % tanh
            ratefun     =	@(S,pars)	pars(1).*tanh(S/pars(2))        ;
        case {'M+T'}                
            % m+t
            ratefun     =	@(S,pars)	1/2*pars(1).*((1-exp(-S./pars(2)))+S./(S+pars(2)))  ;
        case {'Root'}               
            % root
            ratefun     =	@(S,pars)	pars(1).*(sqrt(5*S/sqrt(1.5)+4)-2)./((sqrt(5*S/sqrt(1.5)+4)-2)+pars(2)); 
        case {'Haldane'}            
            % haldane
            ratefun     =	@(S,pars)	pars(1).*S./(pars(2)+S+S.^2/pars(3));
        case {'Steele'}             
            % steele
            ratefun     =	@(S,pars)	pars(1).*S/pars(5).*exp(1-S./pars(5)); 
        case {'Peeters & Eilers'}   
            % peeters&eilers
            ratefun     =	@(S,pars)	pars(1)*2*(1+pars(4)).*S./pars(6)./((S./pars(6)).^2+2.*S./pars(6)*pars(4)+1);
        otherwise
            error('no such function available')
end
