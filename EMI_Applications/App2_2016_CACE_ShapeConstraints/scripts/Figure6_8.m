
% -------------------------------------------------------------------------
% EMI Toolbox - Figure6_8.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces Figures 6-8 in [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-10-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% This script plots Figure 6a, 6b, 6c, 6d, 7, 8a, 8b.

%addpath(genpath('C:\Z\Research\Code\Tools\Intern\Qualitative\SCS\ReleasedPublicly\SCS_v2.1'));

disp(['===================== FIGURE 6-8 ====================='])

switch mode
    case 'fast'
 
        load('MAT_files\Output678.mat')
                
    case 'simulateall'
        
        select_model	=	{'Root','Monod','M+T','Tessier','Tanh','Haldane','Steele','Peeters & Eilers'}   ;
        
        Knots6a         =	{(0:1/(2^4):1)*SFP.S0} ;
        
        [Output,WRMSR]	=	SimulateAndFitIndirect(select_model,Knots6a,{'C','N'},SFP);
        
        save Output678 Output select_model WRMSR
end

% ------------- FIGURE 6a/b ----------------

disp('Creating figures')

model       =   'M+T'       ;

% ------------- FIGURE 6a ----------------

panel       =   char(96+1)	;

disp(['===================== FIGURE 6' num2str(panel) ' ====================='])

[tf,index]  =   ismember(model,select_model)        ;
Output6     =	Output(index)                       ;

tmeas       =   Output6.true_rate_ode_tmeas         ;
Ctrue       =   Output6.true_rate_ode               ;
Chat_lib    =   Output6.Lib_models_selected.ode     ;
name_lib    =   Output6.Lib_models_selected.name	;
Chat_scs    =   Output6.SCS_models(1).ode           ;
name_scs    =   Output6.SCS_models(1).name          ;

figno       =	60+1            ;
fig6a       =	figure(figno)   ;
figure(figno),
hold on, box on

%plot(tmeas,Ctrue(1,:),'+','LineWidth',1,'MarkerSize',6,'Color',1/255*[125 46 141]) % measured S
%plot(tmeas,Ctrue(2,:),'x','LineWidth',0.5,'MarkerSize',6,'Color',1/255*[125 46 141],'MarkerFaceColor',1/255*[255 153 200]) % measured P
%plot(tmeas,Ctrue(3,:),'o','LineWidth',0.5,'MarkerSize',6,'Color',1/255*[125 46 141],'MarkerFaceColor',1/255*[255 153 200]) % measured P
plot(tmeas,Ctrue(1,:),'+','LineWidth',1,'MarkerSize',6,'Color',0/255*[125 46 141]) % measured S
plot(tmeas,Ctrue(2,:),'x','LineWidth',0.5,'MarkerSize',6,'Color',0/255*[125 46 141]) % measured P
plot(tmeas,Ctrue(3,:),'o','LineWidth',0.5,'MarkerSize',6,'Color',0/255*[125 46 141]) % measured P

plot(tmeas,Chat_lib(1,:),'-','LineWidth',2,'Color',1/1*[ 0 1 0 ]) % library S
plot(tmeas,Chat_scs(1,:),'--','LineWidth',2,'Color',1/1*[ 0 0 1 ]) % SCS S
plot(tmeas,Chat_lib(2,:),'-','LineWidth',2,'Color',1/1*[ 0 1 0 ]) % library P
plot(tmeas,Chat_lib(3,:),'-','LineWidth',2,'Color',1/1*[ 0 1 0 ]) % library X
plot(tmeas,Chat_scs(2,:),'--','LineWidth',2,'Color',1/1*[ 0 0 1 ]) % SCS P
plot(tmeas,Chat_scs(3,:),'--','LineWidth',2,'Color',1/1*[ 0 0 1 ]) % SCS X

xlabel('time [h]')
ylabel('concentratios [mg/L]')
set(gca,'Xlim',[0 SFP.Tend],'Ylim',[-1.5 (SFP.S0+1.5)],'FontSize',18)

leg_names = {'S measured','P measured','X measured',...
    [ name_lib ],[ name_scs ]} ;
[legend_h,object_h,plot_h,text_strings] = columnlegend(2, leg_names(:),'location','east','boxoff') ;
legend_h.Position(1)	=	.3  ;
legend_h.Position(2)	=	.15 ;
%legend(leg_names,'Location','NorthEast')
%legend('boxoff')

set(fig6a,'name',['Figure 6' panel],'numbertitle','off')

disp('---Finished---')

% ------------- FIGURE 6b ----------------

panel       =   char(96+2)      ;

disp(['===================== FIGURE 6' num2str(panel) ' ====================='])

figno       =	60+2            ;
fig6b       =	figure(figno)   ;
figure(figno), hold on, box on

PlotResiduals(tmeas,Ctrue,Chat_lib,Chat_scs,SFP.sigmaY);
legend({name_lib,name_scs,'\pm\sigma'},'Position',[0.8,0.48,0.15,0.15])

set(fig6b,'name',['Figure 6' panel],'numbertitle','off')

disp('---Finished---')

% ------------- FIGURE 7a ----------------

disp('===================== FIGURE 7a =====================')


model       =   'Peeters & Eilers'                  ;
[tf,index]  =   ismember(model,select_model)        ;
    
Output8     =	Output(index)                       ;

tmeas       =   Output8.true_rate_ode_tmeas         ;
Ctrue       =   Output8.true_rate_ode               ;
Chat_lib    =   Output8.Lib_models_selected.ode     ;
name_lib    =   Output8.Lib_models_selected.name	;
Chat_scs1	=   Output8.SCS_models(1).ode           ;
name_scs1	=   Output8.SCS_models(1).name          ;
Chat_scs2	=   Output8.SCS_models(2).ode           ;
name_scs2	=   Output8.SCS_models(2).name          ;

    
fig7a       =	figure(71)                          ;
figure(71), hold on, box on

plot(tmeas,Ctrue(1,:),'+','LineWidth',1,'MarkerSize',6,'Color',0/255*[125 46 141]) % measured S
plot(tmeas,Ctrue(2,:),'x','LineWidth',0.5,'MarkerSize',6,'Color',0/255*[125 46 141]) % measured P
plot(tmeas,Ctrue(3,:),'o','LineWidth',0.5,'MarkerSize',6,'Color',0/255*[125 46 141]) % measured P

plot(tmeas,Chat_lib(1,:),'-','LineWidth',2,'Color',1/1*[ 0 1 0 ]) % library S
plot(tmeas,Chat_scs1(1,:),'--','LineWidth',2,'Color',1/1*[ 0 0 1  ]) % SCS S
plot(tmeas,Chat_scs2(1,:),'--','LineWidth',2,'Color',1/1*[ 1 0 0 ]) % SCS S

plot(tmeas,Chat_lib(2,:),'-','LineWidth',2,'Color',1/1*[ 0 1 0 ]) % library S
plot(tmeas,Chat_scs1(2,:),'--','LineWidth',2,'Color',1/1*[ 0 0 1  ]) % SCS S
plot(tmeas,Chat_scs2(2,:),'--','LineWidth',2,'Color',1/1*[ 1 0 0 ]) % SCS S

plot(tmeas,Chat_lib(3,:),'-','LineWidth',2,'Color',1/1*[ 0 1 0 ]) % library S
plot(tmeas,Chat_scs1(3,:),'--','LineWidth',2,'Color',1/1*[ 0 0 1  ]) % SCS S
plot(tmeas,Chat_scs2(3,:),'--','LineWidth',2,'Color',1/1*[ 1 0 0 ]) % SCS S

    xlabel('time [h]')
    ylabel('concentratios [mg/L]')
    set(gca,'Xlim',[0 SFP.Tend],'Ylim',[-1.5 (SFP.S0+1.5)],'FontSize',18)
    leg_names	=	{	'S measured',       'P measured',       'X measured',       ...
                        [ name_lib ],  [name_scs1 ],   [name_scs2 ]      }	;
[legend_h,object_h,plot_h,text_strings] = columnlegend(2, leg_names(:),'location','east','boxoff') ;
legend_h.Position(1) =  .39;
legend_h.Position(2) =  0 ;
    %legend({'measured S','measured P',[Output8.Lib_models_selected.name ' S'],[Output8.Lib_models_selected.name ' P'],'SCS2 S','SCS2 P','SCS3 S','SCS3 P'},'Position',[0.62,0.4,0.3,0.3])
    legend('boxoff')

set(fig7a,'name','Figure 7a','numbertitle','off')

disp('---Finished---')


% ------------- FIGURE 7b ----------------
 
disp('===================== FIGURE 7b =====================')
 
fig7b       =	figure(72)              ;

SS          =   Output8.true_rate_SS    ;
rate_true	=   Output8.true_rate_rate  ;
rate_2      =   Output8.Lib_models(2).rate(Output8.true_rate_SS,Output8.Lib_models(2).pars(3:end)) ;
rate_SCS2   =   Output8.SCS_models(1).rate(Output8.true_rate_SS,Output8.SCS_models(1).pars(3:end)) ;
rate_SCS3   =   Output8.SCS_models(2).rate(Output8.true_rate_SS,Output8.SCS_models(2).pars(3:end)) ;

figure(fig7b), 
    hold on, box on
    
    plot(SS,rate_true,'LineWidth',7,'Color',1/255*[215 187 192])
	plot(SS,rate_2,'k--','LineWidth',2)
	plot(SS,rate_SCS2,':k','LineWidth',2)
    plot(SS,rate_SCS3,':r','LineWidth',2)
axis([0, 25, 0, 2])
legend('true rate','Tessier','SCS2','SCS3')
set(gca,'FontSize',20,'XTick',0:5:25)
xlabel('substrate concentration S [mg N/L]')
ylabel('growth rate r_g(S) [1/day]')

set(fig7b,'name','Figure 7b','numbertitle','off')

disp('---Finished---')


% ------------- FIGURE 7c ----------------

panel	=   char(96+3)	;

disp(['===================== FIGURE 7' num2str(panel) ' ====================='])

figno = 70+3 ;
fig7c = figure(figno);
figure(figno), hold on, box on

PlotResiduals2(tmeas,Ctrue,Chat_lib,Chat_scs1,Chat_scs2,SFP.sigmaY);
legend({name_lib,name_scs1,name_scs2,'\pm\sigma'},'Position',[0.8,0.48,0.15,0.15])
gcfpos = get(gcf,'Position') ;
gcfpos(4)=gcfpos(4)*2 ;
set(gcf,'Position',gcfpos)


set(fig7c,'name',['Figure 7' panel],'numbertitle','off')

disp('---Finished---')

% ------------- FIGURE 8 ----------------

disp('===================== FIGURE 8 =====================')

fig8	=	figure(8);
figure(8), 
    hold on, box on
    PlotWRMSR(WRMSR,select_model,fig8);
    set(fig8,'name','Figure 8','numbertitle','off')
    

disp('---Finished---')
