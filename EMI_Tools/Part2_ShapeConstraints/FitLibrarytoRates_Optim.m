function OptimPars = FitLibrarytoRates_Optim(x,y,Model)

% -------------------------------------------------------------------------
% EMI Toolbox - FitLibrarytoRates_Optim.m
% -------------------------------------------------------------------------
% Description
%
% Direct estimation of rate law parameters.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	OptimPars = FitLibrarytoRates_Optim(x,y,Model)
%
% INPUT
%   x           :   Substrate concentration
%   y           :   Rate measurements
%   Model       :   Rate model   
%
% OUTPUT
%   OptimPars	:   Optimal parameter values
%
% -------------------------------------------------------------------------
% Last modification: 2016-07-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification.
%
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
%
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

[p0,lb,ub]      =	DefineParameterBounds                                   ;

% case 'lsqnonlin'
options         =	optimset('TolX',1e-8,'TolFun',1e-8,'MaxFunEvals',2000)  ;
options.Display	=   'off'                                                   ;
phat            =	lsqnonlin(@(p) OptimFun(p),p0,lb,ub,options)            ;
OptimPars       =	phat                                                    ;


if sum(abs(ub-phat)<1e-3*ub)>=1
    sum(abs(ub-phat)<1e-3*ub)
    display('***Warning: Optimal parameters very close to upper bound.')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function F = OptimFun(p)
    
    F           =	y - Model.ratefun(x,p)                                  ;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [p0,lb,ub] = DefineParameterBounds
    
    ModelName	=   Model.name;
    
    switch ModelName
        case 'Monod'
            lb	=   [   0	0       ]   ;
            ub	=   [   100	1000    ]   ;
            p0	=       0.5*(ub-lb)     ;
        case 'Tessier'
            lb	=   [   0   1e-16   ]   ;
            ub	=   [   100	1000    ]   ;
            p0	=       0.5*(ub-lb)     ;
        case 'Smiths'
            lb	=   [   0	1e-16   ]   ;
            ub	=   [   100	1000    ]   ;
            p0	=       0.5*(ub-lb)     ;
    end
end


end