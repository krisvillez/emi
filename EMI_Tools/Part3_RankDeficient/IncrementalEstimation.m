function model = IncrementalEstimation (model,options,theta_0) 

% -------------------------------------------------------------------------
% EMI Toolbox - IncrementalEstimation.m
% -------------------------------------------------------------------------
% Description
%
%   Parameter estimation via incremental parameter estimation. A parameter
%   estimation problem is solved for each of the identified subsystems.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	model = IncrementalEstimation (model,options,theta_0) 
%
% INPUT
%   model	:   model as structure
%   options	:   options.verbose determines command line output
%   theta_0	:   initial parameter guesses
%   
% OUTPUT
%	model	:   model with updated parameter vector
%   
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


if options.verbose, disp('PARARAMETER ESTIMATION - INCREMENTAL'), end

% ------------------------------------------------------------------------
%	Get information from structure
% ------------------------------------------------------------------------

ratecell        =	model.kinetic.ratecell              ;

Sigmabar_o      =	model.projection.Sigmabar_o         ;

index_bar_o     =	model.static.index_bar_o            ;
N_              =	model.static.N_                     ;
V_o             =	model.static.V_o                    ;
Vbar_o          =	model.static.Vbar_o                 ;
R               =	model.static.R                      ;
rho_o           =	model.static.rho_o                  ;

n0              =	model.experiment.n_0                ;
V               =	model.experiment.V                  ;
t_              =	model.experiment.t_                 ;
select_h        =	model.experiment.select_h           ;

vrtx_sim_ext	=	model.kinetic.graph.vrtx_sim_ext	;
vrtx_sim_dir	=	model.kinetic.graph.vrtx_sim_dir	;
vrtx_par        =	model.kinetic.graph.vrtx_par        ;
vrtx_interp     =	model.kinetic.graph.vrtx_interp     ;
partition       =	model.kinetic.partition             ;
T               =	model.kinetic.T                     ;

% ------------------------------------------------------------------------
%	Pre-processing
% ------------------------------------------------------------------------

% time vectors:
t_opt       =	t_(t_<=max(t_(select_h)))   ;
t_h         =	t_(select_h)                ;


switch model.kinetic.graph.objective
    case 'no_overlap'
        % subsystems:
        j_          =	unique (partition)	;
        j_          =	j_(~isnan(j_))      ;
        J           =	length (j_)         ;
    otherwise
        J = size(model.kinetic.partition,2) ;
end

        
% parameter estrimates - pre-allocation:
theta_hat	=	nan(T,J)            ;
thetahat_sd =	nan(T,J)            ;
% ------------------------------------------------------------------------
%	Parameter estimation - subsystem by subsystem
% ------------------------------------------------------------------------

for j=1:J
    if options.verbose>=2
        disp(['	Subsystem: ' num2str(j)])
    end
    
    vrtx_sim        =	[   vrtx_sim_ext{j}	;	vrtx_sim_dir{j}	]   ;
    vrtx_sim_obs	=       intersect(index_bar_o,vrtx_sim)         ;
    
    % ----------------------------------------------------------------
    %   Define subsystem outputs
    % ----------------------------------------------------------------
    
    V_eval      =	[	eye(R)	V_o	]                       ;
    V_eval      =	V_eval(vrtx_sim_ext{j},vrtx_sim_obs)    ;
    
    select      =	ismember (index_bar_o,vrtx_sim)         ;
    
    Sig_Ytilde	=	Sigmabar_o(select,select)               ;
    Gamma       =   chol (Sig_Ytilde^(-1),'lower')          ;
    
    xtil_j      =   model.observations.xbarhat_o(select,:)	;
    
    % ----------------------------------------------------------------
    %   Select subsystem parameters
    % ----------------------------------------------------------------
    select_theta	=	ismember((R+rho_o)+(1:T),vrtx_par{j})       ;
    Tj              =	sum(select_theta)                           ;
    
    if nargin<3 || isempty(theta_0)
        theta_j_init                =	rand(Tj,1)*10               ;
    else
        theta_j_init                =	theta_0(select_theta)       ;
    end
    
    Lamba_theta                     =	eye(T)                     	;
    Lamba_theta                     =   Lamba_theta(:,select_theta)	;
    Lamba_theta(~select_theta,:)	=	nan                        	;
    
    SIM_theta       =	@(theta_) Lamba_theta*theta_                ;
    
    % ----------------------------------------------------------------
    %   Set up expression for concentrations
    % ----------------------------------------------------------------
    
    index_interp	=	ismember (index_bar_o,vrtx_interp{j})           ;
    V_interp        =	Vbar_o(:,index_interp)                          ;
    N_interp        =	(N_'*V_interp*(V_interp'*V_interp)^(-1))'       ;
    N_rec           =   V_interp*N_interp                               ;
    N_sim           =	N_-N_rec                                        ;
    N_sim           =	N_sim(vrtx_sim_ext{j},:)                        ;
    Y_interp        =	model.observations.xbarhat_o(index_interp,:)'	;
    
    if any(index_interp)
        x_interp	=	@(t) interp1(t_h,Y_interp,t)'                   ;
    else
        x_interp	=	@(t) zeros(0,1)                                 ;
    end
    SIM_c           =	@(t,x_)	1/V*(N_interp'*x_interp(t)+N_sim'*x_+n0) ;
    
    % ----------------------------------------------------------------
    %   Set up dynamic subsystem model
    % ----------------------------------------------------------------
    
    % select rate expressions:
    SIM_ratecell	=	ratecell(vrtx_sim_ext{j})                               ;
    % convert selected array of rate expressions to function:
    SIM_ratefun     =	@(c_,theta_) cellfun(@(expr) max(expr(max(c_,0),theta_),0), SIM_ratecell) ;
    % set up ODE expression:
    SIM_model       =   @(t,x_,theta_) SIM_ratefun(SIM_c(t,x_),SIM_theta(theta_))      ;
    
    ObjectiveFun	=	@(theta_) sum( sum( ...
        ( Gamma*( xtil_j - Simulation(theta_,SIM_model,t_opt,select_h,V_eval) ) ).^2 ...
        ) ) ;
    
    % ----------------------------------------------------------------
    %   Parameter estimation
    % ----------------------------------------------------------------
    
    % Get simulation results with initial parameter guesses:
    [~,xhat_]               =	Simulation (theta_j_init,SIM_model,t_opt,select_h,V_eval) ;
    model.part{j}.xhat_init	=   xhat_	;
    
    % Define search region:
    pL          =	1e-3*ones(Tj,1)                                  ;
    pU          =	1e+1*ones(Tj,1)                                  ;
    
    % Optimization:
    [thetahat_j,fval,exitflag,output,lambda,grad,hessian] = ...
        fmincon(@(p) ObjectiveFun(p),...
        theta_j_init,[],[],[],[],pL,pU,[],options.optim1)                   ;
    
    if isempty(hessian)
        hessian = zeros(Tj);
    end
    
    % Get simulation results with final parameter estimates:
    [~,xhat_]        =	Simulation (thetahat_j,SIM_model,t_opt,select_h,V_eval) ;
    model.part{j}.xhat_fin	=   xhat_	;
    model.part{j}.xtil_     =   xtil_j     ;
    
    % ------------------------------------------------------------------------
    %	Output
    % ------------------------------------------------------------------------

    % Postprocessing:
    theta_hat(select_theta,j)       =	thetahat_j                          ;
    thetahat_j_cov                  =	pinv((hessian+hessian')/2)          ;
    thetahat_j_cov                  =	(thetahat_j_cov+thetahat_j_cov')/2  ;
    thetahat_sd(select_theta,j)   =	diag(thetahat_j_cov).^(1/2)         ;
    
    % Command line output:
    if options.verbose>=2
        vertexnames     =	model.kinetic.graph.vertexnames         ;
        disp(['	            ' 'Initial ' '    Final ' ])
        for t=1:Tj
            lbl         =	vertexnames{vrtx_par{j}(t)}             ;
            init        =	sprintf('% 10.6f',thetahat_j(t))       	;
            est         =	sprintf('% 10.6f',thetahat_j(t))       	;
            est_sd      =	sprintf('% 10.6f',3*thetahat_sd(t,j))	;
            disp(['         ' lbl ' : ' init '  ' est '  (' char(177) est_sd ')' ])
            
        end
    end
    
end

% Postprocessing:
theta_hat_fin = nan(T,1);
for t=1:T
    available = ~isnan(theta_hat(t,:)) ;
    nEst = sum(available) ;
    if nEst>=1
        theta_hat_fin(t) = mean(theta_hat(t,available)) ;
    end
end
    
% Save results:
model.kinetic.theta_hat = theta_hat_fin ;

