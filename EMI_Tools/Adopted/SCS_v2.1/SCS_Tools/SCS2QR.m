function [QR,Trans] = SCS2QR(Model)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - SCS2QR.m
% -------------------------------------------------------------------------
% Description
%
% This function computes the Triangular Qualitative Representation for
% vizualization (with PlotQRT.m).
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[QR,Trans] = SCS2QR(Model)
%
% INPUT
%	Model:     	Structure describing the spline model including the
%               qualitative representation as a matrix (field name: 'QR'). 
%               (required)
%
% OUTPUT
%   QR:         Qualitative Representation
%   Trans:      Transitions
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-06-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

QR              =   Model.QR            ;
Trans           =   Model.SolX          ;

m               =   size(QR,1)          ;
rows            =   1:(m-1)             ;

QR(rows+1,1)    =   Trans               ;
QR(rows,2)      =   Trans               ;

QR              =   QR(:,[1:2 4:5])     ;   % reduce to episode bounds and first and second derivative

QR(:,1:2)       =   QR(:,1:2)*Model.t_scale+Model.t_ref ;