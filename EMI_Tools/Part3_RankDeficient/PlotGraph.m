function [fighan,figname] = PlotGraph (model,options,scenario_name)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotGraph.m
% -------------------------------------------------------------------------
% Description
%
%   Customized plotting function to visualize the model graph.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[fighan,figname] = PlotGraph (model,options,scenario_name)
%
% INPUT
%   model           :   model as structure
%   options         :   plotting fine-tuning options
%   scenario_name	:   name for title of figure
%   
% OUTPUT
%	fighan          :   figure handle
%   figname         :   figure title
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification.
%
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
%
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


% ------------------------------------------------------------------------
% model info
% ------------------------------------------------------------------------

switch model.kinetic.graph.objective
    case 'no_overlap'
        graph_type = 0;
    otherwise
        if isfield(options,'showmeasurementarcs') && ~options.showmeasurementarcs
            graph_type = 1;
        else
            graph_type = 0;
        end
end
switch graph_type
    case 1
        
        J = size(model.kinetic.partition,2) ;
        
        for j=1:J
            
            G               =	model.kinetic.graph.G           ;
            vertexnames     =	model.kinetic.graph.vertexnames ;
            
            index_bar_o     =	model.static.index_bar_o        ;
            if nargin<2 || isempty(index_bar_o)
                index_bar_o	=	[ ]                             ;
            end
            nNode           =   size(G.Nodes,1)                 ;
            index_bar_u     =	setdiff(1:nNode,index_bar_o)	;
            
            partition = model.kinetic.partition(:,j);
            
            if isfield(options,'showmeasurementarcs') && ~options.showmeasurementarcs
                EdgeSimul   =	~(partition(G.Edges.EndNodes(:,1))==partition(G.Edges.EndNodes(:,2)))	;
                EdgeSimul = any([ EdgeSimul                 ~partition(G.Edges.EndNodes(:,2)) ],2);
            else
                EdgeSimul	=	ismember(G.Edges.EndNodes(:,1),index_bar_o(j))	;
            end
            NodeExc         =	~(partition)                ;
            visited         =	find(~NodeExc)                  ;
            notvisited      =	find(NodeExc)                   ;
            
            % ------------------------------------------------------------------------
            % graphics options
            % ------------------------------------------------------------------------
            FS = options.FS ;
            
            Shade               =	[   1	1	1	]*1         ;
            if isfield(options,'showmeasurementarcs') && ~options.showmeasurementarcs
                LSinterpolate	=	'none'                      ;
            else
                LSinterpolate	=	':'                         ;
            end
            
            LS                  =	cell(length(EdgeSimul),1)	;
            for e=1:length(EdgeSimul)
                if EdgeSimul(e),    LS{e}	=	LSinterpolate	;
                else,               LS{e}	=	'-'             ;
                end
            end
            
            % ------------------------------------------------------------------------
            % actual plotting
            % ------------------------------------------------------------------------
            figname{j}         =   [	'Scenario'	scenario_name '_graph_' num2str(j)	]	;
            fighan(j)          =       figure ('name',figname{j})                 ;
            
            axes ('Position',[.01 .01 .98 .98])
            hold on,
            
            % plot graph with Matlab tools
            p               =	plot(G)             ;
            p.NodeLabel     =	vertexnames         ;
            p.EdgeColor     =	'k'                 ;
            p.NodeColor     =	NodeExc*Shade       ;
            p.LineWidth     =	3                   ;
            p.LineStyle     =	LS                  ;
            p.MarkerSize	=   p.LineWidth*7       ;
            p.ArrowSize     =   p.LineWidth*5       ;
            p.NodeLabel     =	{}                  ;
            p.EdgeAlpha     =	1                   ;
            if nargin>=3 && isfield(options,'Xdata')
                if length(p.XData)==length(options.Xdata)
                    p.XData     =	options.Xdata       ;
                end
            end
            if nargin>=3 && isfield(options,'Ydata')
                if length(p.YData)==length(options.Ydata)
                    p.YData     =	options.Ydata       ;
                end
            end
            
            % add additional elements
            CircleSize      =	p.MarkerSize*2      ;
            
            % OBSERVABLE EXTENTS
            % observable extents and extent directions - grey vertices
            index_bar_o_j = intersect(index_bar_o,visited);
            plot (p.XData(index_bar_o_j),p.YData(index_bar_o_j),'ko','MarkerFaceColor',[1 1 1]*.73,'MarkerSize',CircleSize,'Linewidth',p.LineWidth)
            % add text
            if ~isempty(index_bar_o_j)
                text(p.XData(index_bar_o_j),p.YData(index_bar_o_j),vertexnames(index_bar_o_j),'Color','k','HorizontalAlignment','center','fontsize',FS)
            end
            
            % UNOBSERVABLE EXTENTS
            % unobservable extents and extent directions included in subsystems - white vertices
            index           =	intersect(visited,index_bar_u)  ;
            plot (p.XData(index),p.YData(index),'ko','MarkerFaceColor','w','MarkerSize',CircleSize,'Linewidth',p.LineWidth)
            % add text
            if ~isempty(index)
                text (p.XData(index),p.YData(index),vertexnames(index),'Color','k','HorizontalAlignment','center','fontsize',FS)
            end
%             % unobservable extents and extent directions not included in subsystems - white vertices
%             index           =	intersect (notvisited,index_bar_u);
%             if ~isempty(index)
%                 plot (p.XData(index),p.YData(index),'ko','MarkerFaceColor','w','MarkerSize',CircleSize,'Linewidth',p.LineWidth)
%             end
            % add text
%             if ~isempty(index)
%                 text (p.XData(index),p.YData(index),vertexnames(index),'Color','k','HorizontalAlignment','center','fontsize',FS)
%             end
            
            % ------------------------------------------------------------------------
            % axes modification
            % ------------------------------------------------------------------------
            axis equal
            set (gca,'Xlim',[min(p.XData(:))-.25 max(p.XData(:))+.25],'Ylim',[min(p.YData(:))-.25 max(p.YData(:))+.25])
            set (gca,'Box','on','Visible','on')
            set (gca,'Ytick',[],'Xtick',[])
            set (gca,'Xlim',[-1.2 3.4],'Ylim',[-1.2 2.2])
            
            if isfield (options,'inset')
                rectangle ('Position',options.inset,...
                    'Linestyle','-',...
                    'EdgeColor','k',...
                    'Linewidth',2   )	;
            end
            h               =   gca     ;
            h.XAxis.Visible	=	'off'   ;
            h.YAxis.Visible =	'off'   ;
            
            drawnow()
            
            
        end

    case 0
        
        G               =	model.kinetic.graph.G           ;
        vertexnames     =	model.kinetic.graph.vertexnames ;
        partition       =	model.kinetic.partition         ;
        
        index_bar_o     =	model.static.index_bar_o        ;
        if nargin<2 || isempty(index_bar_o)
            index_bar_o	=	[ ]                             ;
        end
        nNode           =   size(G.Nodes,1)                 ;
        index_bar_u     =	setdiff(1:nNode,index_bar_o)	;


        if isfield(options,'showmeasurementarcs') && ~options.showmeasurementarcs
            EdgeSimul	=	~(partition(G.Edges.EndNodes(:,1))==partition(G.Edges.EndNodes(:,2)))	;
        else
            EdgeSimul	=	ismember(G.Edges.EndNodes(:,1),index_bar_o)	;
        end
        NodeExc         =	isnan(partition)                ;
        visited         =	find(~NodeExc)                  ;
        notvisited      =	find(NodeExc)                   ;
        
        % ------------------------------------------------------------------------
        % graphics options
        % ------------------------------------------------------------------------
        FS = options.FS ;
        
        Shade               =	[   1	0	0	]*1         ;
        if isfield(options,'showmeasurementarcs') && ~options.showmeasurementarcs
            LSinterpolate	=	'none'                      ;
        else
            LSinterpolate	=	':'                         ;
        end
        
        LS                  =	cell(length(EdgeSimul),1)	;
        for e=1:length(EdgeSimul)
            if EdgeSimul(e),    LS{e}	=	LSinterpolate	;
            else,               LS{e}	=	'-'             ;
            end
        end
        
        % ------------------------------------------------------------------------
        % actual plotting
        % ------------------------------------------------------------------------
        figname{1}         =   [	'Scenario'	scenario_name '_graph'	]	;
        fighan          =       figure ('name',figname{1})                 ;
        
        axes ('Position',[.01 .01 .98 .98])
        hold on,
        
        % plot graph with Matlab tools
        p               =	plot(G)             ;
        p.NodeLabel     =	vertexnames         ;
        p.EdgeColor     =	'k'                 ;
        %p.NodeColor     =	NodeExc*Shade       ;
        p.LineWidth     =	3                   ;
        p.LineStyle     =	LS                  ;
        p.MarkerSize	=   p.LineWidth*7       ;
        p.ArrowSize     =   p.LineWidth*5       ;
        p.NodeLabel     =	{}                  ;
        p.EdgeAlpha     =	1                   ;
        if nargin>=3 && isfield(options,'Xdata')
            if length(p.XData)==length(options.Xdata)
                p.XData     =	options.Xdata       ;
            else
                disp('NoMatch')
            end
        else
            disp('NoData')
        end
        if nargin>=3 && isfield(options,'Ydata')
            if length(p.YData)==length(options.Ydata)
                p.YData     =	options.Ydata       ;
            end
        end
        
        % add additional elements
        CircleSize      =	p.MarkerSize*2      ;
        
        % OBSERVABLE EXTENTS
        % observable extents and extent directions - grey vertices
        plot (p.XData(index_bar_o),p.YData(index_bar_o),'ko','MarkerFaceColor',[1 1 1]*.73,'MarkerSize',CircleSize,'Linewidth',p.LineWidth)
        % add text
        if ~isempty(index_bar_o)
            text(p.XData(index_bar_o),p.YData(index_bar_o),vertexnames(index_bar_o),'Color','k','HorizontalAlignment','center','fontsize',FS)
        end
        
        % UNOBSERVABLE EXTENTS
        % unobservable extents and extent directions included in subsystems - white vertices
        index           =	intersect(visited,index_bar_u)  ;
        plot (p.XData(index),p.YData(index),'ko','MarkerFaceColor','w','MarkerSize',CircleSize,'Linewidth',p.LineWidth)
        % unobservable extents and extent directions not included in subsystems - white vertices
        index           =	intersect (notvisited,index_bar_u);
        if ~isempty(index)
            plot (p.XData(index),p.YData(index),'ko','MarkerFaceColor','w','MarkerSize',CircleSize,'Linewidth',p.LineWidth)
        end
        % add text
        if ~isempty(index_bar_u)
            text (p.XData(index_bar_u),p.YData(index_bar_u),vertexnames(index_bar_u),'Color','k','HorizontalAlignment','center','fontsize',FS)
        end
        
        % ------------------------------------------------------------------------
        % axes modification
        % ------------------------------------------------------------------------
        axis equal
        set (gca,'Xlim',[min(p.XData(:))-.25 max(p.XData(:))+.25],'Ylim',[min(p.YData(:))-.25 max(p.YData(:))+.25])
        set (gca,'Box','on','Visible','on')
        set (gca,'Ytick',[],'Xtick',[])
        set (gca,'Xlim',[-1.2 3.4],'Ylim',[-1.2 2.2])
        
        if isfield (options,'inset')
            rectangle ('Position',options.inset,...
                'Linestyle','-',...
                'EdgeColor','k',...
                'Linewidth',2   )	;
        end
        h               =   gca     ;
        h.XAxis.Visible	=	'off'   ;
        h.YAxis.Visible =	'off'   ;
        
        drawnow()
        
        
        
end


end

