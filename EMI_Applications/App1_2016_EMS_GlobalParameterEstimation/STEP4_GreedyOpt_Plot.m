
% -------------------------------------------------------------------------
% EMI Toolbox - STEP4_GreedyOpt_Plot.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

% A=sprintf('This program generates the figures used in the manuscript to vizualize \n the results of the final branch-and-bound algorithm execution. \n Especially the last figure takes considerable time. \n');
% disp(A)
% disp(sprintf('Press any key to continue\n'))
%pause

% ============================================================
% TOOLS

currentpath =cd ;
cd ..
cd ..
addpath(genpath([cd '\EMI_Tools']))
cd(currentpath)

Indices

% =========================================================================
% 0. LOAD RESULTS


load('.\Results\LOCAL')

[g2,index]	=   sort(g2)        ;
g1          =   g1(index)       ;
ExitFlag	=   ExitFlag(index) ;
Count       =   Count(index)    ;

m           =	length(g2)      ;
dens        =	(1:m)/m         ;

fighan = figure;
    FigPrep, AxisPrep, hold on
    plot(dens,g2.^(1/2),'k-','Linewidth',2)
    set(gca,'Ylim',[3.5 30],'Ytick',[3.5 4:9 10:5:40],'Yticklabel',[3.5 4:9 10:5:40])
    set(gca,'Yscale','log')
    PlotHorizontal(15.2783.^(1/2),'b-','Linewidth',2)
    PlotHorizontal(16.1119.^(1/2),'r-','Linewidth',2)
    legend({'Gradient-based search','Deterministic - Upper bound ($\overline{h}$)','Deterministic - Lower bound ($\underline{h}$)'},'Interpreter','latex','Location','East')
    ylabel('WRMSR [-]')
    xlabel('Cumulative fraction of searches [%]')
    grid on
    
    
drawnow
disp(sprintf(['All figures generated. \n']))

SaveMyFigure(fighan,'Figures\Fig11_Greedy')

drawnow
disp(sprintf(['All figures saved. \n']))
  
    
accepted        =	(g2.^(1/2))<=(16.1119.^(1/2)*1.01)	;
acc_rate        =	sum(accepted)/m                     ;


for iter=1:500
    
    rate(iter)	=	acc_rate*(1-acc_rate)^(iter-1)      ;
end


itermin         =   find(cumsum(rate)>=0.999,1,'first')	;

disp([' Acceptance rate: ' num2str((acc_rate))])
disp([' Required greedy searches: ' num2str(ceil(itermin))])
disp([' Expected number of simulations: ' num2str(ceil(itermin*mean(Count)))])

return
    