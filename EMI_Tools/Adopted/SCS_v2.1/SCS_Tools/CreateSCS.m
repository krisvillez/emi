function Model = CreateSCS(y,t,tk,Order,Lambda,ArgInc)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - CreateSCS.m
% -------------------------------------------------------------------------
% Description 
%
% CreateSCS checks which of the transitions are specified at a
% resolution enabling to solve for them to absolute precision.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model = CreateSCS(y,t,tk,Order,Lambda,ArgInc)
%
%
% INPUT
%	y:          Column vector representing the series of dependent data.
%               (required)
%	t:          Column vector representing the series of independent data. 
%               (optional, default=[1:length(y)])
%   tk:         Column vector representing the series of knot locations.
%               (optional, default=[1:length(y)]) 
%	Order:      Scalar indicating the order of the spline function.
%               (optional, default=4 (cubic splines)) 
%	Lambda:		Column vector of regularization coefficients for the
%               quadratic penalty on the function values and derivatives.
%               Its length must be one (scalar) or equal to the order of
%               the spline function. When this input is a scalar, the
%               highest derivative penalty is penalized only. Otherwise,
%               the elements of the vector are ordered from the penalty for
%               the function value to the one for the last computable
%               derivative.
%               (optional, default=0)
%   ArgInc:     Matrix describing the intervals of the domain which are
%               included in the computation of the objective function.
%               An empty input corresponds to the inclusion of the complete
%               spline function domain. 
%               (optional, default=[]) 
%
% OUTPUT
%	Model:     	Structure describing the spline model.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =========================================================================
% Modify arguments if necessary

if nargin<2 || isempty(t) % Assume equidistant measurements
    ny  =   size(y,1)   ;
    t   =   (1:ny)'     ;
end

if nargin<3 || isempty(tk) % Assume an interpolating spline is used
    tk  =   t           ;
end

if nargin<4 || isempty(Order) % Assume a cubic spline is used
    Order   =   4           ;
end

if nargin<5 || isempty(Lambda) % Assume that a low regularization parameter
    Lambda	=   0		;
end

if nargin<6 || isempty(ArgInc)
	ArgInc 	=	[]	;
end

if length(Lambda)==1
    Lambda  =   [ sparse(Order-1,1) ; Lambda]   ;
else
    Lambda  =   Lambda(:)                       ;
end

% =========================================================================
% Check inputs

assert(size(y,2)==1,'Multivariate support not available')
assert(all(diff(t)>=0),'Data series is not Ordered properly')
assert(all(diff(tk)>=0),'Knot series is not Ordered properly')
assert(length(y)==length(t),'argument and measurement vector do not match')
assert(length(Lambda)==Order,'vector Lambda not of proper size')

% =========================================================================
% Scale argument and measurement axis
% dtk         =   diff(tk)        ;
% dtk         =   dtk(dtk~=0)     ;
% t_scale     =   min(dtk)        ;
t_scale     =   max(tk)-min(tk) ;
t_ref       =   min(tk)         ;
y_scale     =   max(y)-min(y)   ;

t_ref   =   0   ;	
t_scale =   1   ;   
y_scale =   1   ;   

t           =   (t-t_ref)/t_scale   ;
tk          =   (tk-t_ref)/t_scale  ;
y           =   (y)/y_scale         ;

Model.t_scale   =   t_scale     ;
Model.t_ref     =   t_ref       ;
Model.y_scale   =   y_scale     ;

if ~isempty(ArgInc)
    Model.ArgInc 	=	ArgInc                  ;
    ArgInc          =   (ArgInc-t_ref)/t_scale  ;
end

% =========================================================================
%   Setup SCS

Model.type  =   'SCSD'  ;

% main inputs
Model.tk	=	tk      ;
Model.ty	=	t       ;
Model.y     =   y       ;
Model.Order =   Order   ;
Model.Lambda =   Lambda   ;

nk  =	length(tk)          ;
rng =	[min(tk) max(tk) ]  ;
ny  =   length(y)           ;

% setup basis

% =========================================================================
% get basis
spline  =   'nspline'           ;   % type of spline
if Order<=2
    spline  =   'bspline'           ;   % type of spline
end

switch spline                       % conditional to spline
    case 'bspline'
        nx      =   nk + Order - 2     ;   % number of basis functions
        basis   =   create_bspline_basis(rng, nx, Order, tk)   ;  % evaluate basis
    case 'nspline'
        nx      =   nk + Order - 4     ;   % number of basis functions
        basis   =   create_nspline_basis(rng, nx, Order, tk)   ;  % evaluate basis
end

% =========================================================================
% basis evaluated in knots
for d=0:Order-1
    Bk{d+1}   =   eval_basis(tk,basis,d)    ;
end
B   =   eval_basis(t,basis,0)    ;

Model.B 	= 	B 					;
Model.Bk	= 	Bk					;
Model.basis = 	basis 				;
Model.x     =   sparse(size(B,2),1) ;


% ---------------------
% Compute weight for each data point

if isempty(ArgInc)
	W	=	speye(ny) 			; % all data weighted equally
else
	w 		=	sparse(ny,1) 	;
    
	for col=1:size(ArgInc,2)
		w 	=	w+and(t>=ArgInc(1,col),t<=ArgInc(2,col)) ; % add '1' if argument value is within considered interval
	end
	w 		=	min(w,1) ; % just to be sure: data points should not have weights larger than one.
	W 		= 	diag(w) 		;
	W		=	sparse(W) 		;
end

% 	W	=	speye(ny) 			; % all data weighted equally
Model.W 	=	W 					;

% ---------------------
% For SOCP:

if Order>=3
    X2P_3		=   GetMapsX2P(Bk,3) 	;
    Model.X2P_3	=	X2P_3 				;
end

if Order>=4
    X2P_4		=   GetMapsX2P(Bk,4) 	;
    Model.X2P_4 =	X2P_4 				;
end


% =========================================================================
% Penalty matrices
    
L	=	zeros(nx) ;
for p=1:Order
    if Lambda(p)>0
        switch p
            case Order
                
				if isempty(ArgInc)
					dtk     =   diff(tk)                ;
				else
					atk 	=	[ tk(1:end-1) tk(2:end) ]	; % knot intervals
					dtk 	=	sparse(nk-1,1) 				; % initialize (zero)
					for col=1:size(ArgInc,2) % for each inclusion interval
						% determine the section of each knot interval covered by the inclusion interval:
						at1 =	min(max(ArgInc(1,col)-atk(:,1),0),atk(:,2)-atk(:,1))	;
						at2 =	min(max(ArgInc(2,col)-atk(:,1),0),atk(:,2)-atk(:,1))    ;
						% covered part of each knot interval:
						dtk =	dtk+(at2-at1);
					end
                end
                
                %dtk     =   diff(tk)                ;
                    
                Bk3     =   Bk{end} 				;
                IB3k    =   diag(dtk)*Bk3(1:nk-1,:) ;
                Pen  	=  IB3k'*IB3k              	;
                
            otherwise
				if isempty(ArgInc)
					Pen = 	eval_penalty(basis,p-1)   ;
                else
					Pen     =	sparse(nx) 				;
					for col=1:size(ArgInc,2)
						Pen 	=	Pen + eval_penalty(basis,p-1,ArgInc(:,col))   ;
					end
                end
                Pen = 	eval_penalty(basis,p-1)   ;
        end
        L	=	L + Lambda(p)*Pen ;
    end
    
end

% figure, surf(L)

Model.L = L ;
