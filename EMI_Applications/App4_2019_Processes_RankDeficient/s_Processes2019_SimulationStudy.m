% -------------------------------------------------------------------------
% EMI Toolbox - s_Processes2019_SimulationStudy.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces all figures related to the simulated example
% appearing in [1]. 
%
% REFERENCES
%   [1] Villez, K, Billeter, J, Bonvin, D (2019). Incremental Parameter
%       Estimation under Rank-Deficient Measurement Conditions. Processes,
%       7(2), 75.
%   [2] Villez, K (2018). Extent-based Model Identification under
%       Incomplete Observability Conditions. (TR-006-03-0). Technical
%       Report, Eawag, D�bendorf, Switzerland.  
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% ------------------------------------------------------------------------- 

clc
clear all
close all

addpath ('.\systemdef\')
addpath ('..\..\EMI_Tools\Part3_RankDeficient\')
rng (42,'twister')

% ========================================================================
%   SYSTEM DEFINITION AND INFORMATION
% ========================================================================

s_SimulationStudy_SystemDef

% ========================================================================
%   SIMULATE EXPERIMENT
% ========================================================================
[n_t,ctil_t]        =	SimulationExperiment (N_,M_eps,M,S,V,select_h,n_0,ratefun,t_,theta_) ;

groundtruth.theta_	=	theta_	;
groundtruth.n_t     =	n_t	;

% ========================================================================
%   TUNING OF PARAMETER ESTIMATION AND GRAPHICAL OUTPUT
% ========================================================================

model = ModelSetup (V,n_0,M_eps,N_,name_species,t_,select_h,ratecell,T,name_parameters);

displaystyle                    =	'grey'                      ;
[options_compute,options_graph] =	OptionsEMI (displaystyle,unit_concentration,unit_extents)   ;
options_graph.gridpos_conc      =   [	2.3     .33      ]   ;
options_graph.gridpos_extent	=   [	0.1     .3      ]   ;

xX      =	[   0       1       0       2       2           ]   ;
yX      =	[	0       0       1       1       0           ]   ;
xT      =	[	-.2     +1      -.2     +2.2    +2.2 	-.8 ]   ;
yT      =	[	-.8     -.8     +1.8	+1.8	-.8     -.2 ]   ;
xCHI    =	[	-.6     +1      3	]   ;
yCHI	=	[	+.5     +.8     0.5	]	;

% ========================================================================
% ========================================================================

scenarioname = {'A','B','C','D','E'};

for Setup = 1:5
    
    disp('====================================================================')
    disp(['  SETUP: ' num2str(Setup) ])
    disp('====================================================================')
    
    % ====================================================================
    %   MEASUREMENT GENERATION
    % ====================================================================
    
    switch Setup
        case 1, M_	= [	M_master(2:3,:)	; M_master(5,:)+ M_master(6,:)	];
        case 2, M_	=	M_master(2:3,:)             ;
        case 3, M_	=   M_master([  2:3 5:6 ],:)	;
        case 4, M_	=   M_master([ 1 3 5 ],:)       ;
        case 5, M_	=   M_master                    ;
        otherwise
    end
  
    ytil_h                      =	M_*ctil_t       ;
    model.static.M_             =	M_              ;
    
    model.observations.ytil_h	=   ytil_h          ;
    groundtruth.y_              =   M_*n_t/V        ;
    
    
    [fighan,figname] = PlotMeasurements (model,options_graph,scenarioname{Setup},groundtruth);
    
    figpath = [ '.\figures\' figname ];
    savefig(fighan,figpath);
    saveas(fighan,figpath,'epsc')
    
    % ====================================================================
    %   INITIAL MODEL
    % ====================================================================
    
     model0	=	model	;
     theta_0	=	theta_  ;
    
    % ====================================================================
    %   SIMULTANEOUS MODEL IDENTIFICATION
    % ====================================================================
    
    disp(['------------------------------------------------------------' ])
    disp('	PART I: SIMULTANEOUS MODEL IDENTIFICATION')
    disp(['------------------------------------------------------------' ])
    
     model1	=	SimultaneousEstimation (model0,theta_0,options_compute)	;
    
    % ====================================================================
    %   INCREMENTAL MODEL IDENTIFICATION
    % ====================================================================
    
    disp(['------------------------------------------------------------' ])
    disp('	PART II: INCREMENTAL MODEL IDENTIFICATION')
    disp(['------------------------------------------------------------' ])
    
    % --------------------------------------------------------------------
    %   LABEL EXTENTS AND EXTENT DIRECTIONS + COMPUTE THEIR VALUES 
    % --------------------------------------------------------------------
    
    % Analysis
    [xbarhat_o,xbar_o,model]	=   Extents (model,options_compute,groundtruth)	;
    groundtruth.xbar_o          =   xbar_o                                          ;
    
    % Produce figure
    [fighan,figname]	=       PlotComputedExtents (model,options_graph,scenarioname{Setup},groundtruth);
    figpath             =	[	'.\figures\'	figname	]   ;
    savefig (fighan,figpath);
    saveas (fighan,figpath,'epsc')
    
    % --------------------------------------------------------------------
    %   SYSTEM PARTITIONING
    % --------------------------------------------------------------------
    
    % Analysis
    model           =	Partitioning (model,options_compute)           ;
    
    % Produce figure 1
    Xdata           =	[ xX xCHI(end-model.static.rho_o+1:end)	xT	]   ;
    Ydata           =	[ yX yCHI(end-model.static.rho_o+1:end)	yT	]   ;
    options_graph.Xdata                 =   Xdata                       ;
    options_graph.Ydata                 =   Ydata                       ;
    options_graph.showmeasurementarcs	=	false                       ;
    [fighan,figname]    =	PlotGraph (model,options_graph,scenarioname{Setup});
    figpath             =	[	'.\figures\'	figname{1}	'_without'	]	;
    savefig (fighan,figpath)    ;
    saveas (fighan,figpath,'epsc')
    
    % Produce figure 2
    options_graph.showmeasurementarcs	=	true                        ;
    options_graph.inset                 = [ .3	1.1	1.4	1.4*0.75	]	;
    [fighan,figname]	=	PlotGraph (model,options_graph,scenarioname{Setup});
    figpath             =	[	'.\figures\'	figname{1}	'_with'	]       ;
    savefig (fighan,figpath)                                            ;
    saveas (fighan,figpath,'epsc')
    
    % --------------------------------------------------------------------
    %   PARAMETER ESTIMATION FOR SUBSYSTEMS
    % --------------------------------------------------------------------
    model1j	=	IncrementalEstimation (model,options_compute,theta_0)                  ;
    
    % --------------------------------------------------------------------
    %   PARAMETER ESTIMATION FOR WHOLE SYSTEM
    % --------------------------------------------------------------------
    theta1	=	model1j.kinetic.theta_hat                                    ;
	model10 =	SimultaneousEstimation (model1j,theta1,options_compute)  ;
    
    [fighan,figname]	=	PlotModelFit (model1,model10,options_graph,scenarioname{Setup},groundtruth);     
    figpath             =	[	'.\figures\'	figname	]               ;
    savefig (fighan,figpath)                                            ;
    saveas (fighan,figpath,'epsc')
    
end

return





