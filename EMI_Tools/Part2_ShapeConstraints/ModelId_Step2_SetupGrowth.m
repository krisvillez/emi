function [ratefun,npar,modelSCS]	=	ModelId_Step2_SetupGrowth(Name,Knots)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Step2_SetupGrowth.m
% -------------------------------------------------------------------------
% Description
%
% Sets up a growth rate law as a function handle.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[ratefun,npar,modelSCS]	=	ModelId_Step2_SetupGrowth(Name,Knots)
%
% INPUT
%   Name        :   Name of the rate law
%   Knots       :   Location of spline knots [necessary if Name starts with
%                   'SCS ' ]
%
% OUTPUT
%   ratefun     :   rate law function handle
%   npar        :   number of parameters involved in the rate law
%   modelSCS    :   shape constrained splines model as a structure
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-17
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if strncmpi(Name,'SCS',3)
    
    % ---------------------------------------------------------------------
    % Setup SCS model
    
    order           =	4                                           ;
    GoThroughOrigin =	true                                        ;
    modelSCS        =	SetupDynSCS( Knots,order,GoThroughOrigin )	;
    
    B12             =   [   -modelSCS.B2k(1:end,:)                  ;       % basis evaluation for curvature at the spline knots
                            +modelSCS.B1k(end,:)                ]	;       % basis evaluation for first derivative at the uttermost right spline knot
    P               =	pinv(full(B12))                             ;       % matrix to reconstruct spline coefficients from derivatives 

    % ---------------------------------------------------------------------
    % Gather output:
    ratefun         =   @(S,pars) eval_basis(min(Knots(end),max(S,Knots(1))),modelSCS.basis)*P*pars(:)	;
    npar            =	getnbasis(modelSCS.basis)-length(getdropind(modelSCS.basis))                    ;
    modelSCS.P      =	P                                           ;
    
    
else
    
    % ---------------------------------------------------------------------
    % Find matching rate in the libary
    
    Library         =	CreateLibrary()             ;
    Names           =   {Library.name}              ;
    index           =   find(ismember(Names,Name))  ;
    
    % ---------------------------------------------------------------------
    % Gather output:
    
    ratefun         =   Library(index).ratefun      ;
    npar            =   length(Library(index).pars)	;
    modelSCS        =   []                          ;
    
end
 

end

