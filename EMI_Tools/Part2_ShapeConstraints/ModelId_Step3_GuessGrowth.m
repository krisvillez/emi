function par1_hat	=	ModelId_Step3_GuessGrowth(TrendModels,ratefun1,npar1,Shape)

% -------------------------------------------------------------------------
% EMI Toolbox - ModelId_Step3_GuessGrowth.m
% -------------------------------------------------------------------------
% Description
%
% Provides and initial guess for the kinetic parameters of the growth rate
% law.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	
%   par1_hat = ModelId_Step3_GuessGrowth(TrendModels,ratefun1,npar1,Shape)
%
% INPUT
%   TrendModels	:   Shape constrained spline models
%   ratefun1	:   Rate law function handle
%   npar1       :   Number of parameters in the rate law
%   Shape       :   Shape constraint [for SCS type rate laws only]
%
% OUTPUT
%   par1_hat	:   Parameter estimates for the growth rate
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-19
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<4 || isempty(Shape)
    Shape	=	[   ]   ;
end

% =========================================================================
% Process available information:

tsim        =	TrendModels{1}.tk(1):1/60:TrendModels{1}.tk(end)	;
Ssim        =   EvalSCS(TrendModels{1},tsim)                        ;
Xsim        =   EvalSCS(TrendModels{2},tsim)                        ;
dPsim       =   -EvalSCS(TrendModels{1},tsim,1)                     ;
x1          =	Ssim                                                ;
y1          =	dPsim./Xsim                                         ;


% =========================================================================
% Estimation

resid       =	@(pars) (ratefun1(x1,pars)-y1)                      ;
lb          =   .0001*ones(npar1,1)                                 ;
if ~isempty(Shape) && strcmp(Shape,'N')
    lb(end) =	-inf                                                ;
end
ub          =	inf(npar1,1)                                        ;
par_init    =   .01*ones(npar1,1)                                   ;
par1_hat    =   CLS(resid,par_init,lb,ub)                           ;
 
% =========================================================================
%     
% figure, 
% hold on
%     plot(x1,y1,'.')
%     plot(x1,ratefun1(x1,par1_hat),'.-')
%     xlabel('Substrate concentration [mg/L]')
%     ylabel('Specific substrate utilization rate [mg/L.h]')
%     pause

end

