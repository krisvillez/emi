function B = DiagBlocks(A,n)

% -------------------------------------------------------------------------
% Spike_B toolbox - AxisPrep.m
% -------------------------------------------------------------------------
% Description
%
% This function builds a block diagonal matrix by repeating a given matrix 
% a number of times in a block-diagonal arrangement.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	B = DiagBlocks(A,n)
%
% INPUT
%   A	:	Matrix to be repeated
%	n 	:	Number of repetitions
%
% OUTPUT
%   B	:	Block diagonal matrix with n repetitions of A on the block
% 			diagonal
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-21
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2006-2015 Kris Villez
%
% This file is part of the Spike_B Toolbox for Matlab/Octave. 
% 
% The Spike_B Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_B Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

a       =   cell(1,n)       ;
[a{:}]  =   deal(sparse(A)) ;
B       =   blkdiag(a{:})   ;