
% -------------------------------------------------------------------------
% EMISSUN Toolbox - Initial.m
% -------------------------------------------------------------------------
% Description
%
% Executes some initial steps to be executed at every script.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Experiment setup

total_time      =	10/24       ;	% [d] total simulated time
sampling_time   =	10/60/24	;	% [d] measurement sampling interval

TAN0            =   5/14        ;	% [mol/L]
% Total Ammonia Nitrogen (TAN) at start of experiment. All other components or in equilibrium.
Volume          =   1          ;	% [L] Liter

% -------------------------------------------------------------------------
% System properties

% Stoichiometry
N           =	[   -3/2	0	-1	+1	0	0	0	0	0	+1	;
                    -1/2    0   0   -1  0   +1  0   0   0   0   ;
                    0       -1  +1  0   0   0   0   +1  0   0   ;
                    0       0   0   -1  +1  0   0   +1  0   0   ;
                    0       0   0   0   0   -1  +1  +1  0   0   ;
                    0       0   0   0   0   0   0   +1  +1  -1  ]   ;
        
% Definition of kinetic species and components
Ebar        =	[	1 0 0 0 0 0 0 0 0 0 ;
                    0 1 1 0 0 0 0 0 0 0 ;
                    0 0 0 1 1 0 0 0 0 0 ;
                    0 0 0 0 0 1 1 0 0 0 ;
                    0 1 0 1 0 1 0 1 0 1 ;
                    0 0 0 0 0 0 0 0 1 1 ]   ;
     
[Sbar,S]	=       size(Ebar)	;   % number of kinetic species and components, number of species

% =========================================================================
% =========================================================================
% =========================================================================

% Reaction rate laws and parameters

global sO2  sNH4 sNH3 sHNO2 sNO2 sHNO3 sNO3 sH sOH sH2O
sO2	= 1;	sNH4 = 2;	sNH3 = 3;	sHNO2 = 4;	sNO2 = 5;  
sHNO3 = 6;  sNO3 = 7;   sH = 8;     sOH = 9;	sH2O = 10;

Rk          =   2           ;
map_c2r     =	false(Rk,S)	;
Reactions	=	cell(Rk,1)	;

for i =1:Rk
    switch i
        case 1 
            % NITRITATION
            map_c2r(i,sNH3)     =	true	;   % Indicate which species enter the rate law expression
            
            name  	=	'Haldane'           ;   % Name
            ratefun =	@(S,theta) ...          % Rate law
                            sign(S)*(abs(S)./((theta(1))+(theta(2))*abs(S)+(theta(3))*abs(S).^2)) ;
            theta   =	[	.025	.1 2.5	]	;       % Kinetic parameters
        case 2 
            % NITRATATION
            map_c2r(i,sHNO2)	=	true    ;	% Indicate which species enter the rate law expression 
            
            name    =   'Monod'             ;   % Name
            ratefun	=	@(S,theta) ...          % Rate law
                            sign(S).*abs(S)./((theta(1))+(theta(2))*abs(S)) ;
            theta	= 	[	0.00011	1.1 ]   ;   % Parameter values 
    end
    
    Reaction.name       =	name        ;
    Reaction.ratefun	=	ratefun     ;
    Reaction.theta      =	theta       ;
    Reactions{i}        =   Reaction    ;
    
end
    
% Equilibrium setup
pKNH3               =	9.24			;   % pKa/pKw values
pKNO2               =	3.25			;
pKNO3               =	-1.4			;
pKw                 =   14				;
pHguess             =   11               ;   % Initial guess for pH during equilibrium computation

% Measurement equations
MG                  =	3               ;
G                   =	zeros(MG,S)     ;   % Measurement gain matrix
G(1,[sNH3 sNH4])	=	1               ;
G(2,[sHNO2 sNO2 ])	=	1               ;
G(3,[sHNO3 sNO3])	=	1               ;

sigma	= [	0.01*ones(1,3)	0.05 ]'     ;   % Measurement error standard deviations

% =============================================
% Derived info

M   		=   MG+1                            ;   % number of measurements
nbar0       =   [ -1  TAN0  0 0 -1 -1 ]'        ;	% complete initial conditions

Nbar        =   (N*Ebar')                       ;   % Reduced stoichiometrix matrix - all reactions
Nbar_k      =   Nbar(any(Nbar~=0,2),:)          ;   % Reduced stoichiometrix matrix - kinetically controlled reactions only

pKa         =	[	pKNH3	pKNO2	pKNO3	]   ;   % acid dissociation constants
IonType     =	[   +1	-1	-1              ]	;   % Acid/Base type of the ionic species in each acid/base reaction
nc_index	=       2:4                         ;   % Indices of components involved in equilibrium computation
 
x2c         =   @(t,x,Model) Extent_2_Concentration(Model,x,pHguess)	; % Function which maps extents to species concentrations

% =============================================
% Simulation - Setup

ty              =	0:sampling_time:total_time	;   % Sampling times
dt              =   1/(60*24)                   ;   % Simulation reporting step
tsim            =   ty(1):dt:ty(end)            ;   % Simulated time points
tsim            =   sort(unique([ tsim ty ]))   ;   % Simulated time points - include measurement sampling times
sample_index 	=   ismember(tsim,ty)           ;   % Boolean indicator pointing out measurement sampling times in simulated time points
H               =   length(ty)                  ;   % Number of measurement samples

% =============================================
% Compile Model

Model.dimensions.H              =	H               ;
Model.dimensions.M              =	M               ;
Model.dimensions.MG             =	MG              ;
Model.dimensions.Rk             =	Rk              ;
Model.dimensions.S              =	S               ;
Model.dimensions.Sbar           =	Sbar            ;

Model.Equilibrium.pKa           =   pKa             ;
Model.Equilibrium.pKw           =   pKw             ;
Model.Equilibrium.IonType       =   IonType         ;
Model.Equilibrium.nc_index      =   nc_index        ;

Model.Measurement.G             =	G               ;
Model.Measurement.tsim          =	tsim            ;
Model.Measurement.sample_index  =   sample_index    ;
Model.Measurement.sigma         =	sigma           ;

Model.map_c2r                   =	map_c2r         ;
Model.Reactions                 =   Reactions       ;
Model.Volume                    =	Volume          ;

Model.nbar0 = nbar0;
Model.Nbar_k                    =	Nbar_k          ;
Model.Ebar                      =	Ebar            ;
Model.x2c                       =	x2c             ;
