function options = OptimsetBB(model)

% -------------------------------------------------------------------------
% Spike_O Toolbox - OptimsetBB.m
% -------------------------------------------------------------------------
% Description
%
% This function generates default options for the branch-and-bound
% algorithm
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	options = optimsetBNB(model)
%
% INPUT
%   model   :   mathematical problem
%
% OUTPUT
%   options :   structure with all default options
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------



type='unknown';
if nargin>=1 && ~isempty(model)
    if isfield(model,'type')
        type =model.type;
    elseif ischar(model)
        type =model ;
    end
end

switch type
    case 'SCSD'
        
        default.boundfun    =   @BoundSCS      ;
        default.boundlow    =   @BoundSCSlow    ;
        default.boundupp    =   @BoundSCSupp    ;
        default.earlystop   =   0               ;
        % Node selection:
            %   1=best upper bound 
            %   2=best lower bound 
            %   3=depth first,
            %   4=breadth first
        default.node        =   4               ;
        % Variable selection:
            %   1=largest range, only currently supported option
        default.var         =   1               ;
        default.rounding    =   1               ;
        % Branching strategy
            %   1=split along 1 selected variable
            %   2=split along all variables
            %   3=split all variables at the same splitting value. this is
            %   only valid if all variables are subject to monotonicity
            %   constraints, e.g. x(j)>=x(j-1) for all j>=2.
        default.branching   =   1               ;
        % Splitting = Number of branches per variable. Only applied if
        % branching is 1 or 2. Total number of branches = (#splitted
        % variables)^splitting.
        default.splitting   =   2               ;
        
        dkvalues = unique(diff(sort(model.tk))) ;
        if all(rem(dkvalues,1)==0)
            resol  = GCDlist(unique(diff(sort(model.tk))))  ;
        else
            resol = min(dkvalues);
        end
        default.resolution  =   resol                    ;
        
        if isfield(model,'QR')
            if any(model.QR(:,end)>0)
                default.resolution	=	(default.resolution)*(1/2)^3	; 
            end
        end
        default.maxiter     =   1e6             ;
        default.verbose     =   2               ; % values: 0-2
        default.verify      =   1               ; % values
        default.graph       =   1               ;
        
        if isfield(model,'tk')
            if isfield(model,'multivar')
                defaultsplit        =   [ nan ; nan ];
                for j=1:model.multivar
                    defaultsplit(1)     = min(defaultsplit(1),model.tk{j}(1)) ;
                    defaultsplit(2)     = min(defaultsplit(2),model.tk{j}(end)) ;
                end
                default.defaultsplit   =   defaultsplit             ;
            else
                default.defaultsplit   =   model.tk([1 end])             ;
            end
        else
            default.defaultsplit   =   [-inf inf]               ;
        end
        
        options             =   default     ;
        
    case 'SCS'
		default.boundfun    =   @BoundSCS      ;
		default.boundlow    =   @BoundSCSlow    ;
        default.boundupp    =   @BoundSCSupp    ;
        default.earlystop   =   0               ;
        % Node selection:
            %   1=best upper bound 
            %   2=best lower bound 
            %   3=depth first,
            %   4=breadth first
        default.node        =   2               ;
        % Variable selection:
            %   1=largest range, only currently supported option
        default.var         =   1               ;
        default.rounding    =   0               ;
        % Branching strategy
            %   1=split along 1 selected variable
            %   2=split along all variables
            %   3=split all variables at the same splitting value. this is
            %   only valid if all variables are subject to monotonicity
            %   constraints, e.g. x(j)>=x(j-1) for all j>=2.
        default.branching   =   1               ;
        % Splitting = Number of branches per variable. Only applied if
        % branching is 1 or 2. Total number of branches = (#splitted
        % variables)^splitting.
        default.splitting   =   2               ;
        
        dkvalues = unique(diff(sort(model.tk))) ;
        if all(rem(dkvalues,1)==0)
            resol  = GCDlist(unique(diff(sort(model.tk))))  ;
        else
            resol = min(dkvalues);
        end
        default.resolution  =   resol                    ;
        
        if isfield(model,'QR')
            AnyDisc     =   false       ;
            if iscell(model.QR)
                nQR         =	length(model.QR)  ;
                for iQR=1:nQR
                    AnyDisc =   or(AnyDisc,any(model.QR{iQR}(:,end)>0)) ;
                end
            else
                AnyDisc     = any(model.QR(:,end)>0)    ;
            end
            if AnyDisc
                default.resolution	=	(default.resolution)*(1/2)^5	; 
            end
        end
        
        default.maxiter     =   1e6             ;
        default.verbose     =   2               ; % values: 0-2
        default.verify      =   0               ; % values
        default.graph       =   1               ;
        if isfield(model,'tk')
            default.defaultsplit   =   model.tk([1 end])               ;
        else
            default.defaultsplit   =   [-inf inf]               ;
        end
        options             =   default     ;
    otherwise
        if exist('Bounds'),		default.boundfun    =   @Bounds      ;
		else 
% 			id 	=	'OptimsetBB:BoundsAbsent'	;
% 			msg	=	'The function ''Bounds'' could not be found. Ensure that your own function is added to the produced structure ''options''.';
% 			warning(id,msg)
		end
		
		if exist('Boundlow'),	default.boundfun    =   @Boundlow      ;
		else 
% 			id 	=	'OptimsetBB:BoundlowAbsent'	;
% 			msg	=	'The function ''Boundlow'' could not be found. Ensure that your own function is added to the produced structure ''options''.';
% 			warning(id,msg)
		end
		
		if exist('Boundupp'),	default.boundfun    =   @Boundupp      ;
		else 
% 			id 	=	'OptimsetBB:BounduppAbsent'	;
% 			msg	=	'The function ''Boundupp'' could not be found. Ensure that your own function is added to the produced structure ''options''.';
% 			warning(id,msg)
		end
		
        default.earlystop   =   0               ;
        default.node        =   4               ;
        default.var         =   1               ;
        default.rounding    =   0               ;
        default.branching   =   1               ;
        default.splitting   =   2               ;
        default.resolution  =   1e-2            ;
        default.maxiter     =   1e6             ;
        default.verbose     =   2               ; % values: 0-3
        default.verify      =   0               ; % values
        default.graph       =   1               ;
        default.defaultsplit   =   []               ;
        options             =   default     ;
end