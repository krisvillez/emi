function [x_h,x_t]	=	Simulation (theta_,ratefun,t_,select_h,V_eval,x_0)

% -------------------------------------------------------------------------
% EMI Toolbox - Simulation.m
% -------------------------------------------------------------------------
% Description
%
%   Simulation of linear combinations of the states described by a set of
%   ordinary differential equations.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:  [x_h,x_t]	=	Simulation (theta_,ratefun,t_,select_h,V_eval,x_0)
%
% INPUT
%   theta_      :   kinetic parameters
%   ratefun     :   vector-valued rate expression
%   t_          :   simulation timestamps
%   select_h    :   index of timestamps with measurement samples
%   V_eval      :   linear project matrix to select/combine the model states
%   x_0         :   initial states
%
% OUTPUT
%	x_h         :   state outputs at measurement sampling timestamps
%   x_t         :   state outputs at simulation timestamps
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% Make sure parameters have reasonable values:
theta_	=	min(max(theta_,1e-6),1e3)	;

% Initial conditions:
S       =	size(V_eval,1);
if nargin<6 || isempty(x_0)
    x_0	=	zeros(S,1);
end

% Solve ODE:
[~,x_t]	=	ode15s(@(t,x_) ratefun (t,x_,theta_),t_,x_0 ) ;

% Post-processing:
x_t     =	x_t'            ;
x_t     =	V_eval'*x_t 	;
x_h     =	x_t(:,select_h)	;

end

