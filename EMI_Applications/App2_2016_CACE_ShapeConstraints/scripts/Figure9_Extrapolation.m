
% -------------------------------------------------------------------------
% EMI Toolbox - Figure9_Extrapolation.m
% -------------------------------------------------------------------------
% Description
%
% This script reproduces Figure 9 in [1].
%
% [1] Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., Villez, K.
% (2016). On the use of shape-constrained splines for biokinetic process
% modeling. Computers and Chemical Engineering, Accepted, In Press.
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

disp(['===================== FIGURE 9 ====================='])

load Output678

WRMSR = [];

for j=1:5
    model_name	=   model_names{j}	;
    S0          =   15      ; % any value below 25.
    X0          =   3       ; % any value in neighbourhood of identified.
    
    Colours     =   [0 0.35 0.65]'*ones(1,3)	;
    LineType    =   {'o-','+-','x-'}        ;
    
    % ====================================================================
    % i. SETUP SELECTED CASE
    % ====================================================================
    
    tsim        =   [	0:1/3:10	]                       ;
    c0          =	[	S0	;	X0	]                       ;
    ratefun     =	SelectFunction(model_name)              ;
    [tf,index]	=   ismember(model_name,model_names)        ;
    
    % ====================================================================
    % iii. SIMULATE WITH TRUE RATE LAW
    % ====================================================================
    
    all_model_names = {};
    Csim_all = {} ;
    Esim_all = {} ;
    
    [~,Ctrue]    =	EvaluateODE(pars,ratefun,c0,tsim)      ;   % noise-free concentrations
    
    
    Csim_all{length(Csim_all)+1,1} = Ctrue ;
    Esim_all{length(Esim_all)+1,1} = [] ;
    all_model_names{length(all_model_names)+1,1} = model_name ;
    
    % ====================================================================
    % iii. SIMULATE WITH EVERY FITTED RATE LAW
    % ====================================================================
    
    
    % 1. Library
    nLib = length(Output(index).Lib_models_selected) ;
    for iLib = 1:nLib
        
        ratefun1    =	Output(index).Lib_models(iLib).rate     ;
        pars1        =	Output(index).Lib_models(iLib).pars     ;
        
        [~,Csim]    =	EvaluateODE(pars1,ratefun1,c0,tsim)       ;   % noise-free concentrations
        
        Esim        =   Csim - Ctrue                            ;
        
        WRMSR(j,length(Esim_all)) = sqrt(sum( sum( (diag(1./sigmaY)*Esim).^2 ) )/(4*31)) ;
        
        Csim_all{length(Csim_all)+1,1}      =	Csim            ;
        Esim_all{length(Esim_all)+1,1}      =	Esim            ;
        all_model_names{length(all_model_names)+1,1} = Output(index).Lib_models(iLib).name ;
        
    end
    
    % 2. SCS
    nLib = length(Output(index).SCS_models) ;
    for iLib = 1
        
        ratefun2    =	Output(index).SCS_models(iLib).rate     ;
        pars2        =	Output(index).SCS_models(iLib).pars     ;
        
        [~,Csim]    =	EvaluateODE(pars2,ratefun2,c0,tsim)       ;   % noise-free concentrations
        
        Esim        =	Csim - Ctrue                            ;
        
        WRMSR(j,length(Esim_all)) = sqrt(sum( sum( (diag(1./sigmaY)*Esim).^2 ) )/(4*31)) ;
        
        Csim_all{length(Csim_all)+1,1}      =	Csim            ;
        Esim_all{length(Esim_all)+1,1}      =	Esim            ;
        all_model_names{length(all_model_names)+1,1} = Output(index).SCS_models(iLib).name ;
        
    end
    
    varnames	=   {'S','P','X'}       ;
    leg_names	=	{   }               ;
    
    if j==3
        figure(9),
        FigPos      =	get(gcf,'Position')	;
        FigPos(3)	=	4/3*FigPos(3)       ;
        
        set(gcf,'Position',FigPos)          ;
        
        subplot(2,1,1), hold on, box on
        set(gca,'Xlim',[tsim(1) tsim(end)],'Xtick',0:2:10,'FontSize',20)
        set(gca,'Xticklabel',[],'Ylim',[-.5 S0+1],'Ytick',0:4:16)
        for j=1:3
            for m=1:length(Csim_all)
                plot(tsim,Csim_all{m}(j,:),LineType{m},'Color',Colours(j,:),'Linewidth',1)
                leg_names{length(leg_names)+1} = [ varnames{j} ' - ' all_model_names{m} ] ;
            end
        end
        legend(leg_names,'Location','EastOutSide')
        ylabel({'concentrations','[mg/L]'},'Position',[-1.5 sum(get(gca,'Ylim'))/2])
        %     grid on
        AxisPos1    =	get(gca,'Position')             ;
        AxisPos1(1) =   AxisPos1(1)+.05                 ;
        AxisPos1(4) =   AxisPos1(4)+(AxisPos1(2)-.5)	;
        AxisPos1(2) =   .55                             ;
        set(gca,'Position',AxisPos1)                    ;
        AxisPos1    =	get(gca,'Position')             ;
        drawnow
        
        subplot(2,1,2), hold on, box on
        set(gca,'Xlim',[tsim(1) tsim(end)],'Xtick',0:2:10,'FontSize',20)
        set(gca,'Ylim',[-1 +1]*.6,'Ytick',-.6:.3:.6)
        for j=1:3
            for m=2:length(Esim_all)
                plot(tsim,Esim_all{m}(j,:),LineType{m},'Color',Colours(j,:),'Linewidth',1)
            end
        end
        xlabel('time [h]')
        ylabel({'prediction errors','[mg/L]'},'Position',[-1.5 sum(get(gca,'Ylim'))/2])
        %     grid on
        AxisPos2        =	get(gca,'Position')         ;
        AxisPos2([1 3]) =	AxisPos1([1 3])             ;
        AxisPos2(2)     =   .15                         ;
        set(gca,'Position',AxisPos2)                    ;
    end
    
    disp('---Finished---')
    
end


figSI1	=	figure(101);
figure(101), 
    hold on, box on
    PlotWRMSR_validation(WRMSR,model_names(1:j),figSI1);
    set(figSI1,'name','Figure 101','numbertitle','off')
    