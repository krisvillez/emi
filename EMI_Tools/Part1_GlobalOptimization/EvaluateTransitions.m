function [CImax,CIrise,CIfall] =EvaluateTransitions(Theta)

% -------------------------------------------------------------------------
% EMI Toolbox - EvaluateTransitions.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the interval location of the extremum and the inflection points
% of the reaction rate function.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[CImax,CIrise,CIfall] =EvaluateTransitions(Theta)
%
% INPUT
%   Theta   :   Parameter vector set
%
% OUTPUT
%   CImax   :   Concentration interval for maximum
%   CIrise  :   Concentration interval for rising inflection point
%   CIfall  :   Concentration interval for falling inflection point
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

CImax = [sqrt(1./prod(Theta(2,2:3))) sqrt(1./prod(Theta(1,2:3))) ] ; % interval indicating the possible substrate concentrations at which a maximum reaction rate is observed

% 2.1. interval indicating the possible relative substrate concentrations at
% which the rising point of inflection is observed for the reaction rate:
theta       =   [   Theta(1,1)	Theta(1,2) Theta(2,3)] ;
rts1        =       roots([3*theta(3)^2 0 -8*theta(1)*theta(3) -2*theta(2)*theta(1) theta(1).^2]);
rts1         =       max(rts1)    ;
theta       =   [ Theta(2,1)	Theta(2,2) Theta(1,3)] ;
rts2 = roots([3*theta(3)^2 0 -8*theta(1)*theta(3) -2*theta(2)*theta(1) theta(1).^2]);
rts2 = max(rts2);
CIrise     =   [rts1 rts2 ];
% 2.2. interval indicating the possible relative substrate concentrations at
% which the falling point of inflection is observed for the reaction rate:
theta       =   [ Theta(1,1)	Theta(2,2) Theta(2,3)] ;
rts1        =       roots([3*theta(3)^2 0 -8*theta(1)*theta(3) -2*theta(2)*theta(1) theta(1).^2]);
rts1 = rts1(rts1>0);
rts1=min(rts1);
theta       =   [ Theta(2,1)	Theta(1,2) Theta(1,3)] ;
rts2 = roots([3*theta(3)^2 0 -8*theta(1)*theta(3) -2*theta(2)*theta(1) theta(1).^2]);
rts2 = rts2(rts2>0);
rts2=min(rts2);
CIfall     =   [rts1 rts2 ];