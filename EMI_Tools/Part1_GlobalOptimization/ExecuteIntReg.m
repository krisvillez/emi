function [g,beta]=ExecuteIntReg(xL,xU,y)

% -------------------------------------------------------------------------
% EMI Toolbox - ExecuteIntReg.m
% -------------------------------------------------------------------------
% Description
%
% Executes regression with bounded inputs.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[g,beta]=ExecuteIntReg(xL,xU,y)
%
% INPUT
%   xL      :   Lower bounds for the inputs
%   xU      :   Upper bounds for the inputs
%   y       :   Outputs
%
% OUTPUT
%   g       :   Relaxed weighted least-squares objective function value
%   beta    :   Relaxed regression vector estimate
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

global options_fmincon

[nSample]    =   length(y);


[nSample,nPar]    =   size(xL); % nPar = no. of regression parameters

% ==========================================
% Initial feasible solution
XX      =   (xL+xU)/2;
yy     =       y ;

    % ==========================================
    % Optimize

beta = pinv(XX)*yy;

g = RegBound( beta,y,xL,xU );

if g~=0
    objfun= @(Beta) RegBound( Beta,y,xL,xU );
    [beta,g]=fminsearch(@(a) objfun(a),beta,options_fmincon);
    
    
    if any(beta<0)
        
        beta = max(beta,0);
        A= -eye(length(beta));
        B= zeros(length(beta),1);
        
        [beta,g]=fmincon(@(a) objfun(a),beta,A,B,[],[],[],[],[],options_fmincon);
    end
end
% else
%     eps_L = xL*beta - y;
%     eps_U = -(xU*beta - y);
%     
%     eps_L = max(eps_L,0);
%     eps_U = max(eps_U,0);
%     
%     pars0 = [ beta ; eps_L ; eps_U ];
%     
%     % ==========================================
%     % Problem setup
%     
%     % Linear inequality constraints:
%     A = [   xL -eye(nSample)    sparse(zeros(nSample)) ; ...
%         -xU sparse(zeros(nSample)) -eye(nSample)  ;
%         0 -ones(1,nPar-1) sparse(zeros(1,2*nSample)) ];
%     B = [ y ; -y ; 0];
%     
%     objfun= @(pars) sum(pars((nPar+1):end).^2) ;
%    
%     
%     % ==========================================
%     % Optimize
%     [pars1,g]=fmincon(@(a) objfun(a),pars0,A,B,[],[],[],[],[],options_fmincon);
%     
%     beta    =	pars1(1:nPar)                       ;
%     epsL    =   pars1(nPar+(1:nSample))           ;
%     epsU    =   pars1(nPar+nSample+(1:nSample))   ;
%     
%     % figure, hold on
%     %     plot(eps_U,'r*-')
%     %     plot(eps_L,'b*-')
% end
    