
% -------------------------------------------------------------------------
% EMI Toolbox - STEP1_ExtentComputation.m
% -------------------------------------------------------------------------
% Description
%
% Compute the experimental extents and associated (approximate) confidence
% intervals.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script computes experimental extents for three sensor \n' ...
            'layouts. \n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

% -------------------------------------------------------------------------
% Load results and gather data

disp(' ')
disp('=========================')
disp(['   Load data'])
disp('=========================')
disp(' ')

FILEPATH = [ FOLDER 'STEP0_ExperimentalData'] ;
load(FILEPATH)
    
% =========================================================================
% =========================================================================
% =========================================================================

for SensorSetup=1:3
    
    switch SensorSetup
        case 1 % Case 1: TAN and TNO3
            
            IncMeas	=	[   1 3	]   ;
            pHmeas  =       0       ;
            
        case 2 % Case 2: TAN, TNO2, and TNO3
            
            IncMeas	=	[	1:3	]   ;
            pHmeas	=       0       ;
            
        case 3 % Case 3: TAN, TNO2, TNO3, and pH
            
            IncMeas	=	[	1:4	]   ;
            pHmeas	=       4       ;
            
        case 4 % Case 4: TAN, TNO3, and pH
            
            IncMeas	=	[	2:4	]   ;
            pHmeas	=       4       ;
            
        case 5 % Case 5: TNO3 and pH
            
            IncMeas	=	[	3:4	]   ;
            pHmeas	=       4       ;
    end
    
    %======================================================================
    %======================================================================
    %======================================================================
    
    % ---------------------------------------------------------------------
    % Setup and preparation
    
    disp(' ')
    disp('=========================')
    disp(['   Measurement Setup: ' num2str(SensorSetup) '  '])
    disp('=========================')
    disp(' ')
    
    Rk	=   size(Model.Nbar_k,1)        ;
    H	=	size(Model.data.ytilde,2)	;
    MG  =   Model.dimensions.MG         ;
    
    Model1                      =	Model                               ; 
    Model1.data.ytilde          =	Model.data.ytilde(IncMeas,:)        ;
    Model1.Measurement.sigma	=	Model.Measurement.sigma(IncMeas)	;
    
    Inc                         =	ismember(   IncMeas,   1:MG)        ;
    Select                      =	IncMeas(Inc)                        ;
    Model1.Measurement.G        =	Model.Measurement.G(Select,:)       ;
    
    Model1.dimensions.MG        =   length(Select)                      ;
    Model1.dimensions.M     	=   length(Select)+1                    ;
     
    % ---------------------------------------------------------------------
    % Computation
    
    disp(['---  Computing Extents'])
    
    [xtilde,Lambda]     =	Measurement_2_Extent(Model1,IncMeas,pHmeas)	;
    
    lambda              =	nan(Rk,H)                                   ;
    for iR=1:Rk
        lambda(iR,:)	=	squeeze(Lambda(iR,iR,:)).^(1/2)             ;
    end
     
    % ---------------------------------------------------------------------
    % Store results
    
    disp(['---  Saving Results'])
    
    FILEPATH	=   [	FOLDER 'STEP1_ExtentComputation_Case' num2str(SensorSetup) ] ;   
    save(FILEPATH,'Model','xtilde','Lambda','lambda')
    
    disp([' '])
    
end


% =========================================================================
% =========================================================================
% =========================================================================

STEP1_ExtentComputation_Display

% =========================================================================
% =========================================================================
% =========================================================================
