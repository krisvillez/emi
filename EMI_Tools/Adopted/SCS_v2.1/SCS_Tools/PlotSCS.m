function Handle = PlotSCS(Model,Argument,Derivative)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - PlotSCS.m
% -------------------------------------------------------------------------
% Description
%
% This function vizualizes a shape constrained spline function and/or one
% or more of its derivatives.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Handle = PlotSCS(Model,Argument,Derivative)
%
% INPUT
%	Model:     	Structure describing the spline model.
%               (required)
%   Argument:   Column vector of argument values at which the basis should
%               be evaluated. 
%               (optional, default: all knot and data argument values)
%   Derivative: The derivative for which the basis should be evaluated. An
%               empty or zero-valued input results in the computation of
%               the basis for the function itself (0th derivative).
%               (optional, default=0)
%
% OUTPUT
%   Handle:     Handles of the produced plot axes.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

CheckPolyCoeff = true ;

t_ref = Model.t_ref ;
t_scale = Model.t_scale ;
y_scale = Model.y_scale ;
if nargin<2 || isempty(Argument)
    Argument = unique(sort([Model.tk ; Model.ty])) ;
else
    Argument = (Argument-t_ref)/t_scale ;
end
if isfield(Model,'QR')
    QR      =   Model.QR        ;
    disc    =   QR(2:end,end)>0 ;
    if any(disc)
        t_add   =   [   QR(disc+1,1)    ;   QR(disc,2)  ];
        t_add   =   unique(t_add)   ;
        tol     =   1e-6    ;
        t_add   =   [   t_add-tol	;   t_add   ;   t_add+tol   ];
        t_add   =   min(max(t_add,Argument(1)),Argument(end)) ;
        Argument      =   unique(sort([   Argument(:)	;   t_add	]))         ;
    end
end
if nargin<3 || isempty(Derivative)
    Derivative = 0 ;
end

ty      =   Model.ty            ;
y       =   Model.y             ;
tk = Model.tk;

n_Derivative = length(Derivative) ;

%figure,
    hold on
    
Handle = sparse(n_Derivative,1) ;


t_ref   =	Model.t_ref     ;
t_scale = 	Model.t_scale	;
y_scale = 	Model.y_scale	;

t_ref   =   0   ;	
t_scale =   1   ;   
y_scale =   1   ;   


for i_Derivative=1:n_Derivative
 
    if n_Derivative>1
        Handle(i_Derivative) = subplot(n_Derivative,1,i_Derivative) ;
    end
    hold on
    
    yhat    =	EvalSCS(Model,Argument,Derivative(i_Derivative))  ;
    yhat_k   =	EvalSCS(Model,tk,Derivative(i_Derivative))  ;
    if Derivative(i_Derivative)==0
        plot(ty*t_scale+t_ref,y*y_scale,'b.')
    end
%     plot(Argument*t_scale+t_ref,yhat*y_scale*(1/t_scale)^(Derivative(i_Derivative)),'k-')
    tks =   tk*t_scale+t_ref;
    plot(tks,yhat_k*y_scale*(1/t_scale)^(Derivative(i_Derivative)),'k.')
    if Derivative(i_Derivative)==0
        title('Function fit')
    else
        title(['Derivative ' num2str(Derivative(i_Derivative)) ])
    end
    if length(tks)>=10
        Xlim    =   [ 2*tks(1)-tks(2) 2*tks(end)-tks(end-1)] ;
    else
        dtks = tks(end)-tks(1) ;
        Xlim    =   [ tks(1)-1/10*dtks tks(end)+1/10*dtks] ;
    end
%     set(gca,'Xlim',Xlim)
    
    if CheckPolyCoeff
        PolyOrder   =   Model.Order-Derivative(i_Derivative)  ;
            tk = Model.tk ;
        DoPlot      =   true                    ;
        switch PolyOrder
            case 4
                p3	=   Model.X2P_4{1}*Model.x	;
                p2  =   Model.X2P_4{2}*Model.x  ;
                p1  =   Model.X2P_4{3}*Model.x  ;
                p0  =   Model.X2P_4{4}*Model.x  ;
                
            case 3
                p3 = Model.X2P_3{1}*Model.x ;
                p2 = Model.X2P_3{2}*Model.x ;
                p1 = Model.X2P_3{3}*Model.x ;
                p0 = 0*p1;
            otherwise
                DoPlot = false ;
        end
        if DoPlot
            
            xp_old	=	[]	;
            yp_old  =	[]	;
            
            for k=1:(length(tk)-1)
                dtk	=   tk(k+1)-tk(k)                       ;
                aa 	=	(tk(k):dtk/(2^4):tk(k+1))           ;
                xx 	=   (aa-tk(k))                          ;
                yy  =   p3(k)*xx.^(PolyOrder-1)     ...
                        + p2(k)*xx.^(PolyOrder-2)   ...
                        + p1(k)*xx.^(PolyOrder-3)   ...
                        + p0(k)*xx.^(PolyOrder-4)           ;
                    
                xp  =   aa*t_scale+t_ref                	;
                yp  =   yy*y_scale*(1/t_scale)^(Derivative(i_Derivative))	;
                
                if ~isempty(xp)
                    plot([xp_old xp],[yp_old yp],'k-')  
                    xp_old	=   xp(end)	;
                    yp_old  =   yp(end)	;
                end
            end
        end
    end
    
    
end

    



