function y = Equation_Measurement(Model,c)

% -------------------------------------------------------------------------
% EMI Toolbox - Equation_Measurement.m
% -------------------------------------------------------------------------
% Description
%
% Compute expected measurements given species concentrations.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	y = Equation_Measurement(Model,c)
%
% INPUT
%   Model   :   Model description given as a structure (see ExtentDAE.m for
%               required structure)
%	c       :	Species concentrations
%
% OUTPUT
%   y       :	Measurements
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

c(c==0)=0;
y = [ Model.Measurement.G*c ; -log10(c(8,:)) ];
    
end

