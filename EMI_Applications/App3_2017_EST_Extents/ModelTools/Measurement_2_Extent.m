function [xtilde,Lambda] = Measurement_2_Extent(Model,IncludedMeas,pHmeas) 

% -------------------------------------------------------------------------
% EMI Toolbox - Measurement_2_Extent.m
% -------------------------------------------------------------------------
% Description
%
% Computes the extents of reaction given the available measurements.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[xtilde,Lambda] = Measurement_2_Extent(Model,IncludedMeas,pHmeas) 
%
% INPUT
%   Model           :   Model description given as a structure (see
%                       SetupSimulation.m for exemplary use)
%   IncludedMeas    :   Integers indicating included measurements 
%   pHmeas          :   Boolean indicating whether pH is included
%
% OUTPUT
%   xtilde          :	Experimental extents (dim: Rk x H)
%   Lambda          :   Variance-covariance matrices (dim: Rk x Rk x H)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if pHmeas
    disp(['     *  Numerical Solution'])
else
    disp(['     *  Analytical Solution'])
end

% -------------------------------------------------------------------------
% Collect data

ytilde  =	Model.data.ytilde	;

% -------------------------------------------------------------------------
% Collect system information

sigma   =	Model.Measurement.sigma	;
Ebar    =	Model.Ebar              ;
G       =   Model.Measurement.G     ;
Nbar_k	=	Model.Nbar_k            ;
nbar0	=	Model.nbar0             ;

H       =	Model.dimensions.H     ;
M       =	Model.dimensions.M     ;
Rk      =	Model.dimensions.Rk     ;

% -------------------------------------------------------------------------
% Processing

A       =   G*pinv(Ebar)            ;
J       =   A*Nbar_k'               ;
y0      =   A*nbar0                 ;


xtilde	=	pinv(J)*( ytilde(IncludedMeas~=pHmeas,:) - repmat(y0,[1 H]))	;

if pHmeas==0
    
    Lambda_k        =   (J'*diag(1./sigma.^2)*J)^(-1)	;
    Lambda          =	repmat(Lambda_k,[1 1 H])        ;
    
else
    
    % optimizer setup
    opt             =   optimoptions('fmincon')         ;
    opt.Algorithm   =   'interior-point'                ;
    opt.Display     =   'off'                           ;
    opt.TolX        =   10^(-12)                        ;
    
    % functions setup
    fun_nbar        =	@(x) nbar0*ones(1,size(x,2))+Nbar_k'*x(1:end-1,:)               ;   % get number of moles of the kinetic species and components from extents
    fun_y           =	@(z) [ y0*ones(1,size(z,2))+J*z(1:end-1,:); z(end,:)]           ;   % expected measurements given number of moles
    fun_wls         =	@(y,z) sum(((y-fun_y(z))./(sigma(:)*ones(1,size(z,2)))).^2)     ;   % objective function
    nonlincon       =	@(z) deal(-1,Equation_Constraint(Model,fun_nbar(z),z(end,:)))	;
        % "deal" allows to generate two outputs with:
        %   1. the first output corresponding to the inequality
        %   constraint residuals, satsified by default
        %   2. the second output corresponding to the equality
        %   constraint residuals
        %   This way, only one equality constraint function needs to
        %   specified
    
    % pre-allocation
    Lambda          =	nan(Rk,Rk,H)                    ;
    J1              =   J                               ;
    
    for h=1:H
        
        % =================================================================
        % BEST-FIT
        
        % initial guess for extents and equilibrium pH
        zhat0       =	[	xtilde(:,h)	;	ytilde(end,h)   ]	;
        
        % optimize
        zhat        =	fmincon(    ...
                            @(z) fun_wls(ytilde(:,h),z),...
                            zhat0,[],[],[],[],...
                            [zeros(Rk,1) ; -inf],[],nonlincon,...
                            opt);
        
                        % get best-fit extents
        xtilde(:,h) = zhat(1:end-1,:)               ;
        
        % =================================================================
        % LAPLACE APPROXIMATION OF ESTIMATION ERROR VAR-COV MATRIX
        
        % Pre-allocation
        X               =   nan(Rk,2)                         ;
        Y               =   nan(Rk,1)                         ;
        
        % Center point
        x0              =	xtilde(:,h)                         ;
        [~,~,pH0]       =	Model.x2c(Model,x0,Model)           ;
        
        % Satellite points
        iter                =   1                               ;
                X(iter,:)   =   0                            ;
                Y(iter,:)	=	0  ;
        for iR=1:Rk
            for j=[-1 +1]
                
                iter        =   iter+1                          ;
                x1          =	x0                              ;
                x1(iR)      =	x0(iR)+1e-12               ;
                [~,~,pH1]	=	Model.x2c(Model,x1,Model)       ;
                X(iter,:)   =   x1-x0                           ;
                Y(iter,:)	=	pH1-pH0                         ;
            end
        end
        
        % Compute Jacobian elements and covariance matrix
        XX = [  X];
        dpHdx           =   	(XX'*XX)^(-1)*XX'*Y                ;
        J               =	[	J1	;	dpHdx(1:end,:)'	]               ;
        Lambda_k        =       (J'*diag(1./sigma.^2)*J)^(-1)	;
        
        Lambda(:,:,h)	=       Lambda_k                        ;
    end
    
end