function [xL,xU,xsimL,xsimU] = SimExtent_bound(Model,Omega) 

% -------------------------------------------------------------------------
% EMI Toolbox - SimExtent_bound.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the bounding extent profiles associated with the parameter sets
% in a given set of parameter vectors.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[xL,xU,xsimL,xsimU] = SimExtent_bound(Model,Omega) 
%
% INPUT
%   Model   :   Model description given as a structure (see ExtentDAE.m for
%               required structure)
%   Omega   :   Bounding box for considered parameter vectors
%
% OUTPUT
%   xL      :   Extent lower bound, at measurement sampling times
%   xU      :   Extent upper bound, at measurement sampling times
%   xsimL	:   Extent lower bound
%	xsimU	:   Extent upper bound
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

xsimL=SimExtent(Model,Omega(2,:));
xsimU=SimExtent(Model,Omega(1,:));

xsimL = min(xsimL,xsimU);
xsimU = max(xsimL,xsimU);

if isfield(Model,'data')
    index   =   Model.Measurement.sample_index  ;
    xL      =   xsimL(:,index)     ;
    xU      =   xsimU(:,index)     ;
else
    xL = [];
    xU = [];
end

% 
% if Graph
%     figure(37), hold on,
%     plot(Model.Tsim,XsimU,'r-','LineWidth',2)
%     if isfield(Model,'I1')
%         plot(Model.Tsim(Model.I1),XsimU1,'ro','MarkerSize',7,'MarkerFaceColor','r')
%     end
%     Xlim = get(gca,'Xlim');
% 
%     grey = ones(1,3)*.5 ;
%     %plot(-Model.Tsim-10,XsimL,'-','Color',grey)
%     
%     plot(Model.Tsim,XsimL,'b:','LineWidth',2)
%     if isfield(Model,'I1')
%         plot(Model.Tsim(Model.I1),XsimL1,'bs','MarkerSize',7,'MarkerFaceColor','b')
%     end
% 
%     xlabel('Time [h]')
%     ylabel('Relative concentration [-]')
%     if exist('FigPrep'), FigPrep, end
%     if exist('AxisPrep'), AxisPrep, end
%     
% %     legend({    
% %         '$\overline{s}(t)$',...
% %         '$\overline{\mathbf{s_{N}}}$',...
% %         '${s}(t)$',...
% %         '$\underline{s}(t)$',...
% %         '$\underline{\mathbf{s_{N}}}$'},...
% %         'Interpreter','latex')
%     
%     legend({    
%         '$\overline{s}(t)$',
%         '$\underline{s}(t)$'},...
%         'Interpreter','latex')
%     set(gca,'Xlim',Xlim)
%     return
% end