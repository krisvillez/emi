function [QL,QU,BestX,Model] = ExtentFit_bound(Model,IntX,~,Graph,Xo)

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentFit_bound.m
% -------------------------------------------------------------------------
% Description
%
% Compute lower and upper bounds for the lack-of-fit objective function
% formulated in extents.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[QL,QU,BestX,Model] = ExtentFit_bound(Model,IntX,[],Graph,Xo)
%
% INPUT
%   Model	:   Model description given as a structure (see ExtentDAE.m
%               for required structure)
%   IntX    :   Considered set relative to root set
%   []      :   Placeholder entry
%   Graph   :   Boolean controlling graphical output
%   Xo      :   Parameter vector for upper bound relative to root set
%
% OUTPUT
%   QL  	:   Lower bound
%   QU    	:   Upper bound
%   BestX	:   Best-known parameter vector, relative to root set
%   Model	:   Updated model (simply returned, placeholder)
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<4 || isempty(Graph)
    Graph = false ;
end
if nargin<5 || or(isempty(Xo),length(Xo)~=size(IntX,2))
    Xo = mean(IntX)';
end

if Graph
    figure(23)
    close 23
    figure(42)
    close 42
end

Omega_a      =	ones(2,1)*Model.Intercept	+	IntX*diag(Model.Slope)	;

% =======================================
% 1. Extremal simulations
[xL,xU,xsimL,xsimU]  =   SimExtent_bound(Model,Omega_a) ;

% ------------------------------------------------
% Graph
if Graph
    tsim = Model.Measurement.tsim ;
    index = Model.Measurement.sample_index ;
    ty  = tsim(index) ;
    
    figure(37), 
    subplot(2,1,1)
        hold on
        plot(tsim,xsimL,'b.')
        plot(tsim,xsimU,'r.')
        plot(ty,xL,'b.')
        plot(ty,xU,'r.')
end
% ------------------------------------------------


% =======================================
% 2. Lower bound
% disp('------- BB LOW -------------------------------------------')
[QL,Eplus,Emin] = ExtentFit_boundlow(Model,IntX,[],Graph,Omega_a,xsimL,xsimU) ;


% ------------------------------------------------
% Graph
if Graph
    
    figure(37), 
    subplot(2,1,1)
        hold on
        plot(tsim,xsimL,'b.')
        plot(tsim,xsimU,'r.')
        plot(ty,xL,'b.')
        plot(ty,xU,'r.')
        plot(ty,Model.data.xtilde,'ko')
    subplot(2,1,2)
        hold on
        plot(ty,Eplus,'bo')
        plot(ty,Emin,'ro')
end
% ------------------------------------------------


% =======================================
% 3. Upper bound
% disp('------- BB UPP -------------------------------------------')
[QU_L,~,~,E_L]	=   ExtentFit_boundupp(Model,[],[],Graph,[],[],xsimL)	;
[QU_U,~,~,E_U]	=   ExtentFit_boundupp(Model,[],[],Graph,[],[],xsimU)	;

[QU,loc]	=   min([QU_U QU_L])    ;
BestX       =	IntX(loc,:)         ;


% ------------------------------------------------
% Graph
if Graph
    
    figure(37), 
    subplot(2,1,1)
    subplot(2,1,2)
        hold on
        plot(ty,E_L,'b+')
        plot(ty,E_U,'r+')
end
% ------------------------------------------------

% disp('------- BB BOUNDS END -------------------------------------------')

end

