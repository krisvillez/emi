function Verifiable = AssessVerifiability(model)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - AssessVerifiability.m
% -------------------------------------------------------------------------
% Description 
%
% This function checks which transitions in the enforced qualitative
% sequence can be solved to absolute/analytical precision (rather than
% bounded numeric precision).  
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model  =   ApplyQR(Model,QR,Trans,disc)	
%
% INPUT
%	Model:		Structure describing an existing spline model (see
%               CreateSCS.m)
%               (required) 
%	QR:			Qualitative sequence or qualitative representation. This
%               can be given as a matrix (see DemoTitanium.m) or as a
%               string (see DemoRefinery.m). In the matrix form, nan
%               entries can be used when the transitions are not known.
%               (required)    
%	Trans:      Vector of values for the episode transitions. This input is
%               ignored if the matrix form for QR is used. Otherwise,
%               absence or empty values imply that the transitions are not
%               known.
%               (optional, default=[])      
%	Disc:		Vector of integers indicating the number of discontinuous
%               derivatives (knot multiplicity) at each transition in the
%               qualitative sequence/representation. This allows to add
%               discontinuities when they are not implied by the
%               qualitative sequence.
%               (optional, default=[])    
%
% OUTPUT
%	Model:     	Structure describing the updated spline model (see
%               CreateSCS.m) 
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-17
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


QR = model.QR ;
[nrow,ncol] = size(QR) ;
cols = 3:(ncol-2) ;

m = nrow-1;

if strcmp(model.type,'SCS')
    Verifiable = true(m,1) ;
else
    
    Verifiable = nan(m,1) ;
    for j=1:m
        
        anyzero = any(QR(j:j+1,cols)==0,2) ;
        if any(anyzero)
            
            if all(anyzero)
                Verifiable(j) = false ;
            else
                Verifiable(j) = true ;
            end
            
        else
            
            IsDisc =	QR(j+1,end)>0	; % check for discontinuity
            
            if IsDisc
                Verifiable(j) = false ;
            else
                if sum(QR(j,cols).*QR(j+1,cols)==-1)==1 % Zero-crossing implied by shape constraints
                    col = cols( find( QR(j,cols).*QR(j+1,cols)==-1 ) ) ;
                    
                    order = ncol-col ;
                    
                    switch order
                        case {1,2,3}
                            % Constant / Linear
                            Verifiable(j) 	=	true ;
                        case {4}
                            % Quadratic / Cubic  case
                            if QR(j,col+1)==QR(j+1,col+1) && QR(j,col+1)~=0
                                % the next derivative has a non-zero, constant sign
                                %	=> the considered derivative is a strictly monotonic function
                                %	=> the considered derivative can only have a single zero-crossing
                                Verifiable(j) 	=	true ;
                            else
                                Verifiable(j) 	=	false ;
                            end
                        otherwise
                            % (!) shape constraints on higher order polynomials/splines not supported by toolbox so set to false by default
                            Verifiable(j) 	=	false ;
                    end
                else % Not a single zero-crossing is implied
                    Verifiable(j) 	=	false ;
                end
            end
            
        end
    end
end

assert(all(~isnan(Verifiable)),'Not all transitions are evaluated for verifiability')

