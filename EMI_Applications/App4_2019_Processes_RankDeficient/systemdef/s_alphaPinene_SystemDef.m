

% ------------------------------------------------------------------------
%   Definition of experimental conditions
% ------------------------------------------------------------------------
V       =   1	;
n_0     =   [ 100 0 0 0 0 ]'*V;
t_h     =	[ 0 1230 3060 4920 7800 10680 15030 22620 36420 ]'/3600;

% ------------------------------------------------------------------------
%   Definition of dynamics
% ------------------------------------------------------------------------
N_      =   [   -1  +1	0   0	0		;
                -1  0   +1	0	0		;
                0	0	-1  +1	0   	;
                0	0	-1  0   +1		;
                0	0	+1	0   -1  ]	;
ratecell = { ...
    @(c_,theta_) theta_(1)*c_(1)    ; ...
    @(c_,theta_) theta_(2)*c_(1)	; ...
    @(c_,theta_) theta_(3)*c_(3)    ; ...
    @(c_,theta_) theta_(4)*c_(3)    ; ...
    @(c_,theta_) theta_(5)*c_(5)	};
theta_  =   10^(-5)*[ 5.84 1.63 4.61 2.65 2.77 ]'*3600;

% ------------------------------------------------------------------------
%   Meta-information
% ------------------------------------------------------------------------
M       =	length(t_h)       ;
[R,S]   =	size(N_)             ;
T       =	length(theta_)       ;
name_species = {'y_1','y_2','y_3','y_4','y_5'};
%name_species = {'\alpha-pinene','dipentene','allo-ocimene','pyronene','dimer'};
name_parameters = {'k_1','k_2','k_3','k_4','k_5'};
unit_concentration	= '%';
unit_extents        = '%';

% ------------------------------------------------------------------------
%   Definition of measurements, assuming all concentrations are measured
% ------------------------------------------------------------------------
M_init	=   eye(S)          ;   % measurement gain matrix for true values
M_eps   =   eye(S)          ;   % measurement gain matrix for noise
M_init	=   sparse(M_init)	;
M_eps	=   sparse(M_eps)   ;

% ------------------------------------------------------------------------
%   Simulation info
% ------------------------------------------------------------------------

ratefun         =	@(c_,theta_) cellfun(@(exp) exp(c_,theta_), ratecell)                     ;
t_              =	unique(sort([ (floor(t_h(1)*60):(ceil(t_h(end))*60))/60   t_h(:)' ]))'	;
[tf,select_h]	=	ismember(t_h,t_)            ;