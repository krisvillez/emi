function [residual] = Equation_Constraint(Model,cbar,pH)

% -------------------------------------------------------------------------
% EMI Toolbox - Equation_Constraint.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the left-hand-side of the algebraic equations expressing
% equilibrium. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[residual] = Equation_Constraint(Model,cbar,pH)
%
% INPUT
%   Model       :   Model description given as a structure (see ExtentDAE.m
%                   for required structure)
%	cbar		:	Concentrations of kinetic species and equilibrium
%                   components
%	pH          :   pH, defined as -log([H+])
%
% OUTPUT
%   residual	:	Left-hand-side of the algebraic equality equations
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

cbar        =   cbar(Model.Equilibrium.nc_index,:) ;
[SkSc,H]	=	size(cbar);

IonType =	Model.Equilibrium.IonType	;
Ka      =	10.^(-Model.Equilibrium.pKa)	;
Kw      =	10.^(-Model.Equilibrium.pKw)	;

cH      =   10.^(-pH)	;
cOH     =   Kw./cH      ;
Contrib	=   diag(-1*(IonType(:)==-1))*cbar + cbar.*repmat(cH,[SkSc 1])./(repmat(cH,[SkSc 1])+repmat(Ka(:),[1 H]));

residual = sum(Contrib,1) +cH-cOH;

% C_NH4   =	Z(1,:)    .*   (   +C_H./(C_H+Ka(1))  )	;
% C_NO2   =	Z(2,:)    .*   (1  -C_H./(C_H+Ka(2))  )	;
% C_NO3   =	Z(3,:)    .*	(1  -C_H./(C_H+Ka(3))  )	;
% 
%     
% balance = C_H-C_OH+C_NH4-C_NO2-C_NO3 ;
   
    
end

