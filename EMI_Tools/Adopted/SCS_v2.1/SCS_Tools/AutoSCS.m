function Model = AutoSCS(y,String,Order,Lambda,KnotDist,Trans,Disc,Graph)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - AutoSCS.m
% -------------------------------------------------------------------------
% Description 
%
% AutoSCS is a function which evaluates or optimizes the fit of a shape
% constrained spline function to a data series. It executes the fitting of
% a shape constrained spline function according to a number of conventional
% settings, such as (i) the data is equally spaced on a regular grid, (ii)
% the spline knots are located on a regular grid, and (iii) the shape
% constraints are expressed a string of primitives representing the
% qualitative sequence. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Model = AutoSCS(y,String,Order,Lambda,KnotDist,Trans,Disc,Graph)
%
% INPUT
%	y:          Column vector containing the data series [required]
%	String:		String representing the sequence of primitives (QS,
%               Qualitative Sequence).
%               (required) 
%	Order:      Scalar indicating the order of the spline function.
%               (optional, default=4 (cubic splines)) 
%	Lambda:		Column vector of regularization coefficients for the
%               quadratic penalty on the function values and derivatives.
%               Its length must be one (scalar) or equal to the order of
%               the spline function. When this input is a scalar, the
%               highest derivative penalty is penalized only. Otherwise,
%               the elements of the vector are ordered from the penalty for
%               the function value to the one for the last computable
%               derivative. 
%               (optional, default=0)
%   KnotDist    Distance between knots, expressed as multiple of the
%               sampling interval.
%               (optional, default=1)
%   Trans       Location of the episode transitions. If empty, these
%               transitions are sought.
%               (optional, default=[])
%   Disc        Vector of integers indicating the number of discontinuous
%               derivatives (knot multiplicity) at each transition in the
%               qualitative sequence/representation. This allows to add
%               discontinuities when they are not implied by the
%               qualitative sequence.
%               (optional, default=[])
%   Graph       Boolean indicating whether graphs should be produced to
%               vizualize progress of the optimization algorithm and the
%               obtained fit.
%               (optional, default=false)
%
% OUTPUT
%	Model:     	Structure describing the fitted spline model.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =========================================================================
% Modify arguments if necessary

if nargin<8 || isempty(Graph) 
    Graph   =   false  ;
end

if nargin<7 || isempty(Disc) 
    Disc    =   []  ;
end

if nargin<6 || isempty(Trans)
    Trans   =   []  ;
end

if length(String)==1
    optimize = false;
else
    if isempty(Trans)
        optimize    =   true    ;
    else
        optimize    =   false   ;
    end
end

if nargin<5 || isempty(KnotDist) 
    KnotDist      =   1       ;
end

if nargin<4 || isempty(Lambda) 
    Lambda  =   0 ; %1e-3	;
end

if nargin<3 || isempty(Order) 
    Order   = 	4       ;
end

t       =   (1:length(y))' ;    
tk      =     [  t(1:KnotDist:end-1) ; t(end)]         ;

Model	=	CreateSCS(y(:),t,tk,Order,Lambda)	;

if optimize
    if Graph
    disp('	BB optimization')
    end
    Model       =   ApplyQR(Model,String,[],Disc)           ;
    
    options         =   OptimsetBB(Model)   ;
    options.node    =   4                   ;
    options.Graph   =   Graph               ;
    options.verbose =   1                   ;
    options.resolution =1/8 ;
    
    nk          =   length(Model.tk)                ;
    nt          =   size(Model.QR,1)-1              ;
    IntX        =   Model.tk([1 nk]'*ones(1,nt))    ;
    DistX       =   []                              ;
    [oo,Model]  =   BB(Model,options,IntX,DistX)    ;

    if Graph
    for iX=1:length(Model.SolX)
        disp(['     Model.SolX(' num2str(iX) '):  ' sprintf('%10.4f', Model.SolX(iX)) ])    
    end
    disp(['     Model.J:        ' sprintf('%10.4f', Model.J) ])
    disp('	BB optimization terminated')
    drawnow
    end
else
    if Graph
    disp('	Evaluation')
    end
    if length(Trans)~=0
        Model	=   ApplyQR(Model,String,Trans,Disc)        ; 
    end
    
    Model   =   FitSCS(Model)                      ;
    
    if Graph
    for iX=1:length(Trans)
        disp(['     Model.SolX(' num2str(iX) '):  ' sprintf('%10.4f', Trans(iX)) ])    
    end
    disp(['     Model.J:        ' sprintf('%10.4f', Model.J) ])
    disp('	Evaluation terminated')
    drawnow
    end
end

if Graph
    tk      =       Model.tk                                    ;
    dtk     =       min(diff(tk))                               ;
    te      =   [	tk(:)	;	(tk(1):dtk/100:tk(end))'	]   ;
    te      =       sort(te)                                    ;
    
    figure
    PlotSCS(Model,te,0:1)	;
end



