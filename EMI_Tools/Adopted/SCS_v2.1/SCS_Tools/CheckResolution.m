function Absolute = CheckResolution(Model,Interval,Exclude)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - CheckResolution.m
% -------------------------------------------------------------------------
% Description 
%
% CheckResolutionSCS checks which of the transitions are specified at a
% resolution enabling to solve for them to absolute precision.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Absolute = CheckResolutionSCS(Model,Interval,Exclude)
%
%
% INPUT
%	Model:     	Structure describing the spline model.
%               (required) 
%   Interval:   Matrix describing the bounds on the transitions. It must be
%               specified as 2-column matrix with each row corresponding to
%               one transition. The left (right) column specifies the lower
%               (upper) bounds for the transitions.
%               (required) 
%   Exclude:    Boolean scalar indicating whether discontinuities should be
%               accounted for. When true, it is considered that transitions
%               implying discontinuities can never be solved to absolute
%               precision.
%               (optional, default=false)
%
% OUTPUT
%   Absolute:   Boolean vector indicating whether the transitions can be
%               solved to absolute precision.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<3 || isempty(Exclude)
    Exclude = false ;
end

m = size(Interval,1) ;
unitk = unique(Model.tk);
                        
Absolute        =   false(m,1)          				;
for j=1:m
	[loc1]      	=   find(unitk<=Interval(j,1),1,'last')  	;
    [loc2]      	=   find(unitk>=Interval(j,2),1,'first') 	;
    Absolute(j) =   all(loc1+1==loc2)        			;
end


if Exclude % Exclude transitions implying discontinuities from evaluation. They are always considered unresolved.
	QR 		=	Model.QR 		;
	discdeg	=	QR(2:end,end) 	;
	Absolute(discdeg>0) = false ;
end
