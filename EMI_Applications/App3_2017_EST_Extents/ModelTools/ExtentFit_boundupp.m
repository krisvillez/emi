function  [QU,Xm,xsim,d] = ExtentFit_boundupp(Model,IntX,~,Graph,Xo,theta,xsim)

% -------------------------------------------------------------------------
% EMI Toolbox - ExtentFit_boundupp.m
% -------------------------------------------------------------------------
% Description
%
% Compute upper bound for the lack-of-fit objective function formulated in
% extents. 
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[QU,Xm,xsim,d] = ExtentFit_boundupp(Model,IntX,[],Graph,Xo,theta,xsim)
%
% INPUT
%   Model	:   Model description given as a structure (see ExtentDAE.m
%               for required structure)
%   IntX    :   Considered set relative to root set
%   []      :   Placeholder entry
%   Graph   :   Boolean controlling graphical output
%   Xo      :   Parameter vector for upper bound relative to root set
%   theta   :   Parameter vector for upper bound in original scale
%   xsim    :   Extent state simulation used for upper bound
%
% OUTPUT
%   QU    	:   Upper bound
%   Xm      :   Return best-fit upper bound parameter vector, relative to
%               root set
%   xsim    :   Extent state simulation used for upper bound
%   d       :   Residuals
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<4 || isempty(Graph)
    Graph = false ;
end
if nargin<5 || isempty(Xo)
    Xm      =	mean(IntX)' ;
else
    Xm      =	min(IntX(2,:)',max(IntX(1,:)',Xo(:)))       ;
end

% =========================================================================
% 1. Extent Simulation

if nargin<=5
    
    % Simulation corresponding to mean parameter vector:
    theta       =	Model.Intercept	+	XM'*diag(Model.Slope)	;
end

if nargin<7 || isempty(xsim)
    xsim=SimExtent(Model,theta);
end
%n_pars = length(theta);

% =========================================================================



% =========================================================================
% 2. Initial substrate concentration estimation

if isfield(Model,'data')
    
    index	=	Model.Measurement.sample_index	;
    H       =	Model.data.H                    ;
    lambda	=	Model.data.lambda               ;
    xtilde	=	Model.data.xtilde               ;
    
    x       =	xsim(:,index)   ;
    xn      =	x./lambda       ;
    yn      =   xtilde./lambda	;
    
    d       =	yn-xn           ;
    QU      =	sum(d.^2,2)/H	;
    
else
    QU      =	0               ;
end

end