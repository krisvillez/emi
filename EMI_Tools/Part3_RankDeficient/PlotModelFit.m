function [fighan,figname] = PlotModelFit (model,model0,options,scenario_name,simulationresult)

% -------------------------------------------------------------------------
% EMI Toolbox - PlotModelFit.m
% -------------------------------------------------------------------------
% Description
%
%   Customized plotting function to visualize the quality of model fit.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[fighan,figname] = PlotModelFit (model,model0,options,scenario_name,simulationresult)
%
% INPUT
%   model               :   [optional] reference model as a structure
%   model0              :   model as a structure
%   options             :   plot fine-tuning options as as structure
%   scenario_name       :   string to use in figure title
%   simulationresult	:   [optional] reference data
%   
% OUTPUT
%	fighan              :   figure handle
%   figname             :   figure title
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


colours     =	options.colours                 ;
linewidth	=	options.linewidth               ;
markers     =	options.markers                 ;

t_        =   model0.experiment.t_          ;
select_h	=   model0.experiment.select_h      ;
M_         =	model0.static.M_               ;
V           =	model0.experiment.V             ;
ytil        =	model0.observations.ytil_h       ;

t_h       =   t_(select_h)                  ;

M           =	size(M_,1)                     ;

PlotTrue    =   ~isempty(simulationresult) ;
if PlotTrue
    n_t       =   simulationresult.n_t               ;
    csim        =	M_*n_t/V                     ;
    measnames1	=	cellstr([ repmat('{y}_',[M 1]) num2str((1:M)') ]);
else
    measnames1	=	{};
end


if isempty(model)
    measnames2	=	cellstr([ repmat('\hat{y}_',[M 1]) num2str((1:M)') repmat(' (P_1^{(j)})',[M 1]) ]);
    measnames3	=	cellstr([ repmat('\hat{y}_',[M 1]) num2str((1:M)') repmat(' (P_{1+0})',[M 1]) ]);
else
    measnames2	=	cellstr([ repmat('\hat{y}_',[M 1]) num2str((1:M)') repmat(' (P_0)',[M 1]) ]);
    measnames3	=	cellstr([ repmat('\hat{y}_',[M 1]) num2str((1:M)') repmat(' (P_1^{(j)})',[M 1]) ]);
end
measnames4	=	cellstr([ repmat('\tilde{y}_',[M 1]) num2str((1:M)')  ]);

str         =   cellstr([repmat('$',[M*(2+PlotTrue)+length(measnames2) 1]) char( ...
                [	measnames1(:);	measnames2(:);	measnames3(:);	measnames4(:)	]...
                )	repmat('$',[M*(2+PlotTrue)+length(measnames2) 1])])	;

if isfield(options,'gridpos_conc')
    initX       =   options.gridpos_conc(1)	;
    initY       =   options.gridpos_conc(2)	;
else
    initX       =   get(gca,'Xlim') ;
    initX       =   initX(1)        ;
    initY       =   get(gca,'Ylim') ;
    initY       =   initY(1)        ;
end

figname     = [ 'Scenario' scenario_name '_model' ] ;
fighan      =	figure('name',figname)              ;
if exist ('FigPrep','file'),	FigPrep,	end
if exist ('AxisPrep','file'),	AxisPrep,	end
hold on

if isempty(model)
    csimP1j      =	M_*model0.SIM.nhat_init/V	;
else
    csimP0      =	M_*model.SIM.nhat_fin/V	;
end
csimP10         =	M_*model0.SIM.nhat_fin/V	;
L               =	0                           ;
if PlotTrue
    for m=1:M
        L       =   L + 1                       ;
        ph(L)   =   plot (t_,csim(m,:),'-','Color',colours(1,:),'linewidth',linewidth*5) ;
    end
end
if isempty(model)
    for m=1:M
        L           =   L + 1                       ;
        ph(L)       =   plot (t_,csimP1j(m,:),'-','Color',colours(4,:),'linewidth',linewidth*3) ;
    end
    for m=1:M
        L           =   L + 1                       ;
        ph(L)       =   plot (t_,csimP10(m,:),'-','Color',colours(7,:),'linewidth',linewidth*1) ;
    end
else
    for m=1:M
        L           =   L + 1                       ;
        ph(L)       =   plot (t_,csimP0(m,:),'-','Color',colours(4,:),'linewidth',linewidth*3) ;
    end
    for m=1:M
        L           =   L + 1                       ;
        ph(L)       =   plot (t_,csimP10(m,:),'-','Color',colours(7,:),'linewidth',linewidth*1) ;
    end
end
for m=1:M
    L           =   L + 1                       ;
    col         =   (2+colours(m,:))/3 ;
    ph(L)       =   plot (t_h,ytil(m,:),markers{m},'Color',colours(m,:),'Markerfacecolor',col,'linewidth',linewidth*1) ;
    if isfield(options,'markersize')
        set(ph(L),'MarkerSize',options.markersize);
    end
end
set (gca,'Ylim',[-0.02 floor(max(ytil(:))*10)/10+.05])
set (gca,'Xtick',[t_h(1):t_h(end)])
%set (gca,'Ytick',[0:.1:.7])
xlabel (['Time [h]'])
ylabel (['Concentration [' options.unit_concentration ']'])

nRow        =   M	;
nCol        =   ceil(length(str)/M) ;
if exist ('GridLegend','file'),  GridLegend (ph,str,nCol,initX,initY);
else legend (ph,str,'Interpreter','latex')
end