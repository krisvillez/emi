function [LB,UB,Trans,Model,ModelLB] = BoundSCS(Model,Interval,DistBnd,Graph,Verify)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - BoundSCS.m
% -------------------------------------------------------------------------
% Description
%
% BoundSCS is a function which evaluates lower and upper bounds for the
% objective function for shape constrained spline fitting.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[LB,UB,Trans,Model] =
%               BoundSCS(Model,Interval,DistBnd,Graph,Verify)
%
% INPUT
%	Model:     	Structure describing the spline model.
%               (required)
%   Interval:   Matrix describing the bounds on the transitions. It must be
%               specified as 2-column matrix with each row corresponding to
%               one transition. The left (right) column specifies the lower
%               (upper) bounds for the transitions.
%               (required)
%   DistBnd:    Matrix describing the bounds on the distances between
%               transitions. It must be specified as 2-column matrix with
%               each row corresponding to one transition. The left (right)
%               column specifies the lower (upper) bounds for the distances
%               between subsequent transitions. An empty entry is
%               interpreted as a monotonicity constraint (transitions are
%               ordered along the data series).
%               (optional, default left bound = 0, default right bound =
%               +inf)
%   Graph       Boolean indicating whether graphs should be produced to
%               vizualize progress of the optimization algorithm and the
%               obtained fit.
%               (optional, default=false)
%   Verify      Boolean indicating whether the capacity to compute the
%               transitions to absolute precision should be exploited when
%               available.
%               (optional, default=false)
%
% OUTPUT
%   LB:         Lower bound for the objective function. If the problem is
%               infeasible, its value is +inf.
%   UB:         Upper bound for the objective function. If the problem is
%               infeasible, its value is +inf.
%   Trans:      Best known feasible solution within the set. If the problem
%               is infeasible, a vector with nan as entries is returned.
%	Model:     	Structure describing the fitted spline model corresponding
%               to the best known feasible solution. If the problem is
%               infeasible, the structure is returned in its original
%               version.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2014-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

ModelLB     =   Model	;

if nargin<4 || isempty(Graph)
    Graph	=   false	;
end

if nargin<5 || isempty(Verify)
    Verify	=   false	;
end

% Graph      =   true    ;
verbose     =	false       ;

Interval	=	Interval'           ;
m           = 	size(Interval,1)	;
xL          =   Interval(:,1)       ;
xU          =   Interval(:,2)       ;

if nargin<3 || isempty(DistBnd)
    DistBnd = [ -inf*ones(m-1,1) +inf*ones(m-1,1) ];
end

assert(all(xL<=xU),'Order of variable bounds inconsistent')

InFeasible = nan(m,2) ;
for j=1:m
    InFeasible(j,1) = any(xL(j)>xU(j:end))  ;
    InFeasible(j,2) = any(xU(j)<xL(1:j))    ;
end

% xCover  =   Interval ;
% for j=m:-1:2
%     if xCover(j,1) < xCover(j-1,2)
%         xCover  =   [   xCover(1:j-2,:) ;   xCover(j-1,1) xCover(j,2) ;	xCover(j+1:end,:) ];
%     end
% end

if any(InFeasible(:))
    LB  =   +inf        ;
    UB  =   +inf        ;
    Trans 	=	nan(m,1)    ;
else
    
    % search for a feasible solution
    DomainLimits        =   Model.tk([1 end])       ;
    DomainLimits        =   DomainLimits(:)         ;
    
    [Trans,feasible]	=   GetFeasibleSolution(Interval,DistBnd,DomainLimits)   ;

    %     if ismember(99,findobj('type','figure'))
    %         figure(99)
    %         if length(Trans)==2
    %             plot(Trans(1),Trans(2),'ko','MarkerFaceColor','r')
    %         end
    %     end
    
    if ~feasible
        LB  =   +inf	;
        UB  =   +inf    ;
    else
        
        QR0             =   Model.QR        ;
        bmax            =   min(2,nargout)  ;
        
        if Graph
            figure
        end
        
        Model0      =   Model	;
        
        for b=1:bmax
            if verbose, disp(['Bound ' num2str(b)]), end
            
            Model   =   Model0          ;
            
            
            % ============================================
            % 1. SETUP/MODIFICATION OF Model AND QR
            QR      =	QR0             ;
            QR(:,1) =	Model.tk(1) 	;
            QR(:,2) =	Model.tk(end) 	;
            
            switch b
                case 2 % upper bound
                    
                    QR(1:end-1,2)   =   Trans              ;
                    QR(2:end,1)     =   Trans              ;
                    QR0             =   QR              ;
                    
                case 1 % lower bound
                    
                    QR(1:end-1,2)   =   xL              ;
                    QR(2:end,1)     =   xU              ;
                    
                    % ------------------------------------
                    % Deal with discontinuities
                    disc_deg    =	QR(2:end,end)       ;
                    anydisc     =   any(disc_deg>0)     ;
                    
                    if anydisc
                        
                        ArgInc      =   [   Model.tk(1) xU(:)'          ;
                            xL(:)'      Model.tk(end)   ];
                        
                        tk          =   Model.tk  ;
                        ty          =   Model.ty    ;
                        
                        discloc     =	find(disc_deg>0)	;
                        ndisc       =   length(discloc)     ;
                        
                        for idisc = 1:ndisc
                            deg     =   disc_deg(discloc(idisc))    ;
                            
                            xLU     = [ xL(discloc(idisc))  ; xU(discloc(idisc)) ] ;
                            xLU     =   setdiff(xLU,tk([1 end]) ); % end knots cannot be used with multiplicity
                            xLU     =   unique(xLU) ;
                            excl        =   ismember(tk,xLU)   ;
                            tk          =   tk(~excl)               ;
                            tk_add  =   repmat(xLU,[1 deg])       	;
                            tk      =   sort([tk ; tk_add(:)])      ;
                            
                            inc     =   and(ty>xL(discloc(idisc)),ty<xU(discloc(idisc)))	;
                            ty_inc  =   ty(inc)                     ;
                            
                            % excl        =   ismember(tk,ty_inc)     ;
                            % tk          =   tk(~excl)               ;
                            
                            % deg     =   max(deg,1)                  ;
                            % tk_add  =   repmat(ty_inc,[1 deg])      ;
                            % tk      =   sort([tk ; tk_add(:)])      ;
                        end
                        
                        y           =   Model.y         ;
                        order       =   Model.Order     ;
                        lambda      =   Model.Lambda	;
                        
                        tk     	=   tk  * Model.t_scale+Model.t_ref ;
                        ty      =   ty  * Model.t_scale+Model.t_ref ;
                        
                        Model   =	CreateSCS(y,ty,tk,order,lambda,ArgInc)	;
                        Model.lowbound	=   true	;
                        
                    end
                    
                    
                    % ------------------------------------
                    % insert episodes if certain episodes have the same sign for the same derivative and an argument gap exists:
                    QR0     =   QR  ;
                    
                    for j=m:-1:1
                        % Define the Interval within which only the considered transition can appear, i.e., no other transitions could possibly be located in the Interval [L,R]:
                        L 	=	max([ QR(j,2) ; QR(1:j,1) ]) ;
                        R 	=	min([ QR(j+1,1) ; QR(j+1:end,2) ]) ;
                        if L < R % check for argument gap
                            
                            % set the signs for the inserted episode
                            signs           =	QR(j:j+1,3:end-1) 		; % get signs of consecutive episodes
                            
                            signs1			=	signs(1,:) 				; % select one row
                            signs1(:,:)     =   nan                     ; % set all to nan
                            setequal        =	signs(1,:)==signs(2,:) 	; % evaluate those which are equal
                            signs1(setequal) =   signs(1,setequal)      ; % set new signs to those equal
                            
                            %                             setpos          =   or( and(signs(1,:)==0,  signs(2,:)>0),...
                            %                                                     and(signs(1,:)>0,  signs(2,:)==0)   ) ;
                            %                             signs1(setpos)  =   +1         ; % set new signs to those equal
                            %
                            %                             setneg          =   or( and(signs(1,:)==0,  signs(2,:)<0),...
                            %                                                     and(signs(1,:)<0,   signs(2,:)==0)   ) ;
                            %                             signs1(setneg)  =   -1        ; % set new signs to those equal
                            
                            signs           =   signs1                  ;
                            
                            if any(~isnan(signs)) % if any signs are given, insert an episode with those signs
                                QR	=	[	QR(1:j,:)						;...
                                    L R signs nan 	;...
                                    QR(j+1:end,:) ];
                            end
                        end
                    end
                    
                    % ------------------------------------
                    % exclude episodes where the right side is before left side:
                    inc			=	QR(:,2)>=QR(:,1) 	;
                    QR 			=	QR(inc,:) 			;
                    
            end
            
            Model.QR	=	QR              ;
            
            if verbose, disp('QR setup finished'), end
            
            % ============================================
            % 2. ACTUAL OPTIMIZATION
            
            Model       =   FitSCS(Model)  ;
            
            % ============================================
            % 3. RESET QR
            % Original QR needs to be reset in order to put out a solution
            % where dimensions of Model.QR match the dimensions of Trans:
            Model.QR    =   QR0 ;
            % ============================================
            
            % ============================================
            % 4. OPTIONAL OUTPUT, INTENDED FOR DEMONSTRATION/DEBUGGING
            if Graph
                subplot(bmax,1,bmax-(b-1))
                PlotSCS(Model) ;
                switch b
                    case 1
                        title('Lower bound')
                    case 2
                        title('Upper bound')
                end
            end
            if verbose, disp('Estimation finished'), end
            
            % ============================================
            % 5. SAVE RESULT / IMPROVE RESULT IF CURRENT SOLUTION BOUNDS
            % PERMIT TO DO SO.
            
            switch b
                case 1
                    ModelLB     =   Model	;
                    LB          =   Model.J ;
                    
                case 2
                    UB  =   Model.J ;
                    
                    %                     Verify =false;
                    if Verify % Whether the improvement of the upper bound is indicated as an option, default is false
                        
                        
                        % The upper bound can potentially be improved. Conditions are:
                        %	A.  The Intervals for the transitions as given
                        %       for the LB cannot include a knot.
                        Absolute 		=	CheckResolution(Model0,Interval) 	;
                        
                        if all(Absolute) % All Intervals do not include a knot (of the original "Model0")
                            if anydisc
                                % The upper bound can be IMPROVED if some
                                % discontinuities are applied
                                
                                % Take the adjusted Model for lower bound
                                % computions
                                QR              =   ModelLB.QR                  ;
                                
                                % If there are zero-valued derivatives on
                                % only one side of a transition point, then
                                % set the transition point equal to its
                                % bound on that side
                                for j=1:m
                                    anyzero = any(QR(j:j+1,3:end-1)==0,2) ;
                                    if any(anyzero)
                                        if ~all(anyzero)
                                            Trans(j) = Interval(j,anyzero) ;
                                            QR(j,2) = Trans(j) ;
                                            QR(j+1,1) = Trans(j) ;
                                        end
                                    end
                                end
                                
                                % Set only those transitions corresponding
                                % to discontinuities to the proposed value
                                % for the upper bound
                                QR(discloc+1,1) =	Trans(discloc)         ;
                                QR(discloc,2)   =   Trans(discloc)         ;
                                
                                % Recompute solution
                                Model       =   Model0                  ;
                                Model.QR	=   QR                      ;
                                Model       =   FitSCS(Model)          ;
                                if Graph
                                    PlotSCS(Model) ;
                                end
                                UB          =   Model.J                 ;
                                
                                % The following line provides the exact
                                % solution for those transitions not
                                % implying discontinuities:
                                x1              =   VerifySCS(Model,Interval)  ;
                                Trans(disc_deg==0) =   x1(disc_deg==0)         ;
                                Model.SolX      =   Trans                  	;
                                
                            else
                                % The upper bound can be SET EQUAL to the
                                % lower bound if, additionally:
                                %   B.	There are no discontinuities
                                %       implied by QR
                                UB      =   LB                          ;
                                Model	=   ModelLB                     ;
                                
                                % The following line provides the exact
                                % solutions:
                                Trans          =   VerifySCS(Model,Interval) 	;
                                Model.SolX  =   Trans                      ;
                            end
                        end
                    end
                    
            end
            
        end
        
        
    end
end


% figure, plot(ModelLB.x)

% Numerical inaccuracies can lead to an upper bound slightly below the lower bound.
% The following line ensures consistency
if UB<LB
    LB 	=	min([UB,LB])-eps ; % subtract small number so the bounds are not considered equal.
end
% pause