function [R,Chat,g] = EvaluateODE(pars,ratefun1,c0,tmeas,Cmeas,sigmaY)

% -------------------------------------------------------------------------
% EMI Toolbox - EvaluateODE.m
% -------------------------------------------------------------------------
% Description
%
% Evaluates the fit of a growth-decay model.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[R,Chat,g] = EvaluateODE(pars,ratefun1,c0,tmeas,Cmeas,sigmaY)
%
% INPUT
%   pars        :   Parameters
%   ratefun1	:   Growth rate law
%   c0          :   Initial concentrations
%   tmeas       :   Sampling times
%   Cmeas       :   Measurements
%   sigmaY      :   Measurement error standard deviations
%
% OUTPUT
%   R           :   Residuals (normalized)
%   Chat        :   Model predictions
%   g           :   Objective function
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-04
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

odeoptions	=	odeset                                                          ;
[~,Ssim] 	= 	ode15s(@FittoRates_odefun,tmeas,c0,odeoptions,ratefun1,pars)	;
Chat        =   [   Ssim(:,1)    Ssim(1,1)-Ssim(:,1)  Ssim(:,2)   ]'            ;

if nargin>=5 && (~isempty(Cmeas) && ~isempty(sigmaY) )
    R       =	diag(1./sigmaY)*(Chat-Cmeas)    ;
    R       =   R(:)                            ;
    g       =	sum(R(:).^2)/length(R(:))       ;
    g       =   sqrt(g)                         ;
else
    g       =   nan     ;
    R       =   nan     ;
end
