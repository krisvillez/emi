function [Tsim,Xsim,Csim,Rsim,OURsim] = BatchSim(Tsim,fun,gamma)

% -------------------------------------------------------------------------
% EMI Toolbox - BatchSim.m
% -------------------------------------------------------------------------
% Description
%
% Simulates the batch process.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Tsim,Xsim,Csim,Rsim,OURsim] = BatchSim(Tsim,fun,gamma)
%
% INPUT
%   Tsim	:	Simulation sampling times
%	fun		:	Relative reaction rate function
%	gamma 	:	Parameter vector
%
% OUTPUT
%   Tsim 	:	Returned simulation sampling times
%	Xsim 	:	Relative substrate concentration
%	Csim 	:	Substrate concentration
%	Rsim 	:	Relative reaction rate
%	OURsim	:   Oxygen Uptake Rate
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

global optionsode

theta = gamma(1:3) ;

if all(theta==0)
    Tsim	=	Tsim                           ;
    Xsim    =   Tsim*0+1                       ;
    Xsim(1) =   0                               ;
else
    odefun      =	@(t,x) fun(1-x,theta)               ;
    optionsode_local = optionsode ;
    converged = false ;
    while ~converged
        
        [TsimOut,Xsim] =   ode23s(odefun,Tsim,0,optionsode_local)   ;
        if length(TsimOut)==length(Tsim)
            converged=true;
        else
            disp('increasing required simulation precision')
            optionsode_local.AbsTol=optionsode_local.AbsTol/10 ;
            optionsode_local.RelTol=optionsode_local.RelTol/10 ;
%             optionsode_local.AbsTol
        end
    end
end

Xsim       =   min(max(Xsim,0),1) ;


if nargout>=3
    
    C0      =	gamma(4)           ;
    Resp	=	gamma(5)           ;
    Yield	=	gamma(6)           ;

    Csim	=	C0*(1-Xsim)         ;
	Rsim	=	odefun(0,Xsim)      ;
	OURsim	=	Rsim*Yield + Resp	;
end