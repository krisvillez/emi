function QR = String2QR(String,Order,Trans,Disc)

% -------------------------------------------------------------------------
% Shape Constrained Splines toolbox - String2QR.m
% -------------------------------------------------------------------------
% Description 
%
% String2QR is a function which converts a qualitative sequence given as a
% string into a matrix describing the same qualitative sequence
% or, pending the provision of values for the transitions, the
% corresponding qualitative representation.  To do so, implicit absence of
% constraints is automatically rendered explicit (e.g., implied
% discontinuities).     
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	QR = String2QR(String,Order,Trans,Disc)
%
% INPUT
%	String:		String representing the sequence of primitives (QS,
%               Qualitative Sequence). 
%               (required) 
%	Order:      Scalar indicating the Order of the spline function.
%               (optional, default=4 (cubic splines)) 
%   Trans       Location of the episode transitions. If empty, these
%               transitions are considered unknown.
%               (optional, default=[])
%   Disc        Vector of integers indicating the number of discontinuous
%               derivatives (knot multiplicity) at each transition in the
%               qualitative sequence/representation. This allows to add
%               discontinuities when they are not implied by the
%               qualitative sequence.
%               (optional, default=[])
%
% OUTPUT
%	QR:     	Matrix describing qualitative sequence/representation.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of a case study in Matlab/Octave. 
% 
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

nQ = length(String) ;

if nargin<2 || isempty(Order)
    Order 	= 4 ; % Assume a cubic spline function
end

if nargin<3 || isempty(Trans)
    Trans   =   zeros(1,nQ+1)     ;
end
if nargin<4 || isempty(Disc)
    Disc	=	zeros(1,nQ-1)   ;
end
Trans = Trans(:)' ;
assert(nQ==length(Trans)-1,'abnormal lengths') 

[Characters,Signs] = Alphabet([],3) ;

[tf,loc] = ismember(String,Characters);

AllSigns 	= 	nan(Order,nQ)	;
rows 		=	2:min(3,Order)	;
 Signs = Signs(rows-1,loc)  ;
AllSigns(rows,:) =Signs;

d1 = 1	;
d2 = 2 ;
for j=1:nQ-1
	SignMat 	=	Signs(:,j:j+1) ;
	
    if all(~isnan(SignMat(:)))
	if any(SignMat(d1,:)==0)
		if all((SignMat(d1,:)==0))
			% no update
		else
			seg 	=	find(SignMat(d1,:)~=0) ;
			switch seg
			 case 1
				if or(all(SignMat(d1:d2,1)==-1),all(SignMat(d1:d2,1)==+1))
					drv = 1 ;
					Disc(j) = max(Disc(j),Order-drv) 	;
					end
			 case 2
			 
				if or(all(SignMat(d1:d2,2)==[+1;-1]),all(SignMat(d1:d2,2)==[-1;+1]))
					drv = 1 ;
					Disc(j) = max(Disc(j),Order-drv) 	;
				end
			end
		end
	else
		if any(SignMat(d2,:)==0)
			if SignMat(d1,1) ~= 	SignMat(d1,2)  % no abrupt changes in 1st derivative allowed
				drv = 1 ;
				Disc(j) = max(Disc(j),Order-drv) 	;
			end
        else
		
            
		Signdiff = SignMat(:,1)~=SignMat(:,2) ;
		cnt = sum(Signdiff) ;
        if cnt>1 % slope sign and curvature change together
			drv 	=	find(Signdiff,1,'first') ;
			Disc(j) = max(Disc(j),Order-drv) 	;
		elseif Signdiff(d1) % same curvature , change in slope sign
            if or(	all(SignMat(d1:d2,1)==+1) , all(SignMat(d1:d2,j)==-1) )
				% either BA or DC transition
				drv = 1 ;
				Disc(j) = max(Disc(j),Order-drv) 	;
			end
		end
		
		end
    end
    end
		
end

QR = [  Trans(1:end-1)     ; 
        Trans(2:end)       ;
		AllSigns 		;
		false Disc      ];
QR 	= 	QR' 			;
