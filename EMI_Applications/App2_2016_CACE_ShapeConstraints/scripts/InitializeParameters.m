
% -------------------------------------------------------------------------
% EMI Toolbox - InitializeParameters.m
% -------------------------------------------------------------------------
% Description
%
% See contents.m 
%
% -------------------------------------------------------------------------
% Last modification: 2016-08-23
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

currentfolder = pwd;
addpath(genpath([currentfolder '\CodeInUse']))
addpath(genpath([currentfolder '\MAT_files']))

pars	=	[ .1      .04      1.6	     3	    20    3       8             2.5 ]	; % added yield and decay rate as param 1 and 2
% pars =    [yield    decay   mu_max    K_S    K_I   beta   Sopt_Steele   S_opt_PE]

P = pars(3:end);

SS      =       0:.01:25         ;
S0      =       25              ;

X0      =       5	;

snr     =   .02 ;
sigmaY  =  [ S0*snr ; S0*snr ; X0*snr ] ;

Tend = 10; % hours
ts = 15/60; % sampling time (hours)

AbsTol			=	1e-6       ;
RelTol			= 	1e-6       ;
odeoptions 		=	odeset('RelTol',RelTol,'AbsTol',AbsTol,'NonNegative',1);

SFP             =	struct('pars',pars,'P',P,'sigmaY',sigmaY,'SS',SS,'S0',S0,'X0',X0,'Tend',Tend,'ts',ts,'AbsTol',AbsTol,'RelTol',RelTol,'odeoptions',odeoptions);
model_names     =	{'Root','Monod','M+T','Tessier','Tanh','Haldane','Peeters & Eilers','Steele'}	;
SFP.model_names =   model_names     ;


