function [xsim,cbarsim,csim,pHsim,Model] = SimExtent(Model,Theta)

% -------------------------------------------------------------------------
% EMI Toolbox - SimExtent.m
% -------------------------------------------------------------------------
% Description
%
% Simulates the batch process.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:  [xsim,cbarsim,csim,pHsim,Model] = SimExtent(Model,Theta)
%
% INPUT
%   Model	:   Model description given as a structure (see ExtentDAE.m
%              	for required structure)
%	Theta	:	Parameter vector
%
% OUTPUT
%   xsim    :   Extent of reactions
%   cbarsim	:   Kinetic species and equilibrium component concentrations
%   csim    :   Species
%   pHsim   :   pH
%   Model   :   Model description with updated parameters if applicable
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

verbose  = false ;
    
if nargin<2
    Theta = [] ;
end

global optionsode
optionsode_local = optionsode ;

Rk = length(Model.Reactions);

global Archive_Xsim Archive_Time Archive_theta

tsim = Model.Measurement.tsim ;

if ~or(isempty(Theta),isempty(Archive_theta)) && any(ismember(Archive_theta,Theta,'rows'))
    %disp('Present in archive')
    [loc] = find(ismember(Archive_theta,Theta,'rows'));
    xsim = Archive_Xsim(:,:,loc);
else
    
    nReaction = length(Model.Reactions) ;
    
    % Get applicable parameters
    if ~isempty(Theta) && ~(isfield(Model,'parfix') && Model.parfix)
        
        for i=1:Rk
            
            Reaction            =	Model.Reactions{i}      ;
            Reaction.theta      =	Theta(Reaction.Theta2r)	;
            Model.Reactions{i}	=	Reaction                ;
        end
        
    end
%     Model.Reactions{1}.theta
    
    x0	=	zeros(nReaction,1)	;
    
    
    global SimTime
    SimTime=0;
    
    if verbose
        disp(' ')
        disp('------- Simulation -------------------------------------------')
    end
    
    tic
    optionsode_local.NonNegative=1;                                
        [oo,xsim]	=	ode15s(                                 ...
        @(t,x) ExtentDAE( t,x,Model,verbose)    ,   ...
        tsim                            ,   ...
        x0                              ,   ...
        optionsode_local                    ...
        );
    SimTime     =	toc         ;
    
%     tic
%     %optionsode_local.NonNegative=1;                                
%         [oo,xsim]	=	ode23s(                                 ...
%         @(t,x) ExtentDAE( t,x,Model,verbose)    ,   ...
%         tsim                            ,   ...
%         x0                              ,   ...
%         optionsode_local                    ...
%         );
%     SimTime     =	toc         ;
    
    if verbose
        disp(['------- Simulation time: ' num2str(SimTime/60) ' minutes'])
        disp('------- Simulation ended -------------------------------------')
    end
    
    xsim        =   xsim'       ;
    
    if size(xsim,1) == length(Model.Measurement.tsim)
        retry=false;
    else
        optionsode_local.AbsTol = optionsode_local.AbsTol/10 ;
        optionsode_local.RelTol = optionsode_local.RelTol/10 ;
    end
    
    
    if ~isempty(Theta) && ~isempty(Archive_Time)
        if SimTime>min(Archive_Time)
            [oo,loc]=min(Archive_Time);
            Archive_Time(loc) = SimTime;
            Archive_Xsim(:,:,loc) = xsim ;
            Archive_theta(loc,:) = Theta ;
        end
    end
    
end


if nargout>=2
    [csim,cbarsim,pHsim] = Model.x2c(tsim,xsim,Model)	;
end


end

