function c = Extent_pH_2_Concentration(Model,pH,cbar)

% -------------------------------------------------------------------------
% EMI Toolbox - Extent_pH_2_Concentration.m
% -------------------------------------------------------------------------
% Description
%
% Computes species concentrations from the concentrations of the kinetic
% species and equilibrium component concentrations.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	c = Extent_pH_2_Concentration(Model,pH,cbar)
%
% INPUT
%   Model	:   Model description given as a structure (see ExtentDAE.m
%              	for required structure)
%	pH      :   pH, defined as -log([H+])
%	cbar    :	Kinetic species and equilibrium component concentrations
%
% OUTPUT
%	c       :	Species concentrations
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

pKa	=	Model.Equilibrium.pKa   ;

H	=	length(pH)  ;
cH	=	10.^(-pH)    ;

S       =   Model.dimensions.S	;
c       =	-ones(S,H)          ;

c(1,:)	=	cbar(1,:)                           ;	%	O2

c(2,:)  =	cbar(2,:)./(1+10.^(-pKa(1))./cH)	;   %   NH4+
c(3,:)	=	cbar(2,:)-c(2,:)                    ;   %   NH3

c(4,:)	=	cbar(3,:)./(1+10.^(-pKa(2))./cH)	;   %   HNO2
c(5,:)  =	cbar(3,:)-c(4,:)                    ;   %   NO2-

c(6,:)	=	cbar(4,:)./(1+10.^(-pKa(3))./cH)	;   %   HNO3
c(7,:)  =   cbar(4,:)-c(6,:)                    ;   %   NO3-

c(8,:) = cH ;
