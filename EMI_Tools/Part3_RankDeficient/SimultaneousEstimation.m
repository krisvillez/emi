function model   =   SimultaneousEstimation (model,theta_init,options)

% -------------------------------------------------------------------------
% EMI Toolbox - SimultaneousEstimation.m
% -------------------------------------------------------------------------
% Description
%
%   This function executes simultaneous parameter estimation.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	model   =   SimultaneousEstimation (model,theta_init,options)
%
% INPUT
%   model       :   the model whose parameters are being estimated
%   theta_init  :   initial guesses for the parameters
%   options     :   fine-tuning of the optimization method 
%
% OUTPUT
%	model       :   the model with estimated parameters
%
% -------------------------------------------------------------------------
% Last modification: 2019-02-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


if options.verbose, disp('PARAMETER ESTIMATION - SIMULTANEOUS'), end

% ------------------------------------------------------------------------
%	Get information from model structure
% ------------------------------------------------------------------------

V           =   model.experiment.V          ;   % volume
n_0         =   model.experiment.n_0        ;   % initial conditions
t_          =	model.experiment.t_         ;   % time
select_h	=	model.experiment.select_h	;   % sampling time selection vector

T           =	model.kinetic.T             ;   % number of kinetic parameters
ratecell	=	model.kinetic.ratecell      ;   % rate expressions

ytil        =	model.observations.ytil_h   ;   % concentration measurements

N_          =	model.static.N_             ;   % stoichiometrix matrix
M_          =	model.static.M_             ;   % concentration measurement gain matrix
M_eps       =	model.static.M_eps          ;   % noise gain matrix

% ------------------------------------------------------------------------
%	Set up optimization problem
% ------------------------------------------------------------------------

Gamma       =   chol(inv(M_*(M_eps*M_eps')*M_'),'lower')	;   % weight matrix for residuals

% scalar dimensions:
[M,S]       =	size(M_)                                                ;   
T          	=	length(theta_init)                                      ;

% time vector used for simulation during parameter estimation:
t_opt       =   t_(and(t_<=max(t_(select_h)),t_>=min(t_(select_h))))    ;

% reformulate cell array of expressions as vector-valued expression:
ratefun     =	@(c_,theta_) cellfun(@(expr) expr(c_,theta_), ratecell)	;

% formulate model in concentration space:
SIM_model	=   @(t,n_,theta_) N_'*max(0,ratefun(n_/V,theta_))          ;

% set up obtjective function
ObjectiveFun	=	@(p) sum( sum( ...
    ( Gamma*( ytil - M_*Simulation(p,SIM_model,t_opt,select_h,eye(S),n_0)/V ) ).^2 ...
    ) ) ;

% ------------------------------------------------------------------------
%	Execute parameter estimation
% ------------------------------------------------------------------------

% Initial parameter guesses:
theta_0                     =	theta_init          ;
theta_0(isnan(theta_init))	=	0                   ;

% Get simulation results with initial parameter guesses:
[~,nhat]            =	Simulation (theta_0,SIM_model,t_,select_h,eye(S),n_0);
J0                  =	ObjectiveFun (theta_0)      ;
model.SIM.nhat_init	=	nhat                        ;

% Define search region:
pL                  =	1e-3*ones(T,1)              ;
pU                  =	1e+3*ones(T,1)              ;

% Optimization:
[theta_hat,fval,exitflag,output,lambda,grad,hessian]	= ...
    fmincon(@(p) ObjectiveFun(p),theta_0,[],[],[],[],pL,pU,[],options.optim2) ;

if isempty(hessian)
    hessian = zeros(T);
end

% Get simulation results with final parameter estimates:
[~,nhat]            =	Simulation (theta_hat,SIM_model,t_,select_h,eye(S),n_0);
J1                  =	ObjectiveFun (theta_hat)	;
model.SIM.nhat_fin	=	nhat                        ;

% ------------------------------------------------------------------------
%	Output
% ------------------------------------------------------------------------

% Postprocessing:
theta_hat(isnan(theta_init))	=	nan                                 ;
theta_hat_cov                   =	inv(hessian)                        ;
theta_hat_cov                   =	(theta_hat_cov+theta_hat_cov')/2    ;
theta_hat_sd                    =	diag(theta_hat_cov).^(1/2)          ;

% Save results:
model.kinetic.theta_init        =	theta_init                          ;
model.kinetic.theta_hat         =	theta_hat                           ;

% Command line output:
if options.verbose>=2
    
    disp(['	RMSR before fine-tuning : ' num2str(sqrt(J0/(M*length(select_h))),'% 7.5f')])
    disp(['	RMSR after fine-tuning :  ' num2str(sqrt(J1/(M*length(select_h))),'% 7.5f')])
    if options.verbose>=2
        disp(['	Parameters  ' 'Initial ' '    Final ' ])
        
        name_parameters	=	model.kinetic.name_parameters	;
        
        for t=1:T
            lbl         =	name_parameters{t}                  ;
            init        =	sprintf('% 10.6f',theta_init(t))       ;
            est         =	sprintf('% 10.6f',theta_hat(t))       ;
            est_sd      =	sprintf('% 10.6f',3*theta_hat_sd(t))	;
            disp(['         ' lbl ' : ' init '  ' est '  (' char(177) est_sd ')' ])
        end
    end
    
end
