function [Tree,model,Progress,Record]= BB(model,options,IntX,DistX,X0)

% -------------------------------------------------------------------------
% Spike_O Toolbox - BB.m
% -------------------------------------------------------------------------
% Description
%
% This function executes the branch-and-bound algorithm.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Tree,model]= BB(model,options,IntX,DistX,X0)
%
% INPUT
%   model   :   mathematical problem as a structure
%   options :   [optional] options for algorithm execution
%   IntX    :   Root set
%   DistX   :   Distance constraints
%   X0      :   Initial guess
%
% OUTPUT
%   Tree    :   Solution tree
%   model   :   updated model with solution as field 'SolX' and selected
%               node as field 'IntX'
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-06-09
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2015 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% =========================================================================
% I. Setup and initialization

if nargin<2 || isempty(options)
    default.boundfun    =   @Bounds      ;
    default.boundlow    =   @Boundlow    ;
    default.boundupp    =   @Boundupp    ;
    default.earlystop   =   1               ;
    default.node        =   2               ;
    default.var         =   1               ;
    default.rounding    =   0               ;
    default.branching   =   1               ;
    default.splitting   =   3               ;
    default.resolution  =   .01             ;
    default.maxiter     =   1e6             ;
    default.verbose     =   2               ; % values: 0-2
    default.verify      =   2               ; % values 
    default.graph       =   2               ;
    default.defaultsplit   =   []               ;
    default.graphprogress = true;
    default.graphinterval = 1;
    options             =   default         ;
end

if isfield(model,'type')
    SCStype = or(strcmp(model.type,'SCS'),strcmp(model.type,'SCSD')) ;
else
    SCStype =false;
end

if SCStype
    DefIntX = DefaultIntX(model) ;
    if nargin <3 || isempty(IntX)
       IntX = DefIntX; 
    else
        IntX(1,:)  = max([ IntX(1,:); DefIntX(1,:)]);
        IntX(2,:)  = min([ IntX(2,:); DefIntX(2,:)]);
    end
end
nib         =   size(IntX,2)        ;

% save original input model (Generative Trend Object)
model0       =   model             ;
%TimeMin     =   min(tk(2:end)-tk(1:end-1))      ;
%TimeMax     =   max(tk(2:end)-tk(1:end-1))      ;

% ----------------------------------------------------
% Get options
boundfun            =   options.boundfun    ;   % function to be used to compute bounds on solution
boundupp            =   options.boundupp    ;   % function to be used to upper bounds on solution
%boundlow            =   options.boundlow    ;   % function to be used to upper bounds on solution
method              =   options.node        ;   % method of node selection for branching: 1: best lower bound; 2: best upper bound; 3: depth-first, 4: breadth-first
graph               =   options.graph       ;
graphprogress       =   options.graphprogress       ;
graphinterval       =   options.graphinterval       ;
earlystop           =   options.earlystop   ;
Resolution          =   options.resolution  ;   % time resolution (only important for discontinuities)
MaxIter             =   options.maxiter     ;   % maximum iterations
Verbose             =   options.verbose     ;   % command line outputs on or off
Verify              =   options.verify      ;   % use shape verification when available

IterDisplay = graphinterval ;

% ----------------------------------------------------
% SPECIFICATIONS FOR LEAF GENERATION
Genleafoptions.rounding         =   options.rounding        ;
Genleafoptions.branching        =   options.branching       ; % method of branching
Genleafoptions.splitting        =   options.splitting       ;
Genleafoptions.defaultsplit     =   options.defaultsplit    ;   % default splits

global BestObj
global lowestlevel
lowestlevel = false ;
BestObj =   inf ;
if earlystop
    if isfield(model,'BestObj')
        BestObj =   model.BestObj ;
    end
end

% interval for locations of interior bounds 
if nargin<4 || isempty(DistX),  
    if SCStype
        DistX   =   DefaultDistX(model)         ;   % set default distance ranges
    else
        DistX   = [];
    end
else
    DistX   =   min(max(DistX,-Inf),Inf)    ;   % reduce range where possible
end
% -------------------------------------------------------------------------
% PREPARATIONS

Indices     % necessary index variables

outsidefirst = false;
if outsidefirst
    % generate vector vv: indicates default order of variables. This is
    % done so that the transition variables at the 'outside' are handled
    % first. This is a heuristic rule which sometimes leads to fast
    % exclusion of extremal regions in the solution space if these are
    % indeed suboptimal
    ni1             =   floor(nib/2)        ;
    ni2             =   nib-ni1             ;
    vv(1:2:nib)     =   1:ni2               ;
    vv(2*(1:ni1))   =   fliplr(ni2+1:nib)   ;
else
    vv = [1:1:nib  ];
end

% -------------------------------------------------------------------------
% Variable resolution
DeltaX      =   Resolution*ones(1,nib)      ;
RangeX      =   Ranger(IntX)                ;

% reset MaxIter when possible
splitting   =	Genleafoptions.splitting                ;
Bins        =   (IntX(2,:)-IntX(1,:))./DeltaX           ;   % width in intervals of width Dcrit
BinMax      =   ceil(log(Bins)/log(splitting))          ;   % max no of binary decisions for each variable
BinMax      =   max(BinMax,1);

MaxIter     =   min(1e7,prod(splitting.^BinMax))    ;
if Verbose, disp(['Maximum iterations set to: ' num2str(prod(splitting.^BinMax))])  , end

IntX0       =   IntX                        ;   % save original variable intervals
opt         =   find(RangeX>0)              ;
nopt        =   sum(opt>0)                  ;
Vol         =   prod(RangeX(opt))           ;   % (knot-based) volume for root set
City        =   sum(RangeX(opt))            ;   % (knot-based) city distance root set
Vol0        =   Vol                         ;   % reference volume
City0       =   City                        ;   % reference city distance
% IntX0
% ----------------------------------------
% make graphs
ProgressReadOut =10000;

if graph
    
    if graphprogress
        figure(73)
        close(73)
        figure(73)
        set(gcf,'Position',[73 73 600 600])
        
        Progress 	= 	nan(min(MaxIter,ProgressReadOut),5)	;
		IterRef 	=	0 							;
    end
    
    figure(99)
    close(99)
    Lim     =   IntX(:,opt)     ;
    
    if nopt==1
        if isfield(options,'graphtype')
            graphtype = options.graphtype;
        else
            graphtype = 3;
        end
        nsub = 1;
        
        figure(99)
        subplot(nsub,1,1)
%        if exist('AxisPrep',2), AxisPrep, end
        hold on
        Xlim    =   Lim(:,1)    ;
        Ylim    =   [ 0; 1]     ;
        
        switch graphtype
            case 1
                set(gca,'Xlim',Xlim,'Ylim',Ylim)
                drawnow
            case 2
                set(gca,'Xlim',Xlim )
                drawnow
			case 3
				subplot(2,1,1), hold on
                set(gca,'Xlim',Xlim,'tickdir','out' )
                subplot(2,1,2), hold on
                set(gca,'Xlim',Xlim ,'tickdir','out')
				%set(gca,'color',ones(1,3)*.5,'tickdir','out')
                drawnow
        end
    elseif nopt==2
        graphtype = 1;
        figure(99)
%         AxisPrep
        hold on
        Xlim    =   Lim(:,1)    ;
        Ylim    =   Lim(:,2)    ;
        axis square
        drawnow
        set(gca,'Xlim',Xlim,'Ylim',Ylim)
        drawnow
    else
        graphtype = 2;
        
        figure(99)
%         AxisPrep
%         hold on
        Xlim    =   [.5 nib+.5]    ;
        Ylim    =   [ min(IntX(1,:)) max(IntX(2,:)) ]   ;
        drawnow
        set(gca,'Xlim',Xlim,'Ylim',Ylim)
        drawnow
        low = min(IntX,[],1);
        upp = max(IntX,[],1);
        han0 = plot([1:nib ; 1:nib],[low; upp],'ko-','MarkerFaceColor','k');
        drawnow
    end

end
% ----------------------------------------
if Verify>=0
    if SCStype
        if nargin('AssessVerifiability')==2
            model       =   AssessVerifiability(model,IntX)  ;
        end
    end
end
% -------------------------------------------------------------------------
% INITIALIZE BnB

Termination =   0   ;   % algorithm termination: 1/0 true/false
Iteration   =   0   ;   % iteration count: integer

if Verbose, disp(['Branching step: ' num2str(Iteration) ]), end  % display

% -----------------------------
% Compute bounds on root node
[LB,UB]     =   boundfun(model,IntX0,DistX)   ;    % evaluate generic bounds on root


% improve upper bound by using provided solution if possible
if nargin>=5 && ~isempty(X0)
    % following line changed: all boundupp functions should have IntX0 as
    % additional input
    X0      =   min(max(IntX0(1,:),X0),IntX0(2,:))  ;
    UBinit  =   boundupp(model,IntX0,X0)    ;
    UB      =   min(UBinit,UB)              ;
end
OB1  = UB ;
model.OB1 = OB1 ;

% -----------------------------
% Gather information of root node
%               parent      level   live    LB  UB   Volume City solution
Root        =   [   0       0       1       LB  UB  Vol0    City0 IntX(:)' ]   ;
% -----------------------------
% Initialize tree matrix and record keeping
nRoot       =       length(Root)                ;   % number of values to be stored per node
Tree        =       zeros(1,nRoot)        ;   % initialize tree matrix
Tree(1,:)   =       Root                        ;   % populate first item (root node)
nTree       =       1                           ;   % number of populated nodes
Record      =   [   Iteration LB UB 1 1 ]       ;   % matrix to keep track of solution quality; not necessary for algorithm

if graph
	if graphtype ==3 
		
		subplot(2,1,1)
		VisualTreeNodes(Tree,Iteration)
		
		
	end
end


% =========================================================================
% II. Actual algorithm
IterRef = Iteration;

while ~Termination
    
    Iteration   =   Iteration +1                ;   % Update iteration count
    % 0. checks
    %assert(all(Tree(:,3)<=Tree(:,4)+1e-3),'Some UB lower than LB')
    
    % 1. locate node to split
    % heuristic: live node with minimal LB
    Alive       =   find(Tree(1:nTree,iLive)==1)    ; % find nodes that are alive and for which knot resolution is not achieved
    if isempty(Alive)   % if no such nodes
        Alive   =   find(Tree(1:nTree,iLive)>=1)    ; % find nodes that are alive
    end
    
    % stop algorithm under these conditions
    Term1           =   all(Tree(1:nTree,iLB)>BestObj)  ;
    Term2           =   isempty(Alive)                  ;
    Term3           =   Iteration>MaxIter               ;
    
    Termination     =   any([Term1 Term2 Term3 ])  ;
        
    if ~Termination 
        
        if Verbose && mod(Iteration,IterDisplay)==0
            clc
        end
        
        if Verbose && mod(Iteration,IterDisplay)==0, disp(['Branching step: ' num2str(Iteration) ]), end  % display
        
        % -----------------------------------------------------------------
        % 1. select node to be branched
        
        [MinUB]     =   min(Tree(Alive,iUB),[],1)           ;   % find minimum upper bound in live nodes
        
        switch method
            
            case 2 % best upper bound
                
                diffloc     =   find(Tree(Alive,iUB)==MinUB)        ;   % find nodes with minumum upper bound
                [oo,loc]    =   max(Tree(Alive(diffloc),iVol),[],1) ;   % among nodes with minimum upper bound, find the one with maximum volume
                branch      =   Alive(diffloc(loc))                 ;   % select this one as branch
            case 1 % best lower bound
                [MinLB]     =   min(Tree(Alive,iLB),[],1)           ;   % find minimum lower bound in live nodes
                diffloc     =   find(Tree(Alive,iLB)==MinLB)        ;   % find nodes with minumum lower bound
                [oo,loc]    =   min(Tree(Alive(diffloc),iUB),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                branch      =   Alive(diffloc(loc(1)))                 ;   % select this one as branch
            case 6 % depth-first
                %branch      =   Alive(find(Tree(Alive,iParent)==Tree(Alive(end),iParent),1,'first')) 
                Level       =   Alive(Tree(Alive,iLevel)==max(Tree(Alive,iLevel)) )  ;
                [MinLB]     =   min(Tree(Level,iLB),[],1)           ;   % find minimum lower bound in live nodes
                diffloc     =   find(Tree(Level,iLB)==MinLB)        ;   % find nodes with minumum lower bound
                [oo,loc]    =   min(Tree(Level(diffloc),iUB),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                branch      =   Level(diffloc(loc))                 ;   % select this one as branch
            case 5 % breadth-first
                Level       =   Alive(Tree(Alive,iLevel)==min(Tree(Alive,iLevel)) )  ;
                %Level       =   Alive(find(Tree(Alive,iLevel)==Tree(Alive(1),iLevel)))                  ;
                 [MinLB]     =   min(Tree(Level,iLB),[],1)           ;   % find minimum lower bound in live nodes
                 diffloc     =   find(Tree(Level,iLB)==MinLB)        ;   % find nodes with minumum lower bound
                 [oo,loc]    =   min(Tree(Level(diffloc),iUB),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                
%                 [MinUB]     =   min(Tree(Level,iUB),[],1)           ;   % find minimum upper bound in live nodes
%                 diffloc     =   find(Tree(Level,iUB)==MinUB)        ;   % find nodes with minumum upper bound
%                 [oo,loc]    =   min(Tree(Level(diffloc),iLB),[],1)  ;   % among nodes with minimum bound, find the one with minimum lower bound
                
                branch      =   Level(diffloc(loc))                 ;   % select this one as branch
            case 4 % worst upper bound
                [MinUB]     =   max(Tree(Alive,iUB),[],1)           ;   % find minimum upper bound in live nodes
                diffloc     =   find(Tree(Alive,iUB)==MinUB)        ;   % find nodes with minumum upper bound
                [oo,loc]    =   max(Tree(Alive(diffloc),iVol),[],1) ;   % among nodes with minimum upper bound, find the one with maximum volume
                branch      =   Alive(diffloc(loc))                 ;   % select this one as branch
            case 3 % worst lower bound
                [MinLB]     =   max(Tree(Alive,iLB),[],1)           ;   % find minimum lower bound in live nodes
                diffloc     =   find(Tree(Alive,iLB)==MinLB)        ;   % find nodes with minumum lower bound
                [oo,loc]    =   min(Tree(Alive(diffloc),iUB),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                branch      =   Alive(diffloc(loc(1)))                 ;   % select this one as branch
            case 7 % max difference
                D           =   Tree(Alive,iUB)-Tree(Alive,iLB)     ;
                [MaxD]      =   min(D,[],1)                         ;   % find maximum difference
                diffloc     =   find(D==MaxD)                       ;   % find nodes with minumum lower bound
                [oo,loc]    =   min(Tree(Alive(diffloc),iLB),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                branch      =   Alive(diffloc(loc))                 ;   % select this one as branch
        end
        try
            level               =   Tree(branch,iLevel)         ;   % level of parent
        catch
            method
            iLevel
            branch
            size(Tree)
            size(Alive)
            max(Alive)
            level               =   Tree(branch,iLevel)         ;   % level of parent
        end
        
        

        % -----------------------------------------------------------------
        % 2. branching
        if SCStype 
            Leafs = GenerateLeafs(Tree,branch,index,nib,vv,Verbose,Genleafoptions,DeltaX,model );
        else
        %    if mod(level,2)==0
                Leafs = GenerateLeafs(Tree,branch,index,nib,vv,Verbose,Genleafoptions,DeltaX);
         %   else
           %     Leafs = GenerateLeafsFirst(Tree,branch,index,nib,vv,Verbose,Genleafoptions,DeltaX);
          %  end
        end
        % -----------------------------------------------------------------
        % 3. Evaluate objective bounds in each of the new leaves
        % and execute fathoming where necessary
        n_leaf = length(Leafs);
        do_fathom = false;
        for i_leaf=n_leaf:-1:1
            % 3A. Evaluate objective bounds
            if Verbose>=3 && mod(Iteration,IterDisplay)==0, disp(['  Analyzing leaf - ' num2str(i_leaf) ' of ' num2str(n_leaf) ]), end                  % display
            Xleaf           =   Leafs{i_leaf,1}                 ;   % copy branch intervals for leaf
            
            DX=(Xleaf(2,:)-Xleaf(1,:));
            lowestlevel = all(DX<=DeltaX+eps);
        
            iTree           =   nTree+i_leaf                    ;
            [LB,UB,Xapp]    =   ProcessLeaf(Xleaf,IntX0,boundfun,model,DistX,Verify)        ;
            Tree            =   StoreLeaf(Tree,iTree,branch,level,opt,LB,UB,Xapp,Xleaf)     ;
            Tree            =   FathomLeaf(Tree,iTree,iLive,LB,UB,Xleaf,DeltaX,model)       ;
            
            if UB<=MinUB
                do_fathom = true ;
            end
            % vizualization
            if graph 
				if graphtype==1
					VisualLeaf(Tree,iTree,iLive,index,opt,nopt,graphtype)  ;
				elseif graphtype==3
					VisualLeaf(Tree,iTree,iLive,index,opt,nopt,graphtype)  ;
					subplot(2,1,1)
					VisualTreeNodes(Tree,Iteration);
				end
            end
            
        end

        % -----------------------------------------------------------------
        % 4. Fathom by dominance
        Tree(branch,iLive)          =   0                           ;   % parent is set dead        
        nTree                       =   nTree+n_leaf                ;   % number of populated nodes
        Alive                       =   find(Tree(1:nTree,iLive))   ;   % find live nodes
        if do_fathom
            %disp(['Iteration: ' num2str(Iteration) ' - fathoming'])
            fathom                      =   Fathoming(Tree,Alive)       ;   % find nodes to be fathomed by dominance
            Tree(Alive(fathom),iLive)   =   0                           ;   % set fathomed nodes as dead
            %disp(['Iteration: ' num2str(Iteration) ' - fathoming done'])
        else
            fathom = [];
            %disp('skip fathoming')
        end
        AliveN                     =   find(Tree(1:nTree,iLive))    ;
%         nAliveN        =   length(AliveN) ;
        % -----------------------------------------------------------------
        % vizualization
        
        if graph
            switch graphtype
                case 1
                    str = 'b' ;
                    VisualTree(Tree,Alive(fathom),str,index,opt,nopt, graphtype)    ;
                case 2
                    if mod(Iteration,graphinterval)==0
                        
                        figure(99)
                        drawnow
                        low = min(Tree(AliveN,index:2:end));
                        upp = max(Tree(AliveN,index+1:2:end));
                        delete(han0)
                        han0 = plot([1:nib ; 1:nib],[low; upp],'ko-','MarkerFaceColor','k');
                        set(gca,'Xlim',Xlim,'Ylim',Ylim)
                        drawnow
                    end
            end
        end
        
        % -----------------------------------------------------------------
        % record keeping
        
        %if Verbose && mod(Iteration,IterDisplay)==0, disp(['Branching step: ' num2str(Iteration) ]), end  % display
        
        Record(Iteration+1,:)   =   [   Iteration min(Tree(Alive,iLB)) min(Tree(1:nTree,iUB)) sum(Tree(1:nTree,iLive)==1) nTree ] ;
        
        Alive                   =   find(Tree(1:nTree,iLive))   ;   % find live nodes
        %         TotCity                 =   sum(Tree(Alive,iCity))          ;
        TotVol                  =   sum(Tree(Alive,iVol))           ;
        if Verbose && mod(Iteration,IterDisplay)==0, disp(['	 Fraction: ' num2str(TotVol/Vol0*100)   '%' ]), end
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['  Volume:            ' num2str(TotVol)   ' of ' num2str(Vol0)   ]), end
        minLB   =   min(Tree(Alive,iLB))    ;
        minUB   =   min(Tree(Alive,iUB))    ;
        BestObj = minUB;
        %         any(Tree(Alive,iLB)>=OB1)
        
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['  Min UB:            ' num2str(minUB)    ]), end
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['  Min LB:            ' num2str(minLB)    ]), end
        
        %         disp(['  City distance:            ' num2str(TotCity)   ' of ' num2str(City0)  ' [' num2str(TotCity/City0*100)   '%]' ])
        %Tree(1:nTree,:)
        
        Tree    =   Tree(Alive,:)   ;
        nTree   =   size(Tree,1)    ;
        
        
            BBox	=	[   min(Tree(:,index:2:end))	;
                max(Tree(:,index+1:2:end)) ];
            BBoxVol = prod(BBox(2,:)-BBox(1,:)) ;

		Progress(Iteration-IterRef,:) = [ BBoxVol TotVol nTree min(Tree(:,iLB)) min(Tree(:,iUB)) ];
		if mod(Iteration,ProgressReadOut)==0
			csvwrite(['Progress' num2str(Iteration) '.csv'],Progress)
			IterRef			=	Iteration	;
			Progress(:,:)	=	nan			;
        end
			
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['  Nodes :            ' num2str(nTree)    ]), end
        
        if mod(Iteration,graphinterval)==0 && graphprogress
            figure(73),
            drawnow
                        
            subplot(3,1,1), hold on,
            if exist('han3'), delete(han3), end
            han3 = plot(Progress(1:(Iteration-IterRef),3),'k.','MarkerFaceColor','k') ;
            drawnow
            
            subplot(3,1,2), hold on,
            if exist('han4'), delete(han4), end
            if exist('han5'), delete(han5), end
            set(gca,'Yscale','log')
            han4=plot(Progress(1:(Iteration-IterRef),4),'r.') ;
            han5=plot(Progress(1:(Iteration-IterRef),5),'b.') ;
            drawnow
            
            subplot(3,1,3), hold on,
            if exist('han1'), delete(han1), end
            if exist('han2'), delete(han2), end
            %han1=plot(Progress(:,1),'r.') ;
            han2=plot(Progress(1:(Iteration-IterRef),2),'k.') ;
            drawnow
            
        end
    else
        Iteration = Iteration-1;
        Alive                   =   find(Tree(1:nTree,iLive))   ;   % find live nodes
        %         TotCity                 =   sum(Tree(Alive,iCity))          ;
        TotVol                  =   sum(Tree(Alive,iVol))           ;
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['  Volume:            ' num2str(TotVol)   ' of ' num2str(Vol0)  ' [' num2str(TotVol/Vol0*100)   '%]' ]), end
        minLB   =   min(Tree(Alive,iLB))    ;
        minUB   =   min(Tree(Alive,iUB))    ;
        BestObj = minUB;
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['  Volume:            ' num2str(TotVol)   ' of ' num2str(Vol0)  ' [' num2str(TotVol/Vol0*100)   '%]' ]), end
        if Verbose>=3 && mod(Iteration,IterDisplay)==0, disp(['  Min UB:            ' num2str(minUB)    ]), end
        if Verbose>=3 && mod(Iteration,IterDisplay)==0, disp(['  Min LB:            ' num2str(minLB)    ]), end
        
    end
    
end

% =========================================================================
% III. Finalization


csvwrite(['Progress' num2str(Iteration) '.csv'],Progress);

% -----------------------------------------------------------------
% 1. Fathoming (safety - remove any spurious solution if still present;
% might not be necessary )
Alive       =   find(Tree(:,iLive))         ;
Tree        =   Tree(1:nTree,:)             ;
fathom      =   Fathoming(Tree,Alive)       ;
Tree(Alive(fathom),iLive)   =   0           ; % make dead


if graph
%     switch graphtype
%         
%         case 2
                             figure(99)
                        drawnow
                        low = min(Tree(:,index:2:end));
                        upp = max(Tree(:,index+1:2:end));
%                        delete(han0)
%                        han0 = plot([1:nib ; 1:nib],[low; upp],'ko-','MarkerFaceColor','k');
                        set(gca,'Xlim',Xlim,'Ylim',Ylim)
                        drawnow
    %end
end
% -----------------------------------------------------------------
% 2. Final solution selection and evaluation
if isempty(Alive)
    model         =   model0   ;
    model.IntX = IntX ;
    model.SolX = mean(IntX,2) ;
    disp('Problem is infeasible (No nodes remained alive)')
else
    if Term1
        disp('Early stopping - No solution provided')
        model = [];
    else
        [model,chosen]     =   SelectFromTree(Tree,model0,DistX,options)  ;
        Tree(chosen,iLive) = -3 ;
        if graph && nib<=2
            VisualLeaf(Tree,chosen,iLive,index,opt,nopt,graphtype)  ;
			if graphtype==3
			figure(99)
			subplot(2,1,1)
				VisualTreeNodes(Tree,Iteration)
			end
        end
    end
end

% -----------------------------------------------------------------
% vizualization
if graph>=2
    figure
    plot(Record(:,1),Record(:,2:3),'o-')
    xlabel('Iteration count')
    ylabel('Bounds')
    
    figure
    plot(Record(:,1),Record(:,4),'o-')
    xlabel('Iteration count')
    ylabel('No. Nodes')

    str = 'm' ;
%     str = 'k' ;
    VisualTree(Tree,Alive,str,index,opt,nopt,graphtype)
end
% -----------------------------------------------------------------

return
% =========================================================================

function [LB,UB,Xapp]     =   ProcessLeaf(Xleaf,KnotInt0,boundfun,model,KnotDistLimit,Verify)

Xapp       =   min(max(Xleaf,ones(2,1)*KnotInt0(1,:)),ones(2,1)*KnotInt0(2,:))    ;   % the applied subset bounds are adjusted for root node set bounds
if or(any(Xleaf(1,:)>KnotInt0(2,:)),any(Xleaf(2,:)<KnotInt0(1,:))) % if a subset is out of the original subset, ignore it
    LB  =   Inf ;
    UB  =   Inf ;
%     if Verbose, disp('      out of bounds'), end % disp
else
    [LB,UB]     =   boundfun(model,Xapp,KnotDistLimit,0,Verify)                          ;   % obtain both objective bounds
end

% -----------------------------------------------------------------
function Tree            =   StoreLeaf(Tree,iTree,branch,level,opt,LB,UB,Xapp,Xleaf)

% compute subset characteristics and store
City            =   sum(Ranger(Xapp(:,opt)))    ;   % city distance
Vol             =   prod(Ranger(Xapp(:,opt)))   ;   % evaluate volume of intervals
Tree(iTree,:) =   [ branch level+1 1 LB UB Vol City Xleaf(:)' ] ;

% -----------------------------------------------------------------
function Tree            =   FathomLeaf(Tree,iTree,iLive,LB,UB,Xleaf,DeltaKnot,model)    

% 3B. Fathom by individual solution aspects
KnotDist        =   Xleaf(2,:)-Xleaf(1,:)         ;   % range of knot intervals
% if isempty(tk)
%     TimeDist        =   KnotDist    ;
% else
%     TimeDist        =   KnotDist*(tk(2)-tk(1))          ;   % only valid for uniform knot distribution!
% end
if LB == Inf                            % Lower bound is infinity
%     disp('Infeasible')
    Tree(iTree,iLive)     =   -2  ;   %   fathom by infeasibility
elseif LB==UB
    Tree(iTree,iLive)     =   -1  ;   %   stop branching as bounds are the same (not really dead)
else
    if isfield(model,'type') && strcmp(model.type,'SCS')
        
        [verified,ListVerified] = SCSverify_Case1(model,Xleaf) ;
        ListVerified(KnotDist<=DeltaKnot+eps) = 1;
    else
        ListVerified = KnotDist<=DeltaKnot+eps ;
    end
    if all(ListVerified)         % All variables are found with specified knot resolution
        %disp('All verified')
%         disp('Satisfied resolution')
%         KnotDist
%         DeltaKnot
        %     if all(TimeDist<=DeltaTime)         %   All variables are found with specified knot and time resolutions
        Tree(iTree,iLive) =   -1  ;   %       stop growth of branch (not really dead)
        %     else                                % Solution found within knot resolution
        %         Tree(iTree,iLive) =   2    ;
        %         %   Setting to 2 puts this node at lower priority, all
        %         %   nodes with 1 are branched until there are no nodes
        %         %   with value 1
        %     end
    end
end

% -----------------------------------------------------------------
