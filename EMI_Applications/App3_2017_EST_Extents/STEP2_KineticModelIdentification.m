
% -------------------------------------------------------------------------
% EMI Toolbox - STEP2_KineticModelIdentification.m
% -------------------------------------------------------------------------
% Description
%
% Estimate parameters for all candidate rate laws for each kinetically
% controlled reaction.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Initial
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Initial

A=sprintf([ '\n'...
            'This script executes parameter estimation for all reactions \n' ...
            'and all candidate rate laws via the branch-and-bound algorithm \n' ...
            'as executed for the manuscript. Computation is rather lengthy \n' ...
            'and should be executed on a dedicated machine or server. For a \n' ...
            'fast evaluation, set the value of ''NegLog2Resol'' in the \n' ...
            'script to a lower integer value.\n \n' ...
            'Use ''STEP2_KineticModelIdentification_Display.m'' to display \n' ...
            'the results.\n'            ]);
disp(A)
disp(sprintf('Press any key to continue\n'))
pause

addpath(genpath('..\..\EMI_part1_EnvModSoft_GO'))

% =========================================================================
% =========================================================================
% =========================================================================

% -------------------------------------------------------------------------
% Choices

SensorSetup     =	3       ;	% Select sensor setup  used for extent computation
Optimize        =	true	;	% Setting to false skips the optimization and keeps initial guess for the parameters
Graph           =	true	;   % Show simulation results after each parameter optimization

NegLog2Resol	=	4       ;	% Determines to which granularity the BB algorithm is executed


% -------------------------------------------------------------------------
% Load extent computation results
FILEPATH	=   [	FOLDER 'Step1_ExtentComputation_Case' num2str(SensorSetup) ] ;
load(FILEPATH)

Rk          =   size(Model.Nbar_k,1)      ;

% -------------------------------------------------------------------------
% Setup candidate rate laws

disp(' ')
disp('==================================')
disp(['   Setting up Rate Law Library'])
disp('==================================')
disp(' ')
J       =   5           ;
Library	=	cell(J,1)	;

for i=1:Rk
    
    for j =1:J
        
        Range = [1e-6   100] ;
        switch j
            case 1
                name       =   'Zero-Order'       ;   % Name
                ratefun	=	@(S,theta)  (S>0)/theta(1) ;
                ntheta = 1 ;
                if i==2
                    Range = [2.5  100] ; % This results in a very stiff system for reaction 2. Parameter range adjusted in view of simulation time.
                end
            case 2
                name       =   'First-Order'       ;   % Name
                ratefun	=	@(S,theta) sign(S).*abs(S)/theta(1) ;
                ntheta = 1 ;
            case 3
                name       =   'Monod'       ;   % Name
                ratefun	=	@(S,theta) sign(S).*abs(S)./((theta(1))+(theta(2))*abs(S)) ;
                ntheta = 2 ;
            case 4
                name       =   'Tessier'       ;   % Name
                ratefun	=	@(S,theta) sign(S).*1/theta(2)*(1-exp(-abs(S)*theta(2)/theta(1)));
                ntheta = 2 ;
            case 5
                name       =   'Haldane'       ;   % Name
                ratefun	=	@(S,theta) sign(S)*(abs(S)./((theta(1))+(theta(2))*abs(S)+(theta(3))*abs(S).^2)) ;
                ntheta = 3 ;
        end
        
        Library{j,i}.name = name ;
        Library{j,i}.ratefun = ratefun ;
        Library{j,i}.ntheta = ntheta ;
        Library{j,i}.Omega = Range(:)*ones(1,ntheta)            ;
        
    end
end

% =========================================================================
% =========================================================================
% =========================================================================
            
% global nArchive            
% global Archive_Xsim Archive_Time Archive_theta            
% global optionsfzero optionsode

FILEPATH     =   [	FOLDER 'NumericSetup' ] ;
load(FILEPATH)

xinput	=	xtilde                                                  ;
ty      =	Model.Measurement.tsim(Model.Measurement.sample_index)	;

    
% =========================================================================
% =========================================================================
% =========================================================================

for i=1:Rk % for each extent...
    
    disp(' ')
    disp('==================================')
    disp(['   Reaction: ' num2str(i) '  '])
    disp('==================================')
    disp(' ')
    
    switch i
        case 1
            x_fun   =   @(t,x) [    ...
                x                                                                   ;
                interp1(ty(:),xinput((i+1):end,:)',t(:),'linear','extrap')'     ]	;
            
        case Rk
            x_fun   =   @(t,x) [	...
                interp1(ty(:),xinput(1:(i-1),:)',t(:),'linear','extrap')'           ;
                x                                                               ]	;
            
        otherwise
            x_fun   =   @(t,x) [	...
                interp1(ty(:),xinput(1:(i-1),:)',t(:),'linear','extrap')'           ;
                x                                                                   ;
                interp1(ty(:),xinput((i+1):end,:)',t(:),'linear','extrap')'     ]	;
            
    end
    
    for j =1:J
        
        disp(['---  Candidate Rate Law ' num2str(j)])
        
        % =================================================================
        % Setup model for single extent
        
        disp(['     *  Setting Up'])
        
        Reaction                =	Library{j,i}        ;
        Omega                   =	Reaction.Omega      ;
        ntheta                  =	Reaction.ntheta     ;
        
        Reaction.Theta2r        =	1:ntheta            ;
        Reactions{1}            =	Reaction            ;
        ExtentModel             =	Model               ;
        ExtentModel.Reactions	=   Reactions           ;
        ExtentModel.map_c2r     =	Model.map_c2r(i,:)	;
        
        pHguess                 =   7                   ;
        ExtentModel.x2c         =   @(t,x,MDL) Extent_2_Concentration(MDL,x_fun(t,x),pHguess) ;
        
        ExtentModel.data.xtilde =	xtilde(i,:)                     ;
        ExtentModel.data.lambda =	lambda(i,:)                     ;
        ExtentModel.data.H      =	length(ExtentModel.data.xtilde) ;
        
        if Graph
            figure(100+i*10+j),
                hold on
                plot(ty,xtilde(i,:),'ko')
                title(['Parameter estimation - Extent ' num2str(i) ' - Rate law ' num2str(j) ' of ' num2str(J) ])
                drawnow
                
%                 ExtentModel.Reactions{1}.theta=Omega(1,:);
%                 [ xsim , cbarsim , csim , pHsim ] = SimExtent( ExtentModel) ;
%                 plot(ExtentModel.Measurement.tsim,xsim,'b-','LineWidth',2)
%                 ExtentModel.Reactions{1}.theta=Omega(2,:);
%                 [ xsim , cbarsim , csim , pHsim ] = SimExtent( ExtentModel) ;
%                 plot(ExtentModel.Measurement.tsim,xsim,'r-','LineWidth',2)
% pause
        end
        
        % =================================================================
        % Parameter estimation
        
        if Optimize
            
            disp(['     *  Parameter Estimation'])
            
            
            % =========================================================================
            % setup to keep computationally intensive simulations in memory
            global nArchive
            global Archive_Xsim Archive_Time Archive_theta
            
            nArchive        =   373                                             ;   % Arbitrary number of simulation to keep in memory
            Archive_Xsim    =	nan(1,length(Model.Measurement.tsim),nArchive)  ;   % extent simulation archive
            Archive_Time  	=	-inf(nArchive,1)                                ;   % time needed for simulation
            Archive_theta   =	nan(nArchive,ntheta)                            ;   % parameters used
            

            [theta,QL,QU,Omega]             =	ExecuteBBsequence(ExtentModel,Omega,NegLog2Resol)       ;
            ExtentModel.Reactions{1}.theta	=   theta                                                   ;
            
        else
            theta                           =	2*ones(1,ntheta)                                        ;
            
            theta   =	[	.03	.1 3	]	;       % Kinetic parameters
%             switch i
%                 case 1
%                     theta   =	[	.03	.1 3	]	;       % Kinetic parameters
%                     
%                 case 2
%                     theta	= 	[	0.00015	1	]   ;   % Parameter values
%             end
            
            ExtentModel.Reactions{1}.theta  =   theta                                                   ;
            [QU,Xm,xsim,d]                  =	ExtentFit_boundupp(ExtentModel,[],[],Graph,[],theta,[]) ;
            
        end
        
        disp(['     *  Model Evaluation'])
        ExtentModel.WSSR        =	sqrt(QU)                                                            ;
        ExtentModel.WSSRlim99	=	sqrt(chi2inv(0.99,[ExtentModel.data.H-ntheta])/ExtentModel.data.H)	;
        
        
        % ===================================
        
        % ===================================
        % Simulation with final parameter set and evaluation
        [ xsim , cbarsim , csim , pHsim ]	=	SimExtent( ExtentModel) ;
        
        ExtentModel.simulation.xsim         =	xsim                    ;
        ExtentModel.simulation.csim         =	csim                    ;
        ExtentModel.simulation.cbarsim      =	cbarsim                 ;
        ExtentModel.simulation.pHsim        =	pHsim                   ;
        
        if Graph
            
            figure(100+i*10+j),
                plot(ExtentModel.Measurement.tsim,xsim,'k--','LineWidth',2)
                drawnow
            
        end
        
        disp(['     *  Saving Results'])
        FILEPATH = [FOLDER 'STEP2_Case' num2str(SensorSetup) '_Reaction' num2str(i) '_RateLaw' num2str(j)] ;
        save(FILEPATH,'ExtentModel')
        
    end
    
    
end

% =========================================================================
% =========================================================================
% =========================================================================

STEP2_KineticModelIdentification_Display

% =========================================================================
% =========================================================================
% =========================================================================


