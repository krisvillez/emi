function A	=	ShapeConstraints1(Model,C1_L,C1_U,CImax)

% -------------------------------------------------------------------------
% EMI Toolbox - ShapeConstraints1.m
% -------------------------------------------------------------------------
% Description
%
% Generates constraint matrix for substrate concentration
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	A	=	ShapeConstraints1(Model,C1_L,C1_U,CImax)
%
% INPUT
%   Model   :   Model as a structure 
%   C1_L    :   Lower bounds for the concentration
%   C1_U    :   Upper bounds for the concentration
%   CI_max  :   Concentration interval within which maximum rate is
%               obtained 
%
% OUTPUT
%   A       :   Constraint matrix
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-05-11
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Alma Masic, Kris Villez
%
% This file is part of the EMI toolbox for efficient model identification. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the EMI Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

%   1. Evaluate monotonicity constraints (hard coded for Haldane kinetics):
A1      =	Model.A1	;

%   2. Evaluate curvature constraints (hard coded for Haldane kinetics):
Concave	=	C1_L>=CImax(2)                                              ;
Concave =	all([ Concave(1:end-2) Concave(2:end-1) Concave(3:end) ],2) ;
Convex	=	C1_U<=CImax(1)                                              ;
Convex	=	all([ Convex(1:end-2) Convex(2:end-1) Convex(3:end) ],2)	;

%   3. Evaluate curvature constraints (hard coded for Haldane kinetics):
A2      =	Model.A2                            ;
A2      =	-diag(Concave)*A2+diag(Convex)*A2	;
A2      =	A2(or(Convex,Concave),:)            ;

%   3. Gather shape constraints:
A       =	[	A1	;	A2	]                   ;
%A       =	[	A1		]                   ;